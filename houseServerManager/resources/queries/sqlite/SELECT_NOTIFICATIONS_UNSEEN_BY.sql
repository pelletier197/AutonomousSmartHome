SELECT *
FROM HOUSE_NOTIFICATIONS 
WHERE NOTIFICATION_UUID NOT IN
( 
SELECT NOTIFICATION_UUID
FROM HOUSE_NOTIFICATION_VIEWS
WHERE RESIDENT_UUID = ?
)
ORDER BY NOTIFICATION_GENERATION_TIME DESC