package embedded.light.hueLight;

import java.io.Serializable;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.ColorisableLight;

public class HueLight implements AbstractLight, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1068914415337593276L;

	public static final int MAX_HUE = 65535;

	/**
	 * The bridge to which the light beyond
	 */
	protected transient PHBridge bridge;
	protected transient PHHueSDK sdk;
	protected transient PHLight light;

	private String id;

	private String name;

	private boolean isOpen;

	public HueLight(PHLight light, PHBridge bridge, PHHueSDK sdk) {

		this.light = light;
		this.bridge = bridge;
		this.sdk = sdk;

		id = light.getUniqueId();
		System.out.println("Created light : " + light + " id = " + id);

		isOpen = light.getLastKnownLightState().isOn();
	}

	@Override
	public void open() {
		toggle(true);
	}

	@Override
	public void close() {
		toggle(false);
	}

	/**
	 * Returns true if the light is open, false otherwise.
	 * 
	 * Note that this function does not get the value directly from the light,
	 * but uses the last known value for the parameter.
	 */
	@Override
	public boolean isOpen() {
		return isOpen;
		// return light.getLastKnownLightState().isOn();
	}

	private void toggle(boolean stat) {
		register();

		PHLightState state = new PHLightState();
		state.setOn(stat);

		this.sdk.getSelectedBridge().updateLightState(light, state);
		this.isOpen = stat;

	}

	protected void register() {

		if (sdk == null || bridge == null || light == null) {
			throw new IllegalStateException("The light is not connected");
		}
		// Connects to the bridge
		if (sdk.getSelectedBridge() != bridge) {
			this.sdk.setSelectedBridge(bridge);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return this.id.equals(((HueLight) obj).id);
	}

	@Override
	public String getLightID() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}
