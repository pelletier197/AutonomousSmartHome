package embedded.light.hueLight;

import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import embedded.controllers.light.DimmableLight;

public class DimmableHue extends HueLight implements DimmableLight {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5248014321732193428L;
	private double brightness;

	public DimmableHue(PHLight light, PHBridge bridge, PHHueSDK sdk) {
		super(light, bridge, sdk);
		brightness = light.getLastKnownLightState().getBrightness();
	}

	@Override
	public void setBrightness(double value) {

		if (value < 0) {
			value = 0;
		}
		if (value > 1) {
			value = 1;
		}
		register();

		PHLightState state = new PHLightState();
		state.setBrightness((int) (value * 254));
		brightness = value;

		sdk.getSelectedBridge().updateLightState(light, state);
		System.out.println("Updating light state : " + value);
		
	}

	@Override
	public double getBrightness() {

		return brightness;
		// return light.getLastKnownLightState().getBrightness();
	}

}
