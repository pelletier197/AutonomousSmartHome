package embedded.light.hueLight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

/**
 * HueProperties.java
 * 
 * Stores the last known connected IP Address and the last known username. This
 * facilitates automatic bridge connection.
 * 
 * Also, as the username (for the whitelist) is a random string, this prevents
 * the need to pushlink every time the app is started (as the username is read
 * from the properties file).
 *
 */
public final class HueProperties {

	private static final String LAST_CONNECTED_IP = "LastIPAddress";
	private static final String USER_NAME = "WhiteListUsername";
	private static final String BRIDGE_UD = "bridgeID";
	private static final String PROPS_FILE_NAME = "data/lights/Hue.properties";
	private static final String SEPARATOR = "\u007F";
	private static Properties props = null;

	private HueProperties() {
	}

	public static void addNewBridge(String username, String ip) {

		if (username == null || ip == null) {
			throw new NullPointerException();
		}

		props.setProperty(LAST_CONNECTED_IP, props.getProperty(LAST_CONNECTED_IP, "") + SEPARATOR + ip);
		props.setProperty(USER_NAME, props.getProperty(USER_NAME, "") + SEPARATOR + username);

		saveProperties();
	}

	/**
	 * Returns the stored Whitelist username. If it doesn't exist we generate a
	 * 16 character random string and store this in the properties file.
	 */
	public static String[] getUsernames() {
		String users = props.getProperty(USER_NAME);
		if (users != null) {

			String[] splitted = users.split(SEPARATOR);
			String[] result = new String[splitted.length - 1];
			System.arraycopy(splitted, 1, result, 0, result.length);
			
			return result;
		} else {
			return new String[0];
		}
	}

	public static String[] getIPs() {
		String ips = props.getProperty(LAST_CONNECTED_IP);
		if (ips != null) {

			String[] splitted = ips.split(SEPARATOR);
			String[] result = new String[splitted.length - 1];
			System.arraycopy(splitted, 1, result, 0, result.length);
			
			return result;
		
		} else {
			return new String[0];
		}
	}

	public static void clearBridgesData() {
		props.clear();

	}

	public static String usernameForIP(String ip) {

		int index = Arrays.asList(getIPs()).indexOf(ip);

		if (index == -1) {
			return null;
		}

		return getUsernames()[index];
	}

	public static void loadProperties() {
		if (props == null) {
			props = new Properties();
			FileInputStream in;

			try {
				in = new FileInputStream(PROPS_FILE_NAME);
				props.load(in);
				in.close();
			} catch (FileNotFoundException ex) {
				saveProperties();
			} catch (IOException e) {
				// Handle the IOException.
			}
		}
	}

	public static void saveProperties() {
		try {
			FileOutputStream out = new FileOutputStream(PROPS_FILE_NAME);
			props.store(out, null);
			out.close();
		} catch (FileNotFoundException e) {
			// Handle the FileNotFoundException.
		} catch (IOException e) {
			// Handle the IOException.
		}
	}

}