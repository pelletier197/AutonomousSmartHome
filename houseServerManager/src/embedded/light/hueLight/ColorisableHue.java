package embedded.light.hueLight;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import embedded.controllers.light.ColorisableLight;

public class ColorisableHue extends DimmableHue implements ColorisableLight {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1825063563801737534L;

	public static final int WHITE_LIGHT_COLOR = 0xF0C080;

	private int color;

	public ColorisableHue(PHLight light, PHBridge bridge, PHHueSDK sdk) {
		super(light, bridge, sdk);

		final PHLightState state = light.getLastKnownLightState();
		float[] xy = new float[] { state.getX(), state.getY() };
		color = PHUtilities.colorFromXY(xy, light.getModelNumber());
	}

	@Override
	public int getColor() {

		return color;
		/*
		 * final PHLightState state = light.getLastKnownLightState();
		 * 
		 * float[] xy = new float[] { state.getX(), state.getY() };
		 * 
		 * System.out.println( "Color for light : " + getLightID() + " = " +
		 * PHUtilities.colorFromXY(xy, light.getModelNumber())); return
		 * PHUtilities.colorFromXY(xy, light.getModelNumber());
		 */
	}

	@Override
	public void setColor(int rgbValue) {

		register();

		int red = (rgbValue >> 16) & 0xff;
		int green = (rgbValue >> 8) & 0xFF;
		int blue = (rgbValue) & 0xff;

		float[] xy = PHUtilities.calculateXYFromRGB(red, green, blue, light.getModelNumber());

		PHLightState state = new PHLightState();
		state.setX(xy[0]);
		state.setY(xy[1]);

		sdk.getSelectedBridge().updateLightState(light, state);

		this.color = rgbValue;

	}

	@Override
	public void resetColor() {
		setColor(WHITE_LIGHT_COLOR);
	}

}
