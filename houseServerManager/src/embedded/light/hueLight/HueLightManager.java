package embedded.light.hueLight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHSDKListener;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHHueParsingError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLight.PHLightType;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.LightManager;
import embedded.light.LightStateListener;

public class HueLightManager implements LightManager, PHSDKListener {

	private List<AbstractLight> lights;
	private List<PHBridge> bridges;
	private PHHueSDK sdk;
	private LightStateListener listener;

	public HueLightManager() {

		// Creates and register to the sdk manager.
		sdk = PHHueSDK.getInstance();
		sdk.getNotificationManager().registerSDKListener(this);

		bridges = new ArrayList<>();
		lights = new ArrayList<>();

		HueProperties.loadProperties();

	}

	@Override
	public void load() {

		System.out.println("Users : " + Arrays.toString(HueProperties.getUsernames()));
		System.out.println(Arrays.toString(HueProperties.getIPs()));

		// No bridges known
		if (HueProperties.getUsernames().length == 0) {

			searchAccessPoints();

		} else {
			String[] usernames = HueProperties.getUsernames();
			String[] IPs = HueProperties.getIPs();

			for (int i = 0; i < usernames.length; i++) {
				PHAccessPoint accessPoint = new PHAccessPoint();

				accessPoint.setIpAddress(IPs[i]);
				accessPoint.setUsername(usernames[i]);
				sdk.connect(accessPoint);

			}

			// Still searches for bridges
			new Thread(new Runnable() {

				@Override
				public void run() {
					System.out.println("Searching accessPoint");
					searchAccessPoints();
				}
			}).start();

		}

	}

	@Override
	public List<AbstractLight> getLights() {
		return this.lights;
	}

	@Override
	public void onAccessPointsFound(List<PHAccessPoint> points) {

		System.out.println("Access points found");

		for (PHAccessPoint point : points) {
			// Conncts to all accessPoints
			if (!sdk.isAccessPointConnected(point)) {
				sdk.connect(point);
			}
		}

	}

	private void computeLights(PHBridge point) {

		final List<PHLight> list = point.getResourceCache().getAllLights();

		// Iterates all the lights
		for (PHLight light : list) {

			int index = -1;
			if ((index = indexOfLightIfExist(light, point)) != -1) {
				// Known light

				// Reloads the light
				HueLight li = (HueLight) lights.get(index);
				li.light = light;
				li.sdk = sdk;
				li.bridge = point;

			} else {

				// It's a new light
				final PHLightType type = light.getLightType();

				if (type == PHLightType.ON_OFF_LIGHT || type == PHLightType.CT_LIGHT
						|| type == PHLightType.UNKNOWN_LIGHT) {
					lights.add(new HueLight(light, point, sdk));
				} else if (type == PHLightType.DIM_LIGHT) {
					lights.add(new DimmableHue(light, point, sdk));
				} else {
					lights.add(new ColorisableHue(light, point, sdk));
				}
			}

		}
	}

	/**
	 * Tests either the given light was already connected before, and returns
	 * its ID in the list of lights, or -1 if it didn't exist before.
	 * 
	 * @param light
	 *            The light to test
	 * @param point
	 *            Its bridge
	 * @return The light's index in {@link #lights} or -1 if it didn't exist
	 */
	private int indexOfLightIfExist(PHLight light, PHBridge point) {

		final HueLight test = new HueLight(light, point, sdk);

		return lights.indexOf(test);
	}

	@Override
	public void onAuthenticationRequired(PHAccessPoint accessPoint) {

		/**
		 * 
		 * The user needs to authenticate
		 */
		System.out.println("Authenticate...");
		sdk.startPushlinkAuthentication(accessPoint);

	}

	@Override
	public void onBridgeConnected(PHBridge bridge, String username) {

		System.out.println("Bridge connected : " + bridge + " " + username);

		sdk.setSelectedBridge(bridge);
		sdk.enableHeartbeat(bridge, PHHueSDK.HB_INTERVAL);

		if (!Arrays.asList(HueProperties.getUsernames()).contains(username)) {
			String lastIpAddress = bridge.getResourceCache().getBridgeConfiguration().getIpAddress();

			HueProperties.addNewBridge(username, lastIpAddress);
			HueProperties.saveProperties();

		}

		if (!bridges.contains(bridge)) {
			bridges.add(bridge);
		}

		computeLights(bridge);
	}

	@Override
	public void onCacheUpdated(List<Integer> arg0, PHBridge arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionLost(PHAccessPoint arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionResumed(PHBridge arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onError(int arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onParsingErrors(List<PHHueParsingError> parsingErrorsList) {
		for (PHHueParsingError parsingError : parsingErrorsList) {
			System.out.println("ParsingError : " + parsingError.getMessage());
		}
	}

	private void searchAccessPoints() {
		PHBridgeSearchManager sm = (PHBridgeSearchManager) sdk.getSDKService(PHHueSDK.SEARCH_BRIDGE);
		sm.search(true, true);

	}

	@Override
	public void close() {

		System.out.println("close light called");

		sdk.disableAllHeartbeat();

		for (PHBridge bridge : bridges) {
			try {
				sdk.disconnect(bridge);
			} catch (Exception e) {

			}
		}
	}
}
