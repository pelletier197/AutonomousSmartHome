package embedded.light;

import embedded.controllers.light.AbstractLight;

public interface LightStateListener {

	public void lightStateChanged(AbstractLight light);

}
