package embedded.light;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.swing.plaf.ListUI;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.LightEvent;
import embedded.controllers.light.LightEventHandler;
import embedded.controllers.light.LightManager;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmManager;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.LightData;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;

public class HouseLightManager {

	private HouseManager man;

	private List<LightManager> managers;

	private AlarmManager lightEventManager;
	private LightEventHandler handler;
	private transient int lastLength = 0;

	private HouseSaver saver;

	public HouseLightManager(HouseManager man, HouseSaver saver) {

		this(man, new ArrayList<>(), saver);
	}

	@SuppressWarnings("unchecked")
	public HouseLightManager(HouseManager man, Collection<LightEvent> events, HouseSaver saver) {

		if (man == null || events == null || saver == null) {
			throw new NullPointerException("Null manager or event");
		}

		this.man = man;
		this.saver = saver;
		this.managers = new ArrayList<>();
		this.handler = new LightEventHandler();
		// creates alarm manager from the alarms.
		this.lightEventManager = new AlarmManager((Collection<Alarm>) (Collection<?>) events, handler);
	}

	@SuppressWarnings("unchecked")
	public List<AbstractLight> getUnasignedLights() {
		return ListUtils.subtract(getAndCheckAllLights(), man.getRoomLights());
	}

	private List<AbstractLight> getAndCheckAllLights() {

		final List<AbstractLight> lights = new ArrayList<>();

		for (LightManager manager : managers) {
			lights.addAll(manager.getLights());
		}

		checkLights(lights);

		return lights;
	}

	private void checkLights(List<AbstractLight> lights) {

		if (!lights.isEmpty()) {
			// Check if the last element is named
			String name = lights.get(lights.size() - 1).getName();
			if (name == null || name.trim().isEmpty()) {
				// Name the unnamed lights
				nameLights(lights);
			}

			// Checks if light count has changed. Will reassign lights if yes
			if (lights.size() != lastLength) {
				lastLength = lights.size();
				man.reassingLights(lights);
			}
		}
	}

	private void nameLights(List<AbstractLight> lights) {

		final int last = lights.size() - 1;
		AbstractLight light = null;
		String name = null;

		for (int i = last; i >= 0; i--) {
			light = lights.get(i);
			name = light.getName();

			if (name == null || name.trim().isEmpty()) {
				light.setName("#" + (i + 1));
			} else {
				break;
			}

		}
	}

	public void addManager(LightManager manager) {
		if (manager == null) {
			throw new NullPointerException("Null manager");
		}
		this.managers.add(manager);
	}

	public synchronized void createLightEvent(LightEvent event) {
		lightEventManager.addAlarm(event);
	}

	public synchronized void removeLightEvent(LightEvent event) {
		lightEventManager.removeAlarm(event);
	}

	public AbstractLight getLightForID(String id) {
		if (id == null) {
			throw new NullPointerException("Null id");
		}
		final List<AbstractLight> lights = getAndCheckAllLights();
		for (AbstractLight light : lights) {
			if (id.equals(light.getLightID())) {
				return light;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<LightEvent> getLightEvents() {
		// Dynamic cast to light events.
		return (List<LightEvent>) (List<?>) lightEventManager.getAlarms();
	}

	public List<LightData> getUnasignedLightsData() {
		final List<LightData> data = new ArrayList<>();
		final List<AbstractLight> lights = getUnasignedLights();

		for (AbstractLight li : lights) {
			data.add(new LightData(li));
		}
		return data;
	}

	public List<AbstractLight> getLightsForIDs(String[] ids) {

		final int toFind = ids.length;
		int found = 0;
		HashMap<String, AbstractLight> lightMap = new HashMap<String, AbstractLight>(ids.length);

		// First put all ids in the Map
		for (String id : ids) {
			lightMap.put(id, null);
		}

		String id = null;

		final List<AbstractLight> lights = getAndCheckAllLights();
		// Associate light
		for (AbstractLight li : lights) {
			id = li.getLightID();
			if (lightMap.containsKey(id)) {
				found++;
				lightMap.put(id, li);

				if (found == toFind) {
					break;
				}
			}
		}
		return new ArrayList<>(lightMap.values());
	}

	public void checkLights() {
		getAndCheckAllLights();
	}

	public int getLightCount() {
		return getAndCheckAllLights().size();
	}

	/**
	 * Retrieves all the lights of the manager. This method must avoid being
	 * called to often, cause it will check every time if new lights have been
	 * inserted in any of the managers.
	 * 
	 * @return A list containing all the lights of the house
	 */
	public List<AbstractLight> getLights() {
		return getAndCheckAllLights();
	}

	public void closeAll() {
		for (LightManager man : managers) {
			man.close();
		}
	}

}
