package embedded.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.sun.media.jfxmedia.Media;

import io.humble.video.Codec;
import io.humble.video.Encoder;
import io.humble.video.MediaPacket;
import io.humble.video.MediaPicture;
import io.humble.video.Muxer;
import io.humble.video.MuxerFormat;
import io.humble.video.PixelFormat;
import io.humble.video.Rational;
import io.humble.video.awt.MediaPictureConverter;
import io.humble.video.awt.MediaPictureConverterFactory;

public class HumbleVideoEncoder implements ImageToMp4Encoder {

	private Encoder encoder;
	private Muxer muxer;
	private MediaPicture picture;
	private MediaPictureConverter converter;
	private MediaPacket packet;

	private long frameNum;

	public HumbleVideoEncoder(String file, int height, int width, int FPS) throws InterruptedException, IOException {
		this(new File(file), height, width, FPS);
	}

	public HumbleVideoEncoder(File f, int height, int width, int FPS) throws InterruptedException, IOException {

		final Rational rate = Rational.make(1, FPS);

		muxer = Muxer.make(f.getPath(), null, null);

		final MuxerFormat format = muxer.getFormat();
		final Codec codec = Codec.findEncodingCodec(format.getDefaultVideoCodecId());

		encoder = Encoder.make(codec);

		encoder.setWidth(width);
		encoder.setHeight(height);
		// We are going to use 420P as the format because that's what most video
		// formats these days use
		final PixelFormat.Type pixelformat = PixelFormat.Type.PIX_FMT_YUV420P;
		encoder.setPixelFormat(pixelformat);
		encoder.setTimeBase(rate);

		if (format.getFlag(MuxerFormat.Flag.GLOBAL_HEADER))
			encoder.setFlag(Encoder.Flag.FLAG_GLOBAL_HEADER, true);

		/** Open the encoder. */
		encoder.open(null, null);

		/** Add this stream to the muxer. */
		muxer.addNewStream(encoder);

		/** And open the muxer for business. */
		muxer.open(null, null);

		picture = MediaPicture.make(encoder.getWidth(), encoder.getHeight(), pixelformat);
		picture.setTimeBase(rate);

		packet = MediaPacket.make();

		frameNum = 0;
	}

	/**
	 * Convert a {@link BufferedImage} of any type, to {@link BufferedImage} of
	 * a specified type. If the source image is the same type as the target
	 * type, then original image is returned, otherwise new image of the correct
	 * type is created and the content of the source image is copied into the
	 * new image.
	 * 
	 * @param sourceImage
	 *            the image to be converted
	 * @param targetType
	 *            the desired BufferedImage type
	 * 
	 * @return a BufferedImage of the specifed target type.
	 * 
	 * @see BufferedImage
	 */

	private BufferedImage convertToType(BufferedImage sourceImage, int targetType) {
		BufferedImage image;

		// if the source image is already the target type, return the source
		// image

		if (sourceImage.getType() == targetType)
			image = sourceImage;

		// otherwise create a new image of the target type and draw the new
		// image

		else {
			image = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), targetType);
			image.getGraphics().drawImage(sourceImage, 0, 0, null);
		}

		return image;
	}

	@Override
	public void inputImage(BufferedImage img, int time) throws IOException {

		final BufferedImage converted = convertToType(img, BufferedImage.TYPE_3BYTE_BGR);

		if (converter == null) {
			converter = MediaPictureConverterFactory.createConverter(converted, picture);
		}

		converter.toPicture(picture, converted, frameNum);

		frameNum++;

		do {
			encoder.encode(packet, picture);
			if (packet.isComplete())
				muxer.write(packet, false);
		} while (packet.isComplete());


	}

	@Override
	public void finish() throws IOException {

		/**
		 * Encoders, like decoders, sometimes cache pictures so it can do the
		 * right key-frame optimizations. So, they need to be flushed as well.
		 * As with the decoders, the convention is to pass in a null input until
		 * the output is not complete.
		 * 
		 * It flushes the last image
		 */
		do {
			encoder.encode(packet, null);
			if (packet.isComplete())
				muxer.write(packet, false);
		} while (packet.isComplete());

		muxer.close();
	}

}
