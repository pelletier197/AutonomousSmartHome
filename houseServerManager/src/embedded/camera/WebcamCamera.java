package embedded.camera;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.List;

import com.github.sarxos.webcam.Webcam;

import embedded.controllers.camera.AbstractCamera;

/**
 * @author sunny
 *
 */
public class WebcamCamera implements AbstractCamera {

	public static final int RESOLUTION_240P = 240;
	public static final int RESOLUTION_480P = 480;
	public static final int RESOLUTION_720P = 720;
	public static final int RESOLUTION_1080P = 1080;

	private static final List<Webcam> webcams = Webcam.getWebcams();
	private Webcam camera;
	private boolean isOpen;

	/**
	 * Creares a webcam camera using the default webcam of the computer.
	 */
	public WebcamCamera() {
		this(0);

	}

	/**
	 * Creates a webcam camera with the camera having the given index priority
	 * in the list of system's webcam.
	 * 
	 * <p>
	 * This index must be included in the range of [0, {@link #getWebcamCount()}
	 * [
	 * </p>
	 * 
	 * @param index
	 *            The index of the camera in system's priority webcam.
	 */
	public WebcamCamera(int index) {
		this(index, RESOLUTION_240P);

	}

	public WebcamCamera(int index, int res) {

		final List<Webcam> cameras = webcams;

		if (index >= cameras.size()) {
			throw new CameraIndexOutOfRangeException("This camera is not available");
		}

		camera = cameras.get(index);
		Dimension resolution = new Dimension(res * 1280 / 720, res); // HD
																		// 1.7777777res
																		// x res
																		// quality
		camera.setCustomViewSizes(new Dimension[] { resolution }); // register
																	// custom
																	// resolution
		camera.setViewSize(resolution); // set it

	}

	/**
	 * Opens the webcam camera. This operation must be performed before getting
	 * an image.
	 */
	@Override
	public boolean open() {

		// Opens the webcam asynchronously
		return camera.open(true);
	}

	/**
	 * Returns a buffered image of the current camera view. This image is picked
	 * asynchronously from the webcam, so it can therefore be the same image as
	 * the one before if the buffer has not been replaced yet.
	 */
	@Override
	public BufferedImage getImage() {
		return camera.getImage();
	}

	/**
	 * Returns a byte array image of the size height * width * 24 =
	 * {@link #getImageSize()}. This image is encoded using rgb color.
	 * 
	 * @return A raw rgb image (24 bits per pixels)
	 */
	@Override
	public byte[] getImageByte() {
		return camera.getImageBytes().array();
	}

	/**
	 * @return True if the camera is open, false otherwise.
	 */
	@Override
	public boolean isOpen() {
		return camera.isOpen();
	}

	/**
	 * Closes the camera and releases the flow to it for another application.
	 */
	@Override
	public boolean close() {
		return camera.close();
	}

	/**
	 * Return the image size in bytes. This is the exact same size of the image
	 * retrieved when {@link #getImageByte()}, or the size of the data for
	 * {@link #getImage()}.
	 */
	@Override
	public int getImageSize() {

		Dimension d = camera.getViewSize();
		final int height = d.height;
		final int width = d.width;

		// 24 bits per pixel
		return height * width * 24;
	}

	/**
	 * @return the width of the image in bytes
	 */
	@Override
	public int getWidth() {
		return camera.getViewSize().width;
	}

	/**
	 * @return the height of the image in bytes
	 */
	@Override
	public int getHeight() {
		return camera.getViewSize().height;

	}

	/**
	 * 
	 * @return The number of webcams available in the system to be used.
	 */
	public static int getWebcamCount() {

		return webcams.size();
	}

	/**
	 * @return The unique ID of the webcam
	 */
	@Override
	public String getID() {
		System.out.println(camera.getName());
		return camera.getName();
	}

	/**
	 * Returns the default encoding of the webcam, RGB_24 bytes per pixels.
	 */
	@Override
	public int getEncoding() {
		// The encoding of the images retrieved
		return Encoding.RGB_24;
	}

	/**
	 * @return The frame per second rate of the webcam.
	 */
	@Override
	public int getFPS() {

		System.out.println(camera.getFPS() + " FPS");
		return (int) camera.getFPS();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((camera == null) ? 0 : camera.hashCode());
		result = prime * result + (isOpen ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebcamCamera other = (WebcamCamera) obj;
		
		return other.getID().equals(getID());
	}
	
	

}
