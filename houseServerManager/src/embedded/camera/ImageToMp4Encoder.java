package embedded.camera;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface ImageToMp4Encoder {

	/**
	 * Writes the image on the video. The given image will be displayed during
	 * time second in the video.
	 * 
	 * @param img
	 *            The image to encode
	 * @param time
	 *            The time in ms during which the image will be displayed
	 * @throws IOException
	 *             If an error occurs.
	 */
	void inputImage(BufferedImage img, int time) throws IOException;

	public void finish() throws IOException;

}
