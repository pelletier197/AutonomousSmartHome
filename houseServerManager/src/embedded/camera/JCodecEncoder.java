package embedded.camera;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.jcodec.api.awt.SequenceEncoder;

public class JCodecEncoder implements ImageToMp4Encoder {

	private SequenceEncoder enc;

	private static final int DEFAULT_FRAMERATE = 5;

	private int frameRate;

	public JCodecEncoder(String fileName) throws IOException {
		this(new File(fileName));
	}

	public JCodecEncoder(File f) throws IOException {
		this.enc = new SequenceEncoder(f);

		this.frameRate = DEFAULT_FRAMERATE;
	}

	@Override
	public void inputImage(BufferedImage img, int time) throws IOException {
		// Each frame is 25 ms on the video
		int frameCount = ((int) (time / 25));
		if (frameCount < 1) {
			frameCount = 1;
		}

		for (int i = 0; i < frameCount; i++) {
			enc.encodeImage(img);
		}
	}

	@Override
	public void finish() throws IOException {
		enc.finish();
	}

}
