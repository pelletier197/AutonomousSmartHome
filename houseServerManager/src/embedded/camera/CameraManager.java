package embedded.camera;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.jcodec.api.awt.SequenceEncoder;

import embedded.controllers.camera.AbstractCamera;
import utils.Formatters;

public class CameraManager {

	/**
	 * Each file is restarted when the hour changes
	 */
	public static final long FILE_LENGTH = 3_600_000;

	/**
	 * The number of frame captured per second
	 */
	public static final int FPS = 5;

	/**
	 * The path to camera files
	 */
	private static final String CAMERA_RECORD_PATH = "/camera/";

	/**
	 * Default font for writing current time on the images retrieved by the
	 * camera
	 */
	private static final Font INFO_FONT = new Font("System", Font.BOLD, 30);
	private static final int TEXT_X = 15, TEXT_Y = 30;

	/**
	 * he list of cameras that the room has access to
	 */
	private List<AbstractCamera> cams;

	/**
	 * Current streams. Each camera can stream at maximum on one different
	 * channel.
	 */
	private HashMap<AbstractCamera, OutputStream> streams;

	/**
	 * Tells either recording is enable
	 */
	private boolean recording = false;

	/**
	 * Tells if the cameras are open.
	 */
	private boolean open = false;

	private List<ImageToMp4Encoder> channels;
	private List<ImageToMp4Encoder> nextChannels;

	/**
	 * Initialize the room camera manager. It will load all kind of camera
	 * detected and implemented on the system.
	 */
	public CameraManager() {

		this.cams = new ArrayList<>();
		this.streams = new HashMap<>();

		loadCameras();

	}

	private void loadCameras() {

		loadWebcams();

	}

	/**
	 * Loads the webcams connected to this system.
	 */
	private void loadWebcams() {
		final int webcams = WebcamCamera.getWebcamCount();

		if (webcams > 0) {
			for (int i = 0; i < webcams; i++) {

				WebcamCamera cam = new WebcamCamera(i, WebcamCamera.RESOLUTION_480P);
				cams.add(cam);
			}
		}
	}

	/**
	 * Open all the cameras. This method must be called before trying to record,
	 * or an exception will be thrown.
	 */
	public void openCameras() {
		for (AbstractCamera cam : cams) {
			cam.open();
		}

		open = true;
	}

	/**
	 * Closes all the cameras and the files to which they are connected. This
	 * operation should normally be called to free the access to the cameras
	 * before the end of the program.
	 * 
	 * <p>
	 * Note that this function may be called to stop recording. This will end the
	 * current recording and close all the webcam, so they won't be recording.
	 * To restart recording then, just call {@link #openCameras()} again then
	 * {@link #startRecording()}.
	 * </p>
	 */
	public void closeCameras() {
		recording = false;
		open = false;

		for (AbstractCamera cam : cams) {
			cam.close();
		}
	}

	
	public void startRecording() {

		recording = true;

		if (!open) {
			throw new IllegalStateException("The cameras are not open");
		}

		new Thread(() -> {

			long begin = System.currentTimeMillis();
			final long start = begin;
			long lastModulo = 0;
			long currentMod = 0;
			long lastFrameLength = 0;
			long currentTime = 0;

			final long sleep = 1000 / FPS;
			long realSleep = 0;

			OutputStream currentStream = null;
			AbstractCamera currentCam = null;
			ImageToMp4Encoder currentChannel = null;
			int currentIndex = 0;

			// Opens channels for the current time
			channels = openChannels(begin);
			nextChannels = null;

			// Opens the next channel in another thread, so there will be no
			// wait time during transition
			new Thread(() -> {
				nextChannels = openChannels(start + FILE_LENGTH);
			}).start();
			// While recording is activated
			while (recording) {

				currentTime = System.currentTimeMillis();

				// Calculates last frame length
				lastFrameLength = begin - currentTime;

				// The time on which the capture began
				begin = currentTime;

				// Computes the current length of the file, and check if the
				// file time ended.
				currentMod = begin % FILE_LENGTH;

				if (currentMod < lastModulo) {
					// Reached the end of the time for the file.
					// Close the channels and open new ones for new
					// files.
					closeChannels(channels);
					channels = nextChannels;

					// Opens the next channel in another thread, so there will
					// be no
					// wait time during transition
					new Thread(() -> {
						nextChannels = openChannels(start + FILE_LENGTH);
					}).start();

				}
				// Updates the lastModulo
				lastModulo = currentMod;

				// Grab a frame for each camera
				for (currentIndex = 0; currentIndex < cams.size(); currentIndex++) {

					currentCam = cams.get(currentIndex);
					currentChannel = channels.get(currentIndex);
					BufferedImage img = currentCam.getImage();

					// Writes down time info on the image
					writeTimeInfo(img, currentIndex);

					// Write to the stream channel if there is one
					if ((currentStream = streams.get(currentCam)) != null) {
						try {
							byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
							System.out.println("Actual pixel length " + pixels.length);
							currentStream.write(pixels);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					// Writes the current data, with the current time
					try {
						// Encodes the same image for framelength ms, as
						// encoding is 1ms by frame
						currentChannel.inputImage(img, (int) lastFrameLength);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				realSleep = sleep - (System.currentTimeMillis() - begin);

				// Sleeps to release processor usage. WIll help perform an
				// approximate of 30fps depending on the encoder.
				try {
					if (realSleep > 0) {
						Thread.sleep(realSleep);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			// Now recording is false, so current files are saved
			if (channels != null) {
				closeChannels(channels);
			}

		}).start();

	}

	private void writeTimeInfo(BufferedImage img, int index) {

		StringBuilder builder = new StringBuilder(Formatters.getFormatedDate());

		Graphics gr = img.getGraphics();
		gr.setFont(INFO_FONT);

		// Draw directly the info to string
		gr.drawString(builder.toString(), TEXT_X, TEXT_Y);

	}

	public void initStreaming(AbstractCamera cam, OutputStream stream) throws IOException {

		if (recording) {
			if (streams.get(cam) != null) {
				throw new IllegalAccessError("This camera is currently used");
			}

			ByteBuffer header = getHeaderBuffer(cam);

			// Writes the array to the stream
			stream.write(header.array());

			// Sets up the stream in the record
			streams.put(cam, stream);

		} else {
			throw new IllegalStateException("The cameras are not recording");
		}

	}

	/**
	 * Ends the streaming of the given camera. This function must be called to
	 * unlock the access to this camera.
	 * 
	 * <p>
	 * Note that this function will close the stream to the output, and free the
	 * camera
	 * </p>
	 * 
	 * @param cam
	 *            The camera to which the stream must be ended
	 * @throws IOException
	 *             If an error occurs while closing the stream.
	 */
	public void endStreaming(AbstractCamera cam) throws IOException {
		streams.remove(cam).close();

	}

	private void closeChannels(List<ImageToMp4Encoder> channels) {

		for (ImageToMp4Encoder chan : channels) {

			try {
				chan.finish();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private List<ImageToMp4Encoder> openChannels(long time) {
		List<ImageToMp4Encoder> chan = new ArrayList<>();

		try {
			for (int i = 0; i < cams.size(); i++) {
				try {
					chan.add(initForRecording(cams.get(i), time));
				} catch (IOException e) {
					cams.remove(i);
					i--;
					e.printStackTrace();
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			// Finishes iterating
		}
		return chan;
	}

	public List<AbstractCamera> getCameras() {
		return new ArrayList<>(cams);
	}

	private ImageToMp4Encoder initForRecording(AbstractCamera cam, long time) throws IOException {

		// Constructs the name of the file for the current hour
		Calendar current = Calendar.getInstance();
		current.setTimeInMillis(time);
		int year = current.get(Calendar.YEAR);
		int month = current.get(Calendar.MONTH);
		int day = current.get(Calendar.DAY_OF_MONTH);
		int hour = current.get(Calendar.HOUR_OF_DAY);
		String name = generateFileName(cam, year, month, day, hour);

		// Creates a directory for the camera data (only on first use normally)
		File dirForCam = createDirForCam(cam);

		// A new file for the current time
		File f = new File(dirForCam.getPath() + "/" + name + ".mp4");

		System.out.println(f + " to create");
		// Will create the file
		if (!f.exists()) {
			f.createNewFile();
		}

		ImageToMp4Encoder encoder = null;
		try {
			encoder = new HumbleVideoEncoder(f, cam.getHeight(), cam.getWidth(), FPS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return encoder;

	}

	private ByteBuffer getHeaderBuffer(AbstractCamera cam) {
		ByteBuffer bufferData = ByteBuffer.allocate(16);

		bufferData.putInt(cam.getHeight());
		bufferData.putInt(cam.getWidth());
		bufferData.putInt(cam.getImageSize());
		bufferData.putInt(cam.getEncoding());

		System.out.println("Wished pixel size = " + cam.getImageSize());

		bufferData.flip();
		return bufferData;
	}

	private File createDirForCam(AbstractCamera cam) {
		File dir = new File(getClass().getResource(CAMERA_RECORD_PATH).getPath() + "/" + cam.getID());

		if (!dir.exists()) {
			dir.mkdirs();
		}

		System.out.println(dir);
		return dir;
	}

	private String generateFileName(AbstractCamera cam, int year, int month, int day, int hour) {

		// Asserts that if two cameras have the same name, a random value is
		// added
		int vidID = (int) (Math.random() * Integer.MAX_VALUE);

		StringBuilder builder = new StringBuilder();
		builder.append(vidID).append("_").append(year).append("-").append(month).append("-").append(day).append("_")
				.append(hour).append("h-").append(((hour + 1) % 24)).append("h");

		return builder.toString();
	}

}
