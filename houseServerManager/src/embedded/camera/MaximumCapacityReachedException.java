package embedded.camera;

public class MaximumCapacityReachedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8490041315983499752L;

	public MaximumCapacityReachedException() {
		// TODO Auto-generated constructor stub
	}

	public MaximumCapacityReachedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public MaximumCapacityReachedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MaximumCapacityReachedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MaximumCapacityReachedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
