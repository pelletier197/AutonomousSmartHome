package embedded.camera;

public class Encoding {

	/**
	 * Encoding for 24 bits per pixel, where each color is RGB color.
	 */
	public static final int RGB_24 = 0;

	/**
	 * Encoding for 32 bits per pixel with Alpha RGB.
	 */
	public static final int ARGB_32 = 0;

}
