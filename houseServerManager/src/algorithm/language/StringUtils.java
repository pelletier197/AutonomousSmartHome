package algorithm.language;

public final class StringUtils {

	/**
	 * Iterates over the string and returns the replacement of the first value
	 * equal to the value in the replacement array.
	 * 
	 * @param value
	 *            The value to replace
	 * @param replacement
	 *            The replacement values where each String[] is a possible
	 *            replacement key-value.
	 * @return The replacement value, or the value itself if no replacement is
	 *         found
	 */
	public static String replaceIfEqual(String value, String[][] replacement) {

		for (String[] replace : replacement) {
			if (value.equals(replace[0])) {
				return value.replaceAll(replace[0], replace[1]);
			}
		}

		return value;
	}

	/**
	 * Iterates over the string and replaces all the occurrence of the first
	 * argument of the first index in the array for the second argument.
	 * 
	 * <p>
	 * Every String[] in the replacement parameter is read, where String[0] is
	 * the value to replace and String[1] is the value to use as replacement
	 * </p>
	 * 
	 * @param value
	 *            The string on which to perform the replacement
	 * @param replacement
	 *            The replacement keys
	 * @return The new string
	 */
	public static String replaceAll(String value, String[][] replacement) {

		for (String[] kv : replacement) {
			value = value.replaceAll(kv[0], kv[1]);
		}
		return value;
	}

	/**
	 * Returns true if any string in the array to verify is equal to the value
	 * itself.
	 * 
	 * @param value
	 *            The value to check
	 * @param toVerify
	 *            The values to verify if equal to value
	 * @return True if any of the string is equal, false otherwise
	 */
	public static boolean anyEqualToString(String value, String[] toVerify) {
		for (String verif : toVerify) {
			if (value.equals(verif)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if any string in the array to verify is contained in the
	 * value to check.
	 * 
	 * @param value
	 *            The value to check
	 * @param toVerify
	 *            The array of string to find in the value.
	 * @return True if value contains any of the strings, false otherwise.
	 */
	public static boolean anyEqualInString(String value, String[] toVerify) {
		for (String verif : toVerify) {
			if (value.contains(verif)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Removes all the string in the array from the value string and returns the
	 * result.
	 * 
	 * @param value
	 *            The value from which to remove the string
	 * @param toRemove
	 *            The strings to remove
	 * @return The result
	 */
	public static String removeAll(String value, String[] toRemove) {

		final String empty = "";

		for (String remove : toRemove) {
			value = value.replaceAll(remove, empty);
		}
		return value;
	}
}
