package algorithm.language.parser;

public class UnsupportedLanguageException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3356347940962514951L;

	public UnsupportedLanguageException() {
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLanguageException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLanguageException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLanguageException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLanguageException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
