package algorithm.language.parser;

public interface StringNumberParser {

	public double parseDouble(String number);
	
	public long parseLong(String number);
	
}
