package algorithm.language.parser;

public class FrenchNumberParser extends EnglishLikePatternNumberParser {

	private static final String[] IGNORE = { " et" };
	// On enlève le "d" pour éviter le match avec dix et "P" de sept
	private static final String[][] REPLACE = { new String[] { "zéro", "zero" }, new String[] { "dix sept", "ixset" },
			new String[] { "dix huit", "ixuit" }, new String[] { "dix neuf", "ixeuf" },
			new String[] { "soixante dix", "soixanteix" }, new String[] { "quatre vingt", "uatreight" } };

	private static final String[] NEGATIVE = { "moins" };
	private static final String[] DIGITS = { "point", "virgule" };
	private static final String[] UNITS = { "zero", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit",
			"neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "ixset", "ixuit", "ixeuf" };
	private static final String[] TEN = {};
	private static final String[] TENS = { null, null, "vingt", "trente", "quarante", "cinquante", "soixante",
			"soixanteix", "uatreight", null };
	private static final String[] BIG = { null, null, "cent", "mille", null, null, "million", null, null, "milliard" };

	public FrenchNumberParser() {
		super(UNITS, TENS, TEN, BIG, IGNORE, REPLACE, NEGATIVE, DIGITS);

	}

	@Override
	public long parseLong(String number) {
		return super.parseLong(number.replace('-', ' '));
	}
}
