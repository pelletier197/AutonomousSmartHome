package algorithm.language.parser;

import java.util.Arrays;

import org.apache.http.ParseException;

import com.ibm.icu.impl.InvalidFormatException;

import algorithm.language.NumberParseException;
import algorithm.language.StringUtils;

/**
 * Number converter to convert words to number for english like word patterns.
 * 
 * <p>
 * The usage of this class is defined to match the pattern the same way for all
 * the English like patterns, but where the words changes. To understand the
 * pattern see the exmples bellow.
 * 
 * <li>five million two thousand four hundred thirty five</li>
 * <li>cinq millions deux mille quatre cent trente cinq</li>
 * 
 * We see here that all words find a perfect match for each word in both
 * languages, but numbers words differ.
 * </p>
 * 
 * <p>
 * This class must therefore be implemented, and the implementer must give the
 * string values for every number values. This class will then perform the
 * match.
 * </p>
 * 
 * @author sunny
 *
 */
public abstract class EnglishLikePatternNumberParser implements StringNumberParser {

	private String[] UNITS;
	private String[] TENS;
	private String[] TEN;
	private String[] BIG;
	private String[] NEGATIVE;
	private String[] DIGITS;
	private String[][] REPLACE;
	private String[] IGNORE;

	/**
	 * Construct a number parser for English like patterns using the given array
	 * of words.
	 * 
	 * <p>
	 * Note that there can be null values in every arrays, and they can be
	 * empty. This class is very flexible in word matching.
	 * </p>
	 * 
	 * @param units
	 *            The units. It is imperative that they are in order from
	 *            [zero...nine].
	 * @param tens
	 *            The tens value, as for English like patterns, tens are
	 *            specific words. It is imperative that they are in order, from
	 *            [ten...nineteen]
	 * @param ten
	 *            The ten array. Contains all ten units. It is imperative that
	 *            they are in order from [null, null, twenty...ninety]
	 * @param big
	 *            The big numbers words. It is imperative that they are in
	 *            order, and that the index of the number is equal to the number
	 *            of zero the big value has. For instance, million will be at
	 *            index six. [null, null, hundred, thousand, null, null,
	 *            million...]
	 * @param ignore
	 *            The words to ignore when parsing a number. They will be
	 *            removed before parsing. For instance, in English the word
	 *            " and " is ignored in "one hundred and three"
	 * 
	 * @param replace
	 *            The words to replace in the numbers before parsing. For
	 *            instance, in English " oh " is considered a zero, so " oh "
	 *            are replaced by zero.
	 * 
	 * @param negative
	 *            The words that indicate that the number is negative, like
	 *            "minus" in English.
	 * 
	 * @param dot
	 *            The words that indicate the dot in a number, "point" or "dot"
	 *            in English.
	 */
	public EnglishLikePatternNumberParser(String[] units, String[] tens, String[] ten, String[] big, String[] ignore,
			String[][] replace, String[] negative, String[] dot) {
		this.UNITS = units;
		this.TENS = tens;
		this.TEN = ten;
		this.BIG = big;
		this.NEGATIVE = negative;
		this.DIGITS = dot;
		this.REPLACE = replace;
		this.IGNORE = ignore;
	}

	/**
	 * Parses the number's value to a double number. This function will use the
	 * values of {@link #DIGITS} to split the number between the int part and
	 * the decimal part, and convert both parts separately.
	 * 
	 * <p>
	 * The parse is made in order that the number can be an enumeration of digit
	 * for the number and the digit, but the digits must be only an enumeration
	 * of units.
	 * </p>
	 */
	@Override
	public double parseDouble(String number) {
		if (!validFormat(number)) {
			throw new NumberParseException("The number format is invalid");
		}

		// Solve digit part and int part as different integers
		String[] parts = splitDigit(number);

		// The number has no digit
		if (parts != null) {

			// Parse the integer part
			double intPart = (double) parseLong(parts[0].trim());

			// Parses the digits
			long digitLong = parseUnitEnumeration(parts[1]);
			double digitPart = (double) digitLong / (double) findBiggerHundred(digitLong);
			
			// Add if number is positive, subtract if negative
			return intPart < 0 ? intPart - digitPart : intPart + digitPart;

		} else {
			return (double) parseLong(number);
		}
	}

	private long parseUnitEnumeration(String digitPart) {
		String[] adjusted = adjust(digitPart.trim().toLowerCase()).split(" ");

		if (!validateOnlyUnits(adjusted)) {
			throw new NumberParseException("The digit part can only contain units");
		}

		return parseLong(adjusted);
	}

	private boolean validateOnlyUnits(String[] words) {

		System.out.println(Arrays.toString(words));
		for (String s : words) {

			if (!containsAnyUnit(s)) {
				return false;
			}

		}
		return true;
	}

	private boolean containsAnyUnit(String word) {
		for (String u : UNITS) {
			if (word.contains(u)) {
				return true;
			}
		}
		return false;
	}

	private long findBiggerHundred(long number) {
		long quotient = 1;

		while (true) {
			if (number / quotient < 1) {
				return quotient;
			}
			quotient *= 10;
		}
	}

	public static void main(String[] args) {
		EnglishUSNumberParser parser = new EnglishUSNumberParser();
	}

	private String[] splitDigit(String number) {
		for (String d : DIGITS) {
			if (number.contains(d)) {
				return number.split(d);
			}
		}
		return null;
	}

	private boolean validFormat(String number) {
		int numberOfDigits = 0;

		for (String d : DIGITS) {
			if (number.contains(d)) {
				if (numberOfDigits > 1) {
					return false;
				}
				numberOfDigits++;
			}
		}
		return true;
	}

	@Override
	public long parseLong(String number) {

		String[] numberWords = adjust(number.toLowerCase()).split(" ");
		System.out.println(adjust(number.toLowerCase()));
		return parseLong(numberWords);

	}

	private long parseLong(String[] number) {

		long result = 0;
		if (number.length != 0) {
			if (isNegative(number[0])) {
				return -parseLong(Arrays.copyOfRange(number, 1, number.length));
			}

			System.out.println(Arrays.toString(number) + "parseLong");
			int[] biggest = indexOfBiggest(number);

			// Number < 100
			if (biggest == null) {
				int[] biggestTens = indexOfTens(number);

				// number < 20
				if (biggestTens == null) {

					long ten = matchTen(number);

					// No ten match
					if (ten != -1) {
						return ten;
						// 0 <= number <= 9
					} else {
						long unit = matchUnit(number);

						if (unit != -1) {
							return unit;
						} else {
							// The number is invalid
							throw new IllegalArgumentException("The number is invalid");
						}
					}
				}
				// Exemple for thirty three, biggestTens[0] = 3, biggestTens[1]
				// = 0
				return 10 * biggestTens[0] + parseLong(Arrays.copyOfRange(number, 1, number.length));

			} else {
				// Gets the power of ten from the order in BIG array
				int bigPow = biggest[0];
				int indexOfBig = biggest[1];

				// We match everything that is before the big value
				// Ex (five hundred) [thousand]
				long prefix = parseLong(Arrays.copyOfRange(number, 0, indexOfBig));

				// If nothing exists on the left side, 1 is used by default
				prefix = prefix == 0 ? 1 : prefix;
				result += prefix * powTen(1, bigPow);

				// We match everything after
				result += parseLong(Arrays.copyOfRange(number, indexOfBig + 1, number.length));

				return result;
			}
		}
		return 0;

	}

	private boolean isNegative(String string) {
		for (String s : NEGATIVE) {
			if (s.contains(string)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * This method should be called to get the long value of an object situated
	 * in the range of 10-19 included. If the match to any of these units fails,
	 * the result is -1.
	 * 
	 * @param Number
	 *            the number to match in a string format
	 * @return The long value of this number
	 */
	private long matchTen(String[] number) {

		for (int i = 0; i < TEN.length; i++) {
			if (TEN[i] != null) {
				if (number[0].contains(TEN[i])) {
					return i + 10;
				}
			}
		}
		return -1;
	}

	private long matchUnit(String[] number) {

		StringBuilder builder = new StringBuilder();
		for (int j = 0; j < number.length; j++) {
			for (long i = 0; i < UNITS.length; i++) {
				if (number[j].contains(UNITS[(int) i])) {
					builder.append(i);
					break;
				}
			}
		}
		return Long.parseLong(builder.toString());
	}

	private int[] indexOfTens(String[] numberWords) {

		int i = 0;
		int j = 0;

		for (; j < numberWords.length; j++) {
			for (i = TENS.length - 1; i >= 0; i--) {
				if (TENS[i] != null) {
					if (numberWords[j].contains(TENS[i])) {
						return new int[] { i, j };
					}
				}
			}
		}
		return null;
	}

	/**
	 * Finds and return an array containing the first big word (the one with the
	 * biggest index) contained in the matching string and its index in the
	 * {@link #BIG} array.
	 * 
	 * @param numberWords
	 *            The words in number
	 * @return An array of [index of the BIG word, index of the word in the
	 *         string] or null if there is no match
	 */
	private int[] indexOfBiggest(String[] numberWords) {

		int i = 0;
		int j = 0;

		int indexOfBiggestBIG = -1;
		int indexOfBiggestString = -1;

		for (i = 0; i < numberWords.length; i++) {
			for (j = 0; j < BIG.length; j++) {
				if (BIG[j] != null) {
					if (numberWords[i].contains(BIG[j])) {
						if (j > indexOfBiggestBIG) {
							indexOfBiggestBIG = j;
							indexOfBiggestString = i;
						} else if (j == indexOfBiggestBIG) {
							throw new IllegalArgumentException("The number format is invalid");
						}
					}
				}

			}
		}
		// No match
		if (indexOfBiggestBIG == -1) {
			return null;
		}
		return new int[] { indexOfBiggestBIG, indexOfBiggestString };
	}

	private String adjust(String number) {

		// Removes the values to ignore
		number = StringUtils.removeAll(number, IGNORE);

		// Replaces the values to replace
		number = StringUtils.replaceAll(number, REPLACE);

		return number;
	}

	private long powTen(long val, long pow) {
		long result = val;

		for (int i = 1; i <= pow; i++) {
			result *= 10;
		}

		return result;
	}
}
