package algorithm.language.parser;

import java.util.Arrays;
import java.util.regex.Pattern;

public class EnglishUSNumberParser extends EnglishLikePatternNumberParser {

	private static final String[] IGNORE = { " and" };
	private static final String[][] REPLACE = { new String[] { "oh", "zero" } };

	private static final String[] NEGATIVE = { "minus" };
	private static final String[] DIGITS = { "point", "dot" };
	private static final String[] UNITS = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
			"nine" };
	private static final String[] TEN = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
			"seventeen", "eighteen", "nineteen" };
	private static final String[] TENS = { null, "dix", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy",
			"eighty", "ninety" };
	private static final String[] BIG = { null, null, "hundred", "thousand", null, null, "million", null, null,
			"billion" };

	public EnglishUSNumberParser() {
		super(UNITS, TENS, TEN, BIG, IGNORE, REPLACE, NEGATIVE, DIGITS);
	}
	
}
