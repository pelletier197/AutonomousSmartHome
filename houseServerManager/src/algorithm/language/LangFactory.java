package algorithm.language;

import algorithm.language.date.EnglishUSDateFormatter;
import algorithm.language.date.StringDateFormatter;
import algorithm.language.formatter.EnglishUSNumberFormatter;
import algorithm.language.formatter.StringNumberFormatter;
import algorithm.language.parser.EnglishUSNumberParser;
import algorithm.language.parser.FrenchNumberParser;
import algorithm.language.parser.StringNumberParser;
import algorithm.language.parser.UnsupportedLanguageException;
import houseDataCenter.applicationRessourceBundle.Language;

public final class LangFactory {

	private LangFactory() {
		// Do not allow object instantiation
	}

	public static final StringNumberParser numberParserFor(Language lang) {

		switch (lang) {
		case FRENCH:
			return new FrenchNumberParser();
		case ENGLISH_US:
			return new EnglishUSNumberParser();
		default:
			break;
		}

		throw new UnsupportedLanguageException("Language is not supported");
	}

	public static final StringNumberFormatter numberFormatterFor(Language lang) {

		switch (lang) {
		case ENGLISH_US:
			return new EnglishUSNumberFormatter();
		default:
			break;
		}

		throw new UnsupportedLanguageException("Language is not supported");
	}

	public static final StringDateFormatter dateFormatterFor(Language lang) {

		switch (lang) {
		case ENGLISH_US:
			return new EnglishUSDateFormatter();
		default:
			break;
		}

		throw new UnsupportedLanguageException("Language is not supported");
	}

}
