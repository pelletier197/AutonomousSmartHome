package algorithm.language.date;

import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import algorithm.language.StringUtils;
import algorithm.language.formatter.EnglishUSNumberFormatter;
import algorithm.language.formatter.StringNumberFormatter;
import algorithm.language.parser.StringNumberParser;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.applicationRessourceBundle.Language;

public class EnglishUSDateFormatter extends DateFormatterBase {

	private StringNumberFormatter formatter;

	private static final String[][] REPLACE_IF_EQUAL_DAY = { new String[] { "one", "first" },
			new String[] { "two", "second" }, new String[] { "three", "third" } };

	private static final String[] DAYS_OF_WEEK = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
			"Saturday" };
	private static final String[] MONTHS = { "January", "February", "March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };

	private static final String MIDNIGHT = "Midnight";
	private static final String NOON = "Noon";
	private static final String AM = " A.M.";
	private static final String PM = " P.M.";
	private static final String OCLOCK = " O'clock";
	private static final String MINUTES_PAST = " minutes past ";
	private static final String MINUTES_TO = " minutes to ";

	public EnglishUSDateFormatter() {
		this.formatter = new EnglishUSNumberFormatter();
	}

	@Override
	public String formatDay(Calendar cal) {

		StringBuilder builder = new StringBuilder();

		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		String dayOfWeekString = DAYS_OF_WEEK[dayOfWeek - 1];
		String dayOfMonthString = formatter.formatLong(dayOfMonth);
		String monthString = MONTHS[month];
		String yearString = formatter.formatLong(year);

		// Replace the values for first days of month
		dayOfMonthString = StringUtils.replaceIfEqual(dayOfMonthString, REPLACE_IF_EQUAL_DAY);

		// Format the date and returns it
		return builder.append(dayOfWeekString).append(" ").append(monthString).append(" ").append(dayOfMonthString)
				.append(" ").append(yearString).toString();
	}

	@Override
	public String formatTime(Calendar calendar) {

		StringBuilder builder = new StringBuilder();

		int hour = calendar.get(Calendar.HOUR);
		int hourDay = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		boolean am = calendar.get(Calendar.AM_PM) == calendar.AM;

		// Threats for midnight
		if (hourDay == 0 || hourDay == 12) {
			builder.append(formatSpecialTime(hourDay, minutes));

		} else {

			builder.append(formatter.formatLong(hour));

			if (minutes == 0) {
				builder.append(OCLOCK);
			} else {
				builder.append(" ").append(formatter.formatLong(minutes));
			}

			builder.append(am ? AM : PM);

		}

		return builder.toString();
	}

	private String formatSpecialTime(int hourOfDay, int minutes) {

		StringBuilder builder = new StringBuilder();
		if (hourOfDay == 0) {
			if (minutes > 0 && minutes <= 30) {
				builder.append(formatter.formatLong(minutes)).append(MINUTES_PAST).append(MIDNIGHT);
			} else if (minutes > 30) {
				builder.append(formatter.formatLong(60 - minutes)).append(MINUTES_TO).append(" one").append(AM);
			} else if (minutes == 0) {
				builder.append(MIDNIGHT);
			}
			// Threats for noon
		} else if (hourOfDay == 12) {
			if (minutes > 0 && minutes <= 30) {
				builder.append(formatter.formatLong(minutes)).append(MINUTES_PAST).append(NOON);
			} else if (minutes >= 30) {
				builder.append(formatter.formatLong(60 - minutes)).append(MINUTES_TO).append(" one").append(PM);
			} else if (minutes == 0) {
				builder.append(NOON);
			}

		} else {
			throw new NumberFormatException("Time not supported");
		}
		return builder.toString();
	}

	public static void main(String[] args) {
		String bundle = Language.ENGLISH_US.getProperty("strings_server");
		BundleWrapper.setResourceBundle(ResourceBundle.getBundle(bundle, new Locale("", "")));
		StringDateFormatter formatter = new EnglishUSDateFormatter();
		Calendar cal = Calendar.getInstance();
		cal.set(2017, 10, 10, 23, 59, 0);

		System.out.println(formatter.formatTime(cal));
	}

}
