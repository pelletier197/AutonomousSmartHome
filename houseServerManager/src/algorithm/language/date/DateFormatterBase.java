package algorithm.language.date;

import java.util.Calendar;

import houseDataCenter.applicationRessourceBundle.BundleWrapper;

public abstract class DateFormatterBase implements StringDateFormatter {


	@Override
	public String formatDay() {
		return formatDay(Calendar.getInstance());
	}

	@Override
	public String formatTime() {
		return formatTime(Calendar.getInstance());
	}

}
