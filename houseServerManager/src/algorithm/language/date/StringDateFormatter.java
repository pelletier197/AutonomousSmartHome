package algorithm.language.date;

import java.util.Calendar;

public interface StringDateFormatter {

	public String formatDay();

	public String formatDay(Calendar calendar);

	public String formatTime();

	public String formatTime(Calendar calendar);
}
