package algorithm.language.formatter;

import java.math.BigDecimal;
import java.math.BigInteger;

public interface StringNumberFormatter {

	public String formatLong(long number);
	
	public String formatDouble(double number);
	
}
