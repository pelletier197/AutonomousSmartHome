package algorithm.language.formatter;

import java.util.Stack;

import utils.Formatters;

public class EnglishUSNumberFormatter implements StringNumberFormatter {

	private static final String[] IGNORE = { " and" };
	private static final String[][] REPLACE = { new String[] { "oh", "zero" } };

	private static final String NEGATIVE = "minus";
	private static final String DIGIT = "point";
	private static final String[] UNITS = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
			"nine" };
	private static final String[] TEN = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",
			"seventeen", "eighteen", "nineteen" };
	private static final String[] TENS = { null, "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
			"eighty", "ninety" };
	private static final String[] BIG = { null, null, "hundred", "thousand", null, null, "million", null, null,
			"billion" };

	@Override
	public String formatLong(long number) {

		if (number < 0) {
			return NEGATIVE + " " + formatLong(-1 * number);
		}
		// For unit numbers
		if (number < 10) {
			return UNITS[(int) number];
		}

		// For ten numbers
		if (number < 20) {
			return TEN[(int) (number - 10)];
		}

		// Rest of numbers
		// Reverses the number so the indexs are easier to manipulate
		String sNum = new StringBuilder(String.valueOf(number)).reverse().toString();
		final int numLength = sNum.length();
		Stack<String> result = new Stack<>();

		// Treats ten then
		char ten = sNum.charAt(1);

		// for X11-X19, where X in [1,9] the difference is important
		if (ten != '1') {
			// Treats unit first
			char unit = sNum.charAt(0);
			if (unit != '0') {
				result.push(UNITS[Character.getNumericValue(unit)]);

			}

			String tenV = TENS[Character.getNumericValue(ten)];

			if (tenV != null) {
				result.push(tenV);
			}
		} else {
			// Push the ten value for the hundred
			result.push(TEN[Integer.parseInt(String.valueOf(sNum.charAt(1)) + sNum.charAt(0)) - 10]);
		}

		// Treats hundred then
		if (number >= 100) {
			// To avoid pushing 'zero hundred'
			if (sNum.charAt(2) != '0') {
				result.push("hundred");

				char hundred = sNum.charAt(2);
				result.push(UNITS[Character.getNumericValue(hundred)]);
			}
			// Now every number can have up to 999 hundred depending on
			// offset left.
			for (int i = 3; i < numLength; i++) {

				// Push the ten value
				result.push(BIG[i]);

				int offset = numLength - i - 1;
				// Sets the offset to two if there is more than 2 numbers left
				offset = offset > 2 ? offset = 2 : offset;

				// go get its amount
				// ex : 500 thousand
				String valueToFind = new StringBuilder(sNum.substring(i, i + offset + 1)).reverse().toString();
				result.push(formatLong(Long.parseLong(valueToFind)));
				i += offset;

			}
		}
		return build(result);
	}

	public static void main(String[] args) {
		EnglishUSNumberFormatter formatter = new EnglishUSNumberFormatter();

		System.out.println(formatter.formatDouble(1_229_023.04));
	}

	@Override
	public String formatDouble(double number) {

		if (number < 0) {
			return NEGATIVE + " " + formatDouble(-1 * number);
		}

		StringBuilder builder = new StringBuilder();

		// Format the whole part first
		long whole = (long) number;
		builder.append(formatLong(whole));

		// Then enumerates all the digits
		String digits = enumerateFractionPart(number);

		if (digits != null) {
			// Appends the digits
			builder.append(' ').append(DIGIT).append(' ');
			builder.append(digits);
		}

		return builder.toString();
	}

	private String enumerateFractionPart(double number) {

		long whole = (long) number;

		double dif = whole - number;
		dif = dif < 0 ? dif * -1 : dif;
		String frac = Formatters.doubleFormat(dif, 0, 4);
		final char[] fracChars = frac.toCharArray();

		System.out.println(frac);

		StringBuilder builder = new StringBuilder();

		if (fracChars.length < 3) {
			// No digits
			return null;
		}
		// jumps 0.
		for (int i = 2; i < fracChars.length; i++) {
			builder.append(UNITS[Character.getNumericValue(fracChars[i])]).append(" ");
		}

		return builder.toString().trim();
	}

	private String build(Stack<String> parts) {

		StringBuilder builder = new StringBuilder();

		while (!parts.isEmpty()) {
			builder.append(parts.pop()).append(" ");
		}

		return builder.toString().trim();
	}

}
