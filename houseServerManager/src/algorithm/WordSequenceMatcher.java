package algorithm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import houseDataCenter.mp3Player.Mp3SongInfo;

/**
 * Represents a class that performs word sequence matching for a given input. It
 * attempts to match the most amount of words of a string received in input and
 * to return the matching string key that corresponds the best to it by finding
 * the key that has the most words in common, without care to the word order in
 * the sentence and the case.
 *
 * <p>
 * For instance, there could be a set of keys containing
 * <li>[I am a person, I am a banana, love cars i, I love cars] strings
 * associated to the objects</li>
 * <li>[Object 1 , Object 2 , Object 3, Object 4]</li>
 * 
 * Would have the given behavior in the given examples
 * <li>Calling {@link #match(String)} will return Object 1 if called with input
 * "I am a person"</li>
 * <li>Calling {@link #match(String)} will return Object 2 if called with input
 * "I am a banana"</li>
 * <li>Calling {@link #match(String)} will return Object 1 if called with input
 * "I am" as it is first in the logical order</li>
 * <li>Calling {@link #match(String)} will return Object 3 if called with input
 * "love"</li>
 * <li>Calling {@link #match(String)} will return Object 3 if called with input
 * "I LOVE CARS"</li>
 * </p>
 * 
 * @author sunny
 *
 */
public class WordSequenceMatcher<T> implements StringSequenceMatcher<T> {

	private Map<String[], T> objectKeyMap;

	public WordSequenceMatcher() {
		this(new HashMap<String, T>());
	}

	public WordSequenceMatcher(Map<String, T> objectKeyMap) {
		this.objectKeyMap = toSplittedMap(objectKeyMap);
	}

	@Override
	public void addKey(String key, T object) {
		objectKeyMap.put(splitKey(key), object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T match(String input) {
		return (T) matchArray(input)[0];
	}

	@Override
	public String matchingkey(String input) {
		// Reconstruct the key from the array of strings
		return String.join(" ", (String[]) matchArray(input)[1]);
	}

	@Override
	public boolean removeKey(String key) {
		String[] splitted = splitKey(key);

		Map.Entry<String[], T> current = null;
		for (Iterator<Map.Entry<String[], T>> it = objectKeyMap.entrySet().iterator(); it.hasNext();) {
			current = it.next();

			// Finds the matching key
			if (Arrays.equals(splitted, current.getKey())) {
				it.remove();
				return true;
			}
		}
		return false;
	}

	@Override
	public void removeAllKeys() {
		objectKeyMap.clear();
	}

	@Override
	public int getKeyCont() {
		return objectKeyMap.size();
	}

	/**
	 * Performs the matching operation and returns the result as an array of
	 * object containing [T , key] where T is the object to be returned when
	 * calling {@link #match(String)} and key is the array corresponding to the
	 * object in {@link #objectKeyMap}.
	 * 
	 * Will try to find the most words in commons between the input and the keys
	 * of the objectMap, and will return the array that's key has the most words
	 * in common.
	 * 
	 * @param input
	 *            The input string to match
	 * @return An array of matching elements.
	 */
	private Object[] matchArray(String input) {

		Object[] bestMatch = new Object[2];
		int bestMatchWordCount = 0;
		final String[] songSplitted = splitKey(input);
		final int songWordCount = songSplitted.length;
		String[] currentName = null;
		int currentMatchWordCount = 0;
		int i = 0;

		for (Map.Entry<String[], T> e : objectKeyMap.entrySet()) {
			currentName = e.getKey();

			// If the song has less letters than the best match
			if (currentName.length < bestMatchWordCount) {
				continue;
			}

			currentMatchWordCount = 0;

			for (i = 0; i < songSplitted.length; i++) {

				// current word matches
				if (contains(currentName, songSplitted[i])) {
					currentMatchWordCount++;
				}

				// Checks to see if there is enough words left to check to beat
				// the best result, and
				// break if there is not
				if (bestMatchWordCount - currentMatchWordCount + 1 > songSplitted.length - i) {
					break;
				}
			}

			// Update the new best match
			if (currentMatchWordCount > bestMatchWordCount) {
				bestMatchWordCount = currentMatchWordCount;
				bestMatch[0] = e.getValue();
				bestMatch[1] = currentName;
			}

			// perfect match for the object, so return it
			if (bestMatchWordCount == songWordCount) {
				return bestMatch;
			}
		}

		return bestMatch;

	}

	private Map<String[], T> toSplittedMap(Map<String, T> map) {

		Map<String[], T> keysObject = new HashMap<>();

		for (Map.Entry<String, T> entry : map.entrySet()) {
			keysObject.put(splitKey(entry.getKey()), entry.getValue());
		}

		return keysObject;

	}

	private boolean contains(String[] array, String s) {

		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(s)) {
				return true;
			}
		}
		return false;
	}

	private String[] splitKey(String key) {
		return key.toLowerCase().split(" ");
	}

}
