package algorithm;

public interface StringSequenceMatcher<T> {
	
	public int getKeyCont();

	public String matchingkey(String input);

	public T match(String input);

	void addKey(String key, T object);
	
	boolean removeKey(String key);
	
	void removeAllKeys();
}
