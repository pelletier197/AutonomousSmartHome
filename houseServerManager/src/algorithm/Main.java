package algorithm;

public class Main {

	public static void main(String[] args) {

		StringSequenceMatcher<String> matcher = new WordSequenceMatcher<>();

		matcher.addKey("Sound of silence", "Sound of silence -");
		matcher.addKey("Sound of madness", "Sound of madness - ");
		matcher.addKey("help is on the way", "help - ");

	}

}
