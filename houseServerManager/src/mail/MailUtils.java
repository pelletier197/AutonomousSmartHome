package mail;

public class MailUtils {

	private MailUtils() {
	}
	
	/**
	 * Validates that the given email is valid.
	 * 
	 * @param mail
	 *            The mail address to validate
	 * @return True if the mail is valid, false otherwise.
	 */
	public static boolean validEmail(String mail) {
		return mail.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$");
	}

}
