package mail;

public interface MailSender {

	public void sendMessage(String subject, String text, String... dest);	
	
}
