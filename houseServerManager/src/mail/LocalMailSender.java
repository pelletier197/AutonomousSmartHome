package mail;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class LocalMailSender implements MailSender {

	private String provider;
	private String address;
	private String pass;

	/**
	 * Creates a mail sender using the given mail address, mail server and
	 * password to connect to the server.
	 * 
	 * @param provider
	 *            The mail address used to connect
	 * @param serverAdress
	 *            The Address of the server
	 * @param password
	 *            The password of the mail address
	 */
	public LocalMailSender(String provider, String serverAdress, String password) {
		if (provider == null || serverAdress == null || password == null) {
			throw new NullPointerException("Null provider not permitted");
		}
		if (!MailUtils.validEmail(provider)) {
			throw new IllegalAccessError("This email is not valid");
		}

		this.address = serverAdress;
		this.provider = provider;
		this.pass = password;

	}

	/**
	 * Sends a message using the current mail service provider with the given
	 * subject, text and recipients.
	 * 
	 * @param subject
	 *            The subject of the mail
	 * @param contentText
	 *            The text content of the message. Can be formatted using HTML.
	 * @param recipients
	 *            The recipients of the message
	 */
	@Override
	public void sendMessage(String subject, String contentText, String... recipients) {

		try {

			Properties properties = new Properties();
			properties.setProperty("mail.transport.protocol", "smtp");
			properties.setProperty("mail.smtp.auth", "true");
			properties.setProperty("mail.smtp.starttls.enable", "true");
			properties.setProperty("mail.smtp.port", "587");
			properties.setProperty("mail.smtp.host", address);
			properties.setProperty("mail.from", provider);

			Session session = Session.getInstance(properties, new Authenticator() {

				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(provider, pass);
				}

			});

			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(provider));

			for (String to : recipients) {
				// Set To: header field of the header.
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			}

			// Set Subject: header field
			message.setSubject(subject);
			message.setText(contentText, "UTF-8", "html");

			// Send message
			Transport.send(message);

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}

}
