package houseServer.request;

import java.util.UUID;

import houseDataCenter.house.exceptions.AccessRefusedError;
import server.communication.Request;

/**
 * Represents a Server manager for specific handler for a given range of flags.
 * 
 * @author sunny
 *
 */
public interface SManager {

	public void roomRequest(Request dat, UUID clientID, byte comType) throws AccessRefusedError;

	public void mobileRequest(Request dat, UUID clientID, byte comType);

	public void close();
}
