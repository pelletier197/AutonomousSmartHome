package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.NEXT_SONG;
import static houseDataCenter.commucationData.TransferFlags.NOTIFY_MEDIA_PLAYING_CHANGED;
import static houseDataCenter.commucationData.TransferFlags.NOTIFY_MEDIA_STATUS_CHANGED;
import static houseDataCenter.commucationData.TransferFlags.NOTIFY_READING_MODE_CHANGED;
import static houseDataCenter.commucationData.TransferFlags.PREVIOUS_SONG;
import static houseDataCenter.commucationData.TransferFlags.REMOVE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_MUSIC_READING_MODE;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_MUSIC_STATUS;
import static houseDataCenter.commucationData.TransferFlags.SET_READING_MODE;
import static houseDataCenter.commucationData.TransferFlags.SWITCH_PLAYING;
import static houseDataCenter.commucationData.TransferFlags.UPLOAD_NEW_MUSIC;
import static houseServer.HouseServerManager.MOBILE_OVERLOAD;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import algorithm.StringSequenceMatcher;
import algorithm.WordSequenceMatcher;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistManager;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseParametersSaver;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDevice;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import houseServer.devices.intelligentDevice.IntelligentDeviceRequestManager;
import houseServer.devices.room.HouseRequestManager;
import server.serialization.SerializationType;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;
import server.serialization.SerializationManager;
import src.main.java.com.mpatric.mp3agic.InvalidDataException;
import src.main.java.com.mpatric.mp3agic.UnsupportedTagException;
import utils.Compresser;
import utils.HouseUtils;

public class SMusicManager extends AbstractSManager {

	private PlaylistManager playlistManager;
	private StringSequenceMatcher<Mp3SongInfo> songNameMatcher;

	public SMusicManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {

		super(deviceManager, houseManager, serverManager, saver);

		HouseParametersSaver paramSaver = new HouseParametersSaver(houseSaver);
		this.songNameMatcher = new WordSequenceMatcher<Mp3SongInfo>();

		List<Mp3SongInfo> songs = paramSaver.readMusics();
		addSongsToMatcher(songs);
		System.out.println(songs + "SONGS");
		this.playlistManager = new PlaylistManager(songs);
	}

	private void addSongsToMatcher(List<Mp3SongInfo> songs) {
		for (Mp3SongInfo song : songs) {
			addSongToMatcher(song);
		}
	}

	private void addSongToMatcher(Mp3SongInfo song) {
		songNameMatcher.addKey(song.getTitle(), song);

	}

	@SocketMethod(flag = REQUEST_MUSIC_STATUS, overload = ROOM_OVERLOAD)
	public void sendMusicStatusToMobile(@SocketParameter Object[] statusInfo, TransferDataRequest request) {
		UUID deviceUUID = (UUID) statusInfo[1];
		sendDevice(deviceUUID, REQUEST_MUSIC_STATUS, statusInfo[0]);
	}

	@SocketMethod(flag = NOTIFY_MEDIA_PLAYING_CHANGED, overload = ROOM_OVERLOAD)
	public void mediaPlayingChanded(TransferDataRequest request) {
		sendAllDevices(new TransferData(NOTIFY_MEDIA_PLAYING_CHANGED, request.getClientId()));
	}

	@SocketMethod(flag = NOTIFY_MEDIA_STATUS_CHANGED, overload = ROOM_OVERLOAD)
	public void mediaStatusChanded(TransferDataRequest request) {
		sendAllDevices(new TransferData(NOTIFY_MEDIA_STATUS_CHANGED, request.getClientId()));
	}

	@SocketMethod(flag = NOTIFY_READING_MODE_CHANGED, overload = ROOM_OVERLOAD)
	public void mediaReadingModeChanded(TransferDataRequest request) {
		sendAllDevices(new TransferData(NOTIFY_READING_MODE_CHANGED, request.getClientId()));
	}

	@SocketMethod(flag = REQUEST_MUSIC_READING_MODE, overload = ROOM_OVERLOAD)
	public void getMusicReadingMode(@SocketParameter Object[] data, TransferDataRequest request) {
		UUID deviceUuid = (UUID) data[1];
		sendDevice(deviceUuid, REQUEST_MUSIC_READING_MODE, data[0]);
	}

	@SocketMethod(flag = REQUEST_MUSIC_STATUS, overload = MOBILE_OVERLOAD)
	public void getMusicStatusMobile(@SocketParameter Object[] data, TransferDataRequest request) {
		// Sends a request to the room, and join the demander to the
		// request
		sendRoom((UUID) data[0], REQUEST_MUSIC_STATUS, new Object[] { request.getClientId(), data[1], data[2] });
	}

	@SocketMethod(flag = NEXT_SONG, overload = MOBILE_OVERLOAD)
	public void playNextSongMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {
		// Sends a request to the room, and join the demander to the
		// request
		sendRoom(roomUuid, NEXT_SONG);
	}

	@SocketMethod(flag = PREVIOUS_SONG, overload = MOBILE_OVERLOAD)
	public void playPreviousSongMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {
		sendRoom(roomUuid, PREVIOUS_SONG);
	}

	@SocketMethod(flag = SWITCH_PLAYING, overload = MOBILE_OVERLOAD)
	public void switchMediaPlayingMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {
		// Sends a request to the room, and join the demander to the
		// request
		sendRoom(roomUuid, SWITCH_PLAYING);
	}

	@SocketMethod(flag = REQUEST_MUSIC_READING_MODE, overload = MOBILE_OVERLOAD)
	public void getReadingModeMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {
		// Sends a request to the room, and join the demander to the
		// request
		sendRoom(request.getClientId(), REQUEST_MUSIC_READING_MODE, request.getClientId());
	}

	@SocketMethod(flag = SET_READING_MODE, overload = MOBILE_OVERLOAD)
	public void setReadingModeMobile(@SocketParameter Object[] data, TransferDataRequest request) {
		UUID roomID = (UUID) data[1];

		// Sends a request to the room, and join the demander to the
		// request
		sendRoom(roomID, SET_READING_MODE, data[0]);
	}

	@SocketMethod(flag = UPLOAD_NEW_MUSIC, overload = MOBILE_OVERLOAD)
	public void uploadMusic(@SocketParameter Object[] data, TransferDataRequest request) {
		final String name = (String) data[0];
		final byte[] bytes = (byte[]) data[1];

		final byte[] uncompressed = Compresser.uncompress(bytes);

		// Saves the song to the server's files
		try {
			File file = new File(HouseParametersSaver.MUSIC_DATA_PATH + "/" + name);

			// Writes down the file
			HouseUtils.writeToFile(uncompressed, file);

			// Reads it back again to extract the song and add it
			Mp3SongInfo newSong = new Mp3SongInfo(file.getPath());
			playlistManager.addSong(newSong);
			addSongToMatcher(newSong);

			// Forward the music to all rooms
			sendAllRooms(UPLOAD_NEW_MUSIC, data);
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (UnsupportedTagException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Mp3SongInfo musicForName(String name) {

		return songNameMatcher.match(name);
	}

}
