package houseServer.request;

import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import server.communication.ServerComManager;

public class SNotificationManager extends AbstractSManager {

	public SNotificationManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);

	}

}
