package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.ACTIVATE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.ALARM_RINGING;
import static houseDataCenter.commucationData.TransferFlags.CREATE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.DEACTIVATE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.REMOVE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_ROOM_ALARMS;
import static houseDataCenter.commucationData.TransferFlags.SNOOZE_ALARM;
import static houseDataCenter.commucationData.TransferFlags.UPDATE_ALARM;
import static houseServer.HouseServerManager.MOBILE_OVERLOAD;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmHandler;
import houseDataCenter.alarm.AlarmManager;
import houseDataCenter.alarm.HouseAlarm;
import houseDataCenter.house.Room;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import server.communication.ServerComManager;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;
import server.serialization.SerializationType;

public class SAlarmManager extends AbstractSManager implements AlarmHandler {

	private AlarmManager alarmManager;

	public SAlarmManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);

		loadAlarmManager();
	}

	@SuppressWarnings("unchecked")
	private void loadAlarmManager() {
		this.alarmManager = new AlarmManager((List<Alarm>) (List<?>) houseSaver.getAlarms(), this);
	}

	private List<Alarm> alarmsForRoom(UUID id) {

		final List<Alarm> alarms = alarmManager.getAlarms();
		List<Alarm> result = new ArrayList<Alarm>();

		for (Alarm a : alarms) {
			if (houseManager.getRoom(((HouseAlarm) a).getRoomUUID()).getCurrentID().equals(id)) {
				result.add(new Alarm(a));
			}
		}

		System.out.println("AlARMS " + result);

		return result;
	}

	@SocketMethod(flag = REQUEST_ROOM_ALARMS, overload = ROOM_OVERLOAD)
	public void getRoomAlarms(TransferDataRequest request) {
		request.respond(alarmsForRoom(request.getClientId()));
	}

	@SocketMethod(flag = CREATE_ALARM, overload = ROOM_OVERLOAD)
	public void createAlarm(@SocketParameter Alarm a, TransferDataRequest request) {

		Room r = houseManager.getRoom(request.getClientId());
		HouseAlarm alarm = new HouseAlarm(a, r.getUUID());

		alarmManager.addAlarm(alarm);
		houseSaver.addAlarm(alarm);
	}

	@SocketMethod(flag = ACTIVATE_ALARM, overload = ROOM_OVERLOAD)
	public void activateAlarm(@SocketParameter UUID alarmId, TransferDataRequest request) {
		setAlarmActive(alarmId, true);
	}

	@SocketMethod(flag = DEACTIVATE_ALARM, overload = ROOM_OVERLOAD)
	public void deactivateAlarm(@SocketParameter UUID alarmId, TransferDataRequest request) {
		setAlarmActive(alarmId, false);
	}

	private void setAlarmActive(UUID alarmId, boolean active) {
		HouseAlarm a = (HouseAlarm) alarmManager.findAlarmByID(alarmId);
		a.setActive(active);
		houseSaver.updateAlarm(a);
	}

	@SocketMethod(flag = UPDATE_ALARM, overload = ROOM_OVERLOAD)
	public void updateAlarm(@SocketParameter Alarm a, TransferDataRequest request) {

		Room r = houseManager.getRoom(request.getClientId());

		HouseAlarm alarm = new HouseAlarm(a, r.getUUID());
		alarmManager.updateAlarm(alarm);
		houseSaver.updateAlarm(alarm);
	}

	@SocketMethod(flag = UPDATE_ALARM, overload = ROOM_OVERLOAD)
	public void removeAlarm(@SocketParameter UUID alarmId, TransferDataRequest request) {
		alarmManager.removeAlarm(alarmId);
		houseSaver.removeAlarm((HouseAlarm) alarmManager.findAlarmByID(alarmId));
	}

	@SocketMethod(flag = SNOOZE_ALARM, overload = ROOM_OVERLOAD)
	public void snoozeAlarm(@SocketParameter UUID alarmId, TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		HouseAlarm h = new HouseAlarm(alarmManager.findAlarmByID(alarmId), r.getUUID());
		alarmManager.snooze(alarmId, h);
	}

	@SocketMethod(flag = REQUEST_ROOM_ALARMS, overload = MOBILE_OVERLOAD)
	public void getRoomAlarmsDevice(@SocketParameter UUID roomId, TransferDataRequest request) {
		List<Alarm> a = alarmsForRoom(roomId);
		request.respond(a);
	}

	@SocketMethod(flag = ACTIVATE_ALARM, overload = MOBILE_OVERLOAD)
	public void activateAlarmMobile(@SocketParameter UUID alarmId, TransferDataRequest request) {
		setAlarmActive(alarmId, true);
	}

	@SocketMethod(flag = DEACTIVATE_ALARM, overload = MOBILE_OVERLOAD)
	public void deactivateAlarmMobile(@SocketParameter UUID alarmId, TransferDataRequest request) {
		setAlarmActive(alarmId, false);
	}

	@SocketMethod(flag = REMOVE_ALARM, overload = MOBILE_OVERLOAD)
	public void removeAlarmMobile(@SocketParameter UUID alarmId, TransferDataRequest request) {
		removeAlarm(alarmId, request);
	}

	@Override
	public void handleAlarm(Alarm alarm) {

		HouseAlarm houseAlarm = (HouseAlarm) alarm;
		UUID id = houseManager.getRoom(houseAlarm.getRoomUUID()).getCurrentID();

		// Notifies the client associated to the room that the alarm rings
		sendRoom(id, ALARM_RINGING, new Alarm(houseAlarm));
		houseSaver.updateAlarm(houseAlarm);
	}
}
