package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.CREATION_CANCELED_PROTOCOL_TIMEOUT;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_ADD_EMAIL_USED;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_BORNDATE;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_CONFIRMATION;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_MAIL;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_NAME;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_PICTURES;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_PIN;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_MAIL_CONFIRMATION_CODE;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_RESIDENTS_INFO;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_RESIDENT_CREATION;
import static houseDataCenter.commucationData.TransferFlags.SET_READING_MODE;
import static houseDataCenter.commucationData.TransferFlags.SUCCESS_CREATION;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;

import java.security.InvalidParameterException;
import java.util.UUID;

import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.exceptions.InvalidBornDate;
import houseDataCenter.house.exceptions.InvalidEmailException;
import houseDataCenter.house.exceptions.InvalidNameException;
import houseDataCenter.house.exceptions.InvalidPINException;
import houseDataCenter.house.exceptions.PictureErrorException;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.residents.ResidentBuilder;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseParametersSaver;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import houseServer.security.ResidentAccepter;
import houseServer.security.TimeoutException;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;
import server.serialization.SerializationManager;
import server.serialization.SerializationType;

public class SResidentManager extends AbstractSManager {

	private ResidentAccepter accepter;

	public SResidentManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);

		this.accepter = new ResidentAccepter(houseManager);
	}

	@SocketMethod(flag = REQUEST_RESIDENTS_INFO, overload = ROOM_OVERLOAD)
	public void getResidentsInfo(TransferDataRequest request) {
		request.respond(REQUEST_RESIDENTS_INFO, houseManager.getResidentInfo());
	}

	@SocketMethod(flag = REQUEST_RESIDENT_CREATION, overload = ROOM_OVERLOAD)
	public void requestResidentCreation(@SocketParameter ResidentBuilder data, TransferDataRequest request) {
		try {
			try {
				int code = accepter.launchAcceptProtocol(data.build());
				System.out.println("Accept protocol launched");

				request.respond(code);
			} catch (IllegalAccessError e) {
				request.respond(FAILURE_ADD_EMAIL_USED);
			}

		} catch (InvalidNameException e) {
			request.respond(FAILURE_CREATION_INVALID_NAME);

		} catch (InvalidBornDate e) {
			request.respond(FAILURE_CREATION_INVALID_BORNDATE);

		} catch (InvalidEmailException e) {
			request.respond(FAILURE_CREATION_INVALID_MAIL);

		} catch (InvalidPINException | InvalidParameterException e) {
			request.respond(FAILURE_CREATION_INVALID_PIN);
		}
	}

	@SocketMethod(flag = REQUEST_MAIL_CONFIRMATION_CODE, overload = ROOM_OVERLOAD)
	public void tryConfirmResidentCreationWithMail(@SocketParameter Object data, TransferDataRequest request) {
		verifyMailConfirmationCode(data, request);
	}

	@SocketMethod(flag = REQUEST_MAIL_AND_RESIDENT_CONFIRMATION, overload = ROOM_OVERLOAD)
	public void tryConfirmResidentCreationWithMailAndResidentConfirmation(@SocketParameter Object data,
			TransferDataRequest request) {
		verifyMailAndResidentConfirmationCode(data, request);
	}

	/**
	 * @param data
	 * @param fromID
	 * @throws AccessRefusedError
	 */
	private void verifyMailConfirmationCode(Object data, TransferDataRequest request) throws AccessRefusedError {
		System.out.println("Verification received");
		Resident r = null;
		try {
			if ((r = accepter.verifyAcceptation((String) data, null)) != null) {
				// Confirmation success
				request.respond(SUCCESS_CREATION);
				houseManager.addResident(r);
				houseSaver.addResident(r);

				System.out.println("verification success");

			} else {

				// Confirmation failed.
				request.respond(FAILURE_CREATION_INVALID_CONFIRMATION);

				System.out.println("Verifiction failed");

			}
		} catch (TimeoutException e) {
			e.printStackTrace();
			request.respond(CREATION_CANCELED_PROTOCOL_TIMEOUT);
			System.out.println("Verifiction failed : timeout");

		}
	}

	/**
	 * @param data
	 * @param fromID
	 * @throws AccessRefusedError
	 */
	private void verifyMailAndResidentConfirmationCode(Object data, TransferDataRequest request)
			throws AccessRefusedError {
		try {

			String[] sData = (String[]) data;
			Resident r = null;
			if ((r = accepter.verifyAcceptation(sData[0], sData[1])) != null) {
				// Confirmation success
				request.respond(SUCCESS_CREATION);
				houseManager.addResident(r);
				houseSaver.addResident(r);

				System.out.println("Verifiction success with mail and resident");

			} else {

				// Confirmation failed.
				request.respond(FAILURE_CREATION_INVALID_CONFIRMATION);

				System.out.println("Verifiction failed with mail and resident");

			}
		} catch (TimeoutException e) {

			e.printStackTrace();
			request.respond(CREATION_CANCELED_PROTOCOL_TIMEOUT);

			System.out.println("Verifiction success with mail and resident");

		}
	}
}
