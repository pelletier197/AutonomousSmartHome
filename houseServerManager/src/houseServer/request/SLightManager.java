package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.ws.Action;

import org.apache.commons.math3.analysis.function.Abs;

import static houseServer.HouseServerManager.MOBILE_OVERLOAD;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;

import algorithm.StringSequenceMatcher;
import algorithm.WordSequenceMatcher;
import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.ColorisableLight;
import embedded.controllers.light.DimmableLight;
import embedded.controllers.light.LightEvent;
import embedded.light.HouseLightManager;
import embedded.light.hueLight.HueLightManager;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.exceptions.AccessRefusedError;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseParametersSaver;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import houseServer.devices.intelligentDevice.IntelligentDeviceRequestManager;
import houseServer.devices.room.HouseRequestManager;
import server.serialization.SerializationType;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;
import server.serialization.SerializationManager;

public class SLightManager extends AbstractSManager {

	private HouseLightManager lightManager;

	private StringSequenceMatcher<AbstractLight> lightNamerMatcher;

	public SLightManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {

		super(deviceManager, houseManager, serverManager, saver);

		loadLightManager();
		this.lightNamerMatcher = new WordSequenceMatcher<>();
	}

	private void loadLightManager() {

		Collection<LightEvent> events = houseSaver.getLightEvents();
		this.lightManager = new HouseLightManager(houseManager, events, houseSaver);

		// Adds the supported light API manager.
		HueLightManager hueMan = new HueLightManager();
		hueMan.load();
		this.lightManager.addManager(hueMan);

		// Attempts reassigning lights
		houseManager.reassingLights(hueMan.getLights());
	}

	@SocketMethod(flag = REQUEST_ROOM_LIGHTS, overload = ROOM_OVERLOAD)
	public void getRoomLights(TransferDataRequest request) {
		request.respond(houseManager.getRoom(request.getClientId()).getLightsData());
	}

	@SocketMethod(flag = REQUEST_AVAILABLE_LIGHTS, overload = ROOM_OVERLOAD)
	public void getAvailableLights(TransferDataRequest request) {
		request.respond(lightManager.getUnasignedLightsData());
	}

	@SocketMethod(flag = LIGHT_TEST, overload = ROOM_OVERLOAD)
	public void lightTest(@SocketParameter String lightId, TransferDataRequest request) {

		AbstractLight light = lightManager.getLightForID((String) lightId);

		new Thread(() -> {
			if (light.isOpen()) {
				light.close();
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				light.open();
			} else {
				light.open();
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				light.close();
			}
		}).start();
	}

	@SocketMethod(flag = UPDATE_ROOM_LIGHTS, overload = ROOM_OVERLOAD)
	public void updateRoomLights(@SocketParameter Object[] obj, TransferDataRequest request) {

		Room aimed = houseManager.getRoom(request.getClientId());
		String PIN = (String) obj[0];

		if (houseManager.accessHouseParam(PIN) != null) {
			List<AbstractLight> add = lightManager.getLightsForIDs((String[]) obj[1]);
			List<AbstractLight> remove = lightManager.getLightsForIDs((String[]) obj[2]);

			aimed.addAllLights(add);
			aimed.removeAllLights(remove);
			request.respond(ACCESS_GRANTED, null);

			for (AbstractLight light : add) {
				// Saves the changes for the lights
				houseSaver.updateLightToRoom(light, aimed);
			}

			for (AbstractLight light : remove) {
				// No room anymore
				houseSaver.updateLightToRoom(light, null);
			}
		} else {
			request.respond(ACCESS_REFUSED, null);
		}
	}

	@SocketMethod(flag = OPEN_ALL_LIGHTS, overload = ROOM_OVERLOAD)
	public void openAllLights(TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.openAllLights();
		notifyDevicesMultipleLightsStateChanged(r);
	}

	@SocketMethod(flag = CLOSE_ALL_LIGHTS, overload = ROOM_OVERLOAD)
	public void closeAllLights(TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.closeAllLights();
		notifyDevicesMultipleLightsStateChanged(r);
	}

	@SocketMethod(flag = RESET_LIGHT_COLOR, overload = ROOM_OVERLOAD)
	public void resetLightColor(TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.resetAllLightsColor();
		notifyDevicesMultipleLightsStateChanged(r);
	}

	@SocketMethod(flag = SET_LIGHTS_COLOR, overload = ROOM_OVERLOAD)
	public void setAllLightsColor(@SocketParameter int color, TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.setAllLightsColor(color);
		notifyDevicesMultipleLightsStateChanged(r);
	}

	@SocketMethod(flag = SET_LIGHTS_LUMINOSITY, overload = ROOM_OVERLOAD)
	public void setAllLightsLuminosity(@SocketParameter double lumnosity, TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.setAllLightsLuminosity(lumnosity);
		notifyDevicesMultipleLightsStateChanged(r);
	}

	@SocketMethod(flag = SET_LIGHT_COLOR, overload = ROOM_OVERLOAD)
	public void setLightColor(@SocketParameter Object[] data, TransferDataRequest request) {
		String id = (String) data[0];
		int color = (int) data[1];

		AbstractLight light = lightManager.getLightForID(id);
		if (light instanceof ColorisableLight) {
			((ColorisableLight) light).setColor(color);
			notifyDevicesLightStateChanged(light.getLightID());
		}
	}

	@SocketMethod(flag = OPEN_LIGHT, overload = ROOM_OVERLOAD)
	public void openLight(@SocketParameter String lightId, TransferDataRequest request) {

		AbstractLight light = lightManager.getLightForID(lightId);
		light.open();
		notifyDevicesLightStateChanged(light.getLightID());
	}

	@SocketMethod(flag = CLOSE_LIGHT, overload = ROOM_OVERLOAD)
	public void closeLight(@SocketParameter String lightId, TransferDataRequest request) {
		AbstractLight light = lightManager.getLightForID(lightId);
		light.close();
		notifyDevicesLightStateChanged(light.getLightID());
	}

	@SocketMethod(flag = SET_LIGHT_NAME, overload = ROOM_OVERLOAD)
	public void setLightName(@SocketParameter Object[] data, TransferDataRequest request) {
		String id = (String) data[0];
		String name = (String) data[1];
		AbstractLight light = lightManager.getLightForID(id);
		light.setName(name);
		houseSaver.updateLight(light);
		updateMatcher();
		notifyDevicesLightStateChanged(light.getLightID());
	}

	@SocketMethod(flag = SET_LIGHT_LUMINOSITY, overload = ROOM_OVERLOAD)
	public void setLightLuminosity(@SocketParameter Object[] data, TransferDataRequest request) {
		String id = (String) data[0];
		double value = (double) data[1];

		AbstractLight light = lightManager.getLightForID(id);
		if (light instanceof DimmableLight) {
			((DimmableLight) light).setBrightness(value);
			notifyDevicesLightStateChanged(light.getLightID());
		}
	}

	@SocketMethod(flag = REQUEST_ROOM_LIGHTS, overload = MOBILE_OVERLOAD)
	public void getRoomLightsMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {
		request.respond(REQUEST_ROOM_LIGHTS, houseManager.getRoom(roomUuid).getLightsData());
	}

	@SocketMethod(flag = GET_LIGHT_STATUS, overload = MOBILE_OVERLOAD)
	public void getRoomLightsStatusMobile(@SocketParameter String lightUuid, TransferDataRequest request) {
		LightData stat = new LightData(lightManager.getLightForID(lightUuid));
		request.respond(GET_LIGHT_STATUS, stat);
	}

	@SocketMethod(flag = RESET_LIGHT_COLOR, overload = MOBILE_OVERLOAD)
	public void resetLightColorMobile(@SocketParameter String lightUuid, TransferDataRequest request) {
		AbstractLight light = lightManager.getLightForID(lightUuid);

		// Reset light's color
		if (light instanceof ColorisableLight) {
			((ColorisableLight) light).resetColor();
			notifyRoomAndDevicesLightStateChanged(light);
		}
	}

	@SocketMethod(flag = OPEN_LIGHT, overload = MOBILE_OVERLOAD)
	public void openLightMobile(@SocketParameter String lightUuid, TransferDataRequest request) {
		AbstractLight light = lightManager.getLightForID(lightUuid);
		light.open();
		notifyRoomAndDevicesLightStateChanged(light);
	}

	@SocketMethod(flag = CLOSE_LIGHT, overload = MOBILE_OVERLOAD)
	public void closeLightMobile(@SocketParameter String lightUuid, TransferDataRequest request) {
		AbstractLight light = lightManager.getLightForID(lightUuid);
		light.close();
		notifyRoomAndDevicesLightStateChanged(light);
	}

	@SocketMethod(flag = SET_LIGHT_LUMINOSITY, overload = MOBILE_OVERLOAD)
	public void setLightLuminosityMobile(@SocketParameter Object[] data, TransferDataRequest request) {
		String id = (String) data[0];
		double value = (double) data[1];

		System.out.println(" Info : brightness" + value);

		AbstractLight light = lightManager.getLightForID(id);

		// Changes luminosity
		if (light instanceof DimmableLight) {
			((DimmableLight) light).setBrightness(value);
			notifyRoomAndDevicesLightStateChanged(light);
		}
	}

	@SocketMethod(flag = SET_LIGHTS_COLOR, overload = MOBILE_OVERLOAD)
	public void setLightColorMobile(@SocketParameter Object[] data, TransferDataRequest request) {
		String id = (String) data[0];
		int value = (int) data[1];

		AbstractLight light = lightManager.getLightForID(id);

		// Changes luminosity
		if (light instanceof ColorisableLight) {
			((ColorisableLight) light).setColor(value);
			notifyRoomAndDevicesLightStateChanged(light);
		}
	}

	private void notifyRoomAndDevicesLightStateChanged(AbstractLight light) {
		notifyRoomLightStateChanged(light);
		notifyDevicesLightStateChanged(light.getLightID());
	}

	private void notifyDevicesLightStateChanged(String lightID) {
		sendAllDevices(LIGHT_STATE_CHANGED, lightID);
	}

	private void notifyDevicesMultipleLightsStateChanged(Room r) {
		TransferData data = new TransferData(MULTIPLE_LIGHTS_STATE_CHANGED, r.getCurrentID());
		sendAllDevices(data);
	}

	private void notifyRoomLightStateChanged(AbstractLight light) {
		LightData li = new LightData(light);
		Room r = getRoomFromLight(light);
		if (light != null) {
			sendRoom(r.getCurrentID(), LIGHT_STATE_CHANGED, li);
		}
	}

	private void updateMatcher() {
		lightNamerMatcher.removeAllKeys();

		for (AbstractLight light : lightManager.getLights()) {
			lightNamerMatcher.addKey(light.getName(), light);
		}
	}

	private Room getRoomFromLight(AbstractLight light) {
		return houseManager.getRoomByLight(light);
	}

	public AbstractLight lightForName(String name) {
		if (lightNamerMatcher.getKeyCont() != lightManager.getLightCount()) {
			updateMatcher();
		}
		return lightNamerMatcher.match(name);
	}
}
