package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.ACCESS_GRANTED;
import static houseDataCenter.commucationData.TransferFlags.ACCESS_REFUSED;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_ROOMS;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_ROOM_NAME;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_SERVER_LANGUAGE;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_TEMP;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_TEMP_DOWN;
import static houseDataCenter.commucationData.TransferFlags.REQUEST_TEMP_UP;
import static houseDataCenter.commucationData.TransferFlags.SET_ALL_ROOMS_LANGUAGE;
import static houseDataCenter.commucationData.TransferFlags.SET_LANGUAGE;
import static houseDataCenter.commucationData.TransferFlags.SET_ROOM_NAME;
import static houseDataCenter.commucationData.TransferFlags.SET_ROOM_TEMP_DISPLAY;
import static houseDataCenter.commucationData.TransferFlags.SET_ROOM_VOLUME;
import static houseDataCenter.commucationData.TransferFlags.SET_TEMP;
import static houseDataCenter.commucationData.TransferFlags.SET_TEMP_DISPLAY;
import static houseDataCenter.commucationData.TransferFlags.VOLUME_DOWN;
import static houseDataCenter.commucationData.TransferFlags.VOLUME_UP;
import static houseServer.HouseServerManager.MOBILE_OVERLOAD;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;

import java.util.UUID;

import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.TempDisplay;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import server.communication.ServerComManager;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;

public class SRoomManager extends AbstractSManager {

	public SRoomManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);
	}

	@SocketMethod(flag = REQUEST_TEMP_UP, overload = ROOM_OVERLOAD)
	public void requestTempUp(TransferDataRequest request) {
		Room room = houseManager.getRoom(request.getClientId());

		if (room != null) {
			double before = room.getTemperature();

			room.tempUp();

			if (before != room.getTemperature()) {
				houseSaver.updateRoom(room);
				sendAllDevices(TransferFlags.SET_TEMP, new Object[] { room.getCurrentID(), room.getTemperature() });
			}

			request.respond(SET_TEMP, room.getTemperature());
		}
	}

	@SocketMethod(flag = REQUEST_TEMP_DOWN, overload = ROOM_OVERLOAD)
	public void requestTempDown(TransferDataRequest request) {
		Room room = houseManager.getRoom(request.getClientId());

		if (room != null) {
			double before = room.getTemperature();

			room.tempUp();

			if (before != room.getTemperature()) {
				houseSaver.updateRoom(room);
				sendAllDevices(TransferFlags.SET_TEMP, new Object[] { room.getCurrentID(), room.getTemperature() });
			}

			request.respond(SET_TEMP, room.getTemperature());
		}
	}

	@SocketMethod(flag = REQUEST_TEMP, overload = ROOM_OVERLOAD)
	public void getTemp(TransferDataRequest request) {
		Room room = houseManager.getRoom(request.getClientId());

		if (room != null) {
			request.respond(SET_TEMP, room.getTemperature());
		}
	}

	@SocketMethod(flag = REQUEST_ROOM_NAME, overload = ROOM_OVERLOAD)
	public void getRoomName(TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		request.respond(REQUEST_ROOM_NAME, r.getName());
	}

	@SocketMethod(flag = SET_ROOM_NAME, overload = ROOM_OVERLOAD)
	public void setRoomName(@SocketParameter String name, TransferDataRequest request) {
		Room r = houseManager.getRoom(request.getClientId());
		r.setName(name);
		houseSaver.updateRoom(r);
	}

	@SocketMethod(flag = SET_ALL_ROOMS_LANGUAGE, overload = ROOM_OVERLOAD)
	public void setAllRoomsLanguage(@SocketParameter Object[] data, TransferDataRequest request) {

		String pin = (String) data[0];
		Language l = (Language) data[1];

		if (houseManager.isAccessGranted(pin)) {
			request.respond(ACCESS_GRANTED);
			sendAllRooms(SET_LANGUAGE, l);
		} else {
			request.respond(ACCESS_REFUSED);
		}
	}

	@SocketMethod(flag = REQUEST_SERVER_LANGUAGE, overload = ROOM_OVERLOAD)
	public void getServerLanguage(TransferDataRequest request) {
		request.respond(REQUEST_SERVER_LANGUAGE, houseManager.getLanguage());
	}

	@SocketMethod(flag = SET_ROOM_TEMP_DISPLAY, overload = ROOM_OVERLOAD)
	public void setAllRoomsTempDisplay(@SocketParameter Object[] data, TransferDataRequest request) {

		String pin = (String) data[0];
		TempDisplay t = (TempDisplay) data[1];

		if (houseManager.isAccessGranted(pin)) {

			houseManager.setAllRoomsTempDisplay(t);
			houseManager.saveAllRooms();

			request.respond(ACCESS_GRANTED);
			sendAllRooms(SET_TEMP_DISPLAY, t);
		} else {
			request.respond(ACCESS_REFUSED);
		}
	}

	@SocketMethod(flag = SET_ROOM_TEMP_DISPLAY, overload = ROOM_OVERLOAD)
	public void setAllRoomsTempDisplay(@SocketParameter TempDisplay display, TransferDataRequest request) {

		Room r = houseManager.getRoom(request.getClientId());
		r.setDisplayTemp(display);
		houseSaver.updateRoom(r);

	}

	@SocketMethod(flag = SET_ROOM_VOLUME, overload = ROOM_OVERLOAD)
	public void setRoomVolume(@SocketParameter double volume, TransferDataRequest request) {

		Room r = houseManager.getRoom(request.getClientId());
		r.setControlVolume(volume);
		houseSaver.updateRoom(r);
		sendAllDevices(SET_ROOM_VOLUME, new Object[] { r.getCurrentID(), r.getControlVolume() });
	}

	@SocketMethod(flag = REQUEST_ROOMS, overload = MOBILE_OVERLOAD)
	public void getRooms(TransferDataRequest request) {
		request.respond(REQUEST_ROOMS, houseManager.getActiveRoomsData());
	}

	@SocketMethod(flag = REQUEST_TEMP_UP, overload = MOBILE_OVERLOAD)
	public void requestTempUpMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {

		Room room = houseManager.getRoom(roomUuid);

		double before = room.getTemperature();

		room.tempUp();

		if (before != room.getTemperature()) {
			houseSaver.updateRoom(room);

			// Notifies the device and the room of temp change
			sendRoom(roomUuid, SET_TEMP, room.getTemperature());
			sendAllDevices(SET_TEMP, new Object[] { room.getCurrentID(), room.getTemperature() });
		}
	}

	@SocketMethod(flag = REQUEST_TEMP_DOWN, overload = MOBILE_OVERLOAD)
	public void requestTempDownMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {

		Room room = houseManager.getRoom(roomUuid);

		double before = room.getTemperature();

		room.tempDown();

		if (before != room.getTemperature()) {
			houseSaver.updateRoom(room);

			// Notifies the device and the room of temp change
			sendRoom(roomUuid, SET_TEMP, room.getTemperature());
			sendAllDevices(SET_TEMP, new Object[] { room.getCurrentID(), room.getTemperature() });
		}
	}

	@SocketMethod(flag = SET_ROOM_VOLUME, overload = MOBILE_OVERLOAD)
	public void setRoomVolumeMobile(@SocketParameter UUID roomUuid, TransferDataRequest request) {

		Room room = houseManager.getRoom(roomUuid);

		double before = room.getTemperature();

		room.tempDown();

		if (before != room.getTemperature()) {
			houseSaver.updateRoom(room);

			// Notifies the device and the room of temp change
			sendRoom(roomUuid, SET_TEMP, room.getTemperature());
			sendAllDevices(SET_TEMP, new Object[] { room.getCurrentID(), room.getTemperature() });
		}
	}

	@SocketMethod(flag = SET_ROOM_VOLUME, overload = MOBILE_OVERLOAD)
	public void requestTempDownMobile(@SocketParameter Object[] data, TransferDataRequest request) {

		UUID uuid = (UUID) data[0];
		double volume = (double) data[1];

		Room r = houseManager.getRoom(uuid);

		double volBefore = r.getControlVolume();
		r.setControlVolume((double) volume);

		if (volBefore != r.getControlVolume()) {
			houseSaver.updateRoom(r);

			// Notifies all the devices and the room
			sendRoom(uuid, SET_ROOM_VOLUME, r.getControlVolume());
			sendAllDevices(SET_ROOM_VOLUME, new Object[] { r.getCurrentID(), r.getControlVolume() });
		}
	}

	@SocketMethod(flag = VOLUME_UP, overload = MOBILE_OVERLOAD)
	public void requestVolumeUpMobile(@SocketParameter UUID uuid, TransferDataRequest request) {

		Room r = houseManager.getRoom(uuid);

		double volBefore = r.getControlVolume();
		r.volumeUp();

		if (volBefore != r.getControlVolume()) {
			houseSaver.updateRoom(r);

			// Notifies all the devices and the room
			sendRoom(uuid, SET_ROOM_VOLUME, r.getControlVolume());
			sendAllDevices(SET_ROOM_VOLUME, new Object[] { r.getCurrentID(), r.getControlVolume() });
		}
	}

	@SocketMethod(flag = VOLUME_DOWN, overload = MOBILE_OVERLOAD)
	public void requestVolumeDownMobile(@SocketParameter UUID uuid, TransferDataRequest request) {

		Room r = houseManager.getRoom(uuid);

		double volBefore = r.getControlVolume();
		r.volumeDown();

		if (volBefore != r.getControlVolume()) {
			houseSaver.updateRoom(r);

			// Notifies all the devices and the room
			sendRoom(uuid, SET_ROOM_VOLUME, r.getControlVolume());
			sendAllDevices(SET_ROOM_VOLUME, new Object[] { r.getCurrentID(), r.getControlVolume() });
		}
	}

}
