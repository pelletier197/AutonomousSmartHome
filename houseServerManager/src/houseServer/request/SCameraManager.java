package houseServer.request;

import java.util.UUID;

import embedded.camera.CameraStreamManager;
import embedded.camera.CameraManager;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import server.communication.Request;
import server.communication.ServerComManager;
import server.serialization.SerializationManager;

public class SCameraManager extends AbstractSManager {

	private CameraManager cameraManager;
	private CameraStreamManager streamManager;
	private IntelligentDeviceManager deviceManager;

	public SCameraManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);

		this.cameraManager = new CameraManager();

		// TODO - UNCOMMENT
		// this.cameraManager.startRecording();

	}

}
