package houseServer.request;

import static houseDataCenter.commucationData.TransferFlags.REQUEST_SERVER_EXTERNAL_IP;
import static houseServer.HouseServerManager.MOBILE_OVERLOAD;

import houseServer.HouseManager;
import houseServer.dataSaving.HouseSaver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import server.communication.ServerComManager;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;

/**
 * This class handles every operation that rely on security of the house
 * parameters.
 * 
 * @author sunny
 *
 */
public class SSystemManager extends AbstractSManager{



	public SSystemManager(ServerComManager serverManager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		super(deviceManager, houseManager, serverManager, saver);
	}

	@SocketMethod(flag = REQUEST_SERVER_EXTERNAL_IP, overload = MOBILE_OVERLOAD)
	public void getServerExternalIpMobile(TransferDataRequest request) {

		request.respond(REQUEST_SERVER_EXTERNAL_IP, serverManager.getExternalIp());
	}

}
