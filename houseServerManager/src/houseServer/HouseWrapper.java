package houseServer;

import houseServer.request.SAlarmManager;
import houseServer.request.SCameraManager;
import houseServer.request.SLightManager;
import houseServer.request.SMusicManager;
import houseServer.request.SNotificationManager;
import houseServer.request.SResidentManager;
import houseServer.request.SRoomManager;
import houseServer.request.SSystemManager;

public class HouseWrapper {

	private SAlarmManager alarmManager;
	private SCameraManager camManager;
	private SLightManager lightManager;
	private SMusicManager musicManager;
	private SSystemManager secuManager;
	private SResidentManager residentManager;
	private SRoomManager roomManager;
	private SNotificationManager notifManager;
	
	private HouseManager houseManager;

	public HouseWrapper(SAlarmManager alarmManager, SCameraManager camManager, SLightManager lightManager,
			SMusicManager musicManager, SSystemManager secuManager, SResidentManager residentManager,
			SRoomManager roomManager, SNotificationManager notif, HouseManager houseManager) {
		this.alarmManager = alarmManager;
		this.camManager = camManager;
		this.lightManager = lightManager;
		this.musicManager = musicManager;
		this.secuManager = secuManager;
		this.residentManager = residentManager;
		this.roomManager = roomManager;
		this.notifManager = notif;
		this.houseManager = houseManager;
	}

	public SResidentManager getResidentManager() {
		return residentManager;
	}

	public SRoomManager getRoomManager() {
		return roomManager;
	}

	public SAlarmManager getAlarmManager() {
		return alarmManager;
	}

	public SCameraManager getCamManager() {
		return camManager;
	}

	public SLightManager getLightManager() {
		return lightManager;
	}

	public SMusicManager getMusicManager() {
		return musicManager;
	}

	public SSystemManager getSecuManager() {
		return secuManager;
	}
	
	public HouseManager getHouseManager(){
		return houseManager;
	}



}
