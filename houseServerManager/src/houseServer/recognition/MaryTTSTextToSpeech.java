package houseServer.recognition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.sound.sampled.AudioInputStream;

import houseDataCenter.applicationRessourceBundle.Language;
import marytts.LocalMaryInterface;
import marytts.MaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.util.data.audio.AudioPlayer;
import marytts.util.data.audio.MaryAudioUtils;

public class MaryTTSTextToSpeech implements TextToSpeechMaker {

	private static final String VOICE = "ttsVoice";

	private MaryInterface marytts;

	public MaryTTSTextToSpeech(Language lang) {

		try {
			marytts = new LocalMaryInterface();
			System.out.println(marytts.getAvailableVoices());
			System.out.println(marytts.getAvailableLocales());
			marytts.setLocale(lang.getLocale());

			// marytts.setVoice(lang.getProperty(VOICE));
		} catch (MaryConfigurationException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void playSound(String text) {
		try {
			AudioPlayer player = new AudioPlayer();

			player.setAudio(marytts.generateAudio(text));
			player.start();

		} catch (SynthesisException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Converts the text into WAV file format and returns the path to the file
	 * generated.
	 */
	@Override
	public String toSpeech(String text) {
		try {
			AudioInputStream stream = marytts.generateAudio(text);

			MaryAudioUtils.writeWavFile(MaryAudioUtils.getSamplesAsDoubleArray(stream), "temp.wav", stream.getFormat());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SynthesisException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "temp.wav";
	}

	@Override
	public List<String> getVoices() {
		return new ArrayList<String>(marytts.getAvailableVoices());
	}

	@Override
	public void setVoice(String voice) {
		marytts.setVoice(voice);
	}

	@Override
	public void close() {

	}

	public static void main(String[] args) {
		MaryTTSTextToSpeech tts = new MaryTTSTextToSpeech(Language.ENGLISH_US);

		tts.playSound("Mon nom est sophia");
	}
}
