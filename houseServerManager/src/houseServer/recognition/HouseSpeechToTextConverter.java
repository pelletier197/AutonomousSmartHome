package houseServer.recognition;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import houseDataCenter.applicationRessourceBundle.Language;

/**
 * This class uses Sphinx speech recognizer to match byte input from a
 * microphone in every room to a grammar specific to every language.
 * 
 * It uses a different thread per client added, which allows to identify from
 * which client the recognition was performed.
 * 
 * @author sunny
 *
 */
public class HouseSpeechToTextConverter {

	private final int PIPE_SIZE = 64_000;

	/**
	 * The map of clients associated to their recognizer values
	 */
	private Map<UUID, ClientDataWrapper> clients;

	/**
	 * The speech input listener that is notified when a given client sends a
	 * speech recognizes
	 */
	private SpeechInputListener listener;

	/**
	 * A boolean telling the microphone input to stop.
	 */
	public boolean stopped;

	/**
	 * The current language
	 */
	private Language lang;

	private Configuration configs;

	/**
	 * Creates a microphone speech input that will recognize words and speech in
	 * the given language, and will notify the given {@link SpeechInputListener}
	 * when speech will be detected.
	 * 
	 * @param lang
	 *            The language in which recognition must be made.
	 * @param listener
	 *            The listener that will be notified of new speech reception.
	 */
	public HouseSpeechToTextConverter(Language lang, SpeechInputListener listener) {

		this.listener = listener;

		this.lang = lang;

		clients = new HashMap<>();
		stopped = true;
	}

	/**
	 * Start the recognition and the analysis of the input for all the clients.
	 */
	public void startRecognition() {
		if (stopped) {
			stopped = false;
			for (Map.Entry<UUID, ClientDataWrapper> entry : clients.entrySet()) {
				startRecognition(entry.getKey(), entry.getValue());
			}
		}
	}

	/**
	 * Starts recognition of the given client by starting its thread of
	 * recognition.
	 * 
	 * @param id
	 *            The id of the client
	 * @param wrapper
	 *            The client data wrapper
	 */
	private void startRecognition(final UUID id, final ClientDataWrapper wrapper) {

		StreamSpeechRecognizer recognizer = wrapper.recognizer;
		System.out.println("Begin start recognition");
		recognizer.startRecognition(wrapper.input);
		System.out.println("End start recognition");
		SpeechResult result = null;
		String hypothesis = null;

		wrapper.thread.setPriority(Thread.NORM_PRIORITY);

		// Test worked, so client is added
		synchronized (clients) {
			clients.put(id, wrapper);
		}

		try {
			while (!wrapper.clientStopped && !stopped) {
				System.out.println("Begin speech result");
				result = recognizer.getResult();
				hypothesis = result.getHypothesis();
				System.out.println("End speech result");
				if (hypothesis != null && !hypothesis.equals("<unk>")) {
					listener.receiveSpeech(hypothesis, id);
				}
				// Waits to let the buffer fill again

				Thread.sleep(500);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds the client to the reconnection engine, and starts its thread if the
	 * recognition is currently activated.
	 * 
	 * @param id
	 *            The id of the client to add
	 */
	public void addClient(UUID id) {
		ClientDataWrapper wrapper = new ClientDataWrapper();

		System.out.println("Add client : current count  = " + clients.size());

		wrapper.thread = new Thread(() -> {
			if (clients.get(id) == null) {
				try {

					wrapper.input = new PipedInputStream(PIPE_SIZE);
					wrapper.output = new PipedOutputStream(wrapper.input);

					wrapper.recognizer = new StreamSpeechRecognizer(createConfigs());

					if (!stopped) {
						startRecognition(id, wrapper);
					}

				} catch (IOException cannotHappen) {
					cannotHappen.printStackTrace();
				}
			}
		});

		wrapper.thread.setPriority(Thread.MAX_PRIORITY);

		wrapper.thread.start();
	}

	private Configuration createConfigs() {
		if (configs == null) {
			// Init the configuration from the language property defined in the
			// file
			configs = new Configuration();
			configs.setAcousticModelPath(lang.getProperty("acousticModel"));
			configs.setDictionaryPath(lang.getProperty("dictionary"));
			configs.setLanguageModelPath(lang.getProperty("langModel"));

			String grammar = lang.getProperty("grammar");

			if (grammar != null) {
				configs.setGrammarPath(grammar);
				configs.setGrammarName(lang.getProperty("grammarName"));
				configs.setUseGrammar(true);
			}
		}

		return configs;
	}

	/**
	 * Safely removes a client from the engine. This will close its recognizer
	 * and end its thread.
	 * 
	 * @param id
	 *            The id of the client to remove
	 */
	public void removeClient(UUID id) {

		ClientDataWrapper wrapper = null;

		synchronized (clients) {
			wrapper = clients.remove(id);
		}

		wrapper.clientStopped = true;

		wrapper.recognizer.stopRecognition();

		try {
			wrapper.output.flush();
			wrapper.output.close();

			wrapper.input.close();
		} catch (Exception cannotHappen) {
			cannotHappen.printStackTrace();
		}
	}

	/**
	 * Must be called when receiving new data for the class to analysis. Those
	 * data are associated to an ID, which will allow to identify which device
	 * has sent those data when the recognizer will read the data.
	 * 
	 * @param data
	 *            The input data to be analyzed
	 * @param ID
	 *            The ID of the data sender.
	 */
	public synchronized void newData(byte[] data, UUID ID) {
		try {
			ClientDataWrapper wrapper = clients.get(ID);

			if (wrapper != null) {
				System.out.println("Writing data to recongizer");
				wrapper.output.write(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Stops the recognition. This is used to completely stop analyzing data
	 * from input.
	 */
	public void stopRecognition() {
		stopped = true;
	}

	private class ClientDataWrapper {

		/**
		 * Clients thread. Forcedly closed.
		 */
		private Thread thread;

		/**
		 * The stream speech recognizer from Sphinx
		 */
		public StreamSpeechRecognizer recognizer;

		/**
		 * Piped input stream, so data written in input are read at the outside.
		 */
		public PipedInputStream input;

		/**
		 * The output, where input data are read when ready to be read.
		 */
		public PipedOutputStream output;

		public boolean clientStopped;

	}
}
