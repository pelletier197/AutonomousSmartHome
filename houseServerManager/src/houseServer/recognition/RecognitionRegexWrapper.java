package houseServer.recognition;

import java.util.ResourceBundle;

import houseDataCenter.applicationRessourceBundle.StringPropertiesWrapper;

public class RecognitionRegexWrapper {

	private static StringPropertiesWrapper resources;

	public static void setResourceBundle(ResourceBundle rb) {
		if (rb == null)
			throw new NullPointerException("rb cannot be null");
		resources = new StringPropertiesWrapper();
		resources.setResourceBundle(rb);
	}

	public static String getRegex(String key) {
		if (resources == null)
			throw new NullPointerException("rb not initialized");

		return resources.getString(key);
	}

}
