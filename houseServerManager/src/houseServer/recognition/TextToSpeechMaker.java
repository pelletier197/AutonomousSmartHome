package houseServer.recognition;

import java.util.List;

public interface TextToSpeechMaker {

	public String toSpeech(String text);

	public void playSound(String text);

	public List<String> getVoices();

	public void setVoice(String voice);

	public void close();

}
