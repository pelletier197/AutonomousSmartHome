package houseServer.recognition.commands;

import java.util.UUID;

import algorithm.language.LangFactory;
import algorithm.language.date.StringDateFormatter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseServer.HouseWrapper;
import houseServer.recognition.AnswerWrapper;
import houseServer.recognition.DataInputCommand;
import houseServer.recognition.TTSServerUtils;

/**
 * 
 * @author sunny
 *
 */
public class InformationCommands extends DataInputCommand {

	private static final String CURRENT_DAY = "currentDay";
	private static final String CURRENT_TIME = "currentTime";

	private StringDateFormatter dateFormatter;

	public InformationCommands() {
		super(CURRENT_DAY, CURRENT_TIME);

	}

	@Override
	public boolean matchAndExecute(String input, UUID clientID, byte comType) {

		String match = super.match(input);

		if (match != null) {
			if (match.equals(CURRENT_DAY)) {
				// Gives today's date
				String currentDay = dateFormatter.formatDay();
				String speak = String.format(AnswerWrapper.getAnswer(CURRENT_DAY), currentDay);

				System.out.println("Speak - " + speak);
				// Packages and send the file to say
				com.send(TTSServerUtils.createTTSPackage(speak), comType, clientID);

			} else if (match.equals(CURRENT_TIME)) {
				// Threats time required
				String currentTime = dateFormatter.formatTime();
				String speak = String.format(AnswerWrapper.getAnswer(CURRENT_TIME), currentTime);
				System.out.println("Speak - " + speak);
				// Packages and send the file to say
				com.send(TTSServerUtils.createTTSPackage(speak), comType, clientID);

			}
		}

		return false;
	}

	@Override
	public void setHouseWrapper(HouseWrapper wrapper) {
		super.setHouseWrapper(wrapper);

		dateFormatter = LangFactory.dateFormatterFor(wrapper.getHouseManager().getLanguage());

	}

}
