package houseServer.recognition.commands;

import java.util.UUID;

import algorithm.language.LangFactory;
import algorithm.language.parser.StringNumberParser;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.TempDisplay;
import houseDataCenter.math.unitConversion.CTempConverter;
import houseDataCenter.math.unitConversion.Unit;
import houseServer.HouseManager;
import houseServer.HouseWrapper;
import houseServer.recognition.DataInputCommand;
import server.communication.Request;
import server.communication.TransferData;

public class TemperatureCommands extends DataInputCommand {

	private static final String SET_SPECIFIC_TEMPERATURE = "specificTemperaure";
	private static final String INCREASE_TEMPERATURE = "increaseTemperature";
	private static final String DECREASE_TEMPEATURE = "decreaseTemperature";

	private StringNumberParser parser;

	public TemperatureCommands() {
		super(SET_SPECIFIC_TEMPERATURE, INCREASE_TEMPERATURE, DECREASE_TEMPEATURE);

	}

	@Override
	public void setHouseWrapper(HouseWrapper wrapper) {
		super.setHouseWrapper(wrapper);

		// Creates a parser for house's language
		this.parser = LangFactory.numberParserFor(wrapper.getHouseManager().getLanguage());
	}

	@Override
	public boolean matchAndExecute(String input, UUID clientID, byte comType) {

		String match = super.match(input);

		if (match != null) {

			if (match.equals(SET_SPECIFIC_TEMPERATURE)) {

				double temp = isolateTemperature(match, input);
				System.out.println("setting temp to " + temp);

				if (temp != -1d) {

					Room r = wrapper.getHouseManager().getRoom(clientID);

					try {
						// Convert to the room's unit before setting.
						// Considers people will say the temperature in settings
						// value
						r.setTemperature(convertToRoomUnit(temp));

						com.send(new TransferData(TransferFlags.SET_TEMP, r.getTemperature()), comType, clientID);

					} catch (Exception e) {

					}
				}

			} else if (match.equals(DECREASE_TEMPEATURE)) {
				double temp = isolateTemperature(match, input);
				updateRoomTemp(-1 * temp, clientID, comType);

				System.out.println("decresing by " + temp);
			} else if (match.equals(INCREASE_TEMPERATURE)) {

				double tempInc = isolateTemperature(match, input);
				updateRoomTemp(tempInc, clientID, comType);

				System.out.println("increasing by " + tempInc);

			}

			return true;
		}
		return false;
	}

	private double convertToRoomUnit(double temp) {
		TempDisplay unit = wrapper.getHouseManager().getTemperatureDisplay();

		if (unit == TempDisplay.TEMP_CELCIUS) {
			return temp;
		} else {
			CTempConverter converter = new CTempConverter();

			// Converts the temperature to Celcius
			return converter.convert(temp, unit.getAssociatedUnit(), Unit.C);

		}
	}

	private void updateRoomTemp(double deltaTemp, UUID clientID, byte comType) {
		if (deltaTemp == -1d) {
			deltaTemp = 1;
		}

		Room r = wrapper.getHouseManager().getRoom(clientID);

		try {
			r.setTemperature(r.getTemperature() + deltaTemp);
			com.send(new TransferData(TransferFlags.SET_TEMP, r.getTemperature()), comType, clientID);
		} catch (Exception e) {

		}
	}

	private double isolateTemperature(String match, String input) {

		int[] groups = super.getDataGroupIndex(match);
		String numberString = super.getDataAtGroup(groups[0], input, match);

		if (numberString != null) {
			// Parsing the number to get the double value
			return parser.parseDouble(numberString);
		}

		return -1;
	}

}
