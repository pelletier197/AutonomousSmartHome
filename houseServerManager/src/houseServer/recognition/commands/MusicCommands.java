package houseServer.recognition.commands;

import java.util.UUID;

import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseServer.recognition.BaseCommand;
import houseServer.recognition.DataInputCommand;
import houseServer.request.SMusicManager;
import server.communication.Request;
import server.communication.TransferData;

public class MusicCommands extends DataInputCommand {

	private static final String SIMPLE_PLAY_MUSIC = "simplePlayMusic";
	private static final String PLAY_PREVIOUS_SONG = "playPrevious";
	private static final String PLAY_NEXT_SONG = "playNext";
	private static final String PAUSE_SONG = "pauseSong";
	private static final String PLAY_SPECIFIC_MUSIC = "playSpecificMusic";

	public MusicCommands() {
		super(SIMPLE_PLAY_MUSIC, PLAY_PREVIOUS_SONG, PLAY_NEXT_SONG, PAUSE_SONG, PLAY_SPECIFIC_MUSIC);
	}

	@Override
	public boolean matchAndExecute(String input, UUID clientID, byte comType) {

		String match = match(input);

		if (match != null) {

			final SMusicManager music = wrapper.getMusicManager();

			// Correspondance is found
			if (match.equals(PAUSE_SONG)) {
				com.send(new TransferData(TransferFlags.SET_PLAYING, false), comType, clientID);
			} else if (match.equals(PLAY_NEXT_SONG)) {
				com.send(new TransferData(TransferFlags.NEXT_SONG), comType, clientID);

			} else if (match.equals(PLAY_PREVIOUS_SONG)) {
				com.send(new TransferData(TransferFlags.FORCE_PREVIOUS_MUSIC), comType, clientID);

			} else if (match.equals(SIMPLE_PLAY_MUSIC)) {
				com.send(new TransferData(TransferFlags.SET_PLAYING, true), comType, clientID);

			} else if (match.equals(PLAY_SPECIFIC_MUSIC)) {

				// Finds the music name
				String name = isolateMusicName(match, input);
				Mp3SongInfo song = music.musicForName(name);

				if (song != null) {
					com.send(new TransferData(TransferFlags.PLAY_FROM, song), comType, clientID);
				} else {
					// Notify user
				}
			}

			return true;
		}
		return false;
	}

	private String isolateMusicName(String match, String input) {

		// Gets the indexs of the data
		final int[] dataGroup = getDataGroupIndex(match);

		// Only 1 data for this pattern
		String result = getDataAtGroup(dataGroup[0], input, match);

		return result;
	}

}
