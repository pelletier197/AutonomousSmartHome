package houseServer.recognition.commands;

import java.util.UUID;

import embedded.controllers.light.AbstractLight;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.math.BaseConverter;
import houseDataCenter.math.Calculator;
import houseServer.HouseManager;
import houseServer.recognition.DataInputCommand;
import houseServer.recognition.RecognitionRegexWrapper;
import houseServer.request.SLightManager;
import server.communication.Request;

public class LightCommands extends DataInputCommand {

	private static final String OPEN_LIGHTS = "openLights";
	private static final String CLOSE_LIGHTS = "closeLights";
	private static final String OPEN_SPECIFIC_LIGHT = "openSpecificLight";
	private static final String SET_LIGHTS_COLOR = "setLightsColor";
	private static final String RESET_LIGHTS_COLOR = "resetLightColor";

	public LightCommands() {
		super(OPEN_LIGHTS, CLOSE_LIGHTS, OPEN_SPECIFIC_LIGHT, SET_LIGHTS_COLOR, RESET_LIGHTS_COLOR);
	}

	@Override
	public boolean matchAndExecute(String input, UUID clientID, byte comType) {

		String match = super.match(input);

		if (match != null) {
			System.out.println("FUUUUUUUUUUUUU"+match);

			if (match.equals(OPEN_LIGHTS)) {
				Room r = wrapper.getHouseManager().getRoom(clientID);
				r.openAllLights();
			} else if (match.equals(CLOSE_LIGHTS)) {
				Room r = wrapper.getHouseManager().getRoom(clientID);
				r.closeAllLights();
			} else if (match.equals(SET_LIGHTS_COLOR)) {
				System.out.println("FUUUUUUUUUUUUU"+Integer.toHexString(2));
				int color = isolateColor(match, input);
				System.out.println("FUUUUUUUUUUUUU"+Integer.toHexString(color));
				Room r = wrapper.getHouseManager().getRoom(clientID);
				System.out.println("FUUUUUUUUUUUUU"+Integer.toHexString(color));
				r.setAllLightsColor(color);
			} else if (match.equals(RESET_LIGHTS_COLOR)) {
				Room r = wrapper.getHouseManager().getRoom(clientID);
				r.resetAllLightsColor();
			} else if (match.equals(OPEN_SPECIFIC_LIGHT)) {
				AbstractLight light = wrapper.getLightManager().lightForName(isolateLightName(input, match));
				light.open();
			}
			return true;
		}

		return false;
	}

	private String isolateLightName(String input, String match) {
		// Gets the indexs of the data
		final int[] dataGroup = getDataGroupIndex(match);

		// Only 1 data for this pattern
		String result = getDataAtGroup(dataGroup[0], input, match);
		
		System.out.println("The name is " + result);

		return result;
	}

	private int isolateColor(String match, String input) {

		// Gets the indexs of the data
		final int[] dataGroup = getDataGroupIndex(match);

		// Only 1 data for this pattern
		String result = getDataAtGroup(dataGroup[0], input, match).trim();

		return intValue(result);
	}

	private int intValue(String colorName) {
		int color = parseInt(RecognitionRegexWrapper.getRegex(colorName));
		System.out.println("Color int = "+  color);
		System.out.println("The color " + colorName + "is equal to : " + Integer.toHexString(color));
		return color;
	}

	/**
	 * Parses the int value from hexadecimal string starting with '#' before it
	 * or not.
	 * 
	 * @param htmlValue
	 * @return
	 */
	private int parseInt(String htmlValue) {
		htmlValue = htmlValue.replaceAll("#", "");
		BaseConverter converter = new BaseConverter();
		System.out.println("ColorHtml = "+htmlValue);
		int value = Integer.parseInt(converter.convert(htmlValue, 16, 10));

		return value;
	}

}
