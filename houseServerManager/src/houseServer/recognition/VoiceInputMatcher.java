package houseServer.recognition;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

import org.hsqldb.Server;
import org.jcodec.codecs.vp8.FilterUtil;

import houseDataCenter.applicationRessourceBundle.Language;
import houseServer.HouseWrapper;
import server.communication.ServerComManager;
import server.serialization.SerializationManager;

public class VoiceInputMatcher {

	private static final String COMMAND_CONFIG_FILE = "/recognition/commands.ini";
	private static final String RECOGNITION_REGEX_KEY = "recognitionRegex";
	private static final String ANSWER_KEY = "answer";
	private static final String AI_NAME = "aiName";

	private String aiName;
	private List<Command> commands;
	private HouseWrapper wrapper;
	private ServerComManager server;
	private Language lang;

	public VoiceInputMatcher(Language lang, HouseWrapper wrapper, ServerComManager server) {
		commands = new ArrayList<>();
		this.wrapper = wrapper;
		this.server = server;
		this.aiName = lang.getProperty(AI_NAME).toLowerCase(Locale.ROOT);
		this.lang = lang;
		loadStrings();
		loadCommands();
	}

	private void loadStrings() {
		Locale loc = new Locale("", "");

		// Loads recognition regex
		ResourceBundle rb = ResourceBundle.getBundle(lang.getProperty(RECOGNITION_REGEX_KEY), loc);
		RecognitionRegexWrapper.setResourceBundle(rb);

		// Loads Answers
		ResourceBundle ans = ResourceBundle.getBundle(lang.getProperty(ANSWER_KEY), loc);
		AnswerWrapper.setResourceBundle(ans);

	}

	private void loadCommands() {
		try {
			URI path = getClass().getResource(COMMAND_CONFIG_FILE).toURI();

			List<String> lines = Files.readAllLines(Paths.get(path));

			for (String s : lines) {
				System.out.println(s);
				// Instantiate every command from the init config file
				Class<?> clazz = Class.forName(s);
				Constructor<?> constructor = clazz.getConstructor();
				Command command = (Command) constructor.newInstance();
				command.setHouseWrapper(wrapper);
				command.setServerManager(server);
				commands.add(command);
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EOFException e) {

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void analyzeAndExecute(String input, UUID clientID, byte comType) {

		// the regex is verified in lower case
		input = input.toLowerCase().trim();

		// First verify the AI name is really there. If not, passes.
		if (input.startsWith(aiName)) {
			for (Command com : commands) {
				System.out.println("Trying to match");
				// Execute the command if it matches its regex
				if (com.matchAndExecute(input, clientID, comType)) {
					System.out.println("Matched : " + com);
					break;
				}
			}
		}

	}

	/**
	 * Analyzes the given string using the defined commands, and returns true if one
	 * of them matches the given input.
	 * 
	 * @param input
	 *            The string input
	 * @return
	 */
	public boolean matches(String input) {
		// the regex is verified in lower case
		input = input.toLowerCase().trim();

		// First verify the AI name is really there. If not, passes.
		if (input.startsWith(aiName)) {
			for (Command com : commands) {

				// Execute the command if it matches its regex
				if (com.match(input) != null) {
					return true;
				}
			}
		}
		return false;
	}

	public void close() {
		TTSServerUtils.closeTTS();
	}
}
