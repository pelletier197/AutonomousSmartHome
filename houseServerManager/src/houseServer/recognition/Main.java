package houseServer.recognition;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.applicationRessourceBundle.Language;
import houseServer.HouseManager;
import houseServer.HouseWrapper;
import javafx.application.Application;
import javafx.stage.Stage;
import server.transport.DataReceptionHandler;
import tests.houseDataCenter.house.HouseParameter;

public class Main extends Application implements DataReceptionHandler, DecodedDataReceptionHandler {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		VoiceInputMatcher matcher = new VoiceInputMatcher(Language.ENGLISH_US,
				new HouseWrapper(null, null, null, null, null, null, null, null,
						new HouseManager(new HouseParameter(), new LinkedList<>(), new ArrayList<>(), null)),
				null);
		matcher.analyzeAndExecute("Sophia could you make the lights red", UUID.randomUUID(), (byte) 1);

	}

	@Override
	public void receive(Object obj, UUID clientID, byte comType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void received(ByteBuffer data, UUID fromID) {
		// TODO Auto-generated method stub

	}

}
