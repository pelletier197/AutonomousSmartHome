package houseServer.recognition;

import java.util.UUID;

public interface SpeechInputListener {
	public void receiveSpeech(String input);

	public void receiveSpeech(String hypothesis, UUID id);
}
