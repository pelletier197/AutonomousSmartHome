package houseServer.recognition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.border.EtchedBorder;

/**
 * A percent pattern uses regex that have percent patterns in it (%) to isolate
 * data in a specific string. When compiling those patterns, the % are removed
 * so the pattern will be matched using {@link #match(String)}.
 * 
 * <p>
 * 
 * </p>
 * <p>
 * Another way to get the index is to call
 * 
 * 
 * @author sunny
 *
 */
public abstract class DataInputCommand extends BaseCommand {

	private Map<String, int[]> matchDataGroupIndex;
	public static final String SPLIT_FLAG = "%";

	public DataInputCommand(String... keys) {
		super(asRegexMap(keys));

		adaptRegexs();
		generateGroupIndexMap(keys);

	}

	private void adaptRegexs() {
		for (Map.Entry<String, Pattern> e : super.regexs.entrySet()) {
			String[] splitted = e.getValue().toString().split(SPLIT_FLAG);

			e.setValue(Pattern.compile(splitted[splitted.length - 1]));
		}
	}

	public int[] getDataGroupIndex(String match) {
		return matchDataGroupIndex.get(match).clone();
	}

	public String getDataAtGroup(int group, String inputSequence, String matchSequence) {
		Pattern p = regexs.get(matchSequence);

		Matcher m = p.matcher(inputSequence);
		m.find();

		System.out.println("Le groupe matché à l'index : " + group + " est " + m.group(group));
		return group <= m.groupCount() ? m.group(group) : null;

	}

	public List<String> getDataAtGroups(int[] groups, String input, String match) {
		Pattern p = regexs.get(match);
		List<String> data = new ArrayList<>();

		Matcher m = p.matcher(input);

		for (int i : groups) {
			data.add(m.group(i));
		}

		return data;

	}

	private void generateGroupIndexMap(String... regexKeys) {

		matchDataGroupIndex = new HashMap<>();

		for (String r : regexKeys) {
			matchDataGroupIndex.put(r, getDataGroupIndexFromKey(r));

		}
	}

	private int[] getDataGroupIndexFromKey(String regexKey) {
		// Isolate all the parts of the string : group indexs and all
		String[] parts = RecognitionRegexWrapper.getRegex(regexKey).split(SPLIT_FLAG);

		// The Indexs of the groups
		int[] groupIndex = new int[parts.length - 1];

		// Number of group data found
		int count = 0;

		// For each potential data group index
		for (int i = 0; i < parts.length - 1; i++) {

			String part = parts[i];

			// Check if it is one
			if (part != null && !part.trim().isEmpty()) {
				groupIndex[i] = (Integer.parseInt(part) + 1);
				count++;
			}
		}
		// Trim the result
		int[] result = new int[count];
		System.arraycopy(groupIndex, 0, result, 0, count);

		return result;

	}

	private static Map<String, Pattern> dataPatternAsRegex(String... regexKeys) {
		Map<String, Pattern> regexs = new HashMap<>(regexKeys.length);
		String[] splittedData;
		for (String s : regexKeys) {
			String regex = RecognitionRegexWrapper.getRegex(s);
			splittedData = regex.split(SPLIT_FLAG);
			// All the flags are declared at the beginning, so we only take the
			// last matching regex part
			regexs.put(s, asRegex(splittedData[splittedData.length - 1]));
		}
		return regexs;
	}

}
