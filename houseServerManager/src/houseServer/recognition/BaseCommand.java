package houseServer.recognition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import houseServer.HouseWrapper;
import server.communication.ServerComManager;
import server.serialization.SerializationManager;

public abstract class BaseCommand implements Command {

	protected HouseWrapper wrapper;
	protected ServerComManager com;
	protected Map<String, Pattern> regexs;

	private static final String[][] REPLACEMENT = { new String[] { "UNITS", "units" },
			new String[] { "NUMBER", "number" }, new String[] { "POLITE", "polite" },
			new String[] { "COLORS", "colors" } };

	private static String[][] replacements;

	public BaseCommand(String regexKey) {
		// The order of the keys is conserved
		this(new LinkedHashMap<String, Pattern>(1));
		this.regexs.put(regexKey, asRegex(regexKey));

	}

	public BaseCommand(Map<String, Pattern> regexs) {
		if (regexs == null) {
			throw new NullPointerException("Null regexs");
		}
		// The order will depend on the user
		this.regexs = regexs;
	}

	@Override
	public void setHouseWrapper(HouseWrapper wrapper) {
		if (wrapper == null) {
			throw new NullPointerException("Null not permitted");
		}
		this.wrapper = wrapper;
		
		// Initializes the TTS Server utils with the language of the house.
		TTSServerUtils.init(wrapper.getHouseManager().getLanguage());
	}

	@Override
	public void setServerManager(ServerComManager man) {
		if (man == null) {
			//throw new NullPointerException("Null not permitted");
		}
		this.com = man;
	}

	public String match(String input) {

		for (Map.Entry<String, Pattern> entry : regexs.entrySet()) {
			if (entry.getValue().matcher(input).find()) {
				return entry.getKey();
			}
		}

		return null;
	}

	public static Map<String, Pattern> asRegexMap(String... regexKeys) {
		// This way the order is kept. May be important
		Map<String, Pattern> regexs = new LinkedHashMap<>(regexKeys.length);

		for (String s : regexKeys) {
			regexs.put(s, asRegex(s));
		}
		return regexs;
	}

	public static Pattern asRegex(String regexKey) {
		return Pattern.compile(applyReplacements(RecognitionRegexWrapper.getRegex(regexKey)));
	}

	private static String applyReplacements(String value) {
		if (replacements == null) {
			loadReplacements();
		}
		for (String[] r : replacements) {
			value = value.replace(r[0], r[1]);
		}
		return value;
	}

	private static void loadReplacements() {
		if (REPLACEMENT.length > 0) {
			replacements = new String[REPLACEMENT.length][REPLACEMENT[0].length];
			for (int i = 0; i < REPLACEMENT.length; i++) {

				// Gets the value for storage and keeps it in memory
				replacements[i] = new String[] { REPLACEMENT[i][0],
						RecognitionRegexWrapper.getRegex(REPLACEMENT[i][1]) };
			}
		} else {
			replacements = new String[0][0];
		}
	}

}
