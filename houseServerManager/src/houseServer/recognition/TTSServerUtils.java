package houseServer.recognition;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import server.communication.Request;
import server.communication.TransferData;

public class TTSServerUtils {

	private static final String FILE = "temp.wav";
	private static TextToSpeechMaker tts;

	private TTSServerUtils() {
	}

	public final static void init(Language lang) {
		if (tts == null) {
			tts = new MaryTTSTextToSpeech(lang);
		}
	}

	public static final TransferData createTTSPackage(String text) {
		checkState();

		String file = tts.toSpeech(text);

		try {
			byte[] bytes = FileUtils.readFileToByteArray(new File(file));
			return new TransferData(TransferFlags.PLAY_AI_ANSWER, bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Validates to see if the class has been initialized and throw an exception if
	 * not.
	 */
	private static void checkState() {

		if (tts == null) {
			throw new IllegalStateException("The class has not been initialized. Please call #init(Language).");
		}
	}

	public static void closeTTS() {
		if (tts != null) {
			tts.close();
			tts = null;
		}
	}
}
