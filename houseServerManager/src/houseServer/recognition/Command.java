package houseServer.recognition;

import java.util.UUID;

import houseServer.HouseWrapper;
import server.communication.ServerComManager;
import server.serialization.SerializationManager;

/**
 * Represents a command that recognizes the input and execute a single or
 * multiple operation on the house.
 * 
 * @author sunny
 *
 */
public interface Command {

	/*
	 * Sets the house wrapper for every house aspects
	 * 
	 * @param wrapper The house wrapper
	 */
	public void setHouseWrapper(HouseWrapper wrapper);

	/**
	 * Sets the server manager
	 * 
	 * @param man
	 *            The server manager
	 */
	public void setServerManager(ServerComManager man);

	/**
	 * Attempts matching without executing the given input. A call of this
	 * function allows to get the key action associated to the given string
	 * input. It allows to know what action to execute, but not does not execut
	 * it.
	 * 
	 * <p>
	 * In the case that no match is found for this command, null is returned
	 * </p>
	 * 
	 * @param input
	 *            The input string for the command recognition
	 * @return The matching action key if recognition works, or null on failure
	 */
	public String match(String input);

	/**
	 * Attempts matching and executing the command action(s) depending on the
	 * given input.
	 * 
	 * @param input
	 *            The string input
	 * @param clientID
	 *            The client's ID to send answer
	 * @param comType
	 *            The communication type, so the answer is well sended
	 * 
	 * @see #match(String)
	 * @return True if the action has been matched and executed for the given
	 *         client, false otherwise
	 */
	public boolean matchAndExecute(String input, UUID clientID, byte comType);

}
