package houseServer;

public class RoomNotFoundException extends Exception {

	public RoomNotFoundException() {
		// TODO Auto-generated constructor stub
	}

	public RoomNotFoundException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public RoomNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public RoomNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RoomNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
