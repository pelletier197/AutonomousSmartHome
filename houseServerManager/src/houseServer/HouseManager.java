package houseServer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.commons.math3.analysis.function.Abs;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;
import com.sun.xml.internal.bind.v2.model.core.ID;

import embedded.controllers.light.AbstractLight;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.data.ResidentData;
import houseDataCenter.house.data.RoomData;
import houseDataCenter.house.data.TempDisplay;
import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.residents.Resident;
import houseServer.dataSaving.HouseSaver;
import houseServer.dataSaving.entities.LightEntity;
import houseServer.security.HouseParamLock;
import tests.houseDataCenter.house.HouseParameter;

/**
 * Manager of a house that controls every room of the given house.
 * 
 * @author sunny
 *
 */
public class HouseManager {

	/**
	 * The house parameter lock. It is used to authenticate attempts to access
	 * the parameters and resident information.
	 */
	private HouseParamLock locker;

	private HouseParameter parameters;

	private List<Room> rooms;
	private List<Resident> residents;

	private HouseSaver saver;

	public HouseManager(HouseParameter params, Collection<Room> rooms, Collection<Resident> residents,
			HouseSaver saver) {

		if (params == null || saver == null) {
			throw new NullPointerException("Null parameters are invalid");
		}

		this.locker = new HouseParamLock(residents);
		this.rooms = new ArrayList<>(rooms);
		this.residents = new ArrayList<>(residents);
		this.parameters = params;
		this.saver = saver;

	}

	public HouseParameter accessHouseParam(String PIN) throws AccessRefusedError {
		locker.nextPIN(PIN);

		if (locker.unlock() != null) {
			return parameters;
		} else {
			System.out.println("Locked");
			return null;
		}
	}

	public boolean isAccessGranted(String PIN) throws AccessRefusedError {
		return accessHouseParam(PIN) != null;
	}

	public Resident getResident(String mail, String PIN) {
		for (Resident r : residents) {
			if (r.getEmail().equals(mail) && r.getPIN().equals(PIN)) {
				return r;
			}
		}
		return null;
	}

	public void modifyRoomName(int roomId, String name) {
		Room found = findRoom(roomId);
		if (found != null) {
			found.setName(name);
		}
	}

	private Room findRoom(int roomId) {
		for (Room r : rooms) {
			if (r.getCurrentID().equals(roomId)) {
				return r;
			}
		}
		return null;
	}

	public void modifyRoomTemp(int roomId, double temp) {
		Room found = findRoom(roomId);
		if (found != null) {
			found.setTemperature(temp);
		}
	}

	public void modifyRoomVolume(int roomId, double volume) {
		Room found = findRoom(roomId);
		if (found != null) {
			found.setControlVolume(volume);
		}
	}

	public List<ResidentData> getResidentInfo() {

		List<ResidentData> result = new ArrayList<>();

		for (Resident r : residents) {
			result.add(new ResidentData(r));
		}

		return result;
	}

	public void modifyPIN(ResidentData info, String oldPIN, String newPIN) {

		Resident r = null;

		if ((r = findResident(info)) == null) {
			throw new IllegalAccessError("The resident was not found");
		}

		System.out.println(r + " found");
		r.setPIN(oldPIN, newPIN);

	}

	public void modifyName(ResidentData info, String newName, String PIN) {

		Resident r = null;

		if ((r = findResident(info)) == null) {
			throw new IllegalAccessError("The resident was not found");
		}

		r.setName(newName, PIN);

	}

	public void modifyBornDate(ResidentData info, long bornDate, String PIN) {

		Resident r = null;

		if ((r = findResident(info)) == null) {
			throw new IllegalAccessError("The resident was not found");
		}

		r.setBornDate(bornDate, PIN);

	}

	private Resident findResident(ResidentData info) {
		for (Resident r : residents) {
			if (r.getName().equals(info.name) && r.getBornDate() == info.bornDate && r.getEmail().equals(info.mail)) {

				return r;
			}
		}
		return null;
	}

	public synchronized void addResident(Resident resident) {

		if (resident == null) {
			throw new NullPointerException("Cannot add null residents");
		}

		if (!residents.contains(resident)) {
			locker.addUnlocker(resident);
			residents.add(resident);
		}

	}

	public synchronized void removeResident(Resident resident) {
		locker.removeResident(resident);
		residents.remove(resident);

	}

	protected List<Room> getRooms() {

		return rooms;
	}

	public List<AbstractLight> getRoomLights() {

		final List<AbstractLight> lights = new ArrayList<>();

		for (Room room : rooms) {
			lights.addAll(room.getLights());
		}

		return lights;

	}

	public void addLightToRoom(int id, AbstractLight light) {

		if (isUsed(light)) {
			throw new IllegalAccessError("This light is used by another room");
		}

		Room room = findRoom(id);
		room.addLight(light);

	}

	private boolean isUsed(AbstractLight light) {
		final String id = light.getLightID();

		for (Room r : rooms) {
			for (AbstractLight l : r.getLights()) {
				if (id.equals(l.getLightID())) {
					return true;
				}
			}
		}

		return false;
	}

	public synchronized boolean removeRoom(UUID id) {

		Iterator<Room> it = rooms.iterator();

		while (it.hasNext()) {
			Room next = it.next();

			if (next.getCurrentID().equals(id)) {
				next.setCurrentID(Room.UNKNOWN_ID);
				System.out.println("Removed from rooms");
				return true;
			}
		}

		return false;

	}

	public synchronized void addRoom(Room device) {
		if (device == null) {
			throw new NullPointerException();
		}
		this.rooms.add(device);

		// Gives a name to the room
		if (device.getName() == null || device.getName().trim().isEmpty()) {
			device.setName("#" + rooms.size());
		}
		System.out.println("Room created : " + device);
	}

	public int getClientCount() {
		return parameters.getClientCount();
	}

	public Language getLanguage() {
		return parameters.getLanguage();
	}

	protected HouseParameter getParams() {
		return parameters;
	}

	public int getResidentCount() {
		return residents.size();
	}

	public List<Resident> getResidents() {
		return residents;
	}

	public Room getRoom(UUID fromID) {

		for (Room room : rooms) {
			if (fromID.equals(room.getCurrentID())) {
				return room;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param UUID
	 * @return
	 */
	public Room getRoom(byte[] UUID) {

		for (Room room : rooms) {
			if (Arrays.equals(room.getUUID(), UUID)) {
				return room;
			}
		}
		return null;
	}

	public List<RoomData> getActiveRoomsData() {

		List<RoomData> data = new ArrayList<>(rooms.size());
		for (Room room : rooms) {
			try {
				data.add(new RoomData(room));
			} catch (Exception e) {
			}
		}
		return data;
	}

	public synchronized void reassingLights(List<AbstractLight> lights) {

		Map<String, AbstractLight> lightMap = getIdLightMap(lights);
		List<LightEntity> entities = saver.getAllLights();

		createLightsIfNotExist(lights, getIdEntityMap(entities));
		restoreLightSavedState(lightMap, entities);

		// Updates all lights from all room
		for (Room r : rooms) {

			for (AbstractLight l : r.getLights()) {
				if (l != null) {
					r.reassingLight(l, lightMap.get(l.getLightID()));
				}
			}
		}
	}

	private void createLightsIfNotExist(List<AbstractLight> lights, Map<String, LightEntity> idEntityMap) {

		for (AbstractLight light : lights) {
			if (idEntityMap.get(light.getLightID()) == null) {
				saver.addLight(light);
			}
		}
	}

	private synchronized void restoreLightSavedState(Map<String, AbstractLight> lightMap, List<LightEntity> entities) {

		// Restore light's names
		for (LightEntity entity : entities) {

			AbstractLight light = lightMap.get(entity.getId());

			// Not found yet
			if (light != null) {
				light.setName(entity.getName());
			}
		}

		// Put the lights back to the rooms
		for (Room r : rooms) {
			List<LightEntity> roomEntities = saver.getLightsForRoom(r);

			List<AbstractLight> roomLights = new ArrayList<>();

			for (LightEntity entity : roomEntities) {
				roomLights.add(lightMap.get(entity.getId()));
			}

			r.setLights(roomLights);
		}
	}

	private synchronized Map<String, AbstractLight> getIdLightMap(List<AbstractLight> lights) {

		Map<String, AbstractLight> result = new HashMap<>();

		for (AbstractLight light : lights) {
			result.put(light.getLightID(), light);
		}

		return result;
	}

	private synchronized Map<String, LightEntity> getIdEntityMap(List<LightEntity> entities) {

		Map<String, LightEntity> result = new HashMap<>();

		for (LightEntity light : entities) {
			result.put(light.getId(), light);
		}

		return result;
	}

	public List<UUID> getActiveRoomIDs() {
		List<UUID> ids = new ArrayList<>();

		for (Room r : rooms) {

			// If the room is connected
			if (r.getCurrentID() != null) {
				ids.add(r.getCurrentID());
			}
		}
		return ids;
	}

	public TempDisplay getTemperatureDisplay() {
		return parameters.getTempDisplay();
	}

	public Room getRoomByLight(AbstractLight light) {

		final String id = light.getLightID();
		if (id == null)
			return null;
		List<AbstractLight> roomlights = null;

		for (Room r : rooms) {
			roomlights = r.getLights();

			if (roomlights != null) {
				for (AbstractLight l : roomlights) {
					if (id.equals(l.getLightID())) {
						return r;
					}
				}
			}
		}

		return null;
	}

	public void saveAllRooms() {
		for (Room r : rooms) {
			saver.updateRoom(r);
		}
	}

	public void setAllRoomsTempDisplay(TempDisplay t) {
		for (Room r : rooms) {
			r.setDisplayTemp(t);
		}
	}

}
