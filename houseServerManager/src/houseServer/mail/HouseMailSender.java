package houseServer.mail;

import java.util.Collection;
import java.util.List;

import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.house.residents.Resident;
import mail.LocalMailSender;
import mail.MailSender;

public class HouseMailSender {

	private static final String MAIL_PROVIDER_RESIDENT_ACCEPTATION = "home.validation@zoho.com";
	private static final String OUTGOING_SERVER_RESIDENT_ACCEPTATION = "smtp.zoho.com";
	private static final String PASSWORD_RESIDENT_ACCEPTATION = "password123";

	private HouseMailSender() {

	}

	/**
	 * Will send an acceptation email to the given new resident, and will also
	 * notify by a different confirmation email the other residents, so the will
	 * know that someone new will have access to the house
	 * 
	 * @param newResident
	 *            The new resident of the house, that will receive a
	 *            confirmation email.
	 * 
	 * @param other
	 *            The other residents who will be notified.
	 */
	public static final void sendAcceptationMail(Resident newResident, List<Resident> other) {
		MailSender mailer = new LocalMailSender(MAIL_PROVIDER_RESIDENT_ACCEPTATION, OUTGOING_SERVER_RESIDENT_ACCEPTATION, PASSWORD_RESIDENT_ACCEPTATION);
		
		final String newName = newResident.getName();
		
		// Gets and format the subject for new resident
		String newResSubj = BundleWrapper.getString("acceptationNewResidentSubject");
		newResSubj = String.format(newResSubj, newName);
		
		String newResBody = BundleWrapper.getString("acceptationNewResidentBody");
		newResBody = String.format(newResBody, newName);
		
		String othersSubject = BundleWrapper.getString("acceptationOthersSubject");
		othersSubject = String.format(othersSubject, newName);
		
		String othersBody = BundleWrapper.getString("acceotstionOthersBody");
		othersBody = String.format(othersBody, newName);
		
		// Sends email to all other residents hg 
		mailer.sendMessage(newResSubj, newResBody, newResident.getEmail());
		mailer.sendMessage(othersSubject, othersBody, toEmailArray(other));
		
	}
	
	private static String[] toEmailArray(Collection<Resident> r){
		String[] mails = new String[r.size()];
		int i = 0;
		for(Resident res : r){
			mails[i] = res.getEmail();
			i++;
		}
		return mails;
	}

	public static final void sendResidentConfirmationEmail(Resident pending, String confirmCode) {
		MailSender mailer = new LocalMailSender(MAIL_PROVIDER_RESIDENT_ACCEPTATION, OUTGOING_SERVER_RESIDENT_ACCEPTATION,
				PASSWORD_RESIDENT_ACCEPTATION);

		mailer.sendMessage(String.format(BundleWrapper.getString("confirmationCodeSubject"), pending.getName()),
				String.format(BundleWrapper.getString("confirmationCodeBody"), pending.getName(), confirmCode),
				pending.getEmail());

	}

	public static final void sendServerBlockedNotification(List<Resident> receivers, Language l) {

	}
}
