package houseServer.dataSaving;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

public class QueryManager {

	private Map<String, String> queryMap;

	public QueryManager(String resourceQueryDir) {
		this.queryMap = new HashMap<>(64);
		loadQueries(getClass().getResource(resourceQueryDir).getFile());
	}

	private void loadQueries(String path) {
		File dir = new File(path);
		File[] files = dir.listFiles();

		for (File f : files) {
			try {
				String key = FilenameUtils.getBaseName(f.getName()).toUpperCase();
				String value = FileUtils.readFileToString(f, Charset.defaultCharset());

				queryMap.put(key, value);
			} catch (IOException e) {

			}
		}
	}

	public String getQuery(String filename) {
		return queryMap.get(filename.toUpperCase());
	}

}
