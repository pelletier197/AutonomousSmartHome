package houseServer.dataSaving.exceptions;

public class ReadException extends RuntimeException {

	public ReadException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReadException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public ReadException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public ReadException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public ReadException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
