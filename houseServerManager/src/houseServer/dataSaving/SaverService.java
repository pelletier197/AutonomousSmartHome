package houseServer.dataSaving;

import houseServer.dataSaving.exceptions.ConnectionException;

public interface SaverService extends HouseSaver {

	public void openConnection() throws ConnectionException;

	public void closeConnection() throws ConnectionException;
}
