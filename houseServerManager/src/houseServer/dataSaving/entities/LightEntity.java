package houseServer.dataSaving.entities;

import java.util.Arrays;

public class LightEntity {

	private String name;
	private String id;
	private byte[] roomUUID;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public byte[] getRoomUUID() {
		return roomUUID;
	}

	public void setRoomUUID(byte[] roomUUID) {
		this.roomUUID = roomUUID;
	}

	@Override
	public String toString() {
		return "LightEntity [name=" + name + ", id=" + id + ", roomUUID=" + roomUUID + "]";
	}

}
