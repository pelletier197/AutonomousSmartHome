package houseServer.dataSaving;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import embedded.controllers.light.LightEvent;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.house.Room;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseServer.HouseManager;
import tests.houseDataCenter.house.HouseParameter;
import utils.FileUtils;
import utils.HouseUtils;
import utils.Mp3Utils;

/**
 * Class that handles the IO for the house. It serializes every parameter for
 * every rooms in the house.
 * 
 * @author sunny
 *
 */
public class HouseParametersSaver extends HouseDataSaver {

	public static final String PARAM_PATH = FileUtils.getAppDataDirectory() + "/house/house.params";
	public static final String MUSIC_DATA_PATH = FileUtils.getAppDataDirectory() + "/musics/";

	private HouseSaver saver;

	public HouseParametersSaver(HouseSaver saver) {
		if (saver == null) {
			throw new NullPointerException("Null saver not permitted");
		}
		this.saver = saver;
	}

	public void saveParams(HouseParameter params) {
		HouseUtils.writeHouseParams(params, PARAM_PATH);
	}

	/**
	 * Loads the house manager from the file. All the parts of the house manager
	 * are loaded separately, so can therefore be saved separately.
	 * 
	 * <p>
	 * If no manager was previously existing, then a new manager is returned.
	 * </p>
	 * 
	 * @return A previously existing house manager if there is one, or a new one
	 *         otherwise.
	 */
	public HouseManager loadManager() {

		HouseParameter params = HouseUtils.readHouseParams(PARAM_PATH);

		if (params == null) {
			params = new HouseParameter();
	
		}

		Collection<Room> rooms = saver.getRooms();
		System.out.println(rooms + "- rooms read");

		if (rooms == null) {
			rooms = new TreeSet<>();
		}

		Collection<Resident> residents = saver.getResidents();

		if (residents == null) {
			residents = new TreeSet<>();
		}
		
		saveParams(params);

		// Creates the manager from either old or new parameters/rooms
		return new HouseManager(params, rooms, residents, saver);
	}

	public List<Mp3SongInfo> readMusics() {
		return Mp3Utils.getMP3FromRepository(new File(MUSIC_DATA_PATH));

	}

}
