package houseServer.dataSaving;

import java.util.List;

import Jampack.House;
import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.LightEvent;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.HouseAlarm;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.notifications.Notification;
import houseDataCenter.house.residents.Resident;
import houseServer.dataSaving.entities.LightEntity;
import houseServer.devices.intelligentDevice.IntelligentDevice;

public interface HouseSaver {

	/////////////
	/// ROOMS ///
	////////////
	public List<Room> getRooms();

	public void updateRoom(Room r);

	public void addRoom(Room r);

	///////////////
	/// LIGHTS ///
	/////////////
	public List<LightEntity> getAllLights();

	public List<LightEntity> getLightsForRoom(Room r);

	public void addLight(AbstractLight light);
	
	public void updateLight(AbstractLight light);

	public void updateLightToRoom(AbstractLight light, Room r);

	/////////////////////
	/// LIGHT EVENTS ///
	///////////////////
	public List<LightEvent> getLightEvents();

	public void updateLightEvent(LightEvent e);

	public void addLightEvent(LightEvent e);

	public void removeLightEvent(LightEvent e);

	///////////////
	/// ALARMS ///
	/////////////
	public List<HouseAlarm> getAlarms();

	public void updateAlarm(HouseAlarm a);

	public void addAlarm(HouseAlarm a);

	public void removeAlarm(HouseAlarm a);

	//////////////////////
	/// NOTIFICATIONS ///
	////////////////////
	public List<Notification> getAllNotifications();

	public List<Notification> getNotificationsUnseenBy(Resident r);

	public void addNotification(Notification n);

	public void deleteNotification(Notification n);

	public void notificationSeenBy(Notification n, Resident r);
	
	//////////////////
	/// Residents ///
	////////////////
	public List<Resident> getResidents();
	
	public void addResident(Resident r);
	
	public void removeResident(Resident r);

}
