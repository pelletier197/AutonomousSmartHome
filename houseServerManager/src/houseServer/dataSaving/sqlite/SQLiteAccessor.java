package houseServer.dataSaving.sqlite;

import static houseServer.dataSaving.sqlite.SQLiteColumns.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.LightEvent;
import embedded.controllers.light.LightEventType;
import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.alarm.HouseAlarm;
import houseDataCenter.alarm.Tone;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.TempDisplay;
import houseDataCenter.house.exceptions.InvalidBornDate;
import houseDataCenter.house.exceptions.InvalidEmailException;
import houseDataCenter.house.exceptions.InvalidNameException;
import houseDataCenter.house.exceptions.InvalidPINException;
import houseDataCenter.house.notifications.Notification;
import houseDataCenter.house.notifications.NotificationCause;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.residents.ResidentBuilder;
import houseServer.dataSaving.HouseSaver;
import houseServer.dataSaving.QueryManager;
import houseServer.dataSaving.entities.LightEntity;
import houseServer.dataSaving.exceptions.QueryException;
import houseServer.dataSaving.exceptions.ReadException;

public class SQLiteAccessor implements HouseSaver {

	private Connection connection;

	private QueryManager queries;

	public SQLiteAccessor(Connection connect) {
		this.connection = connect;
		this.queries = new QueryManager("/queries/sqlite/");
	}

	@Override
	public List<Room> getRooms() {

		List<Room> result = new ArrayList<Room>();

		ResultSet reader = executeQuery(queries.getQuery("SELECT_ROOMS"));

		try {
			while (reader.next()) {
				byte[] uuid = reader.getBytes(ROOM_UUID);
				String name = reader.getString(ROOM_NAME);
				double temp = reader.getDouble(ROOM_TEMP);
				double volume = reader.getDouble(ROOM_VOLUME);
				TempDisplay dis = TempDisplay.valueOf(reader.getString(ROOM_TEMP_DISPLAY));

				Room room = new Room(uuid);
				room.setTemperature(temp);
				room.setControlVolume(volume);
				room.setName(name);
				room.setDisplayTemp(dis);

				result.add(room);
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return result;
	}

	@Override
	public void updateRoom(Room r) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("UPDATE_ROOM"));

		try {
			writer.setString(1, r.getName());
			writer.setDouble(2, r.getTemperature());
			writer.setDouble(3, r.getControlVolume());
			writer.setString(4, r.getDisplayTemp().name());
			writer.setBytes(5, r.getUUID()); // Param

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new QueryException(e);
		}
	}

	@Override
	public void addRoom(Room r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_ROOM"));

		try {
			writer.setBytes(1, r.getUUID());
			writer.setString(2, r.getName());
			writer.setDouble(3, r.getTemperature());
			writer.setDouble(4, r.getControlVolume());
			writer.setString(5, r.getDisplayTemp().name());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new QueryException(e);
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public List<LightEvent> getLightEvents() {
		List<LightEvent> result = new ArrayList<LightEvent>();

		ResultSet reader = executeQuery(queries.getQuery("SELECT_LIGHT_EVENTS"));

		try {
			while (reader.next()) {

				String uuid = reader.getString(LIGHT_EVENT_UUID);
				LightEventType type = LightEventType.valueOf(reader.getString(LIGHT_EVENT_TYPE));
				int hour = reader.getInt(LIGHT_EVENT_HOUR);
				int minute = reader.getInt(LIGHT_EVENT_MINUTE);
				boolean active = reader.getBoolean(LIGHT_EVENT_ACTIVE);
				List<AlarmDay> days = daysFromString(reader.getString(LIGHT_EVENT_DAYS));
				String[] aimedLights = getLightEventTargets(uuid);

				LightEvent event = new LightEvent(hour, minute, aimedLights, type,
						days.toArray(new AlarmDay[days.size()]));
				event.changeUUID(UUID.fromString(uuid)); // Forces alarm to
															// change UUID
				event.setActive(active);

				result.add(event);
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return result;
	}

	private String[] getLightEventTargets(String uuid) {

		List<String> uuids = new ArrayList<>();

		PreparedStatement writer = preparedSatatement(
				"SELECT LIGHT_UUID FROM HOUSE_LIGHT_EVENT_TARGET WHERE LIGHT_EVENT_UUID = ?");
		try {
			writer.setString(1, uuid);

			ResultSet reader = writer.executeQuery();

			uuids.add(reader.getString(LIGHT_UUID));

		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return uuids.toArray(new String[uuids.size()]);
	}

	@Override
	public void updateLightEvent(LightEvent e) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("UPDATE_LIGHT_EVENTS"));

		try {

			String uuid = e.getId().toString();

			writer.setString(1, e.getType().name());
			writer.setInt(2, e.getHour());
			writer.setInt(3, e.getMin());
			writer.setBoolean(4, e.isActive());
			writer.setString(5, daysToString(e.getAlarmDays()));
			writer.setString(6, uuid);

			String[] aimedLights = e.getAimedLights();
			deleteLightEventTargets(uuid); // Delete old aimed lights
			writeLightEventTargets(uuid, aimedLights); // Writes the new ones

			writer.executeUpdate();

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	private void writeLightEventTargets(String uuid, String[] ids) {

		try {
			PreparedStatement writer = preparedSatatement(
					"INSERT INTO HOUSE_LIGHT_EVENT_TARGET (LIGHT_EVENT_UUID, LIGHT_UUID) VALUES (?, ?)");

			for (String id : ids) {
				writer.setString(1, uuid);
				writer.setString(2, id);
				writer.executeUpdate();
			}
			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);

		}
	}

	private void deleteLightEventTargets(String eventUUID) {
		try {
			PreparedStatement writer = preparedSatatement(
					"DELETE FROM HOUSE_LIGHT_EVENT_TARGET WHERE LIGHT_EVENT_UUID = ?");

			writer.setString(1, eventUUID);
			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@Override
	public void addLightEvent(LightEvent e) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_LIGHT_EVENT"));

		try {

			String uuid = e.getId().toString();

			writer.setString(1, uuid);
			writer.setString(2, e.getType().name());
			writer.setInt(3, e.getHour());
			writer.setInt(4, e.getMin());
			writer.setBoolean(5, e.isActive());
			writer.setString(6, daysToString(e.getAlarmDays()));

			String[] aimedLights = e.getAimedLights();
			writeLightEventTargets(uuid, aimedLights);

			writer.executeUpdate();

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	@Override
	public void removeLightEvent(LightEvent e) {
		try {
			PreparedStatement writer = preparedSatatement(queries.getQuery("DELETE_LIGHT_EVENT"));

			String uuid = e.getId().toString();

			writer.setString(1, uuid);
			writer.executeUpdate();
			deleteLightEventTargets(uuid);

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<HouseAlarm> getAlarms() {
		List<HouseAlarm> result = new ArrayList<HouseAlarm>();

		ResultSet reader = executeQuery(queries.getQuery("SELECT_ALARMS"));

		try {
			while (reader.next()) {

				String uuid = reader.getString(ALARM_UUID);
				byte[] roomUUID = reader.getBytes(ROOM_UUID);
				int hour = reader.getInt(ALARM_HOUR);
				int minute = reader.getInt(ALARM_MINUTE);
				boolean active = reader.getBoolean(ALARM_ACTIVE);
				List<AlarmDay> days = daysFromString(reader.getString(ALARM_DAYS));

				double volume = reader.getDouble(ALARM_TONE_VOLUME);
				String tonePath = reader.getString(ALARM_TONE_PATH);
				Tone t = new Tone(tonePath, volume);

				HouseAlarm alarm = new HouseAlarm(hour, minute, t, roomUUID, days.toArray(new AlarmDay[days.size()]));
				alarm.changeUUID(UUID.fromString(uuid)); // Forces alarm to
															// change UUID
				alarm.setActive(active);

				result.add(alarm);
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return result;
	}

	@Override
	public void updateAlarm(HouseAlarm a) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("UPDATE_ALARM"));

		try {

			writer.setInt(1, a.getHour());
			writer.setInt(2, a.getMin());
			writer.setBoolean(3, a.isActive());
			writer.setString(4, daysToString(a.getAlarmDays()));
			writer.setDouble(5, a.getTone().getToneVolume());
			writer.setString(6, a.getTone().getTonePath());
			writer.setString(7, a.getId().toString()); // Param

			writer.executeUpdate();

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	@Override
	public void addAlarm(HouseAlarm a) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_ALARM"));

		try {

			writer.setString(1, a.getId().toString());
			writer.setBytes(2, a.getRoomUUID());
			writer.setInt(3, a.getHour());
			writer.setInt(4, a.getMin());
			writer.setBoolean(5, a.isActive());
			writer.setString(6, daysToString(a.getAlarmDays()));
			writer.setDouble(7, a.getTone().getToneVolume());
			writer.setString(8, a.getTone().getTonePath());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	@Override
	public void removeAlarm(HouseAlarm a) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("DELETE_ALARM"));

		try {

			writer.setString(1, a.getId().toString());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException x) {
			throw new ReadException(x);
		}
	}

	@Override
	public List<Notification> getAllNotifications() {

		ResultSet reader = executeQuery(queries.getQuery("SELECT_NOTIFICATIONS"));

		return readNotifications(reader);
	}

	@Override
	public List<Notification> getNotificationsUnseenBy(Resident r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("SELECT_NOTIFICATIONS_UNSEEN_BY"));

		try {
			writer.setString(1, r.getUUID().toString());

			return readNotifications(writer.executeQuery());
		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@SuppressWarnings("deprecation")
	private List<Notification> readNotifications(ResultSet reader) {

		List<Notification> result = new ArrayList<>();

		try {
			while (reader.next()) {
				String uuid = reader.getString(NOTIFICATION_UUID);
				NotificationCause cause = NotificationCause.valueOf(reader.getString(NOTIFICATION_TYPE));
				long time = reader.getDate(NOTIFICATION_GENERATION_TIME).getTime();

				Notification notif = new Notification(cause, time);
				notif.changeUUID(UUID.fromString(uuid));

				result.add(notif);
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return result;

	}

	@Override
	public void addNotification(Notification n) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_NOTIFICATION"));

		try {
			writer.setString(1, n.getUUID().toString());
			writer.setString(2, n.getCause().name());
			writer.setDate(3, new Date(n.getTime()));

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@Override
	public void deleteNotification(Notification n) {

		try {
			// Delete the view
			PreparedStatement writer = preparedSatatement(queries.getQuery("DELETE_NOTIFICATION_VIEW"));
			String uuid = n.getUUID().toString();
			writer.setString(1, uuid); // Double delete
			writer.executeUpdate();
			writer.close();

			writer = preparedSatatement(queries.getQuery("DELETE_NOTIFICATION"));
			writer.setString(1, uuid); // Double delete
			writer.executeUpdate();
			writer.close();

		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@Override
	public void notificationSeenBy(Notification n, Resident r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_NOTIFICATION_VIEW"));

		try {
			String uuid = n.getUUID().toString();
			writer.setString(1, uuid);
			writer.setString(2, r.getUUID().toString());
			writer.setDate(3, new Date(System.currentTimeMillis()));

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@Override
	public List<LightEntity> getAllLights() {

		ResultSet reader = executeQuery(queries.getQuery("SELECT_LIGHTS"));

		return readLightEntity(reader);
	}

	@Override
	public List<LightEntity> getLightsForRoom(Room r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("SELECT_LIGHTS_FOR_ROOM"));
		try {
			writer.setBytes(1, r.getUUID());

			return readLightEntity(writer.executeQuery());

		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	private List<LightEntity> readLightEntity(ResultSet reader) {

		List<LightEntity> result = new ArrayList<LightEntity>();

		try {
			while (reader.next()) {
				String lightUUID = reader.getString(LIGHT_UUID);
				byte[] roomUUID = reader.getBytes(ROOM_UUID);
				String lightName = reader.getString(LIGHT_NAME);

				LightEntity entity = new LightEntity();
				entity.setId(lightUUID);
				entity.setRoomUUID(roomUUID);
				entity.setName(lightName);

				result.add(entity);
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

		return result;
	}

	@Override
	public void addLight(AbstractLight light) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_LIGHT"));

		try {
			writer.setString(1, light.getLightID());
			writer.setString(2, light.getName());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

	}

	@Override
	public void updateLightToRoom(AbstractLight light, Room r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("UPDATE_LIGHT_ROOM"));

		try {
			writer.setBytes(1, r != null ? r.getUUID() : null);
			writer.setString(2, light.getLightID());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}
	}

	@Override
	public void updateLight(AbstractLight light) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("UPDATE_LIGHT"));

		try {
			writer.setString(1, light.getName());
			writer.setString(2, light.getLightID());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public List<Resident> getResidents() {
		List<Resident> result = new ArrayList<>();

		ResultSet reader = executeQuery(queries.getQuery("SELECT_RESIDENTS"));

		try {
			ResidentBuilder builder = new ResidentBuilder();

			while (reader.next()) {
				builder.setName(reader.getString(RESIDENT_NAME));
				builder.setEmail(reader.getString(RESIDENT_EMAIL));
				builder.setBornDate(reader.getDate(RESIDENT_BORNDATE).getTime());
				String password = reader.getString(RESIDENT_PASSWORD);
				builder.setPIN(password);
				builder.setPINConfirm(password);
				try {
					Resident r = builder.build();
					r.changeUUID(UUID.fromString(reader.getString(RESIDENT_UUID)));
					result.add(r);
				} catch (InvalidBornDate | InvalidNameException | InvalidPINException | InvalidEmailException e) {
					e.printStackTrace(); // Happens if database is corrupted.
											// Only passes over the data
				}
			}

			reader.close();
		} catch (SQLException e) {
			throw new ReadException(e);
		}
		return result;
	}

	@Override
	public void addResident(Resident r) {

		PreparedStatement writer = preparedSatatement(queries.getQuery("INSERT_RESIDENT"));

		try {
			writer.setString(1, r.getUUID().toString());
			writer.setString(2, r.getName());
			writer.setString(3, r.getEmail());
			writer.setString(4, r.getPIN());
			writer.setDate(5, new Date(r.getBornDate()));

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {

		}
	}

	@Override
	public void removeResident(Resident r) {
		PreparedStatement writer = preparedSatatement(queries.getQuery("DELETE_RESIDENT"));

		try {
			writer.setString(1, r.getUUID().toString());

			writer.executeUpdate();

			writer.close();
		} catch (SQLException e) {

		}
	}

	private PreparedStatement preparedSatatement(String query) {
		try {
			return connection.prepareStatement(query);
		} catch (SQLException e) {
			throw new QueryException(e);
		}
	}

	private ResultSet executeQuery(String query) {
		try {
			ResultSet result = connection.prepareStatement(query).executeQuery();
			return result;
		} catch (Exception e) {
			throw new QueryException(e);
		}
	}

	private List<AlarmDay> daysFromString(String days) {

		AlarmDay[] values = AlarmDay.values();

		List<AlarmDay> result = new ArrayList<>();

		for (char c : days.toCharArray()) {
			int value = Character.getNumericValue(c);
			result.add(values[value]);
		}

		return result;

	}

	private String daysToString(List<AlarmDay> days) {

		StringBuilder builder = new StringBuilder();

		for (AlarmDay day : days) {
			builder.append(day.ordinal());
		}

		return builder.toString();
	}

}
