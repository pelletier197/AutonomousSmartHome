package houseServer.dataSaving.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.sqlite.JDBC;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.LightEvent;
import houseDataCenter.alarm.HouseAlarm;
import houseDataCenter.house.Room;
import houseDataCenter.house.notifications.Notification;
import houseDataCenter.house.residents.Resident;
import houseServer.dataSaving.SaverService;
import houseServer.dataSaving.entities.LightEntity;
import houseServer.dataSaving.exceptions.CommitException;
import houseServer.dataSaving.exceptions.ConnectionException;
import houseServer.dataSaving.exceptions.QueryException;
import houseServer.dataSaving.exceptions.RollbackException;

public class SQLiteSaverService implements SaverService {

	private static final String DB_NAME = "/HouseData.db3";
	private Connection connection;

	private SQLiteAccessor accessor;

	public SQLiteSaverService() throws ClassNotFoundException {
		// Validates the lib is there
		Class.forName("org.sqlite.JDBC");
	}

	@Override
	public void openConnection() throws ConnectionException {
		try {
			String path = getClass().getResource(DB_NAME).getFile();
			connection = DriverManager.getConnection("jdbc:sqlite:" + path);
			connection.setAutoCommit(false);

			accessor = new SQLiteAccessor(connection);
		} catch (Exception e) {
			throw new ConnectionException(e);
		}
	}

	@Override
	public void closeConnection() throws ConnectionException {
		try {
			connection.close();
		} catch (Exception e) {
			throw new ConnectionException(e);
		}
	}

	@Override
	public List<Room> getRooms() {

		try {
			List<Room> rooms = accessor.getRooms();
			commit();
			return rooms;

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void updateRoom(Room r) {
		try {
			accessor.updateRoom(r);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addRoom(Room r) {
		try {
			accessor.addRoom(r);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public List<LightEvent> getLightEvents() {
		try {
			List<LightEvent> events = accessor.getLightEvents();
			commit();
			return events;

		} catch (QueryException e) {
			rollback();
			throw e;
		}

	}

	@Override
	public void updateLightEvent(LightEvent ev) {
		try {
			accessor.updateLightEvent(ev);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addLightEvent(LightEvent ev) {
		try {
			accessor.addLightEvent(ev);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void removeLightEvent(LightEvent ev) {
		try {
			accessor.removeLightEvent(ev);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}

	}

	@Override
	public List<HouseAlarm> getAlarms() {
		try {
			List<HouseAlarm> alarms = accessor.getAlarms();
			commit();
			return alarms;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void updateAlarm(HouseAlarm a) {
		try {
			accessor.updateAlarm(a);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addAlarm(HouseAlarm a) {
		try {
			accessor.addAlarm(a);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void removeAlarm(HouseAlarm a) {
		try {
			accessor.removeAlarm(a);
			commit();

		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public List<Notification> getAllNotifications() {
		try {
			List<Notification> notifs = accessor.getAllNotifications();
			commit();
			return notifs;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public List<Notification> getNotificationsUnseenBy(Resident r) {
		try {
			List<Notification> notifs = accessor.getNotificationsUnseenBy(r);
			commit();
			return notifs;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addNotification(Notification n) {
		try {
			accessor.addNotification(n);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void deleteNotification(Notification n) {
		try {
			accessor.deleteNotification(n);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void notificationSeenBy(Notification n, Resident r) {
		try {
			accessor.notificationSeenBy(n, r);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	private void rollback() {
		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new RollbackException(e);
		}
	}

	private void commit() {
		try {
			connection.commit();
		} catch (SQLException e) {
			throw new CommitException(e);
		}
	}

	@Override
	public List<LightEntity> getAllLights() {
		try {
			List<LightEntity> entities = accessor.getAllLights();
			commit();
			return entities;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public List<LightEntity> getLightsForRoom(Room r) {
		try {
			List<LightEntity> entities = accessor.getLightsForRoom(r);
			commit();
			return entities;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addLight(AbstractLight light) {
		try {
			accessor.addLight(light);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void updateLight(AbstractLight light) {
		try {
			accessor.updateLight(light);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void updateLightToRoom(AbstractLight light, Room r) {
		try {
			accessor.updateLightToRoom(light, r);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public List<Resident> getResidents() {
		try {
			List<Resident> residents = accessor.getResidents();
			commit();
			return residents;
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void addResident(Resident r) {
		try {
			accessor.addResident(r);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

	@Override
	public void removeResident(Resident r) {
		try {
			accessor.removeResident(r);
			commit();
		} catch (QueryException e) {
			rollback();
			throw e;
		}
	}

}
