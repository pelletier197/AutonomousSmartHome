package houseServer.dataSaving.sqlite;

public class SQLiteColumns {

	//////////////
	/// ROOMS ///
	////////////
	public static final String ROOM_UUID = "ROOM_UUID";
	public static final String ROOM_NAME = "ROOM_NAME";
	public static final String ROOM_TEMP = "ROOM_TEMP";
	public static final String ROOM_VOLUME = "ROOM_VOLUME";
	public static final String ROOM_TEMP_DISPLAY = "ROOM_TEMP_DISPLAY";

	//////////////////
	/// RESIDENTS ///
	////////////////
	public static final String RESIDENT_UUID = "RESIDENT_UUID";
	public static final String RESIDENT_NAME = "RESIDENT_NAME";
	public static final String RESIDENT_EMAIL = "RESIDENT_EMAIL";
	public static final String RESIDENT_PASSWORD = "RESIDENT_PASSWORD";
	public static final String RESIDENT_BORNDATE = "RESIDENT_BORNDATE";

	///////////////
	/// ALARMS ///
	/////////////
	public static final String ALARM_UUID = "ALARM_UUID";
	public static final String ALARM_HOUR = "ALARM_HOUR";
	public static final String ALARM_MINUTE = "ALARM_MINUTE";
	public static final String ALARM_ACTIVE = "ALARM_ACTIVE";
	public static final String ALARM_DAYS = "ALARM_DAYS";
	public static final String ALARM_TONE_VOLUME = "ALARM_TONE_VOLUME";
	public static final String ALARM_TONE_PATH = "ALARM_TONE_PATH";

	/////////////////////
	/// LIGHT EVENTS ///
	///////////////////
	public static final String LIGHT_EVENT_UUID = "LIGHT_EVENT_UUID";
	public static final String LIGHT_EVENT_TYPE = "LIGHT_EVENT_TYPE";
	public static final String LIGHT_EVENT_HOUR = "LIGHT_EVENT_HOUR";
	public static final String LIGHT_EVENT_MINUTE = "LIGHT_EVENT_MINUTE";
	public static final String LIGHT_EVENT_ACTIVE = "LIGHT_EVENT_ACTIVE";
	public static final String LIGHT_EVENT_DAYS = "LIGHT_EVENT_DAYS";
	
	/////////////////////
	/// LIGHT EVENTS ///
	///////////////////
	public static final String LIGHT_UUID = "LIGHT_UUID";
	public static final String LIGHT_NAME = "LIGHT_NAME";
	
	//////////////////////
	/// NOTIFICATIONS ///
	////////////////////
	public static final String NOTIFICATION_UUID = "NOTIFICATION_UUID";
	public static final String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
	public static final String NOTIFICATION_GENERATION_TIME = "NOTIFICATION_GENERATION_TIME";

	///////////////////////////
	/// NOTIFICATION VIEWS ///
	/////////////////////////
	public static final String NOTIFICATION_SEEN_TIME = "NOTIFICATION_SEEN_TIME";
	


}
