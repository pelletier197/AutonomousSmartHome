package houseServer.security;

import java.util.Random;

/**
 * Random code generator for verification purpose.
 * 
 * @author Sunny
 * 
 */
public class CodeGenerator {

	/**
	 * Allowed characters in the generate code.
	 */
	private static final char[] characters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', '0'

	};

	/**
	 * Generates a code of the given length. This code contains upper case
	 * letters and random numbers between [0,9]
	 * 
	 * @param length
	 *            The length of the code
	 * 
	 * @return A randomly generated access code
	 */
	public static String generateAccessCode(int length) {

		StringBuilder builder = new StringBuilder();

		Random randGen = new Random();
		final int bound = characters.length;

		for (int i = 0; i < length; i++) {
			builder.append(characters[randGen.nextInt(bound)]);
		}

		return builder.toString();
	
	}

}
