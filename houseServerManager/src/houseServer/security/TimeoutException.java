package houseServer.security;

public class TimeoutException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2302887209951736400L;

	public TimeoutException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TimeoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TimeoutException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TimeoutException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TimeoutException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	

	
	
}
