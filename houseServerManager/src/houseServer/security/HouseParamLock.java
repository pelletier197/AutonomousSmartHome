package houseServer.security;

import java.util.Collection;

import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.security.LockBase;


/**
 * <p>
 * Controls house locking center. Normally, no parameter should be modified if
 * this locker does no give the access. The access to the house will only be
 * granted if the recognition of a given resident is allowed, either via its
 * physical traits (fingerprint AND facial recognition) or its PIN
 * identification number.
 * </p>
 * 
 * <p>
 * To be allowed to access the house via a certain type of recognition, use
 * {@link #nextFace(byte[])}, {@link #nextFinger(byte[][])} and
 * {@link #nextPIN(String)}. Once one of this method is called, if
 * {@link #unlock()} is not called before the specified {@link #TIMEOUT}, the
 * access will be cancelled and next values will be returned to null. During
 * recognition, the PIN is always used over physic recognition, because more
 * reliable.
 * </p>
 * 
 * <p>
 * To allow a given Resident to unlock the house, he must be added as an
 * unlocker via {@link #addUnlocker(Resident)}.
 * </p>
 */
public class HouseParamLock extends LockBase {

	public HouseParamLock(Collection<Resident> unlockers) {
		// The unlock call is made to ensure the retriever is a resident, but no
		// need to keep it unlocked. Should allow face recognition but tests are not good enough
		super(0);

		for (Resident r : unlockers) {
			addUnlocker(r);
		}
	}
	
	

}
