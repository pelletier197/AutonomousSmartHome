package houseServer.security;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.math3.exception.NullArgumentException;

import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.residents.Resident;
import houseServer.HouseManager;
import houseServer.mail.HouseMailSender;
import mail.MailSender;

public class ResidentAccepter {

	private static final long PROTOCOL_DURATION = 300000;

	private static final int MAIL_ONLY = TransferFlags.REQUEST_MAIL_CONFIRMATION_CODE;
	private static final int MAIL_AND_RESIDENT = TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION;

	/**
	 * Hashmap containing the code associated to a resident wished to be
	 * created, its request flag and the time of the begin of the protocol. If
	 * the flag is to {@link #MAIL_ONLY}, then only the confirmation is
	 * requested. Other way, a resident confirmation is needed.
	 */
	private HashMap<String, Object[]> pendingCreation;

	private MailSender mailer;

	private static final int CODE_LENGTH = 10;

	private HouseManager manager;

	public ResidentAccepter(HouseManager manager) {

		if (manager == null) {
			throw new NullArgumentException();

		}
		this.manager = manager;
		this.pendingCreation = new HashMap<>();

	}

	public int launchAcceptProtocol(Resident r) {

		String currentCode = CodeGenerator.generateAccessCode(CODE_LENGTH);
		int code = manager.getResidentCount() > 0 ? MAIL_AND_RESIDENT : MAIL_ONLY;

		pendingCreation.put(currentCode, new Object[] { r, System.currentTimeMillis() });

		if (!validResidentAllowed(r)) {
			throw new IllegalAccessError("This resident cannot be added : this email is already used.");
		}
		HouseMailSender.sendResidentConfirmationEmail(r, currentCode);

		return code;
	}

	private boolean validResidentAllowed(Resident toAdd) {
		for (Resident r : manager.getResidents()) {
			if (r.getEmail().equals(toAdd.getEmail())) {
				return false;
			}
		}
		return true;
	}

	public Resident verifyAcceptation(String code, String residentConfirm) throws AccessRefusedError {

		Object[] info = pendingCreation.get(code);

		System.out.println(code + " " + this.pendingCreation);

		// Code is not valid
		if (info == null) {
			return null;
		}

		System.out.println(info[0] + " " + info[1] + " liste :" + this.pendingCreation);

		Resident resident = (Resident) info[0];
		long protocolBegin = (Long) info[1];

		// New resident must have confirmation in this case
		if (manager.getResidentCount() > 0) {

			if (!manager.isAccessGranted(residentConfirm)) {
				return null;
			}
		}

		if (System.currentTimeMillis() - protocolBegin > PROTOCOL_DURATION) {
			// Will never be accepted
			pendingCreation.remove(code);
			throw new TimeoutException("Protocol timed out");
		}

		List<Resident> residents = manager.getResidents();
		pendingCreation.remove(code);

		HouseMailSender.sendAcceptationMail(resident, residents);
		// Protocol success.
		return resident;

	}

}
