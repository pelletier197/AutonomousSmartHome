package houseServer.security;

import java.util.UUID;

import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Device;
import houseDataCenter.house.Room;
import houseDataCenter.house.residents.Resident;
import houseServer.HouseManager;
import houseServer.devices.intelligentDevice.IntelligentDevice;
import server.communication.Request;
import server.communication.TransferData;

/**
 * Manager for accepting device. Used to work in pair with the server, using
 * transfer codes to verify if the given device should either complete its
 * connection as an intelligent device (phone, tablet, etc), or a room
 * controller.
 * 
 * Once the device verifies with one of the codes, it will be granted the access
 * to the server.
 * 
 * @author Sunny
 *
 */
public class DeviceAccepter {

	private static final int DEVICE_INTELLIGENT = TransferFlags.DEVICE_INTELLIGENT;
	private static final int DEVICE_ROOM_CONTROLLER = TransferFlags.ROOM_CONTROLLER;

	/**
	 * The manager used to accept intelligent device connection
	 */
	private HouseManager manager;

	/**
	 * Creates a device accepter that will use the given device manager to accept or
	 * refuse the connection of the devices.
	 * 
	 * @param manager
	 *            The device manager
	 */
	public DeviceAccepter(HouseManager manager) {
		if (manager == null) {
			throw new NullPointerException("Null manager not allowed");
		}
		this.manager = manager;
	}

	/**
	 * Verifies the attempt of identification from a client on the server.
	 * 
	 * <p>
	 * This function will either return a {@link Room} if the device request a
	 * connection as if, or a {@link IntelligentDevice} if the device request it.
	 * </p>
	 * 
	 * <p>
	 * In the second case, the device will have to give a username (email), and a
	 * password, which will be a resident's PIN. In the case that the information
	 * does not correspond to a resident's, then null is returned.
	 * </p>
	 * <p>
	 * In both case, the devices will have in their properties the current client ID
	 * that is given to them.
	 * </p>
	 * 
	 * @param data
	 *            The data sent by the client. The data should be Object[]{byte[]
	 *            deviceUUID, int deviceType, String mail (if device is
	 *            Intelligent), String password (if device is Intelligent)}
	 * @param fromID
	 *            The client that
	 * 
	 * @param ip
	 *            The ip address of the client.
	 * @return Either a Room or an IntelligentDevice object
	 */
	public Device verify(TransferData data, UUID id, String ip, byte comType) {

		try {

			Object[] dat = (Object[]) data.getData();

			byte[] UUID = (byte[]) dat[0];
			int deviceType = (int) dat[1];

			// The room controllers are always on the local host
			if (deviceType == DEVICE_ROOM_CONTROLLER && ip.startsWith("192.168")) {
				// The room is accepted directly, as it can only control itself
				Room room = new Room(UUID);
				room.setCurrentID(id);

				return room;
			} else if (deviceType == DEVICE_INTELLIGENT) {

				String mail = (String) dat[2];
				String password = (String) dat[3];

				System.out.println(manager.getResidents());

				// Checks if the given device has given a good password.
				Resident r = manager.getResident(mail, password);

				if (r != null) {

					// Access granted
					IntelligentDevice device = new IntelligentDevice(UUID, r, comType);
					device.setCurrentID(id);

					return device;
				}

			}
		} catch (Exception e) {
			// If any exception occurs because parameters are not good, null is
			// returned.
		}

		return null;
	}

}
