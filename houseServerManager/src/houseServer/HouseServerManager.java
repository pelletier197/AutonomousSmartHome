package houseServer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.ApplicationPorts;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Device;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.RoomData;
import houseServer.dataSaving.HouseParametersSaver;
import houseServer.dataSaving.Saver;
import houseServer.dataSaving.SaverService;
import houseServer.dataSaving.exceptions.ConnectionException;
import houseServer.dataSaving.sqlite.SQLiteSaverService;
import houseServer.devices.intelligentDevice.IntelligentDevice;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import houseServer.devices.intelligentDevice.IntelligentDeviceRequestManager;
import houseServer.devices.room.HouseRequestManager;
import houseServer.request.SAlarmManager;
import houseServer.request.SCameraManager;
import houseServer.request.SLightManager;
import houseServer.request.SMusicManager;
import houseServer.request.SNotificationManager;
import houseServer.request.SResidentManager;
import houseServer.request.SRoomManager;
import houseServer.request.SSystemManager;
import houseServer.security.DeviceAccepter;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.dispatch.DispatchOverloadSelector;
import server.dispatch.TransferDataDispatcher;
import server.serialization.SerializationManager;
import server.serialization.SerializationType;
import server.transport.ConnectionListener;
import server.transport.DataReceptionHandler;
import server.transport.ServerManager;
import utils.HouseUtils;

/**
 * The server based application, containing the request manager, along with the
 * house manager, and the device manager. All new devices, intelligent like room
 * controllers will connect to those servers and will need to provide data
 * before being accepted as devices that can communicate with the server.
 * 
 * <p>
 * First, when a new connection will be established, the connected device will
 * need to send its UUID, which can be accessed via runtime request @see
 * {@link HouseUtils #getDeviceUUID()}. Request of this field will be made via
 * {@link TransferFlags} flags.
 * 
 * </p>
 * 
 * <p>
 * If the device is not recognized as an existing room in memory, it will need
 * to send its information about the device instance via flags again.
 * </p>
 * 
 * <p>
 * Every other requests will be handled by the {@link HouseRequestManager}
 * depending on the flags received and will manage to answer to the client that
 * requested the data, or sending error code. Note that answers might be
 * different if the request is either sent by an intelligent device or a room.
 * See flags documentation for further explanation
 * </p>
 * 
 * <p>
 * In addition to accepting and refusing connections, this class implements
 * {@link Saver} interface, which will allow to the {@link HouseRequestManager}
 * instantiated by this class to save the house parameters when it is wished.
 * The parameters will however automatically be saved every
 * {@value #SAVE_TIMEOUT} ms by default.
 * </p>
 * 
 * @author sunny
 *
 */
public class HouseServerManager implements ConnectionListener, DispatchOverloadSelector {

	public static final int ROOM_OVERLOAD = 0;
	public static final int MOBILE_OVERLOAD = 1;

	/**
	 * The port on the server
	 */
	private static final int PORT = ApplicationPorts.MAIN_APP;

	/**
	 * Server manager for handling and receiving connections
	 */
	private ServerComManager serverManager;

	/**
	 * House controls manager. CHecks the parameters of the rooms and ensure they
	 * are well handled.
	 */
	private HouseManager houseManager;

	/**
	 * The manager for intelligent devices.
	 */
	private IntelligentDeviceManager deviceManager;

	/**
	 * The request manager for room communication only.
	 */
	private HouseRequestManager roomRequestManager;

	/**
	 * The intelligent device for intelligent device requests
	 */
	private IntelligentDeviceRequestManager deviceRequestManager;

	/**
	 * The alarm manager that verifies periodically if the alarms of every rooms
	 * rang. It will notify the client in the case that any of them is ringing.
	 */
	private HouseWrapper wrapper;

	/**
	 * The device accepter for the server. It's used to accept and identify what
	 * device is trying to connect. To do so, the client must identify what type of
	 * device it is, and its UUID
	 */
	private DeviceAccepter accepter;

	private SaverService saver;

	private HouseParametersSaver paramSaver;

	private TransferDataDispatcher nextDispatcher;

	/**
	 * This set contains the client ID that have been verified. This protects the
	 * server from responding requests that have been passed by unverified devices.
	 */
	@SuppressWarnings("rawtypes")
	private Map<UUID, Class> verified;

	/**
	 * Constructs a house server manager that will instantiate a server and handle
	 * device connections, device requests and will manage the periodically perform
	 * tasks that are made to ensure the house is fully autonomous.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public HouseServerManager() throws Exception {

		// Creates the saver service
		this.saver = new SQLiteSaverService();
		this.saver.openConnection();

		loadHouseManager();

		// Initializes the resourceBundle
		ResourceBundle rb = ResourceBundle.getBundle(this.houseManager.getLanguage().getProperty("strings_server"),
				new Locale("", ""));
		BundleWrapper.setResourceBundle(rb);

		// Instantiate the managers for accepting and watching
		this.deviceManager = new IntelligentDeviceManager();
		this.accepter = new DeviceAccepter(houseManager);
		this.verified = new HashMap<>();

		// Opens the connection to the server
		serverManager = new ServerComManager(PORT, 20);
		serverManager.addConnectionListener(this);
		serverManager.getDataDispatcher().setDispatchOverloadSelector(this);

		// Create the managers for every specific need
		createManagers();
		addDataReceivers();

		this.nextDispatcher = (TransferDataDispatcher) serverManager.getDataDispatcher();

		// Load the request managers
		loadRoomRequestManager();
		loadIntelligentDeviceRequestManager();

		// Opens connections
		serverManager.open();
	}

	private void createManagers() {
		SAlarmManager alarmManager = new SAlarmManager(serverManager, houseManager, deviceManager, saver);
		SLightManager lightManager = new SLightManager(serverManager, houseManager, deviceManager, saver);
		SCameraManager camManager = new SCameraManager(serverManager, houseManager, deviceManager, saver);
		SMusicManager musicManager = new SMusicManager(serverManager, houseManager, deviceManager, saver);
		SSystemManager secuManager = new SSystemManager(serverManager, houseManager, deviceManager, saver);
		SResidentManager residentManager = new SResidentManager(serverManager, houseManager, deviceManager, saver);
		SRoomManager roomManager = new SRoomManager(serverManager, houseManager, deviceManager, saver);
		SNotificationManager notifier = new SNotificationManager(serverManager, houseManager, deviceManager, saver);
		this.wrapper = new HouseWrapper(alarmManager, camManager, lightManager, musicManager, secuManager,
				residentManager, roomManager, notifier, houseManager);
	}

	private void addDataReceivers() {

		serverManager.addDataReceiver(wrapper.getAlarmManager());
		serverManager.addDataReceiver(wrapper.getLightManager());
		serverManager.addDataReceiver(wrapper.getCamManager());
		serverManager.addDataReceiver(wrapper.getMusicManager());
		serverManager.addDataReceiver(wrapper.getSecuManager());
		serverManager.addDataReceiver(wrapper.getResidentManager());
		serverManager.addDataReceiver(wrapper.getRoomManager());
		serverManager.addDataReceiver(roomRequestManager);
		serverManager.addDataReceiver(deviceRequestManager);

	}

	/**
	 * Loads the request manager that will handle requests from the house rooms.
	 */
	private void loadRoomRequestManager() {
		this.roomRequestManager = new HouseRequestManager(serverManager, houseManager, deviceManager, saver);
		this.roomRequestManager.setWrapper(wrapper);
	}

	/**
	 * Loads the house manager and its properties that will always be kept in
	 * memory. The data from this manager are saved periodically by a background
	 * thread.
	 */

	private void loadHouseManager() {

		this.paramSaver = new HouseParametersSaver(saver);

		this.houseManager = paramSaver.loadManager();
	}

	/**
	 * Loads the intelligent device manager that will handle the requests from
	 * intelligent devices. Those will mainly be used to control the house from
	 * outside it.
	 */
	private void loadIntelligentDeviceRequestManager() {
		this.deviceRequestManager = new IntelligentDeviceRequestManager(this.houseManager, serverManager, deviceManager,
				saver);
		this.deviceRequestManager.setWrapper(wrapper);

	}

	/**
	 * Closes the connection to the server. This function should never be called as
	 * the server is made to run indefinitely.
	 */
	public void close() {

		serverManager.close();
		deviceRequestManager.close();
		try {
			saver.closeConnection();
		} catch (ConnectionException e) {
			e.printStackTrace();
		}

	}

	@Override
	public int selectOverload(Request request) {
		TransferData data = (TransferData) request.getData();

		try {

			int flag = data.getFlag();

			// If it is not for identification purpose.
			if (flag != TransferFlags.REQUEST_IDENTIFICATION_DATA && flag != TransferFlags.REQUEST_DEVICE_TYPE) {

				Class<?> dev = verified.get(request.getClientId());

				if (dev != null) {
					// Check to know if the device is verified. As long
					// as it is not done, no requests are treated.
					if (dev == Room.class) {
						return ROOM_OVERLOAD;
					} else {
						return MOBILE_OVERLOAD;
					}
				} else {
					request.respond(new TransferData(TransferFlags.NOT_AUTHENTICATED, null));
				}

			} else if (flag == TransferFlags.REQUEST_IDENTIFICATION_DATA) {

				// A device tries to identify
				handleIdentificationDataReception(request, data);

			}
		} catch (Exception e) {
			// Catch all exception so server never crashes
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Handles an identification request from the client. It must either identify as
	 * an intelligent device or a room controller.
	 * 
	 * @param data
	 *            The data that were transfered from the client.
	 * @param fromID
	 *            The id of the client that wants to identify
	 */
	private void handleIdentificationDataReception(Request request, TransferData data) {

		Device device = null;

		UUID clientId = request.getClientId();

		if ((device = accepter.verify(data, clientId, serverManager.getClientIPAddress(clientId),
				request.getCommunicationType())) != null) {

			System.out.println("Verify worked");

			if (device instanceof Room) {
				// Checks to know if the room already exist inside the server.
				// In this case, the current id is only associated to this room
				if (houseManager.getRooms().contains(new Room(device.getUUID()))) {

					Room room = houseManager.getRoom(device.getUUID());
					room.setCurrentID(clientId);

				} else {
					// In the other case, a new room is created
					Room room = (Room) device;
					houseManager.addRoom(room);
					saver.addRoom(room);
				}

				verified.put(clientId, Room.class);

				roomRequestManager.handleNewRoomConnected(clientId);

			} else {
				// A new device is created
				deviceManager.addDevice((IntelligentDevice) device);
				verified.put(clientId, IntelligentDevice.class);
				deviceRequestManager.handleNewDeviceConnected(clientId);

			}

			System.out.println("Sent granted1");
			request.respond(new TransferData(TransferFlags.ACCESS_GRANTED));
			System.out.println("Sent granted2");
		} else {
			System.out.println("Sent refuse1");
			request.respond(new TransferData(TransferFlags.ACCESS_REFUSED));
			System.out.println("Sent refuse2");

		}
	}

	/**
	 * Handles the connection of a new client. This will simply send an hand sharing
	 * protocol request from the device, for it to send data about the device
	 * identity, either to know if its a room controller or an intelligent device.
	 * 
	 * @param clientSocket
	 *            The client socket that sent the request.
	 * @param generatedID
	 *            The current client ID. This ID will be set later when the device
	 *            will authenticate.
	 */
	@Override
	public void handleConnection(UUID generatedID) {

	}

	@Override
	public void connectionLost(UUID clientID) {

		System.out.println("Try remove client : " + clientID);

		// Removes the device from either rooms or devices when it is
		// disconnected.
		if (!deviceManager.remove(clientID)) {
			houseManager.removeRoom(clientID);
		}

		roomRequestManager.handleRoomDisconnected(clientID);
		deviceRequestManager.handleDeviceDisconnected(clientID);

		System.gc();
	}

}
