package houseServer.devices.intelligentDevice;

import houseDataCenter.house.Device;
import houseDataCenter.house.residents.Resident;

/**
 * Represents a intelligent device that can pass requests to the server under
 * the identity of a given resident. This class is valid for a single session
 * with the server.
 * 
 * @author sunny
 *
 */
public class IntelligentDevice extends Device {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1844394139213363231L;

	private byte comType;
	private Resident currentUser;

	public IntelligentDevice(byte[] uuid, Resident user, byte comType) {
		super(uuid);
		if (user == null) {
			throw new NullPointerException("Cannot have a null user");
		}
		this.currentUser = user;
		this.comType = comType;
	}

	public byte getComType() {
		return comType;
	}

	public void setComType(byte comType) {
		this.comType = comType;
	}

	public Resident getCurrentUser() {
		return currentUser;
	}
}
