package houseServer.devices.intelligentDevice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import houseDataCenter.house.Device;
import houseDataCenter.house.Room;

public class IntelligentDeviceManager {

	private List<IntelligentDevice> devices;

	public IntelligentDeviceManager() {
		this.devices = new ArrayList<>();
	}

	public void addDevice(IntelligentDevice device) {
		if (device == null) {
			throw new NullPointerException("Null device not permitted");
		}

		if (!contains(device)) {
			this.devices.add(device);
		}

	}

	public List<IntelligentDevice> getDevices() {
		return devices;
	}

	public synchronized boolean remove(UUID id) {

		Iterator<IntelligentDevice> it = devices.iterator();
		boolean result = false;

		while (it.hasNext()) {
			IntelligentDevice next = it.next();

			if (next.getCurrentID().equals(id)) {
				it.remove();
				System.out.println("Removed from intelligents ");
				result = true;
			}
		}

		return result;

	}

	public boolean contains(Device device) {

		for (IntelligentDevice dev : devices) {
			if (device.equals(dev)) {
				return true;
			}
		}

		return false;
	}

	public boolean contains(UUID deviceID) {

		for (IntelligentDevice dev : devices) {
			if (dev.getCurrentID().equals(deviceID)) {
				return true;
			}
		}

		return false;
	}

	public List<UUID> getAllIds() {

		List<UUID> ids = new ArrayList<>();

		for (IntelligentDevice d : devices) {
			ids.add(d.getCurrentID());
		}

		return ids;
	}

	public byte getDeviceComType(UUID deviceUUID) {
		return devices.stream().filter(m -> deviceUUID.equals(m.getCurrentID())).findFirst().orElse(null).getComType();
	}
}
