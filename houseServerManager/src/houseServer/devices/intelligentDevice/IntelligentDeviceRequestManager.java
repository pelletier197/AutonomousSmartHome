package houseServer.devices.intelligentDevice;

import static houseDataCenter.commucationData.TransferFlags.BEGIN_ALARM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_CAMERA_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_LIGHT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_MUSIC_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_RESIDENT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_ROOM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_SYSTEM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_VOCAL_REQUEST_FLAG;
import static houseDataCenter.commucationData.TransferFlags.END_ALARM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_CAMERA_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_LIGHT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_MUSIC_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_RESIDENT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_ROOM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_SYSTEM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_VOCAL_REQUEST_FLAG;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.hsqldb.Server;

import houseServer.HouseManager;
import houseServer.HouseWrapper;
import houseServer.dataSaving.HouseSaver;
import houseServer.request.SAlarmManager;
import houseServer.request.SCameraManager;
import houseServer.request.SLightManager;
import houseServer.request.SMusicManager;
import houseServer.request.SResidentManager;
import houseServer.request.SRoomManager;
import houseServer.request.SSystemManager;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.serialization.SerializationManager;

public class IntelligentDeviceRequestManager {

	private static final String MUSIC_PATH = "/music/";

	private HouseManager manager;

	private ServerComManager serverManager;

	private IntelligentDeviceManager deviceManager;

	private SAlarmManager alarmManager;

	private SLightManager lightManager;

	private SCameraManager camManager;

	private SMusicManager musicManager;

	private SSystemManager secuManager;

	private SResidentManager residentManager;

	private SRoomManager roomManager;

	private HouseSaver saver;

	public IntelligentDeviceRequestManager(HouseManager hManager, ServerComManager sManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {
		if (hManager == null || sManager == null || deviceManager == null) {
			throw new NullPointerException("Null manager or server not accepted");
		}
		this.manager = hManager;
		this.serverManager = sManager;
		this.deviceManager = deviceManager;
		this.saver = saver;
	}

	public static void sendAllDevices(ServerComManager manager, List<IntelligentDevice> devices, TransferData data) {
		HashMap<Byte, List<UUID>> comToID = new HashMap<>();
		List<UUID> ids = null;
		Byte com = 0;

		for (IntelligentDevice device : devices) {
			System.out.println(device.getCurrentID());
			com = device.getComType();

			if ((ids = comToID.get(com)) == null) {
				ids = new ArrayList<>();
				comToID.put(com, ids);
			}

			ids.add(device.getCurrentID());

		}

		manager.sendToAll(data, comToID);
	}

	public void setWrapper(HouseWrapper wrapper) {

		this.alarmManager = wrapper.getAlarmManager();
		this.camManager = wrapper.getCamManager();
		this.lightManager = wrapper.getLightManager();
		this.musicManager = wrapper.getMusicManager();
		this.secuManager = wrapper.getSecuManager();
		this.residentManager = wrapper.getResidentManager();
		this.roomManager = wrapper.getRoomManager();
	}

	public void handleNewDeviceConnected(UUID id) {

	}

	public void handleDeviceDisconnected(UUID id) {

	}

	public void close() {
		// TODO Auto-generated method stub

	}

}
