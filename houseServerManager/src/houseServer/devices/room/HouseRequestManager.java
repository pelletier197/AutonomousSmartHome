package houseServer.devices.room;

import static houseDataCenter.commucationData.TransferFlags.ALARM_RINGING;
import static houseDataCenter.commucationData.TransferFlags.AUDIO_DATA_INPUT;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_ALARM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_CAMERA_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_LIGHT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_MUSIC_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_RESIDENT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_ROOM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_SYSTEM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.BEGIN_VOCAL_REQUEST_FLAG;
import static houseDataCenter.commucationData.TransferFlags.END_ALARM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_CAMERA_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_LIGHT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_MUSIC_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_RESIDENT_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_ROOM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_SYSTEM_MANAGEMENT_FLAGS;
import static houseDataCenter.commucationData.TransferFlags.END_VOCAL_REQUEST_FLAG;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections.map.HashedMap;

import Jampack.House;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.mp3Player.PlaylistManager;
import houseServer.HouseManager;
import houseServer.HouseServerManager;
import houseServer.HouseWrapper;
import houseServer.dataSaving.HouseSaver;
import houseServer.dataSaving.Saver;
import houseServer.devices.intelligentDevice.IntelligentDeviceManager;
import houseServer.recognition.HouseSpeechToTextConverter;
import houseServer.recognition.SpeechInputListener;
import houseServer.recognition.VoiceInputMatcher;
import houseServer.request.SAlarmManager;
import houseServer.request.SCameraManager;
import houseServer.request.SLightManager;
import houseServer.request.SMusicManager;
import houseServer.request.SResidentManager;
import houseServer.request.SRoomManager;
import houseServer.request.SSystemManager;
import houseServer.security.ResidentAccepter;
import server.serialization.SerializationType;
import server.communication.Request;
import server.communication.ServerComManager;
import server.communication.TransferData;
import server.dispatch.SocketMethod;
import server.dispatch.TransferDataRequest;
import server.serialization.SerializationManager;

/**
 * Server based AI taking decisions toward a configured house in the server's
 * parameters.
 * 
 * <p>
 * This AI most likely entirely control the house, passing from music selection,
 * house parameter control, identity control of residents, connected objects
 * (not implemented yet), all that controlled via client-side application using
 * either control request or vocal requests via {@link #manager} given at
 * construction.
 * </p>
 * 
 * <p>
 * Using {@link TransferFlags} and {@link Request} to read input from clients,
 * it is imperative that the flags input and associated object are respected
 * from the client, to ensure that no exception will be thrown internally.
 * </p>
 * 
 * @author sunny
 *
 */
public class HouseRequestManager implements SpeechInputListener {

	/**
	 * 
	 */
	private ServerComManager serverManager;

	/**
	 * 
	 */
	private PlaylistManager playlistManager;

	/**
	 * 
	 */
	private HouseManager houseManager;

	/**
	 * 
	 */
	private IntelligentDeviceManager deviceManager;

	/**
	 * 
	 */
	private ResidentAccepter accepter;

	/**
	 * 
	 */
	private HouseSpeechToTextConverter recognizer;

	/**
	 * 
	 */
	private SAlarmManager alarmManager;

	private SLightManager lightManager;

	/**
	 * 
	 */
	private HouseSaver saver;

	private SCameraManager camManager;

	private SMusicManager musicManager;

	private SSystemManager secuManager;

	private SResidentManager residentManager;

	private SRoomManager roomManager;

	private VoiceInputMatcher voiceAnalyzer;

	/**
	 * @param manager
	 * @param houseManager
	 * @param deviceManager
	 * @param saver
	 */
	public HouseRequestManager(ServerComManager manager, HouseManager houseManager,
			IntelligentDeviceManager deviceManager, HouseSaver saver) {

		if (manager == null || houseManager == null || saver == null || deviceManager == null) {
			throw new NullPointerException();
		}

		this.deviceManager = deviceManager;
		this.saver = saver;
		this.serverManager = manager;
		this.houseManager = houseManager;
		this.playlistManager = new PlaylistManager();
		this.accepter = new ResidentAccepter(houseManager);
	}

	public static void sendAllRooms(ServerComManager server, HouseManager man, TransferData data) {

		Map<Byte, List<UUID>> comIds = new HashMap<>();
		comIds.put(Room.ROOM_COM_TYPE, man.getActiveRoomIDs());

		server.sendToAll(data, comIds);
	}

	@SocketMethod(flag = AUDIO_DATA_INPUT, overload = HouseServerManager.ROOM_OVERLOAD)
	public void receiveSpeach(byte[] data, TransferDataRequest request) {
		recognizer.newData(data, request.getClientId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see houseServer.SpeechInputListener#receiveSpeech(java.lang.String)
	 */
	@Override
	public void receiveSpeech(String input) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see houseServer.SpeechInputListener#receiveSpeech(java.lang.String,
	 * java.lang.Integer)
	 */
	@Override
	public void receiveSpeech(String hypothesis, UUID id) {
		System.out.println("Received speech from client : " + id + " - " + hypothesis);
		this.voiceAnalyzer.analyzeAndExecute(hypothesis, id, SerializationType.JAVA_OBJECT);

	}

	public void setWrapper(HouseWrapper wrapper) {

		this.alarmManager = wrapper.getAlarmManager();
		this.camManager = wrapper.getCamManager();
		this.lightManager = wrapper.getLightManager();
		this.musicManager = wrapper.getMusicManager();
		this.secuManager = wrapper.getSecuManager();
		this.residentManager = wrapper.getResidentManager();
		this.roomManager = wrapper.getRoomManager();

		this.recognizer = new HouseSpeechToTextConverter(houseManager.getLanguage(), this);
		this.voiceAnalyzer = new VoiceInputMatcher(houseManager.getLanguage(), wrapper, serverManager);
		this.recognizer.startRecognition();

	}

	public void handleNewRoomConnected(UUID fromID) {
		recognizer.addClient(fromID);
	}

	public void handleRoomDisconnected(UUID fromID) {
		recognizer.removeClient(fromID);
	}

}
