package main;

import java.net.InetAddress;

import houseServer.HouseServerManager;

public class Main {

	private static HouseServerManager app;

	private static final long WAIT_BEFORE_RELAUNCH = 10_000;

	public static void main(String[] args) throws Exception {
		app = new HouseServerManager();

		System.out.println("Max memory available : " + Runtime.getRuntime().maxMemory());

		System.out.println(InetAddress.getLocalHost().getHostAddress());
	}

	public static void restart() throws Exception {
		app.close();
		app = null; // Clears all data from the server

		Thread.sleep(WAIT_BEFORE_RELAUNCH);
		System.gc(); // Forces GC to clean
		Thread.sleep(WAIT_BEFORE_RELAUNCH);

		// Restarts everything
		app = new HouseServerManager();
		System.out.println("Server restarted");
	}
}
