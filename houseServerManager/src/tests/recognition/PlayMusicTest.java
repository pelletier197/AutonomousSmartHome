package tests.recognition;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import houseDataCenter.applicationRessourceBundle.Language;
import houseServer.HouseWrapper;
import houseServer.recognition.Command;
import houseServer.recognition.VoiceInputMatcher;
import houseServer.recognition.commands.MusicCommands;
import server.communication.ServerComManager;
import server.serialization.SerializationManager;
import server.transport.DataReceptionHandler;
import server.transport.ServerManager;

public class PlayMusicTest {

	private VoiceInputMatcher matcher;

	@Before
	public void setUp() throws Exception {

		matcher = new VoiceInputMatcher(Language.ENGLISH_US,
				new HouseWrapper(null, null, null, null, null, null, null, null, null), new ServerComManager(1000, 1));
	}

	@Test
	public void testMatches() {
		assertTrue(matcher.matches("sophia play lol"));
		assertTrue(matcher.matches("sophia Could you play the sound of silence please"));
		assertTrue(matcher.matches("sophia Would you play the sound of silence please"));
		assertTrue(matcher.matches("sophia play the sound of silence please"));
		assertTrue(matcher.matches("sophia play the sound of silence"));
		assertFalse(matcher.matches("sophia buy me a beer"));
		assertFalse(matcher.matches("play the sound of silence"));
		assertTrue(matcher.matches("sophia Could you play the previous song please"));
		assertTrue(matcher.matches("sophia Could you play the next song please"));
		assertTrue(matcher.matches("sophia would you you play the next song please"));
		assertFalse(matcher.matches("buy me a beer"));
		assertFalse(matcher.matches("sophia "));

	}

}
