package tests;

import houseServer.HouseServerManager;
import server.client.ClientComManager;
import server.communication.Request;
import server.communication.TransferData;

 class HouseServerTestTools  {

	private static final int PORT = 30_000;

	// The independent server manager app
	private HouseServerManager server;

	// The client to make requests
	private ClientComManager client;

	private Object syncReception;

	public HouseServerTestTools() throws Exception {

		this.server = new HouseServerManager();

		this.client = new ClientComManager(PORT);

	}

	public Request syncRequest(TransferData data) {
		client.send(data);

		synchronized (this) {
			// Blocks until reception
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// This is the answer from the server
		return (Request) syncReception;

	}

}
