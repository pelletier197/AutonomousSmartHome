package tests.house;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.house.Room;
import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.residents.Resident;
import houseServer.HouseManager;
import tests.houseDataCenter.house.HouseParameter;
import tests.houseDataCenter.house.TestUtils;

public class HouseManagerTest {

	private Resident r;
	private HouseManager hm;

	@Before
	public void setUp() throws Exception {

		this.r = TestUtils.generateResident1();
		hm = new HouseManager(new HouseParameter(), new ArrayList<>(), new ArrayList<>(), null);
		hm.addResident(r);
	}

	@Test
	public void testHouseManager() {
		try {
			hm = new HouseManager(null, new ArrayList<>(), new ArrayList<>(), null);
			fail();
		} catch (Exception e) {

		}
		try {
			hm = new HouseManager(new HouseParameter(), null, new ArrayList<>(), null);
			fail();
		} catch (Exception e) {

		}
		try {
			hm = new HouseManager(new HouseParameter(), new ArrayList<>(), null, null);
			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testAccessHouseParamString() throws AccessRefusedError {
		assertNotNull(hm.accessHouseParam(r.getPIN()));

		Resident r2 = TestUtils.generateResident2();
		assertNull(hm.accessHouseParam(r2.getPIN()));

		hm.addResident(r2);
		assertNotNull(hm.accessHouseParam(r2.getPIN()));

	}

	@Test
	public void testModifyRoomName() {
		Room room = new Room(new byte[128]);
		room.setCurrentID(UUID.randomUUID());
		room.setName("Tata");

		hm.addRoom(room);

		hm.modifyRoomName(12345, "Toto");
		assertTrue(room.getName().equals("Toto"));
	}

	@Test
	public void testModifyRoomTemp() {
		Room room = new Room(new byte[128]);
		room.setCurrentID(UUID.randomUUID());
		room.setTemperature(5);

		hm.addRoom(room);

		hm.modifyRoomTemp(12345, 25);
		assertTrue(room.getTemperature() == 25);

		try {
			hm.modifyRoomTemp(12345, 31);
			assertTrue(room.getTemperature() == 25);

			fail();
		} catch (Exception e) {

		}
		try {
			hm.modifyRoomTemp(12345, 4);
			assertTrue(room.getTemperature() == 25);

			fail();
		} catch (Exception e) {

		}

	}

	@Test
	public void testModifyRoomVolume() {
		Room room = new Room(new byte[128]);
		room.setCurrentID(UUID.randomUUID());
		room.setControlVolume(1);

		hm.addRoom(room);

		hm.modifyRoomVolume(12345, 0.5);
		assertTrue(room.getControlVolume() == 0.5);

		try {
			hm.modifyRoomVolume(12345, 1.0001);
			assertTrue(room.getControlVolume() == 0.5);

			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testGetResidentInfo() {
		// One resident
		assertFalse(hm.getResidentInfo().isEmpty());

		// 2 times the same resident
		hm.addResident(TestUtils.generateResident1());
		assertFalse(hm.getResidentInfo().isEmpty());

		// Removes the resident
		hm.removeResident(r);
		assertTrue(hm.getResidentInfo().isEmpty());

	}

	@Test
	public void testModifyPIN() {
		Resident r = TestUtils.generateResident1();

		hm.addResident(r);

		// New PIN
		hm.modifyPIN(hm.getResidentInfo().get(0), r.getPIN(), "12345");
		r = hm.getResidents().get(0);
		assertTrue(r.getPIN().equals("12345"));

		// Same PIN
		hm.modifyPIN(hm.getResidentInfo().get(0), r.getPIN(), "12345");
		assertTrue(r.getPIN().equals("12345"));

		try {
			// Wrong old pin
			hm.modifyPIN(hm.getResidentInfo().get(0), "54321", "12345");
			fail();
		} catch (IllegalAccessError e) {
			assertTrue(r.getPIN().equals("12345"));
		}

		try {
			// Wrong old pin
			hm.modifyPIN(hm.getResidentInfo().get(0), "12345", null);
			fail();
		} catch (Exception e) {
			assertTrue(r.getPIN().equals("12345"));

		}

		// New PIN
		hm.modifyPIN(hm.getResidentInfo().get(0), r.getPIN(), "11111");
		assertTrue(r.getPIN().equals("11111"));
	}

	@Test
	public void testModifyName() {
		Resident r = TestUtils.generateResident1();

		hm.addResident(r);

		// New name
		hm.modifyName(hm.getResidentInfo().get(0), "Antonio", r.getPIN());
		r = hm.getResidents().get(0);
		assertTrue(r.getName().equals("Antonio"));

		// Same name
		hm.modifyName(hm.getResidentInfo().get(0), r.getName(), r.getPIN());
		assertTrue(r.getName().equals("Antonio"));

		try {
			// Wrong pin
			hm.modifyName(hm.getResidentInfo().get(0), "Tony", "33333");
			fail();
		} catch (IllegalAccessError e) {
			assertTrue(r.getName().equals("Antonio"));
		}

		try {
			// null name
			hm.modifyName(hm.getResidentInfo().get(0), null, r.getPIN());
			fail();
		} catch (Exception e) {
			assertTrue(r.getName().equals("Antonio"));

		}

		// New name
		hm.modifyName(hm.getResidentInfo().get(0), "Tony", r.getPIN());
		assertTrue(r.getName().equals("Tony"));
	}

	@Test
	public void testModifyBornDate() {
		Resident r = TestUtils.generateResident1();

		hm.addResident(r);

		// New name
		hm.modifyBornDate(hm.getResidentInfo().get(0), 2, r.getPIN());
		r = hm.getResidents().get(0);
		assertTrue(r.getBornDate() == 2);

		// Same name
		hm.modifyBornDate(hm.getResidentInfo().get(0), 2, r.getPIN());
		assertTrue(r.getBornDate() == 2);

		try {
			// Wrong pin
			hm.modifyBornDate(hm.getResidentInfo().get(0), 4, "33333");
			fail();
		} catch (IllegalAccessError e) {
			assertTrue(r.getBornDate() == 2);
		}

		try {
			// null name
			hm.modifyBornDate(hm.getResidentInfo().get(0), -1, r.getPIN());
			fail();
		} catch (Exception e) {
			assertTrue(r.getBornDate() == 2);

		}

		// New name
		hm.modifyBornDate(hm.getResidentInfo().get(0), 8, r.getPIN());
		assertTrue(r.getBornDate() == 8);
	}

}
