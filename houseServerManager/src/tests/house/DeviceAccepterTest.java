package tests.house;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.house.residents.Resident;
import houseServer.HouseManager;
import houseServer.dataSaving.HouseParametersSaver;
import houseServer.devices.intelligentDevice.IntelligentDevice;
import houseServer.security.DeviceAccepter;
import server.communication.Request;
import server.communication.TransferData;
import server.serialization.SerializationType;
import tests.houseDataCenter.house.TestUtils;

public class DeviceAccepterTest {

	private DeviceAccepter acc;
	private HouseParametersSaver man;
	private HouseManager manager;

	private static final int INTEL = TransferFlags.DEVICE_INTELLIGENT;
	private static final int ROOM = TransferFlags.ROOM_CONTROLLER;

	@Before
	public void setUp() throws Exception {
		man = new HouseParametersSaver(null);
		manager = man.loadManager();

		// Creates empty house manager
		acc = new DeviceAccepter(manager);
	}

	@Test
	public void testDeviceAccepter() {
		try {
			acc = new DeviceAccepter(null);
			fail();
		} catch (NullPointerException e) {

		}
	}

	private byte[] randomUUID() {
		byte[] uuid = new byte[128];
		Random ran = new Random();

		ran.nextBytes(uuid);

		return uuid;

	}

	@Test
	public void testVerify() {

		byte[] uuid1 = randomUUID();

		// Creates a room
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, ROOM }), UUID.randomUUID(), "192.168.2.1",
				SerializationType.JAVA_OBJECT) instanceof Room);

		// Creates a room
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, ROOM }), UUID.randomUUID(), "193.168.2.1",
				SerializationType.JAVA_OBJECT) == null);

		// Attempts creating an intelligent device without password
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, INTEL }), UUID.randomUUID(), "192.168.2.1",
				SerializationType.JAVA_OBJECT) == null);

		// Attempts creating an intelligent device with invalid password
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, INTEL, "76545" }), UUID.randomUUID(), "192.168.2.1",
				SerializationType.JAVA_OBJECT) == null);

		Resident res = TestUtils.generateResident1();
		manager.addResident(res);

		// Try with invalid password again
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, INTEL, res.getEmail(), "76545" }), UUID.randomUUID(),
				"192.168.2.1", SerializationType.JAVA_OBJECT) == null);

		// This resident should have access
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, INTEL, res.getEmail(), res.getPIN() }),
				UUID.randomUUID(), "192.168.2.1", SerializationType.JAVA_OBJECT) instanceof IntelligentDevice);

		manager.removeResident(res);

		// Resident should not have access anymore
		assertTrue(acc.verify(new TransferData(1, new Object[] { uuid1, INTEL, res.getEmail(), res.getPIN() }),
				UUID.randomUUID(), "192.168.2.1", SerializationType.JAVA_OBJECT) == null);

	}

}
