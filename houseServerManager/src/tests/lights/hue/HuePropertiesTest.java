package tests.lights.hue;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import embedded.light.hueLight.HueProperties;

public class HuePropertiesTest {

	private static final String USER_1 = "aBcD6E7fGjbsdv8dfuhvudfyv98";
	private static final String USER_2 = "vndfvg994t8hg948tgeh9g83i4u";
	private static final String IP_1 = "192.168.2.3";
	private static final String IP_2 = "202.163.4.91";

	@Test
	public void testAddNewBridge() {

		HueProperties.loadProperties();

		assertTrue(HueProperties.getUsernames().length == 0);
		assertTrue(HueProperties.getIPs().length == 0);

		HueProperties.addNewBridge(USER_1, IP_1);

		assertTrue(HueProperties.getUsernames().length == 1);
		assertTrue(HueProperties.getIPs().length == 1);
		assertTrue(HueProperties.getUsernames()[0].equals(USER_1));
		assertTrue(HueProperties.getIPs()[0].equals(IP_1));

		HueProperties.addNewBridge(USER_2, IP_2);

		assertTrue(HueProperties.getUsernames().length == 2);
		assertTrue(HueProperties.getIPs().length == 2);
		assertTrue(HueProperties.getUsernames()[0].equals(USER_1));
		assertTrue(HueProperties.getIPs()[0].equals(IP_1));

		assertTrue(HueProperties.getUsernames()[1].equals(USER_2));
		assertTrue(HueProperties.getIPs()[1].equals(IP_2));

		HueProperties.clearBridgesData();

	}

}
