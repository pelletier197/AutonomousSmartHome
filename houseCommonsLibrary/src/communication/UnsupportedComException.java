package communication;

public class UnsupportedComException extends RuntimeException {

	public UnsupportedComException() {

	}

	public UnsupportedComException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedComException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedComException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedComException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
