package communication;

import static communication.ComType.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import serverDataManaging.ConnectionLostListener;
import serverDataManaging.DataReceptionHandler;
import serverDataManaging.DataReceptionWorker;
import serverDataManaging.ServerManager;

public class ServerComManager implements DataReceptionHandler {

	private JSEncoder js;
	private JavaObjectEncoder jobject;

	private DecodedDataReceptionHandler handler;
	private ServerManager server;

	public ServerComManager(ServerManager server, DecodedDataReceptionHandler handler) {

		if (server == null || handler == null) {
			throw new NullPointerException();
		}

		this.server = server;
		this.server.setDataReceptionHandler(this);
		this.handler = handler;
		this.js = new JSEncoder();
		this.jobject = new JavaObjectEncoder();
	}

	public void send(Object obj, UUID clientID, byte comType) {
		try {

			if (comType == JAVA_OBJECT) {
				server.send(jobject.toBytes(obj), clientID);

			} else if (comType == JAVASCRIPT) {
				server.send(js.toBytes(obj), clientID);

			} else {
				throw new UnsupportedComException("The encoding : " + comType + " is not supported");
			}
		} catch (IOException e) {
			// Should not happen
			e.printStackTrace();
		}
	}

	public void sendToAll(Object obj, List<UUID> ids, byte comType) {
		try {
			switch (comType) {
			case JAVA_OBJECT:
				server.sendToAll(jobject.toBytes(obj), ids);
				break;
			case JAVASCRIPT:
				server.sendToAll(js.toBytes(obj), ids);
				break;
			default:
				throw new UnsupportedComException("The encoding : " + comType + " is not supported");
			}
		} catch (IOException e) {
			// Should not happen
			e.printStackTrace();

		}
	}

	public void sendToAll(Object obj, Map<Byte, List<UUID>> comTypeToIds) {
		for (Map.Entry<Byte, List<UUID>> entry : comTypeToIds.entrySet()) {
			sendToAll(obj, entry.getValue(), entry.getKey());
		}
	}

	@Override
	public void received(ByteBuffer data, UUID fromID) {

		// Type of communication
		final byte comType = data.get();

		// The data
		final byte[] dat = new byte[data.remaining()];
		data.get(dat, 0, dat.length);

		// The result
		Object result = null;

		try {
			if (comType == JAVA_OBJECT) {
				result = jobject.fromBytes(dat);
			} else if (comType == JAVASCRIPT) {
				result = js.fromBytes(dat);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Received : " + result);
		// Notifies the receiver
		if (result != null) {
			handler.receive(result, fromID, comType);
		}
	}

	public String getClientIPAddress(UUID fromID) {
		return server.getClientIPAddress(fromID);
	}

	public String getServerExternalIP() {
		return server.getServerExternalIP();
	}

}
