package communication;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.UUID;

import serverDataManaging.ClientManager;
import serverDataManaging.DataReceptionHandler;

public class JavaObjectCommunicator implements DataReceptionHandler {

	private ClientManager manager;
	private JavaObjectEncoder jobject;
	private DecodedDataReceptionHandler handler;

	public JavaObjectCommunicator(ClientManager manager, DecodedDataReceptionHandler handler) {

		if (manager == null || handler == null) {
			throw new NullPointerException("Null not permitted");
		}

		this.manager = manager;
		this.manager.setDataReceptionHandler(this);

		this.handler = handler;
		this.jobject = new JavaObjectEncoder();
	}

	public void send(Object obj) {

		try {
			final byte[] bytes = jobject.toBytes(obj);
			ByteBuffer buf = ByteBuffer.allocate(bytes.length + 1);
			buf.put(ComType.JAVA_OBJECT);
			buf.put(bytes);
			System.out.println("Object to sent : " + obj.toString());

			manager.send(buf.array());

			System.out.println("Object sent : " + obj.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void received(ByteBuffer data, UUID fromID) {

		try {
			Object obj = jobject.fromBytes(data.array());
			System.out.println("Received object :" + obj);
			handler.receive(obj, fromID, ComType.JAVA_OBJECT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setDecodedDataReceptionHandler(DecodedDataReceptionHandler handler) {
		if (handler == null) {
			throw new NullPointerException("Null handler");
		}
		this.handler = handler;
	}
}
