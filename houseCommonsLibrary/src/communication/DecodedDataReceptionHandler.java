package communication;

import java.util.UUID;

public interface DecodedDataReceptionHandler {

	public void receive(Object obj, UUID clientID, byte comType);

}
