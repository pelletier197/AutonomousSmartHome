package communication;

/**
 * Contains the flags for decoding the communication type.
 * 
 * @author sunny
 *
 */
public class ComType {

	public static final byte JAVA_OBJECT = 1;
	public static final byte JAVASCRIPT = 2;
}
