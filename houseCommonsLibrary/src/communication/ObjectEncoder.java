package communication;

import java.io.IOException;

public interface ObjectEncoder {

	public Object fromBytes(byte[] bytes) throws IOException;

	public byte[] toBytes(Object obj) throws IOException;
}
