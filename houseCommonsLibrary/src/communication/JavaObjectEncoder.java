package communication;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class JavaObjectEncoder implements ObjectEncoder {

	public JavaObjectEncoder() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object fromBytes(byte[] bytes) throws IOException {
		// TODO Auto-generated method stub
		try {
			return toObject(bytes);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public byte[] toBytes(Object obj) throws IOException {
		return toData(obj);
	}

	private Object toObject(byte[] array) throws IOException, ClassNotFoundException {

		ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(array));
		Object obj = input.readObject();
		input.close();
		return obj;

	}

	private byte[] toData(Object obj) throws IOException {

		ByteArrayOutputStream str = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(str);

		out.writeObject(obj);

		return str.toByteArray();

	}
}
