package houseDataCenter.applicationRessourceBundle;

import java.util.ResourceBundle;

public class BundleWrapper {

	private static StringPropertiesWrapper resources = null;

	public static void setResourceBundle(ResourceBundle rb) {
		if (rb == null)
			throw new NullPointerException("rb cannot be null");
		resources = new StringPropertiesWrapper();
		resources.setResourceBundle(rb);
	}

	public static String getString(String key) {
		if (resources == null)
			throw new NullPointerException("rb not initialized");

		return resources.getString(key);
	}
}
