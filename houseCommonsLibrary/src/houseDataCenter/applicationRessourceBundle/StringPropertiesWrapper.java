package houseDataCenter.applicationRessourceBundle;

import java.util.ResourceBundle;

public class StringPropertiesWrapper {

	private ResourceBundle resources = null;

	public void setResourceBundle(ResourceBundle rb) {
		if (rb == null)
			throw new NullPointerException("rb cannot be null");
		resources = rb;
	}

	public String getString(String key) {
		if (resources == null)
			throw new NullPointerException("rb not initialized");

		return resources.getString(key);
	}

}
