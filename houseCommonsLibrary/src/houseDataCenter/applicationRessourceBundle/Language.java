package houseDataCenter.applicationRessourceBundle;

import java.util.Locale;
import java.util.ResourceBundle;

public enum Language {

	FRENCH(Locale.FRENCH, "language.french", "Français"), ENGLISH_US(Locale.US, "language.english", "English"), SPANISH(
			new Locale("es"), "language.spanish", "Español"), RUSSIAN(new Locale("ru"), "language.russian", "Pусский");

	private ResourceBundle bundle;
	private String name;
	private Locale local;

	private Language(Locale local, String languagePropertiesPath, String name) {

		bundle = ResourceBundle.getBundle(languagePropertiesPath, new Locale("", "", ""));
		this.local = local;
		this.name = name;
	}

	public String getProperty(String key) {
		return bundle.getString(key);
	}

	@Override
	public String toString() {
		return name;
	}

	public Locale getLocale() {
		// TODO Auto-generated method stub
		return local;
	}
}
