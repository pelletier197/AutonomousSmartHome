package houseDataCenter.applicationRessourceBundle;

public class BundleKey {

	// Mp3 player keys
	public static final String unknownArtist = "unknownArtist";
	public static final String unknownAlbum = "unknownAlbum";
	public static final String unknownSong = "unknownSong";
	public static final String search = "search";
	public static final String artist = "artist";
	public static final String song = "song";
	public static final String album = "album";
	public static final String all = "all";
	public static final String anchor = "anchor";
	public static final String unknownPlaylist = "unknownPlaylist";
	public static final String songs = "songs";

	// Converter keys
	public static final String convertFrom = "convertFrom";
	public static final String distance = "distance";
	public static final String surface = "surface";
	public static final String volume = "volume";
	public static final String speed = "speed";
	public static final String angle = "angle";
	public static final String temperature = "temperature";
	public static final String pressure = "pressure";
	public static final String weight = "weight";
	public static final String time = "time";
	public static final String power = "power";
	public static final String work = "work";
	public static final String force = "force";
	public static final String frequency = "frequency";
	public static final String data = "data";
	public static final String convert = "convert";

	public static final String resident_tooltip = "residentTool";
	public static final String mail_tooltip = "mailTool";
	public static final String creation_timeout_error = "creationTimeout";
	public static final String error_mail = "errorMail";
	public static final String error_resident_pin = "errorResident";

	public static final String pin = "pin";
	public static final String mail = "mail";
	public static final String born_date = "bornDate";
	public static final String born_date_small = "bornDateSmall";
	public static final String name = "name";
	public static final String pin_confirm = "pinConfirm";
	public static final String creation_failure_borndate = "creationFailureBorndate";
	public static final String creation_failure_mail = "creationFailureMail";
	public static final String creation_failure_name = "creationFailureName";
	public static final String creation_failure_photos = "creationFailurePhotos";
	public static final String creation_failure_pin = "creationFailurePIN";
	public static final String creation_failure_mail_used = "creationFailureMailUsed";

	public static final String add_lights = "addLights";
	public static final String remove_lights = "removeLights";
	public static final String invalid_pin = "invalidPIN";
	public static final String finish = "finish";
	public static final String confirm_resident_pin = "confirmResidentPIN";
	public static final String no_name_light = "noNameLight";

	public static final String monday = "monday";
	public static final String tuesday = "tuesday";
	public static final String wednesday = "wednesday";
	public static final String thursday = "thursday";
	public static final String friday = "friday";
	public static final String saturday = "saturday";
	public static final String sunday = "sunday";
	public static final String all_days = "allDays";

	public static final String resident_validation = "resident_validation";
	public static final String mail_validation = "mail_validation";
	public static final String resident_confirmation = "resident_confirmation";

	public static final String new_resident = "new_resident";
	public static final String select_music = "select_music";
	public static final String or = "or";
	public static final String select_alarm = "select_alarm";

	public static final String room_namme = "room_namme";
	public static final String temp_unit = "temp_unit";
	public static final String language_displayed = "language_displayed";
	public static final String confirmation_text_change_server_lang = "confirmation_text_change_server_lang";
	public static final String server_language = "server_language";
	public static final String confirmation_delete_alarm = "confirmation_delete_alarm";
	public static final String once = "once";
}
 