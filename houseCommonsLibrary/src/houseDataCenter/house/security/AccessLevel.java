package houseDataCenter.house.security;

public enum AccessLevel {
	REGULAR(1), ADMINISTRATOR(2);

	private int power;

	private AccessLevel(int power) {
		this.power = power;
	}

	/**
	 * Tells if the current access level is high enough to access a given
	 * Functionality.
	 * 
	 * <p>
	 * This function is used to give certain users specific rights, but that
	 * they must have the same rights than a lower access level.
	 * </p>
	 * 
	 * @param minimumAccessRight
	 * @return
	 */
	public boolean isAccessAuthorized(AccessLevel minimumAccessRight) {
		return this.power >= minimumAccessRight.power;
	}

	/**
	 * Returns true if the current AccessLevel is any of the following array of
	 * levels.
	 */
	public boolean isAny(AccessLevel... accessLevels) {

		for (AccessLevel lev : accessLevels) {
			if (lev == this) {
				return true;
			}
		}

		return false;
	}

}
