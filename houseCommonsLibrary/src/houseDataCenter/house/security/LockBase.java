package houseDataCenter.house.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.residents.Resident;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * <p>
 * Controls house locking center. Normally, no parameter should be modified if
 * this locker does no give the access. The access to the house will only be
 * granted if the recognition of a given resident is allowed, either via its
 * physical traits (fingerprint AND facial recognition) or its PIN
 * identification number.
 * </p>
 * 
 * <p>
 * To be allowed to access the house via a certain type of recognition, use
 * {@link #nextFace(byte[])}, {@link #nextFinger(byte[][])} and
 * {@link #nextPIN(String)}. Once one of this method is called, if
 * {@link #unlock()} is not called before the specified {@link #TIMEOUT}, the
 * access will be cancelled and next values will be returned to null. During
 * recognition, the PIN is always used over physic recognition, because more
 * reliable.
 * </p>
 * 
 * <p>
 * To allow a given Resident to unlock the house, he must be added as an
 * unlocker via {@link #addUnlocker(Resident)}.
 */
public class LockBase {

	/**
	 * The maximum of tentative a user can do to unlock the house before it
	 * blocks completely.
	 */
	public static final int MAX_MISSED_ATTEMPTS = 5;

	/**
	 * {@value #TIMEOUT} milliseconds of timeout between sets. If unlock() is
	 * not called between this timeout value in milliseconds, the unlock will be
	 * automatically cancelled.
	 */
	private static final long TIMEOUT = 5000L;

	/**
	 * Constant holding a value for indefinite unlock time. Setting the unlock
	 * time to this value would keep the house unlocked until {@link #lock()}
	 * gets called.
	 */
	public static final long INDEFINITE_UNLOCK = -1;


	/**
	 * The next PIN that will be used during the next {@link #unlock()} call. If
	 * the PIN itself correspond to one of the PIN usable by a given Resident.
	 * If an attempt to unlock is missed 2 times via a PIN, the unlock procedure
	 * will be cancelled until #manualUnlock is called.
	 */
	private String nextPin;

	/**
	 * List of the available residents that can unlock the house via
	 * {@link #unlock()}. To be able to unlock the house, the face must match,
	 * or the PIN.
	 */
	private List<Resident> unlockers;

	/**
	 * The number of attempt currently missed. Once this number reaches 3 or
	 * more, an exception will be thrown while calling {@link #unlock()}. To
	 * reset this number to zero, a resident will have to unlock manually, then
	 * {@link #manualUnlock()} may be called.
	 */
	protected int attempt;

	/**
	 * The time at which the last next was set. If time given by
	 * {@link #TIMEOUT} is passed during the next {@link #unlock()} call, the
	 * result will automatically be null, so the unlock will not be allowed and
	 * the next values will be set to null.
	 */
	private long lastNextSet;

	/**
	 * Boolean telling either the house is the unlocked state or the locked
	 * state. While being in the locked state, the access to the house should
	 * not be authorized. The unlocked state is temporary and can be attributed
	 * via {@link #setUnlockTime(long)}.
	 */
	protected BooleanProperty unlocked;

	/**
	 * The unlock time. This value represents the sleep time given when
	 * {@link #unlock()} is called before the access is closed back again. If
	 * this value is changed while the house is unlocked, changing it will have
	 * no effect.
	 */
	private long unlockTime;


	/**
	 * Constructs a house lock for a house. This class should mainly be used to
	 * protect a given house from strangers to come in, either via a PIN, or via
	 * facial and finger print recognition.
	 * 
	 * The unlock time represents the time during which the house will be
	 * unlocked when {@link #unlock()} will be called with a successful result.
	 * This time can be set to indefinite, by calling the constructor using
	 * {@link #INDEFINITE_UNLOCK} {@link #unlock()}
	 * 
	 * @param unlockTime
	 *            The time that the house is unlocked when calling
	 *            {@link #unlock()} with positive result.
	 * @param allowFacialRecognition
	 *            Tells either facial recognition is allowed or not on the
	 *            server.
	 */
	public LockBase(long unlockTime) {

		if (unlockTime < 0 && unlockTime != INDEFINITE_UNLOCK) {
			throw new IllegalArgumentException("Cannot unlock for a negative time");
		}

		unlockers = new ArrayList<>();

		attempt = 0;

		this.unlockTime = unlockTime;

		unlocked = new SimpleBooleanProperty(false);
	}

	/**
	 * Add the given unlocker to the available unlockers that can open the
	 * house. Those unlockers are recognized using either PIN recognition or
	 * facial and fingerprint recognition when {@link #unlock()} is called with
	 * the given next parameter.
	 * 
	 * @see #nextPIN(String)
	 * @see #nextFace(byte[])
	 * @see #nextFinger(byte[][])
	 * 
	 * @param resident
	 *            The added unlocker that may open the house.
	 */
	public synchronized void addUnlocker(Resident resident) {
		if (resident == null) {
			throw new NullPointerException("Cannot add a null resident");
		}
		this.unlockers.add(resident);
	}

	/**
	 * Returns an unmodifiable list of all the residents allowed to unlock the
	 * house to access given parameters.
	 * 
	 * @return An unmodifiable list of unlockers
	 */
	public List<Resident> getUnlockers() {
		return Collections.unmodifiableList(unlockers);
	}

	public void nextPIN(String PIN) {
		checkTimeout();
		this.nextPin = PIN;
		// Resets the timeout time
		this.lastNextSet = System.currentTimeMillis();
	}

	private void checkTimeout() {
		if (System.currentTimeMillis() - lastNextSet > TIMEOUT) {
			clearNexts();
		}
	}

	private void clearNexts() {
		nextPin = null;
	}

	public Resident unlock() throws AccessRefusedError {
		Resident unlocker = null;
		if (!unlocked.get()) {
			// Check if the number of attempt is passed.
			if (attempt > MAX_MISSED_ATTEMPTS - 1) {
				System.out.println("Locker exception");
				throw new AccessRefusedError("The house cannot be unlocked");
			} 

			// Check to know if unlock may be allowed. This will occur if the
			// timeout is not completed.
			checkTimeout();

			unlocker = identifyFromPIN();

			// Clears old input. The user will need to enter again if he wants
			// to.
			clearNexts();

			System.out.println(unlocker);

			if (unlocker != null) {
				attempt = 0;
				unlocked.set(true);

				// If the unlock time is finite.
				if (unlockTime != INDEFINITE_UNLOCK) {
					// During a x second interval, the house is unlocked.
					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(unlockTime);
							} catch (Exception e) {
								e.printStackTrace();
							}

							unlocked.set(false);
						}
					}).start();

				}
			}
		}
		return unlocker;
	}

	public void setUnlockTime(long time) {
		if (time < 0 && time != INDEFINITE_UNLOCK) {
			throw new IllegalArgumentException("Cannot unlock for a negative time");
		}
		this.unlockTime = time;
	}

	private Resident identifyFromPIN() {

		if (nextPin != null && !nextPin.isEmpty()) {
			for (Resident r : unlockers) {
				if (r.getPIN().equals(nextPin)) {
					return r;
				}
			}
			// On attempt to identify from PIN fail, we increase the number of
			// try. The PIN must be different from null to be sure it is an
			// attempt.
			attempt++;
		}

		return null;
	}

	public boolean isUnlocked() {
		return unlocked.get();
	}

	public ReadOnlyBooleanProperty unlockedProperty() {
		return BooleanProperty.readOnlyBooleanProperty(unlocked);
	}

	public void lock() {
		attempt = 0;
		unlocked.set(false);

	}

	public void removeResident(Resident resident) {
		unlockers.remove(resident);
	}
}
