package houseDataCenter.house.security;

public class DoorLock extends LockBase {

	public static final long DEFAULT_UNLOCK = 5000;

	public DoorLock() {
		super(DEFAULT_UNLOCK);
	}

	public void manualUnlock() {
		// Resets the number of attempts to zero, so #unlock can be called
		// again.
		super.attempt = 0;
		super.unlocked.set(false);

	}
}
