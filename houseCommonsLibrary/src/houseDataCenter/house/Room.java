package houseDataCenter.house;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import embedded.controllers.camera.AbstractCamera;
import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.ColorisableLight;
import embedded.controllers.light.DimmableLight;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.data.TempDisplay;
import server.serialization.SerializationType;

public class Room extends Device {

	public static final byte ROOM_COM_TYPE = SerializationType.JAVA_OBJECT;
	
	/**
	 * Max temperature allowed for a room.
	 */
	public static final double MAX_TEMP = 30d;
	/**
	 * The minimum temperature for the room.
	 */
	public static final double MIN_TEMP = 5d;
	
	public static final TempDisplay DEFAULT_TEMP = TempDisplay.TEMP_CELCIUS;

	private static final long serialVersionUID = 4696508934896570501L;

	/**
	 * The room's name. Must be set manually.
	 */
	private String roomName;

	/**
	 * The current temperature of the room.
	 */
	private double temperature;

	/**
	 * The audio volume in the room.
	 */
	private double controlVolume;

	/**
	 * A list of the lights available for this room. Those rooms have to be
	 * registered manually.
	 */
	private transient List<AbstractLight> lights;

	/**
	 * The cameras associated to the room
	 */
	private List<AbstractCamera> cams;

	/**
	 * The number of cameras registered to the room.
	 */
	private int cameraCount;

	/**
	 * The display temperature.
	 */
	private TempDisplay displayTemp;

	/**
	 * Constructs a new room that will have the given UUID to identify it among
	 * other rooms.
	 * 
	 * <p>
	 * This UUID must be unique to allow, so the room will be recognisable.
	 * </p>
	 * 
	 * 
	 * @param UUID
	 *            Room's UUID.
	 */
	public Room(byte[] UUID) {
		super(UUID);

		this.temperature = MIN_TEMP;
		this.roomName = null;
		this.controlVolume = 1;
		this.lights = new ArrayList<>();
		this.displayTemp = DEFAULT_TEMP;
	}

	/**
	 * 
	 * @return The room's name. May be null.
	 */
	public String getName() {
		return roomName;
	}

	/**
	 * Sets the room name to the given name. May be null.
	 * 
	 * @param name
	 *            The new room's name
	 */
	public void setName(String name) {

		this.roomName = name;
	}

	/**
	 * 
	 * @return Room's temperature in celcius.
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * Sets the room temperature in celcius. The temperature must be between
	 * {@value #MIN_TEMP} and {@value #MAX_TEMP}.
	 * 
	 * @param temperature
	 *            The temperature of the room in celcius.
	 */
	public void setTemperature(double temperature) {

		if (temperature <= MAX_TEMP && temperature >= MIN_TEMP) {
			this.temperature = temperature;
		} else {
			throw new IllegalArgumentException("Illegal temperature for thermostat");
		}
	}

	/**
	 * 
	 * @return A list of all the available lights for the room.
	 */
	public List<AbstractLight> getLights() {
		return new ArrayList<>(lights);
	}

	/**
	 * 
	 * @return The control volume of the room between {0,1]
	 */
	public double getControlVolume() {
		return controlVolume;
	}

	/**
	 * Sets the control volume of the room. The control volume must be in the
	 * range of {0,1]
	 * 
	 * @param controlVolume
	 */
	public void setControlVolume(double controlVolume) {
		if (controlVolume > 1 || controlVolume < 0) {
			throw new IllegalArgumentException("Illegal volume : Value must be between 0 and 1 include");
		}
		this.controlVolume = controlVolume;
	}

	/**
	 * Sets the temperature down of 0.5 Celcius if the new temperature is in the
	 * valide range.
	 */
	public void tempDown() {
		temperature -= 0.5;

		if (temperature < MIN_TEMP) {
			temperature = MIN_TEMP;
		}
	}

	/**
	 * Sets the temperature up of 0.5 Celcius if the new temperature is in the
	 * valid range.
	 */
	public void tempUp() {
		temperature += 0.5;

		if (temperature > MAX_TEMP) {
			temperature = MAX_TEMP;
		}
	}
	
	

	public TempDisplay getDisplayTemp() {
		return displayTemp;
	}

	public void setDisplayTemp(TempDisplay displayTemp) {
		if(displayTemp ==null){
			throw new NullPointerException("Null display temp");
		}
		this.displayTemp = displayTemp;
	}

	public void addLight(AbstractLight light) {
		this.lights.add(light);
	}

	public List<LightData> getLightsData() {

		List<LightData> res = new ArrayList<>();

		for (AbstractLight l : lights) {
			res.add(new LightData(l));
		}

		return res;
	}

	public void addAllLights(List<AbstractLight> add) {
		lights.addAll(add);
		System.out.println(lights + " added lights");

	}

	public void removeAllLights(List<AbstractLight> remove) {

		lights.removeAll(remove);
		System.out.println(lights + " remove lights");

	}

	public void openAllLights() {

		for (AbstractLight li : lights) {
			li.open();
		}
	}

	public void closeAllLights() {

		for (AbstractLight li : lights) {
			li.close();
		}
	}

	public void reassingLight(AbstractLight old, AbstractLight newL) {

		lights.remove(old);
		lights.add(newL);
	}

	public void setAllLightsColor(int value) {
		for (AbstractLight l : lights) {
			if (l instanceof ColorisableLight) {
				((ColorisableLight) l).setColor(value);
			}
		}
	}

	public void resetAllLightsColor() {
		for (AbstractLight l : lights) {
			if (l instanceof ColorisableLight) {
				((ColorisableLight) l).resetColor();
			}
		}
	}

	public void setAllLightsLuminosity(double value) {
		for (AbstractLight l : lights) {
			if (l instanceof DimmableLight) {
				((DimmableLight) l).setBrightness(value);
			}
		}
	}

	public void setLightColor(String id, int color) {
		AbstractLight light = getLightById(id);

		if (light instanceof ColorisableLight) {
			((ColorisableLight) light).setColor(color);
		}
	}

	private AbstractLight getLightById(String id) {
		for (AbstractLight li : lights) {
			if (li.getLightID().equals(id)) {
				return li;
			}
		}
		return null;

	}

	public void openLight(String id) {
		AbstractLight light = getLightById(id);
		light.open();
	}

	public void closeLight(String id) {
		AbstractLight light = getLightById(id);
		light.close();
	}

	public void setLightLuminosity(String id, double value) {
		AbstractLight light = getLightById(id);

		if (light instanceof DimmableLight) {
			((DimmableLight) light).setBrightness(value);
		}
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		// default deserialization
		ois.defaultReadObject();
		lights = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "Room [roomName=" + roomName + ", temperature=" + temperature + ", controlVolume=" + controlVolume
				+ ", lights=" + lights + ", cams=" + cams + ", cameraCount=" + cameraCount + ", getUUID()=" + getUUID()
				+ "[length = " + getUUID().length + "]]";
	}

	public void setLights(List<AbstractLight> roomLights) {
		this.lights = new ArrayList<>(roomLights);
	}

	public void volumeUp() {
		if (controlVolume + 0.05 > 1)
			controlVolume = 1;
		else
			controlVolume += 0.05;
	}

	public void volumeDown() {
		if (controlVolume - 0.05 < 0)
			controlVolume = 0;
		else
			controlVolume -= 0.05;
	}

}
