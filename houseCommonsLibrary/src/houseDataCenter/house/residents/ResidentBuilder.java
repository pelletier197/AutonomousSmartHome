package houseDataCenter.house.residents;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import houseDataCenter.house.exceptions.InvalidBornDate;
import houseDataCenter.house.exceptions.InvalidEmailException;
import houseDataCenter.house.exceptions.InvalidNameException;
import houseDataCenter.house.exceptions.InvalidPINException;
import houseDataCenter.house.exceptions.PictureErrorException;
 
public class ResidentBuilder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3987636069110853538L;

	private long bornDate;
	private String name;
	private String PIN;

	private String confirm;

	private String email;

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setBornDate(long bornDate) {
		this.bornDate = bornDate;
	}

	public long getBornDate() {
		return this.bornDate;
	}

	public void setPIN(String PIN) {
		this.PIN = PIN;
	}

	public void setPINConfirm(String confirm) {
		this.confirm = confirm;
	}

	public void setEmail(String mail) {
		this.email = mail;
	}

	public void clear() {
		this.name = null;
		this.bornDate = 0;
		this.PIN = null;
		this.confirm = null;
	}

	public Resident build() throws InvalidBornDate, InvalidNameException, InvalidPINException, InvalidEmailException {

		if (!PIN.equals(confirm)) {
			throw new InvalidParameterException();
		}
		Resident result = new Resident(name, bornDate, PIN, email);

		clear();

		return result;
	}

	@Override
	public String toString() {
		return "ResidentBuilder [bornDate=" + bornDate + ", name=" + name + ", PIN=" + PIN + ", confirm=" + confirm
				+ ", email=" + email + "]";
	}

}
