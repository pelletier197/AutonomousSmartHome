package houseDataCenter.house.residents;

import java.io.Serializable;
import java.util.UUID;

import houseDataCenter.house.exceptions.InvalidBornDate;
import houseDataCenter.house.exceptions.InvalidEmailException;
import houseDataCenter.house.exceptions.InvalidNameException;
import houseDataCenter.house.exceptions.InvalidPINException;

/**
 * House reserved class packaging all data reserved for house's residents.
 * Residents have to register their personal information about their face,
 * finger prints and other information.
 * 
 * All those information about the resident are used to ensure easy recognition
 * of him.
 * 
 * @author sunny
 *
 */
public class Resident implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2563953433038031938L;

	/**
	 * 5 Number PIN
	 */
	public static final int PIN_LENGTH = 5;

	/**
	 * Resident's name given at construction. This value may not be changed.
	 */
	private String name;

	/**
	 * Born date given at construction. This value may be changed.
	 */
	private long bornDate;

	/**
	 * Person's age. Computed every time {@link #getAge()} is called.
	 */
	private long age;

	/**
	 * The personal identification number of the resident used to access house
	 * parameters. This PIN will always have the length of {@link #PIN_LENGTH}.
	 */
	private String PIN;

	/**
	 * Resident's Personal email. This email will need to be confirmed by
	 * external application later.
	 */
	private String email;

	/**
	 * The UUID of the resident
	 */
	private UUID residentID;

	protected Resident(String name, long bornDate, String PIN, String email)
			throws InvalidBornDate, InvalidNameException, InvalidPINException, InvalidEmailException {
		if (!validName(name)) {
			throw new InvalidNameException("The name is not a valid name.");
		}
		if (!validBorn(bornDate)) {
			throw new InvalidBornDate("The born date is not valid.");
		}

		if (!validEmail(email)) {
			throw new InvalidEmailException("Invalid email");
		}

		if (!validPIN(PIN)) {
			throw new InvalidPINException("Invalid PIN");
		}

		this.name = formatName(name);
		this.bornDate = bornDate;
		this.email = email.trim();
		setPIN(null, PIN);
		this.residentID = UUID.randomUUID();
	}

	private boolean validEmail(String mail) {
		return mail.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$");
	}

	/**
	 * Validate that the given born is a valid born, which is the case whem the
	 * current time > bornDate
	 * 
	 * @param bornDate
	 *            The given borndate.
	 * @return True if the date is valid, false otherwise.
	 */
	private boolean validBorn(long bornDate) {

		return System.currentTimeMillis() > bornDate && bornDate > 0;

	}

	/**
	 * Valid the name to ensure that it only contains letters and hyphens.
	 * 
	 * @param name
	 *            The name to be validated
	 * @return True if the name is valid, false otherwise.
	 */
	private boolean validName(String name) {

		return !name.trim().isEmpty() && name != null && name.matches("^[\\p{L} .'-]+$");
	}

	/**
	 * Format the given name to a regular name format. This function will
	 * capitalize every letter directly after - and spaces, and lower cases the
	 * rest of the expression.
	 * 
	 * @param name
	 * @return
	 */
	private String formatName(String name) {

		StringBuilder nameBuilder = new StringBuilder();
		char[] charName = name.trim().toLowerCase().toCharArray();
		boolean capitalizeNext = true;

		for (char c : charName) {
			if (c == ' ' || c == '-') {
				// The next character must be capitalized
				capitalizeNext = true;
				nameBuilder.append(c);

			} else if (capitalizeNext) {
				// Capitalizes the next character
				nameBuilder.append(Character.toUpperCase(c));
				capitalizeNext = false;

			} else {
				nameBuilder.append(c);
			}
		}
		return nameBuilder.toString();
	}

	/**
	 * Computes the current age of the resident by subtracting the current time
	 * to the born date given at construction.
	 */
	private void computeAge() {
		this.age = System.currentTimeMillis() - bornDate;
	}

	/**
	 * 
	 * @return The current age of the resident in milliseconds.
	 */
	public long getAge() {
		computeAge();
		return age;
	}

	/**
	 * 
	 * @return The born date given at construction.
	 */
	public long getBornDate() {
		return bornDate;
	}

	/**
	 * Retrieves the name of the resident. This name might be different than the
	 * one given at construction. The name may have been formatted to match a
	 * regular name path, with capital at the beginning of name parts.
	 * 
	 * @return The formatted name of the resident.
	 */
	public String getName() {
		return new String(name);
	}

	public String getEmail() {
		return email;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String oldPIN, String newPIN) {
		// Set protected because it can only be set once the house is unlocked
		// for security reasons.
		if (!validPIN(newPIN)) {
			throw new IllegalArgumentException("Invalid PIN");
		}
		// Only allow modification if oldPIN Matches currentPIN or null at
		// construction
		if (this.PIN == null || oldPIN.equals(this.PIN)) {
			this.PIN = newPIN;
		} else {
			throw new IllegalAccessError("Invalid old PIN");
		}
	}

	private boolean validPIN(String PIN) {
		return PIN != null && PIN.matches("\\d{5}") && PIN.length() == PIN_LENGTH;
	}

	@Override
	public String toString() {
		return "Resident [name=" + name + ", bornDate=" + bornDate + ", age=" + age + ", PIN=" + PIN + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((residentID == null) ? 0 : residentID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resident other = (Resident) obj;

		return other.residentID.equals(this.residentID);
	}

	public void setName(String name, String PIN) {

		if (PIN.equals(this.PIN)) {
			if (validName(name)) {
				this.name = name;
			} else {
				throw new IllegalArgumentException("Invalid name");
			}

		} else {
			throw new IllegalAccessError("Invalid PIN");
		}
	}

	public void setBornDate(long bornDate, String PIN) {
		if (!validBorn(bornDate)) {
			throw new IllegalArgumentException("born date is invalid");
		}
		if (!PIN.equals(this.PIN)) {
			throw new IllegalAccessError("PIN is invalid");

		}
		this.bornDate = bornDate;
	}

	/**
	 * Changes the resident's UUID. This method should never be called outside
	 * reading the object and constructing it again.
	 * 
	 * @param id
	 */
	@Deprecated
	public void changeUUID(UUID id) {
		this.residentID = id;
	}

	public UUID getUUID() {
		return residentID;
	}
}
