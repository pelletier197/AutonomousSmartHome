package houseDataCenter.house.exceptions;

public class AccessRefusedError extends RuntimeException {

	public AccessRefusedError() {
		super();
	}

	public AccessRefusedError(String message) {
		super(message);
	}

}
