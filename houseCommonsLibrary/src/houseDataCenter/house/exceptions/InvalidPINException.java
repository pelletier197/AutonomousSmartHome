package houseDataCenter.house.exceptions;

public class InvalidPINException extends Exception{

	public InvalidPINException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidPINException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidPINException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidPINException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidPINException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
