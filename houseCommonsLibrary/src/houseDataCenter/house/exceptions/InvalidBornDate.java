package houseDataCenter.house.exceptions;

public class InvalidBornDate extends Exception{

	public InvalidBornDate() {
		super();
	}

	public InvalidBornDate(String message) {
		super(message);
	}

	
}
