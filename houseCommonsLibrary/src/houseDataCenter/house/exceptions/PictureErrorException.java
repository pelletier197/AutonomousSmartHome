package houseDataCenter.house.exceptions;

public class PictureErrorException extends Exception{

	public PictureErrorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PictureErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PictureErrorException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PictureErrorException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PictureErrorException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
