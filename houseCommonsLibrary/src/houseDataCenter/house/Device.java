package houseDataCenter.house;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

/**
 * The interface for a device that is used to control the house. This is used to
 * contain common methods between IntelligentDevice and Room. Both devices must
 * register sending their UUID, and a client ID is given to them then.
 * 
 * @author sunny
 *
 */
public class Device implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6116472871709378674L;

	/**
	 * Unknown id flag
	 */
	public static final UUID UNKNOWN_ID = null;

	/**
	 * The current ID of the device. Transient because is changed every time the
	 * client is connected.
	 */
	private transient UUID currentID = null;

	/**
	 * The UUID of the device.
	 */
	private byte[] uuid;

	/**
	 * Transient variable used to tell if the device's id has been set for the
	 * current connection.
	 */
	private transient boolean idSet = false;

	/**
	 * Creates a device controlled object, using the given UUID as principal
	 * identifier for a device.
	 * 
	 * @param uuid
	 *            The UUID used to identify a device.
	 */
	public Device(byte[] uuid) {
		if (uuid == null) {
			throw new NullPointerException("Cannot construct from null UUID");
		}
		this.uuid = uuid.clone();
	}

	/**
	 * 
	 * @return The current ID of the program. This value will be equal to
	 *         {@link #UNKNOWN_ID} until the ID is set for the current
	 *         connection.
	 */
	public java.util.UUID getCurrentID() {
		return currentID;
	}

	/**
	 * Sets the current ID of the device. Once this method is called,
	 * {@link #isIdSet()} will always return true.
	 * 
	 * @param id
	 *            The new ID of the program
	 */
	public void setCurrentID(UUID id) {
		this.currentID = id;
		this.idSet = true;
	}

	/**
	 * Tells wither if the ID of the device has been set for the current usage
	 * of the program.
	 * 
	 * @return True if the device ID has been set, false otherwise.
	 */
	public boolean isIdSet() {
		return idSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + uuid.hashCode();
		return result;
	}

	/**
	 * 
	 * @return A copy of the device's UUID.
	 */
	public byte[] getUUID() {
		return uuid.clone();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (Arrays.equals(this.uuid, other.uuid))
			return true;
		else
			return false;

	}

}
