package houseDataCenter.house.notifications;

import java.util.UUID;

public enum NotificationCause {

	SERVER_BLOCKED, NEW_RESIDENT, RESIDENT_DELETED, ALERT_FIRE, ALERT_ALARM

}
