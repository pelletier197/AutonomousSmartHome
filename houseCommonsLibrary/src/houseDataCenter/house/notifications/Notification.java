package houseDataCenter.house.notifications;

import java.util.UUID;

public class Notification {

	private NotificationCause cause;
	private long time;
	private UUID id;

	public Notification(NotificationCause cause, long time) {
		this.cause = cause;
		this.time = time;
		this.id = UUID.randomUUID();
	}

	public NotificationCause getCause() {
		return cause;
	}

	public long getTime() {
		return time;
	}

	/**
	 * Changes the UUID of the notification. This method should never be used
	 * except when loading the notification from a serialized object.
	 * 
	 * @param uuid
	 *            the uuid of the object
	 */
	@Deprecated
	public void changeUUID(UUID uuid) {
		this.id = uuid;
	}

	@Override
	public String toString() {
		return "Notification [cause=" + cause + ", time=" + time + ", id=" + id + "]";
	}

	public UUID getUUID() {
		return id;
	}
}
