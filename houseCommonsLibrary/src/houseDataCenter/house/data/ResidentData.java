package houseDataCenter.house.data;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.opencv.core.MatOfByte;

import com.fasterxml.jackson.annotation.JsonProperty.Access;

import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.security.AccessLevel;
import utils.HouseUtils;

import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_core.*;

public class ResidentData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8470848325337011115L;
	public final String name;
	public final long bornDate;
	public final String mail;
	public AccessLevel access;

	public ResidentData(Resident resident) {

		this.name = resident.getName();
		this.bornDate = resident.getBornDate();
		this.mail = censorMail(resident.getEmail());
	}

	private String censorMail(String mail) {
		String sub = mail.substring(mail.indexOf("@"), mail.length());
		
		return "*******" + sub;
	}

}
