package houseDataCenter.house.data;

import java.io.Serializable;

import embedded.controllers.light.AbstractLight;
import embedded.controllers.light.ColorisableLight;
import embedded.controllers.light.DimmableLight;

public class LightData implements Serializable {

	private String name;
	private String id;
	private int color;
	private boolean open;
	private double luminosity = 1.0;
	private boolean colorisable;
	private boolean dimmable;

	public LightData(AbstractLight light) {
		this.name = light.getName();
		this.id = light.getLightID();
		this.open = light.isOpen();

		if (light instanceof ColorisableLight) {
			colorisable = true;
			color = ((ColorisableLight) light).getColor();
		}

		if (light instanceof DimmableLight) {
			this.dimmable = true;
			this.luminosity = ((DimmableLight) light).getBrightness();

			System.out.println("Brightness : " + luminosity);
		}
	}

	public void updateColor(int color) {
		this.color = color;
	}

	public void updateLuminosity(double luminosity) {
		this.luminosity = luminosity;
	}

	public void updateOpen(boolean value) {
		this.open = value;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public int getColor() {
		return color;
	}

	public boolean isOpen() {
		return open;
	}

	public double getLuminosity() {
		return luminosity;
	}

	public boolean isColorisable() {
		return colorisable;
	}

	public boolean isDimmable() {
		return dimmable;
	}

	public void updateName(String name) {
		System.out.println("Name updated : "+ name);
		this.name = name;
	}

	
	@Override
	public String toString() {
		return "LightData [name=" + name + ", id=" + id + ", color=" + color + ", open=" + open + ", luminosity="
				+ luminosity + ", colorisable=" + colorisable + ", dimmable=" + dimmable + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LightData other = (LightData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
