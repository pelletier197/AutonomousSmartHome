package houseDataCenter.house.data;

import houseDataCenter.math.unitConversion.Unit;

public enum TempDisplay {
	TEMP_FARA(Unit.C), TEMP_CELCIUS(Unit.F);
	
	private Unit associatedUnit;
	
	private TempDisplay(Unit unit){
		this.associatedUnit = unit;
	}
	public Unit getAssociatedUnit(){
		return associatedUnit;
	}
}
