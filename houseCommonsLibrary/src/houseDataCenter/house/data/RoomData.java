package houseDataCenter.house.data;

import java.io.Serializable;
import java.util.Formatter;
import java.util.UUID;

import houseDataCenter.house.Room;
import houseDataCenter.math.unitConversion.CTempConverter;
import houseDataCenter.math.unitConversion.Unit;
import src.main.java.com.mpatric.mp3agic.NotSupportedException;
import utils.Formatters;

public class RoomData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4608249513413878268L;

	private static final String CELCIUS = "°C", FARANEITH = "°F";

	public String name;
	public double volume;
	public double temp;
	public UUID currentID;
	public TempDisplay displayUnit;

	public RoomData(final Room room) {

		if ((currentID = room.getCurrentID()) == null) {
			throw new IllegalStateException("The room has not connected to the server");
		}

		this.name = room.getName();
		this.volume = room.getControlVolume();
		this.temp = room.getTemperature();
		this.displayUnit = room.getDisplayTemp();

	}

	public String getDisplayTemperature() {
		switch (displayUnit) {
		case TEMP_CELCIUS:
			return Formatters.doubleFormat(temp, 1) + CELCIUS;
		case TEMP_FARA:
			return Formatters.doubleFormat(new CTempConverter().convert(temp, Unit.C, Unit.F), 1) + FARANEITH;
		default:
			throw new RuntimeException("Cannot convert to this unit");
		}
	}

	@Override
	public String toString() {
		return "RoomData [name=" + name + ", volume=" + volume + ", temp=" + temp + ", currentID=" + currentID + "]";
	}

}
