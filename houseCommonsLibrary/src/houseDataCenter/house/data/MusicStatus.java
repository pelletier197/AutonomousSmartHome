package houseDataCenter.house.data;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import javax.imageio.ImageIO;

import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistPlayer.READING_MODE;
import utils.Compresser;
import utils.HouseUtils;

public class MusicStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8697486153754980707L;

	public final String name;
	public final String album;
	public final String artist;
	public final byte[] cover;
	public final boolean playing;
	public final READING_MODE mode;

	public MusicStatus(Mp3SongInfo song, boolean playing, READING_MODE mode) {

		this.name = song.getTitle();
		this.album = song.getAlbumName();
		this.artist = song.getArtistName();
		this.cover = Compresser.compressFast(song.getAlbumCover());
		this.playing = playing;
		this.mode = mode;
	}

	public MusicStatus(Mp3SongInfo song, boolean playing, READING_MODE mode, int picWidth, int picHeight) {

		this.name = song.getTitle();
		this.album = song.getAlbumName();
		this.artist = song.getArtistName();
		this.cover = Compresser.compressFast(song.getAlbumCover());
		this.playing = playing;
		this.mode = mode;
	}


	private byte[] resized(byte[] albumCover, int width, int height) {
		ByteArrayInputStream stream = new ByteArrayInputStream(albumCover);

		try {
			BufferedImage img = ImageIO.read(stream);

			// Check if resize may lower the size of the image
			if (img.getWidth() > width || img.getHeight() > height) {

				Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
				BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

				ByteArrayOutputStream buffer = new ByteArrayOutputStream();

				ImageIO.write(imageBuff, "jpg", buffer);

				return buffer.toByteArray();

			} else {
				return albumCover;
			}

		} catch (IOException e) {

		}

		return albumCover;
	}

	@Override
	public String toString() {
		return "MusicStatus [name=" + name + ", album=" + album + ", artist=" + artist + ", playing=" + playing + "]";
	}

}
