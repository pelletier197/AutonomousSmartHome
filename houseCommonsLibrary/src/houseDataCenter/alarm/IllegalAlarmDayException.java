package houseDataCenter.alarm;

public class IllegalAlarmDayException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2409445161904712817L;

	public IllegalAlarmDayException() {
		// TODO Auto-generated constructor stub
	}

	public IllegalAlarmDayException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public IllegalAlarmDayException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public IllegalAlarmDayException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public IllegalAlarmDayException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
