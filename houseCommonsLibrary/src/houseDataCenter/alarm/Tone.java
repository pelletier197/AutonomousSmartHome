package houseDataCenter.alarm;

import java.io.Serializable;

public class Tone implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2532670373172707998L;

	/**
	 * The path to the music tone
	 */
	private String tonePath;

	private double toneVolume;

	public Tone(String path, double volume) {
		this.tonePath = path;
		this.toneVolume = volume;
	}

	public void setTone(String pathToTone, double volume) {

	}

	public String getTonePath() {
		return tonePath;
	}

	public double getToneVolume() {
		return toneVolume;
	}

	@Override
	public String toString() {
		return "Tone [tonePath=" + tonePath + ", toneVolume=" + toneVolume + "]";
	}

	
}
