package houseDataCenter.alarm;

import houseDataCenter.house.Room;

public class HouseAlarm extends Alarm {

	private byte[] roomUUID;

	public HouseAlarm(int hour, int minutes, Tone tone, byte[] roomUUID) {
		super(hour, minutes, tone);
		if (roomUUID == null) {
			throw new NullPointerException("Null room");
		}
		this.roomUUID = roomUUID;
	}
	
	

	public HouseAlarm(int hour, int minutes, Tone tone, byte[] roomUUID, AlarmDay... days) {
		super(hour, minutes, tone, days);

		if (roomUUID == null) {
			throw new NullPointerException("Null room");
		}
		this.roomUUID = roomUUID;	}



	public HouseAlarm(Alarm alarm, byte[] roomUUID) {
		super(alarm);
		if (roomUUID == null) {
			throw new NullPointerException();
		}
		this.roomUUID = roomUUID;
	}

	public byte[] getRoomUUID() {
		return roomUUID;
	}

}
