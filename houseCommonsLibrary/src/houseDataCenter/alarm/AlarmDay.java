package houseDataCenter.alarm;

import java.util.Comparator;

public enum AlarmDay {

	ALL(-1), SUNDAY(1), MONDAY(2), TUESDAY(3), WEDNESDAY(4), THURSDAY(5), FRIDAY(6), SATURDAY(7);

	public final int day;

	private AlarmDay(int day) {
		this.day = day;
	}

	public static Comparator<AlarmDay> getDayComparator() {
		return new Comparator<AlarmDay>() {

			@Override
			public int compare(AlarmDay one, AlarmDay o) {
				return one.day - o.day;
			}
		};
	}

}
