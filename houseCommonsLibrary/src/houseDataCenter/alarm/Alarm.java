package houseDataCenter.alarm;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * 
 * An alarm class managing an alarm that can be repeated an a given hour an
 * minute of a given day.
 * 
 * @author Sunny
 *
 */
public class Alarm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3182220625238197248L;

	/**
	 * A day in millis
	 */
	private static final long DAY_MILLIS = 86_400_000L;

	/**
	 * The next computed alarm time.
	 */
	private long nextAlarm;

	/**
	 * The list of days on which the alarm will ring.
	 */
	private List<AlarmDay> days;

	/**
	 * Parameter used by the {@link AlarmManager}. Parameter that may be used to
	 * create an alarm that will only start once and will then be deleted.
	 */
	private boolean useOnce = false;

	/**
	 * Used to know if the alarm rang at least once.
	 */
	private boolean rang = false;

	/**
	 * Indicates wether the alarm is active or not.
	 */
	private boolean active = true;

	/**
	 * ID generated at construction, used to know how to compute equals of 2
	 * alarms, as 2 alarms could have the same properties and still not be the
	 * same.
	 */
	private UUID ID;

	/**
	 * The hours and minutes of the alarm on which it will ring.
	 */
	private int hour, min;

	/**
	 * The sound played by the alarm
	 */
	private Tone tone;

	/**
	 * Creates an alarm defined to ring once at the given hour and the given
	 * minute time of the day, either today of tomorrow
	 * 
	 * @param hour
	 * @param minutes
	 */
	public Alarm(int hour, int minutes, Tone tone) {
		setDays();
		setNextAlarmTime(hour, minutes);
		ID = UUID.randomUUID();
		this.tone = tone;

	}

	/**
	 * Creates an alarm defined to ring every day on the given time and on the
	 * days given in parameter. In the case that days parameter contains
	 * {@link AlarmDay.ALL}. The alarm will be set to ring on the given time
	 * every day
	 * 
	 * @param hour
	 *            The hour of the day between 0 and 24 include
	 * @param minutes
	 *            The minutes of the day between 0 and 60 include
	 * @param days
	 *            The days on which the alarm will ring.
	 */
	public Alarm(int hour, int minutes, Tone tone, AlarmDay... days) {
		setDays(days);
		setNextAlarmTime(hour, minutes);
		ID = UUID.randomUUID();
		this.tone = tone;
	}

	/**
	 * Constructs an alarm that will have the exact same properties as the one
	 * in parameter.
	 * 
	 * @param alarm
	 *            The alarm to get the properties from
	 */
	public Alarm(Alarm alarm) {
		this(alarm.getHour(), alarm.getMin(), new Tone(alarm.getTone().getTonePath(), alarm.getTone().getToneVolume()),
				(AlarmDay[]) alarm.days.toArray(new AlarmDay[alarm.days.size()]));
		this.setActive(alarm.isActive());
		this.ID = alarm.ID; // They must have the same ID

	}

	public Tone getTone() {
		return tone;
	}

	public void setTone(Tone tone) {
		this.tone = tone;

	}

	/**
	 * Verifies either the alarm ringed or not. This function must be called
	 * periodically by a manager, so it will be notified that the alarm rang.
	 * 
	 * <p>
	 * When this method is called, the next alarm is automatically computed if
	 * it rang, and will also be deactivated if the useOnce parameter is set to
	 * true, which means it will not ring again until reactivation.
	 * </p>
	 * 
	 * @return True if the alarm rang and was updated, false otherwise.
	 */
	public boolean verify() {

		// If the alarm is time is passed, true is returned, meaning the alarm
		// should
		// ring, and the next alarm time is updated.
		if (nextAlarm < System.currentTimeMillis() && active) {

			rang = true;

			computeAlarm();

			if (useOnce) {
				active = false;
			}

			return true;

		} else {

			return false;
		}
	}

	/**
	 * @return True if the alarm is set to be used once, false otherwise.
	 */
	public boolean isUseOnce() {
		return useOnce;
	}

	/**
	 * @return True if the alarm has rang at least once, false otherwise.
	 */
	public boolean hasRang() {
		return rang;
	}

	/**
	 * @return True if the alarm is active, false otherwise.
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * 
	 * Sets the alarm to active. That made, the next time for the alarm to ring
	 * is updated if value is true.
	 * 
	 * @param value
	 *            The activity value of the alarm
	 * 
	 */
	public void setActive(boolean value) {
		this.active = value;

		if (active) {
			computeAlarm();
		}

	}

	/**
	 * Sets the next alarm time in the day. This is used to change the time on
	 * which the alarm will ring.
	 * 
	 * @param hour
	 *            The hour of the day it will now ring.
	 * @param minutes
	 *            The minute of the day it will ring.
	 */
	public void setNextAlarmTime(int hour, int minutes) {

		if (hour < 0 || hour > 24 || minutes < 0 || minutes > 60) {
			throw new IllegalAlarmDayException("Hours must be [0,24] and minutes [0,60]");
		}

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minutes);
		calendar.set(Calendar.SECOND, 0);

		// Sets the alarm for today, at the given hour and minute
		nextAlarm = calendar.getTimeInMillis();

		// Then compute the day if required
		computeAlarm();

		this.hour = hour;
		this.min = minutes;
	}

	/**
	 * Computed the next alarm considering it will ring on the same exact hour
	 * and minute as the current alarm. It considers counting the number of days
	 * before the next alarm day using {@link #days}.
	 */
	private void computeAlarm() {

		// If we passed the next alarm time

		// Count the number of days before the next alarm
		int daysBeforeNext = daysBeforeNextAlarm();

		// Sets the time for the number of days remaining before next alarm.
		nextAlarm += DAY_MILLIS * daysBeforeNext;

	}

	/**
	 * @return True if the alarm is set to be used all days of the week.
	 */
	public boolean isAllDay() {
		return days.contains(AlarmDay.ALL) || days.size() >= 7;
	}

	/**
	 * Set the days on which the alarm will ring. Note that if this list is
	 * empty, the alarm is set to ring only once.
	 * 
	 * @param days
	 */
	public void setDays(AlarmDay... days) {
		if (days == null) {
			setDays(new ArrayList<>());
		} else {
			setDays(Arrays.asList(days));
		}

	}

	public void setDays(List<AlarmDay> days) {
		if (days != null) {
			this.days = new ArrayList<>(days);
		} else {
			this.days = new ArrayList<>();
		}

		if (this.days.isEmpty()) {
			useOnce = true;
		} else {
			useOnce = false;
		}

		Collections.sort(this.days, AlarmDay.getDayComparator());

		// Computes the next alarm
		computeAlarm();

	}

	/**
	 * 
	 * @return The time on which the next alarm will ring in milliseconds.
	 */
	public long getNextAlarmTime() {
		return nextAlarm;

	}

	/**
	 * 
	 * @return The hour on which the alarm will ring.
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * 
	 * @return The minute on which the alarm will ring.
	 */
	public int getMin() {
		return min;
	}

	/**
	 * Returns a copy of the alarm days of the alarm. Those are the day of the
	 * week on which the alarm will ring.
	 * 
	 * @return A copy of the alarm days.
	 */
	public List<AlarmDay> getAlarmDays() {
		return new ArrayList<>(days);
	}

	/**
	 * Returns the time computed before the next alarm ring. The time given is
	 * computed in millisecond and might not be an exact representation of time
	 * before the alarm rings. It will depends on the AlarmManager.
	 * 
	 * <p>
	 * There are 2 different cases where time returned be negative
	 * <li>The alarm is not active : then, -1 is returned.</li>
	 * <li>The alarm is about to ring but the manager have not already verified
	 * time time via {@link #verify()}. The next call of this method will true
	 * and recompute the alarm. Then, -2 is returned</li>
	 * <p>
	 * 
	 * @return The time before the next alarm in ms, -1 or -2.
	 */
	public long timeBeforeNext() {

		if (!active) {
			return -1;
		}

		long time = nextAlarm - System.currentTimeMillis();

		if (time < 0) {
			return -2;
		}

		return time;

	}

	/**
	 * Counts the number of days before the next alarm. If the alarm is set to
	 * ring in the current day.
	 * 
	 * <p>
	 * In the case that the alarm is set to be used once, 0 is returned if the
	 * current time is before the current time, or 1 if the alarm is set to ring
	 * tomorrow.
	 * </p>
	 * <p>
	 * In the alternative that its not use once, 0 is also returned if it is set
	 * to ring today. Otherwise, the number of days before the next alarm is
	 * computed, using floor modulo, which means the return will always be > 0,
	 * even if the next alarm day is a lower day of the week.
	 * </p>
	 * 
	 * @return The number of days before the next alarm to ring, in the range of
	 *         [0,6]
	 */
	private int daysBeforeNextAlarm() {
		Calendar calendar = Calendar.getInstance();

		// If the alarm is set to be used once, we check. If the alarm is today,
		// we return 0, else, return 1, so it is tomorrow.
		if (isUseOnce()) {
			if (calendar.getTimeInMillis() < nextAlarm) {
				return 0;
			} else {
				return 1;
			}
		}

		// Returns 0 if the alarm is set to be ringing today at a time later
		// than now
		if (calendar.getTimeInMillis() < nextAlarm && isAlarmDay(calendar.get(Calendar.DAY_OF_WEEK))) {
			return 0;
		}

		// Gets the current day of the week
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

		// Finds the day of the next alarm
		int nextDay = nextAlarmDay(dayOfWeek);

		// In this case, the alarm rings one day only, so we return 7 as the
		// alarm will ring again in 7 days.
		if (dayOfWeek == nextDay && calendar.getTimeInMillis() > nextAlarm) {
			return 7;
		}

		// If it is a new week, nextDay < dayOfWeek. Use floorMod to get
		// positive value of modulo, so return > 0

		return Math.floorMod(nextDay - dayOfWeek, 7);

	}

	/**
	 * 
	 * @param dayNumber
	 *            The day number to be treated.
	 * @return True if the given dayNmber is an alarm day on which the alarm
	 *         will ring.
	 */
	private boolean isAlarmDay(int dayNumber) {

		if (isAllDay()) {
			// Always true because alarms rings every day
			return true;
		}

		for (AlarmDay day : days) {
			if (day.day == dayNumber) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Finds the day on which the next alarm will ring and return its value as
	 * an integer contained in the range [1,7].
	 * 
	 * It will most likely iterates on the alarm's days, and find the closest
	 * value in the alarm days that is higher or is the first day of the alarm
	 * days if current day is the last day of the week.
	 * 
	 * @see {@link Calendar} for more details about days of week.
	 * 
	 * @param currentDay
	 *            The number of the current day.
	 * @return The value of the next alarm day in the range [1,7]
	 */
	private int nextAlarmDay(int currentDay) {

		// If all day, next alarm is the day after
		// This value will be between 1 and 7
		if (isAllDay()) {
			return (currentDay % 7) + 1;
		}

		// Finds a day with a higher day value in the week
		for (AlarmDay day : days) {
			if (day.day > currentDay) {
				return day.day;
			}
		}
		// We reached the last day of the week. We therefore return the first
		// day of the week.
		return days.get(0).day;

	}

	@Override
	public String toString() {
		return "Alarm [nextAlarm=" + nextAlarm + ", days=" + days + ", useOnce=" + useOnce + ", rang=" + rang
				+ ", active=" + active + ", ID=" + ID + ", hour=" + hour + ", min=" + min + ", tone=" + tone + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alarm other = (Alarm) obj;
		if (!ID.equals(other.ID))
			return false;
		return true;
	}

	public UUID getId() {
		return ID;
	}

	/**
	 * Changes the UUID of the alarm. This function should never be called
	 * outside of reassignment of lights that are being constructed from
	 * serialized objects.
	 * 
	 * @param uuid
	 */
	@Deprecated
	public void changeUUID(UUID uuid) {
		this.ID = uuid;
	}
}
