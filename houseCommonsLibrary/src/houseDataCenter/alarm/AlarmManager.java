package houseDataCenter.alarm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class AlarmManager {

	private static final int SNOOZE_TIMEOUT_MINUTES = 5;

	private List<Alarm> alarms;
	private List<Alarm> snoozeAlarms;

	private AlarmHandler handler;

	private boolean stopped;

	public AlarmManager(Collection<Alarm> alarms, AlarmHandler handler) {

		if (handler == null) {
			throw new NullPointerException("Null alarm handler");
		}

		assertNonNullAlarm(alarms);
		this.alarms = new ArrayList<>(alarms);
		this.snoozeAlarms = new ArrayList<>();
		this.handler = handler;
		stopped = false;

		init();

	}

	private void assertNonNullAlarm(Collection<Alarm> alarms) {
		for (Alarm alarm : alarms) {
			if (alarm == null) {
				throw new NullPointerException("No Null alarm is permitted");
			}
		}
	}

	private void init() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (!stopped) {
					try {
						Iterator<Alarm> iterator = alarms.iterator();
						Alarm alarm = null;

						// Iterates alarm and notify if a alarm rings
						while (iterator.hasNext()) {
							alarm = iterator.next();

							if (alarm.verify()) {
								handler.handleAlarm(alarm);

							}
						}

						// Iterates over the snooze alarms
						iterator = snoozeAlarms.iterator();

						while (iterator.hasNext()) {
							alarm = iterator.next();

							if (alarm.verify()) {
								handler.handleAlarm(alarm);
								// Removes the snooze alarm from the list
								iterator.remove();
							}
						}

						// 5 Second sleep.
						Thread.sleep(5000);
					} catch (Exception e) {

					}
				}

			}
		}).start();

	}

	public void stop() {
		this.stopped = true;
	}

	/**
	 * Snoozes the alarm with the given id. This method does not verify if the
	 * alarm rang before calling this method. It will only create a temporary
	 * alarm due to ring in five minutes ahead of now, with the exact same tone
	 * than the alarm with the id in parameter.
	 * 
	 * @param id
	 *            The id of the alarm to snooze
	 */
	public synchronized void snooze(UUID id) {
		// Creates a snooze with default Alarm target
		snooze(id, new Alarm(1, 1, new Tone("", 0)));

	}

	/**
	 * Has the same effect than {@link #snooze(UUID)}, but will use the
	 * snoozeTarget to create the snooze alarm.
	 * 
	 * It will however replace the ringing time for five minutes ahead of now.
	 * And will clear the days of repeat.
	 * 
	 * @param id
	 * @param snoozeTarget
	 */
	public void snooze(UUID id, Alarm snoozeTarget) {
		Alarm a = findAlarmByID(id);

		Calendar current = Calendar.getInstance();
		current.add(Calendar.MINUTE, SNOOZE_TIMEOUT_MINUTES);
		snoozeTarget.setDays();
		snoozeTarget.setNextAlarmTime(current.get(Calendar.HOUR_OF_DAY), current.get(Calendar.MINUTE));
		snoozeTarget.setActive(true);
		System.out.println("Snooze alarm created : " + snoozeTarget);
		snoozeAlarms.add(snoozeTarget);
	}

	public synchronized void addAlarm(Alarm alarm) {
		if (alarm == null) {
			throw new NullPointerException("Null alarm");
		}
		alarms.add(alarm);
	}

	public synchronized boolean removeAlarm(Alarm alarm) {
		return alarms.remove(alarm);
	}

	public synchronized boolean removeAlarm(UUID ID) {
		Alarm toremove = findAlarmByID(ID);
		System.out.println("ALARM TO REMOVE : " + toremove);
		return alarms.remove(toremove);
	}

	public List<Alarm> getAlarms() {
		return alarms;
	}

	public void activateAlarm(UUID id) {
		Alarm a = findAlarmByID(id);
		if (a != null) {
			a.setActive(true);
		}
	}

	public void deactivateAlarm(UUID id) {
		Alarm a = findAlarmByID(id);
		if (a != null) {
			a.setActive(false);
		}
	}

	public synchronized Alarm findAlarmByID(UUID id) {
		for (Alarm a : alarms) {
			if (a.getId().equals(id)) {
				return a;
			}
		}
		return null;
	}

	/**
	 * Replace the alarm that have the exact same id as the one in parameter
	 * from the manager and replaces it by the one received in parameters.
	 * 
	 * @param a
	 *            The used alarm to replace the old one
	 */
	public synchronized void updateAlarm(Alarm a) {

		// Checks if the remove worked
		System.out.println("TRYING TO UPDATE ALARM + " + a);
		if (a != null && removeAlarm(a.getId())) {
			System.out.println("UPDATE SUCCESS TO UPDATE ALARM + " + a);
			addAlarm(a);
		}
	}
}
