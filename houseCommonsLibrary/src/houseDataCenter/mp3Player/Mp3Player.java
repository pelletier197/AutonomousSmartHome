package houseDataCenter.mp3Player;

/**
 * Methods that should implement every Mp3 sound player. It should be able to
 * play a given song situated on the disk given by a path.
 * 
 * It should also be able to seek, pause and resume the song.
 * 
 * @author sunny
 *
 */
public interface Mp3Player {

	public void play(String path);

	public void resume();

	public void pause();
	
	public double getCurrentTime();
	
	public double getTotalDuration();
	
	public boolean isPlaying();

	public void seek(double progress);
	
	public void setOnfinished(SongEvent event);

	public void dispose();

	public void setVolume(double value);

}
