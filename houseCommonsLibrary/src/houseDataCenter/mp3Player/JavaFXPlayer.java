package houseDataCenter.mp3Player;

import java.io.File;
import java.nio.file.Paths;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaPlayer.Status;
import javafx.util.Duration;

public class JavaFXPlayer implements Mp3Player {

	/**
	 * Current javaFx media player. Playing structure may be changed in future
	 * version of the program.
	 */
	private MediaPlayer media;

	private double volume = 1;

	@Override
	public void play(String path) {
		if (media != null) {
			if (this.media.getStatus() == Status.PLAYING) {

				media.stop();
			}
			this.media.dispose();
		}

		if (path != null) {

			Media newMedia = new Media(path);
			this.media = new MediaPlayer(newMedia);
			this.media.setVolume(volume);
			media.play();
		}
	}

	@Override
	public void resume() {
		if (media != null && this.media.getStatus() != Status.PLAYING) {
			media.play();
		}
	}

	@Override
	public void pause() {
		if (media != null) {
			media.pause();
		}
	}

	@Override
	public boolean isPlaying() {
		return media == null ? false : media.getStatus() == Status.PLAYING;
	}

	@Override
	public void seek(double progress) {
		media.seek(Duration.seconds((progress * media.getTotalDuration().toSeconds())));
	}

	@Override
	public void setOnfinished(final SongEvent event) {
		
				media.setOnEndOfMedia(new Runnable() {
			
			@Override
			public void run() {
				event.handle();				
			}
		});
	}

	@Override
	public double getCurrentTime() {
		return media == null ? 0 : media.getCurrentTime().toSeconds();
	}

	@Override
	public double getTotalDuration() {
		// TODO Auto-generated method stub
		return media == null ? 0 : media.getTotalDuration().toSeconds();
	}

	@Override
	public void dispose() {
		if (media != null) {
			media.stop();
			media.dispose();
		}
	}

	@Override
	public void setVolume(double value) {
		this.volume = value;
		if (media != null)
			this.media.setVolume(value);
	}

}
