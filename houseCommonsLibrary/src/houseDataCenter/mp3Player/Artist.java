package houseDataCenter.mp3Player;

import java.io.Serializable;

/**
 * Container grouping information about a given artist. In the current case,
 * this class is a container for artist's name.
 * 
 * @author sunny
 *
 */
public class Artist implements Serializable, Comparable<Artist> {

	private static final long serialVersionUID = 630168697074575445L;

	/**
	 * Artist's name
	 */
	private String name;

	/**
	 * Constructs an artist with an empty name.
	 */
	public Artist() {
		this(null);
	}

	/**
	 * Constructs an artist with the given name.
	 * 
	 * @param name
	 *            Artist's name.
	 * @see Artist#setName(String)
	 */
	public Artist(String name) {
		setName(name);
	}

	/**
	 * Sets the artist name. If the name is null or empty, the resulting name
	 * will be an empty name.
	 * 
	 * @param name
	 *            Artist's name.
	 */
	public void setName(String name) {
		if (name == null || name.trim().isEmpty()) {
			name = "";
		}
		this.name = name;

	}

	/**
	 * @return Artist's name, or an empty string if the artist has no name.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Two artists are equal by name using String equality.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artist other = (Artist) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * Two artists are compared by their name using string comparison.
	 */
	@Override
	public int compareTo(Artist artist) {

		return this.name.compareTo(artist.name);
	}
}
