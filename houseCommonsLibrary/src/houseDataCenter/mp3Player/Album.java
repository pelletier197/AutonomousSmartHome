package houseDataCenter.mp3Player;

import java.io.Serializable;


/**
 * An album is a song part that contains all data returning to the album,
 * including the name and the cover (stored as a byte array). This Serializable
 * class is stored as a single part of a {@link Mp3SongInfo}
 * 
 * @author sunny
 *
 */
public class Album implements Serializable, Comparable<Album> {

	private static final long serialVersionUID = 554466016142507297L;

	/**
	 * Album's name
	 */
	private String name;

	/**
	 * Album's cover stored as a byte array
	 */
	private byte[] cover;

	/**
	 * Constructs an album with a null name and null album cover.
	 */
	public Album() {
		this(null);
	}

	/**
	 * Constructs an album with the specified name and null album cover.
	 * 
	 * @param album
	 *            Album's name. Can be null.
	 */
	public Album(String album) {
		this(album, null);
	}

	/**
	 * Constructs an album with the given name and album cover.
	 * 
	 * @param album
	 *            Album's name
	 * @param cover
	 *            Album's cover
	 */
	public Album(String album, byte[] cover) {
		setName(album);
		setCover(cover);
	}

	/**
	 * Sets the album cover
	 * 
	 * @param cover
	 *            The new cover, can be null.
	 */
	public void setCover(byte[] cover) {
		this.cover = cover;

	}

	/**
	 * Sets the name of the album. If null or empty, the name is set to an empty
	 * String.
	 * 
	 * @param name
	 *            The name of the album.
	 */
	public void setName(String name) {
		if (name == null || name.trim().isEmpty()) {
			name = "";
		}
		this.name = name;
	}

	/**
	 * @return The current album's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return The current album's cover
	 */
	public byte[] getCover() {
		return cover;
	}

	/**
	 * Compares the given album to the current album. By definition, albums are
	 * comparable by their name using String comparison.
	 */
	@Override
	public int compareTo(Album other) {

		return this.name.compareTo(other.name);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Two album are equals by their name.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
