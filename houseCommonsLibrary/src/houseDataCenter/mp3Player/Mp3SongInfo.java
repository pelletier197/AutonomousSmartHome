package houseDataCenter.mp3Player;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLDecoder;

import org.apache.commons.io.FilenameUtils;

import src.main.java.com.mpatric.mp3agic.ID3v1;
import src.main.java.com.mpatric.mp3agic.ID3v1Tag;
import src.main.java.com.mpatric.mp3agic.ID3v2;
import src.main.java.com.mpatric.mp3agic.ID3v24Tag;
import src.main.java.com.mpatric.mp3agic.InvalidDataException;
import src.main.java.com.mpatric.mp3agic.Mp3File;
import src.main.java.com.mpatric.mp3agic.UnsupportedTagException;

/**
 * Container class regrouping all important information about a given mp3 song.
 * This class is a container for their artist, their album and the song,
 * including the path to this song on the disk.
 * 
 * This class only uses to read tags from a given mp3 file and save them as
 * specific parameter. It can then be serialized to a form easier to open than a
 * regular mp3 file.
 * 
 * @author sunny
 *
 */
public class Mp3SongInfo implements Comparable<Mp3SongInfo>, Serializable {

	private static final long serialVersionUID = -633577370072545161L;

	/**
	 * The path to the given mp3 song.
	 */
	private String path;

	/**
	 * Song's artist
	 */
	private Artist artist;
	/**
	 * Mp3 song associated to this song.
	 */
	private Song song;

	/**
	 * Song's album.
	 */
	private Album album;

	/**
	 * Read the tags from the mp3 file specified by the abstract path given in
	 * parameter.Exception will occur if the mp3 file is unreadable or has an
	 * incorrect format.
	 * 
	 * @param path
	 *            The path to the mp3 file. Must be an existent path in memory.
	 * @throws UnsupportedTagException
	 *             If the mp3 tags are not supported
	 * @throws InvalidDataException
	 *             If an exception occurs during reading.
	 * @throws IOException
	 *             If the file is unreachable.
	 */
	public Mp3SongInfo(String path) throws UnsupportedTagException, InvalidDataException, IOException {

		// Attempts reading information from the 2 type of tags. New generation
		// mp3 will often use ID3V2 tags, so they are read first.
		this.album = new Album();
		this.song = new Song(null);
		this.artist = new Artist();

		// Relative path name
		this.path = path;
		
		Mp3File file = new Mp3File(path);
		ID3v2 tags = file.getId3v2Tag();
		ID3v1 tagsv1 = file.getId3v1Tag();

		if (tags == null) {
			file.setId3v2Tag(new ID3v24Tag());
			tags = file.getId3v2Tag();
		}
		if (tagsv1 == null) {
			file.setId3v1Tag(new ID3v1Tag());
			tagsv1 = file.getId3v1Tag();
		}

		if (tags.getAlbumImage() != null) {
			album.setCover(tags.getAlbumImage());
		}

		// Get length from files
		song.setLength(file.getLengthInSeconds());

		// get title from tags
		if (tags.getTitle() == null) {
			if (tagsv1.getTitle() == null) {
				tags.setTitle(FormatTitleFromFile());
			} else {
				tags.setTitle(tagsv1.getTitle());
			}

		}

		song.setTitle(tags.getTitle().trim());

		// Get artist from tags
		if (tags.getArtist() == null) {
			if (tagsv1.getArtist() == null || tagsv1.getArtist().isEmpty()) {

				tags.setArtist("");
			} else {

				tags.setArtist(tagsv1.getArtist());
			}

		}

		artist.setName(tags.getArtist());

		// Get album's name from tags.
		if (getAlbumName() == null) {
			if (tagsv1.getAlbum() == null) {
				tags.setAlbum("");
			} else {
				tags.setAlbum(tagsv1.getAlbum());
			}
		}
		album.setName(tags.getAlbum());

		// Sets the cover from the tags. Null or not, doesn't matter.
		album.setCover(tags.getAlbumImage());

	}

	/**
	 * Used to get album's name from song's path when no name tags are found
	 * directly in file. This way, the song is ensured to have a title
	 * corresponding to user's knowledge.
	 * 
	 * @return A name formatted from the last part (base name) of a file.
	 */
	@SuppressWarnings("deprecation")
	private String FormatTitleFromFile() {
		final String path = new File(getPathAsURI()).getName();
		String name = null;
		try {
			// Decode spaces into readable characters from UTF8. However, spaces
			// are showed as % symbol.
			name = isolateName(new File(URLDecoder.decode(path)).getPath());

		} catch (Exception e) {
			// Empty name if doesn't work.
			name = "";
		}
		return name;
	}

	/**
	 * Returns the filename without the path going to it. For exemple, sending
	 * 
	 * c://desktop/user/abc/workspace/yolo.mp3
	 * 
	 * will simply return yolo.
	 * 
	 * @param path
	 *            The path to the song.
	 * @return The name isolated from the file path.
	 */
	private String isolateName(String path) {

		return FilenameUtils.getBaseName(path);
	}


	/**
	 * 
	 * @return The song's title read from tags.
	 */
	public String getTitle() {
		return song.getTitle();
	}

	/**
	 * 
	 * @return The name of the song's artist
	 */
	public String getArtistName() {
		return artist.getName();
	}

	/**
	 * 
	 * @return Album's cover retrieved from tags.
	 */
	public byte[] getAlbumCover() {

		// If the album is not unserialized yet, we generate the file and get
		// the album.

		return album.getCover();
	}

	/**
	 * 
	 * @return Album's name.
	 */
	public String getAlbumName() {
		return album.getName();

	}

	/**
	 * 
	 * @return Song's artist
	 */
	public Artist getArtist() {
		return artist;
	}

	/**
	 * 
	 * @return Song with its information
	 */
	public Song getSong() {
		return song;
	}

	/**
	 * 
	 * @return Song's album
	 */
	public Album getAlbum() {
		return album;
	}

	/**
	 * 
	 * @return Song's length in seconds
	 */
	public long getLength() {
		return song.getLength();
	}

	/**
	 * 
	 * @return The path to the song as a valid URI represented as a string
	 */
	public String getPathAsURI() {
		return new File(path).toURI().toString();
	}

	/**
	 * Direct path to the file.
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * Two songs are comparable by their name using regular string comparison.
	 */
	@Override
	public int compareTo(Mp3SongInfo o) {

		return this.getTitle().compareTo(o.getTitle());
	}

	@Override
	public String toString() {
		return "Mp3SongInfo [name=" + getTitle() + ", artist=" + getArtistName() + ", albumName=" + getAlbumName()
				+ ", length=" + getLength() + ", pathToSong=" + getPathAsURI() + "]";
	}

	// TODO - CHANGE EQUALS AND HASHCODE TO PATH WHEN DATA TRANSFER AT OPENING
	// IS MADE

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((album == null) ? 0 : album.hashCode());
		result = prime * result + ((artist == null) ? 0 : artist.hashCode());
		result = prime * result + ((song == null) ? 0 : song.hashCode());
		return result;
	}

	/**
	 * Redefines equals for the songs so that they are equal by everything but
	 * by paths
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mp3SongInfo other = (Mp3SongInfo) obj;
		if (album == null) {
			if (other.album != null)
				return false;
		} else if (!album.equals(other.album))
			return false;
		if (artist == null) {
			if (other.artist != null)
				return false;
		} else if (!artist.equals(other.artist))
			return false;
		if (song == null) {
			if (other.song != null)
				return false;
		} else if (!song.equals(other.song))
			return false;
		return true;
	}

}
