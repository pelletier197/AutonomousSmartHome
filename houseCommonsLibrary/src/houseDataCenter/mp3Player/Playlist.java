package houseDataCenter.mp3Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * A playlist is a single playing list of songs that might be played in any
 * order. By default, regular sort of a playlist is made from alphabetic
 * comparison.
 * 
 * This class provides utils to keep information about its content. For
 * instance, the playlist must be provided with a name at creation. It might
 * also contains a certain number of songs which's length is always computed
 * during addition of a song.
 * 
 * @author sunny
 *
 */
public class Playlist implements Serializable {

	private static final long serialVersionUID = -7067537096281822340L;

	/**
	 * The list of songs that this playlist contains
	 */
	private List<Mp3SongInfo> songs;

	/**
	 * Playlist's name
	 */
	private String name;

	/**
	 * Playlist's length computed from {@link #songs} content.
	 */
	private long length;

	/**
	 * Creates a playlist with an empty name and an empty set of songs.
	 */
	public Playlist() {
		this(new ArrayList<>(), "");
	}

	/**
	 * Constructs a playlist with the given name and an empty set of songs.
	 * 
	 * @param name
	 *            Playlist name. May not be null, but can be empty.
	 */
	public Playlist(String name) {
		this(Collections.emptyList(), name);
	}

	/**
	 * Constructs a playlist with the given set of songs and with the given
	 * name. The songs parameter may not be null, but can have a size of 0.
	 * 
	 * @param songs
	 *            The songs that will be added.
	 * @param name
	 *            The name of the playlist.
	 */
	public Playlist(Collection<Mp3SongInfo> songs, String name) {

		if (songs == null) {
			throw new NullPointerException("Null list");
		}

		this.songs = new ArrayList<>();

		setName(name);
		setSongs(songs);

		computeLength();
	}

	/**
	 * Sets the playlist name. If null or empty, the result is an empty name.
	 * 
	 * @param name
	 *            New playlist's name.
	 */
	public void setName(String name) {
		if (name == null || name.trim().isEmpty())
			this.name = "";
		else
			this.name = name;
	}

	/**
	 * Tells either this playlist contains the given song or not.
	 * 
	 * @param song
	 * @return True if the song is contained, false otherwise.
	 * 
	 */
	public boolean contains(Mp3SongInfo song) {
		return songs.contains(song);
	}

	/**
	 * Sets the playlist new set of songs. After this method is called, length
	 * will be computed, which may occur in problems if UI is not updated
	 * consequently.
	 * 
	 * @param newSongs
	 *            The new songs of the playlist.
	 */
	public void setSongs(Collection<Mp3SongInfo> newSongs) {
		if (newSongs == null)
			throw new NullPointerException();

		this.songs.clear();
		this.songs.addAll(newSongs);

		computeLength();
	}

	/**
	 * Add the given song to the playlist. The song's length is added to the
	 * current length of the playlist.
	 * 
	 * @param song
	 *            The song to be added.
	 * @return True if the song is successfully added, false otherwise or if the
	 *         song is null.
	 */
	public boolean addSong(Mp3SongInfo song) {

		if (song != null) {
			songs.add(song);
			length += song.getLength();
			return true;
		}
		return false;

	}

	/**
	 * Add all the songs in the given collection to the current list of songs
	 * contained in the playlist. The length of the songs is then computed and
	 * added to the current length.
	 * 
	 * @param collection
	 *            The songs to be added.
	 */
	public void addSongs(Collection<Mp3SongInfo> collection) {
		this.songs.addAll(collection);
		computeLength(collection);

	}

	/**
	 * Computes the length of the given collection and add it to the current
	 * length.
	 * 
	 * @param collection
	 *            The collection of songs from which time is computed.
	 */
	private void computeLength(Collection<Mp3SongInfo> collection) {
		for (Mp3SongInfo song : collection) {
			if (song != null) {
				length += song.getLength();
			}
		}
	}

	/**
	 * Computes the length of the full playlist by iterating over the list of
	 * element.
	 */
	private void computeLength() {

		this.length = 0;

		computeLength(songs);
	}

	/**
	 * Removes the given song from the playlist's songs. The computed time is
	 * then modified in consequence.
	 * 
	 * @param song
	 *            The song to be removed.
	 * @return True if the song is successfully removed, false if ortherwise or
	 *         if the song is null.
	 */
	public boolean removeSong(Mp3SongInfo song) {
		boolean retour = songs.remove(song);

		if (retour) {
			length -= song.getLength();
		}

		return retour;
	}

	/**
	 * Removes all the songs contained in the given collection. All song's
	 * length are subtracted to the current length before being removed from the
	 * playlist.
	 * 
	 * @param songs
	 *            The songs that must be removed. May not be null.
	 * @return True if at least one song is removed, false otherwise.
	 */
	public boolean removeAllSongs(Collection<Mp3SongInfo> songs) {

		boolean operated = false;

		for (Mp3SongInfo song : songs) {
			if (this.songs.remove(song)) {
				operated = true;
				length -= song.getLength();
			}

		}

		return operated;
	}

	/**
	 * 
	 * @return Total length of the playlist in seconds.
	 */
	public long getLength() {
		return length;
	}

	/**
	 * 
	 * @return An unmodifiable list of the songs contained in this playlist.
	 * @see Collections#unmodifiableList(List)
	 */
	public List<Mp3SongInfo> getSongs() {
		return Collections.unmodifiableList(songs);
	}

	/**
	 * 
	 * @return Playlist's name .
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return The number of song that this playlist contains.
	 */
	public int getNumberOfSongs() {
		return songs.size();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * By default, playlists are equal by name.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Playlist other = (Playlist) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
