package houseDataCenter.mp3Player;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Container class used to keep together album information. Album's songs, and
 * total duration are kept with its Album.
 * 
 * @see Album
 * @see Mp3SongInfo
 * 
 * @author sunny
 *
 */
public class AlbumInfo implements Comparable<AlbumInfo> {

	/**
	 * Album's info container for the name and album
	 */
	private Album album;

	/**
	 * The songs that have the current album has their album.
	 */
	private SortedSet<Mp3SongInfo> songs;

	/**
	 * Total duration of the album. Computed from length of all songs contained
	 * in {@link #songs}
	 */
	private long length;

	/**
	 * Constructs album information from the given album.
	 * 
	 * @param album
	 *            The album from which information are created. Cannot be null.
	 */
	public AlbumInfo(Album album) {
		if (album == null) {
			throw new NullPointerException("Null Album");
		}
		this.album = album;
		this.songs = new TreeSet<>();
		this.length = 0;
	}

	/**
	 * Add the song in parameter, only if the song's album is the current
	 * object. In this case, true is returned, otherwise, false.
	 * 
	 * @param song
	 *            The song to be added in this album's information. If null,
	 *            false will be returned.
	 * @return True if the song was successfully added, false otherwise.
	 */
	public boolean addSong(Mp3SongInfo song) {

		boolean added = false;

		if (song != null && song.getAlbum().equals(album)) {
			added = songs.add(song);
			length += song.getLength();
		}

		return added;
	}

	/**
	 * Removes the given song from the album's info. If the song is null or if
	 * the song is not contained in the album's songs, false is returned.
	 * 
	 * @param song
	 *            The song to be removed
	 * @return True if the song was successfully removed, false otherwise.
	 * 
	 */
	public boolean remove(Mp3SongInfo song) {
		if (song != null) {
			if (songs.remove(song)) {
				length -= song.getLength();
				return true;
			}
		}
		return false;
	}

	/**
	 * @return The album containing album's name and cover.
	 */
	public Album getAlbum() {
		return album;
	}

	/**
	 * @return The songs that has {@link #album} as their album in their
	 *         characteristics.
	 */
	public SortedSet<Mp3SongInfo> getSongs() {
		return songs;
	}

	/**
	 * @return The total duration of this album computed from the list of songs
	 *         of this album.
	 */
	public long getLength() {
		return length;
	}

	/**
	 * @return The number of songs that has {@link #album} as their album.
	 */
	public int getNumberOfSongs() {
		return songs.size();
	}

	/**
	 * Album info are comparable by their album.
	 * 
	 * @see Album#compareTo(Album)
	 */
	@Override
	public int compareTo(AlbumInfo album) {

		return this.album.compareTo(album.album);
	}
}
