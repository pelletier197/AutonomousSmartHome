package houseDataCenter.mp3Player;

import java.io.Serializable;

/**
 * Container class that uses to contain every important data about a specific
 * song, including its title and its length.
 * 
 * The length of the song will always have a value >= 0, and the title will most
 * likely always be at least empty and never null.
 * 
 * @author sunny
 *
 */
public class Song implements Serializable {

	/**
	 * Generated UID
	 */
	private static final long serialVersionUID = 4938211973166608514L;

	/**
	 * The title of the song.
	 */
	private String title;

	/**
	 * The length of the song in seconds.
	 */
	private long length;

	/**
	 * Creates a song that will have the given title and a default length of 0
	 * second.
	 * 
	 * @param title
	 *            The title of the song.
	 */
	public Song(String title) {
		this(title, 0);
	}

	/**
	 * Constructs a song that will have the given title and the given length. If
	 * the length is < 0 , the length is set to 0 by default.
	 * 
	 * Also, if the title is null, it is set to an empty title string.
	 * 
	 * @param title
	 *            The title of the song. Can be null.
	 * @param length
	 *            The length of the song in seconds.
	 */
	public Song(String title, long length) {

		this.title = title;
		setLength(length);
	}

	/**
	 * 
	 * @return Song's title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the song's title. If this title is null or empty, it is set to empty
	 * String.
	 * 
	 * @param title
	 *            The title of the song.
	 */
	public void setTitle(String title) {
		if (title == null) {
			title = "";
		}
		this.title = title;
	}

	/**
	 * 
	 * @return The length of the song in seconds.
	 */
	public long getLength() {
		return length;
	}

	/**
	 * Sets the length of the song. This length might be >= 0 to 0, or it will
	 * be set automatically for outside values.
	 * 
	 * @param length
	 *            The new length of the song in seconds.
	 */
	public void setLength(long length) {
		if (length >= 0)
			this.length = length;
		else
			length = 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (length ^ (length >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * Songs are equal by title and by length. Normally, it will be almost
	 * impossible to find 2 songs with the same name AND the exact same length.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (length != other.length)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	

}
