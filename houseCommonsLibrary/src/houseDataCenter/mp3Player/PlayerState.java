package houseDataCenter.mp3Player;

public class PlayerState {

	private boolean isPlaying;
	private double volume;
	private PlaylistPlayer player;
	
	public static PlayerState generatePlayerState(PlaylistPlayer player) {
		return new PlayerState(player);
	}

	private PlayerState(PlaylistPlayer player) {
		this.player = player;
		this.volume = player.volumeProperty().get();
		this.isPlaying = player.isPlaying();
	}
	
	public void restore() {
		if(isPlaying) {
			player.play();
		}
		player.setVolume(volume);
	}
}
