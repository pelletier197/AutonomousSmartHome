package houseDataCenter.mp3Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;

/**
 * A playlist manager is a group of playlists that are kept together. It
 * contains a main playlist, which is a group of all songs that are contained in
 * all the sub playlist and aditionnal songs. It also keep in memory a mapping
 * of the album and artists that every mp3 song have. The songs with the same
 * album or artist are kept together, which makes research easier when looking
 * for a specific album. Computation of those albums are made automatically when
 * adding or removing a song from the main playlist.
 * 
 * This main playlist allows addition (add) modification, but should never be
 * part of a subtraction modification (remove).
 * 
 * The manager also contains a list of sub playlists, which are formed from the
 * songs found in the main Playlist. As said higher, a song is not obligated to
 * be in a sub playlist to be in the main playlist, but must absolutely be in
 * the main playlist to be in a sub one.
 * 
 * To freely add songs to sub Playlists, use
 * {@link #addSongToSubPlaylist(Mp3SongInfo, int)},
 * {@link #addSongToSubPlaylist(Mp3SongInfo, Playlist)},
 * {@link #addSongToSubPlaylists(Collection, int)}. You can remove them manually
 * by removing it directly from the playlist.
 * 
 * To freely add and remove songs from the main Playlist, use
 * {@link #addSong(Mp3SongInfo)} and {@link #addSongs(Collection)},
 * {@link #removeSong(Mp3SongInfo)}.
 * 
 * @author sunny
 *
 */
public class PlaylistManager {

	/**
	 * The main playlist of the manager. All songs contained in sub playlist
	 * must be part of this collection.
	 */
	private Playlist mainSongs;

	/**
	 * A list of all the sub playlists that this manager contains.
	 */
	private List<Playlist> subPlaylists;

	/**
	 * Mapping of the albums that exist in the songs of the manager.
	 */
	private Map<Album, AlbumInfo> albums;

	/**
	 * Mapping of the artists that exist in the songs of the manager.
	 */
	private Map<Artist, ArtistInfo> artists;

	/**
	 * Constructs an empty playlist manager with no songs at creation.
	 */
	public PlaylistManager() {
		this(new ArrayList<>());
	}

	/**
	 * Constructs a playlist manager that will have the given songs as part of
	 * the main playlist. Songs contained in multiple copies will be ignored
	 * during generation of the main playlist. The default playlist created from
	 * this playlist will be named "Main Playlist" by default.
	 * 
	 * @param mainPlaylistSongs
	 *            The songs to be added in the main playlist.
	 */
	public PlaylistManager(List<Mp3SongInfo> mainPlaylistSongs) {
		this(new Playlist(mainPlaylistSongs, "Main Playlist"));
	}

	/**
	 * Creates a playlist that will have the given playlist as its main
	 * playlist. The playlist will then be initialized will all the artists and
	 * albums contained in the songs information.
	 * 
	 * @param mainPlaylist
	 *            The playlist displayed as the main playlist
	 */
	public PlaylistManager(Playlist mainPlaylist) {
		this.mainSongs = mainPlaylist;
		this.subPlaylists = new ArrayList<>();
		this.albums = new TreeMap<>();
		this.artists = new TreeMap<>();

		initPlaylist();
	}

	/**
	 * Initalizes the playlist and the songs contained in it. Every album and
	 * artist is put as a new key for the mapping of albums and artist. They
	 * will now be accessible from getters.
	 */
	private void initPlaylist() {
		final List<Mp3SongInfo> songs = mainSongs.getSongs();
		for (Mp3SongInfo song : songs) {
			// Add the artist information
			ArtistInfo Ainfo = artists.get(song.getArtist());
			if (Ainfo != null) {
				Ainfo.addSong(song);
			} else {
				Ainfo = new ArtistInfo(song.getArtist());
				Ainfo.addSong(song);
				artists.put(song.getArtist(), Ainfo);
			}

			// Add the Album information
			AlbumInfo albumInfo = albums.get(song.getAlbum());
			if (albumInfo != null) {
				albumInfo.addSong(song);
			} else {
				albumInfo = new AlbumInfo(song.getAlbum());
				albumInfo.addSong(song);
				albums.put(song.getAlbum(), albumInfo);
			}

		}
	}

	/**
	 * Retrieves the main playlist of this manager. This one contains all the
	 * songs contained in the manager.
	 * 
	 * @return The main playlist of the manager.
	 */
	public Playlist getMainPlaylist() {
		return mainSongs;
	}

	/**
	 * Retrives all the sub playlists of this manager. Those playlists should
	 * not be modified outside the manager.
	 * 
	 * @return An unmodifiable list of the sub playlists.
	 */
	public List<Playlist> getSubPlaylists() {
		return Collections.unmodifiableList(subPlaylists);
	}

	/**
	 * Returns the index of the playlist in parameter in the manager. In the
	 * case that the playlist is not contained in the sub playlists, -1 is
	 * returned.If this playlist correspond to the mainSongs, -2 is returned.
	 * Finally, if the playlist correspond to one in the sub playlists, its
	 * index is returned.
	 * 
	 * @param playlist
	 * @return
	 */
	public int indexOf(Playlist playlist) {
		if (playlist == mainSongs) {
			return -2;
		} else {
			return subPlaylists.indexOf(playlist);
		}
	}

	/**
	 * Retrieves the first playlist that has the exact same name as the one in
	 * parameter. Can either return a sub playlist or the main playlist.
	 * 
	 * @param name
	 *            The name of the (sub) playlist to find.
	 * @return The playlist associated to the name, or null if no playlist is
	 *         associated.
	 */
	public Playlist getPlaylist(String name) {
		if (name.equals(mainSongs.getName())) {
			return mainSongs;
		} else {
			for (Playlist playlist : subPlaylists) {
				if (playlist.getName().equals(name)) {
					return playlist;
				}
			}
		}
		return null;
	}

	/**
	 * Return the album information associated to the given album. This album
	 * information will contain data about close songs by album.
	 * 
	 * @param album
	 *            The album from which songs are searched.
	 * @return The album information associated to the album.
	 */
	public AlbumInfo getAlbumInfo(Album album) {
		return albums.get(album);
	}

	/**
	 * Retrieves the album information associated to the album that has the
	 * exact given name. This method consider that album are equals by name and
	 * has the exact same return than {@code getAlbumInfo(new Album(name))}
	 * 
	 * @param name
	 *            The name of the album
	 * @return The album information associated to the album.
	 */
	public AlbumInfo getAlbumInfo(String name) {
		return albums.get(new Album(name));
	}

	/**
	 * Retrieves all the album information contained in this manager. The
	 * execution time of this method is proportional to the quantity of album
	 * contained in the manager.
	 * 
	 * @return All the album information that exist in the manager
	 */
	public Set<AlbumInfo> getAlbumInfo() {
		final Set<AlbumInfo> answer = new TreeSet<>();

		for (Map.Entry<Album, AlbumInfo> entry : albums.entrySet()) {
			answer.add(entry.getValue());
		}

		return answer;

	}

	/**
	 * Return the artist information associated to the given artist. This artist
	 * information will contain data about close songs by artist.
	 * 
	 * @param artist
	 *            The artist from which information is searched.
	 * @return The artist information associated to the artist.
	 */
	public ArtistInfo getArtistInfo(Artist artist) {
		return artists.get(artist);
	}

	/**
	 * Retrieves the artist information associated to the artist that has the
	 * exact given name. This method consider that artists are equals by name
	 * and has the exact same return than
	 * {@code getArtistInfo(new Artist(name))}
	 * 
	 * @param name
	 *            The name of the artist
	 * @return The artist information associated to the artist.
	 */
	public ArtistInfo getArtistInfo(String name) {
		return artists.get(new Artist(name));
	}

	/**
	 * Retrieves all the artist information contained in this manager. The
	 * execution time of this method is proportional to the quantity of artist
	 * contained in the manager.
	 * 
	 * @return All the artist information that exist in the manager
	 */
	public Set<ArtistInfo> getArtistInfo() {

		final Set<ArtistInfo> answer = new TreeSet<>();

		for (Map.Entry<Artist, ArtistInfo> entry : artists.entrySet()) {
			answer.add(entry.getValue());
		}

		return answer;

	}

	/**
	 * Return the playlist given by the specified index in the sub playlist
	 * list. Note that if this index is equal to -2, the main playlist is
	 * returned.
	 * 
	 * @see #indexOf(Playlist)
	 * 
	 * @param index
	 *            The index of the searched playlist.
	 * @return The playlist at the specified index in the sub playlist.
	 */
	public Playlist getSubPlaylist(int index) {

		if (index == -2) {
			return mainSongs;
		}
		return subPlaylists.get(index);
	}

	/**
	 * Add the given song to the sub playlist positioned at the given index. If
	 * the given song is not contained in the main playlist, it will be added to
	 * it.
	 * 
	 * @param song
	 *            The song to be added
	 * @param index
	 *            The index of the sub playlist on which it is added.
	 */
	public void addSongToSubPlaylist(Mp3SongInfo song, int index) {
		subPlaylists.get(index).addSong(song);
		if (!mainSongs.contains(song)) {
			secureAdd(song);
		}
	}

	/**
	 * Add a given song to the given playlist that is considered as a sub
	 * playlist. If this sub playlist does not exist in the manager, an
	 * {@link IndexOutOfBoundsException} may be thrown.
	 * 
	 * Otherwise, if the given song must be added in the main playlist, use
	 * {@link #addSong(Mp3SongInfo)} instead.
	 * 
	 * If the song is not contained in the main songs, it will be added
	 * automatically.
	 * 
	 * @param song
	 *            The song to be added
	 * @param playlist
	 *            The playlist in which it is added
	 */
	public void addSongToSubPlaylist(Mp3SongInfo song, Playlist playlist) {
		addSongToSubPlaylist(song, indexOf(playlist));
	}

	/**
	 * Add a given songs to the given playlist that is considered as a sub
	 * playlist. If this sub playlist does not exist in the manager, an
	 * {@link IndexOutOfBoundsException} may be thrown.
	 * 
	 * Otherwise, if the given songs must be added in the main playlist, use
	 * {@link #addSong(Mp3SongInfo)} instead.
	 * 
	 * If the songs are not contained in the main songs, they will be added
	 * automatically after being added in the sub list.
	 * 
	 * @param songs
	 *            The songs to be added
	 * @param playlist
	 *            The playlist in which they are added
	 */
	public void addSongToSubPlaylists(Collection<Mp3SongInfo> songs, int index) {

		subPlaylists.get(index).addSongs(songs);

		// Add all the songs not already contained to the main songs
		Collection<Mp3SongInfo> notContained = CollectionUtils.subtract(songs, mainSongs.getSongs());
		for (Mp3SongInfo song : notContained) {
			secureAdd(song);
		}
	}

	/**
	 * Add a sub playlist to the manager. All the songs that are contained in
	 * this sub playlist will be added to the main playlist if it is not already
	 * done.
	 * 
	 * @param playlist
	 *            The playlist that must be added.
	 */
	public void addSubPlaylist(Playlist playlist) {
		if (playlist.getName() == null || playlist.getName().trim().isEmpty()) {
			playlist.setName("#" + subPlaylists.size());
		}

		if (playlist != null) {

			subPlaylists.add(playlist);

			// Add all the songs not already contained
			Collection<Mp3SongInfo> notContained = CollectionUtils.subtract(playlist.getSongs(), mainSongs.getSongs());

			for (Mp3SongInfo song : notContained) {
				secureAdd(song);
			}
		}
	}

	/**
	 * Add all the given sub playlist to the manager. All the songs contained in
	 * those playlist will be added to the current main playlist if they are not
	 * already contained in it.
	 * 
	 * The execution time of this method might be relatively long if the amount
	 * of playlist and their size is big. use it carefully.
	 * 
	 * @param playlists
	 *            The sub playlists to be added.
	 */
	public void addSubPlaylists(Collection<Playlist> playlists) {
		for (Playlist playlist : playlists) {
			addSubPlaylist(playlist);
		}
	}

	/**
	 * Retrieves all the songs contained in the main playlist into a sorted set
	 * collection. This allow the songs to be alphabetically sorted by their
	 * name. Also, all double songs that might exist are ignored.
	 * 
	 * @return A sorted set of the songs available in the manager.
	 */
	public SortedSet<Mp3SongInfo> getAllSongsSorted() {
		return new TreeSet<>(mainSongs.getSongs());
	}

	/**
	 * Add the given song to the main playlist. The information of this song are
	 * stored in internal structure to get easy access to its album information
	 * and artist information via {@link #getAlbumInfo()} and
	 * {@link #getArtistInfo()}.
	 * 
	 * This song won't be added if it is already existing inside the manager
	 * 
	 * @param song
	 *            The song that will be added to the main playlist.
	 * @return True if the song is not already contained in the playlist, false
	 *         otherwise.
	 */
	public boolean addSong(Mp3SongInfo song) {

		if (song != null && !mainSongs.contains(song)) {

			secureAdd(song);

			return true;
		}

		return false;
	}

	/**
	 * Performs a secure addition to the main playist. This will not ensure that
	 * the playlist does not already contain it.
	 * 
	 * @param song
	 *            The song to add
	 */
	private void secureAdd(Mp3SongInfo song) {
		if (song != null) {
			mainSongs.addSong(song);

			// Add the artist information
			ArtistInfo Ainfo = artists.get(song.getArtist());
			if (Ainfo != null) {
				Ainfo.addSong(song);
			} else {
				Ainfo = new ArtistInfo(song.getArtist());
				Ainfo.addSong(song);
				artists.put(song.getArtist(), Ainfo);
			}

			// Add the Album information
			AlbumInfo albumInfo = albums.get(song.getAlbum());
			if (albumInfo != null) {
				albumInfo.addSong(song);
			} else {
				albumInfo = new AlbumInfo(song.getAlbum());
				albumInfo.addSong(song);
				albums.put(song.getAlbum(), albumInfo);
			}
		}
	}

	/**
	 * Add the given collection of songs to the main playlist. Will have the
	 * exact same effect then adding them separately.
	 * 
	 * @see #addSong(Mp3SongInfo)
	 * @param songs
	 */
	public void addSongs(Collection<Mp3SongInfo> songs) {
		for (Mp3SongInfo song : songs) {
			addSong(song);
		}
	}

	/**
	 * Removes the given song from the main playlist and all the sub playlist in
	 * which it is contained. They are also removed from all artist and album
	 * information to which they are associated.
	 * 
	 * After this method is called, the given song will not be accessible
	 * anymore from the manager.
	 * 
	 * @param song
	 *            The song to be removed.
	 * @return True if the given song existed in the main playlist, false
	 *         otherwise.
	 */
	public boolean removeSong(Mp3SongInfo song) {
		boolean remove = mainSongs.removeSong(song);
		if (remove) {
			artists.get(song.getArtist()).remove(song);
			albums.get(song.getAlbum()).remove(song);

			for (Playlist playlist : subPlaylists) {
				playlist.removeSong(song);
			}
		}
		return remove;
	}

	public void removeSongForPath(String path) {
		Mp3SongInfo song = null;

		for (Mp3SongInfo info : mainSongs.getSongs()) {
			if (song.getPath().equals(path)) {
				song = info;
				break;
			}
		}
		if (song != null) {
			removeSong(song);
		}
	}

	public Mp3SongInfo findSong(Mp3SongInfo song) {
		for(Mp3SongInfo s : getAllSongsSorted()){
			if(s.equals(song)){
				return s;
			}
		}
		return null;
	}

}
