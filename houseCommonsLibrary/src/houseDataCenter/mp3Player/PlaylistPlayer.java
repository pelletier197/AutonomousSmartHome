package houseDataCenter.mp3Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import utils.Formatters;

/**
 * @author sunny
 *
 */

public class PlaylistPlayer {

	/**
	 * An enum of the possible reading mode.
	 * 
	 * <p>
	 * <b> NORMAL </b>:Used for regular reading in alphabetic order of the songs
	 * contained in the currently played playlist.
	 * </p>
	 * <p>
	 * <b> RANDOM </b>:Random reading through the playlist's song. Two songs are
	 * never played 2 times before the whole playlist is passed in the reading
	 * state.
	 * </p>
	 * <p>
	 * <b> REPEAT </b>:Indefinitely plays the current song in repeat until the
	 * song is manually changed via {@link PlaylistPlayer#playFrom(Mp3SongInfo)}
	 * </p>
	 * 
	 * @author sunny
	 *
	 */
	public enum READING_MODE {
		RANDOM, NORMAL, REPEAT;
	}

	/**
	 * The songs that must be played. Songs are passed from this parameter to
	 * {@link #previouslyPlayed} and {@link #playing}. It ensure memory economy
	 * and eliminated risks of duplicating a song.
	 */
	private List<Mp3SongInfo> songsToPlay;

	/**
	 * The songs that have previously been played through the player. It is
	 * possible to play them back again by using {@link #previous()}
	 */
	private Stack<Mp3SongInfo> previouslyPlayed;

	/**
	 * A property of the currently playing song.
	 */
	private ObjectProperty<Mp3SongInfo> playing;

	/**
	 * A property of the current reading mode.
	 */
	private ObjectProperty<READING_MODE> readingMode;

	/**
	 * Boolean property telling either the player is playing or not. This value
	 * will be to false when media will be on pause or no song will be playing.
	 */
	private BooleanProperty isPlaying;

	/**
	 * A property controlling media's volume.
	 */
	private DoubleProperty volume;

	/**
	 * Service reading timer and updating song's progress that can be accessed
	 * via {@link #songProgressProperty()}. This property varies from 0 to 1,
	 * where 0 is the beginning of the song and 1 is the end.
	 */
	private Service<Void> timer;

	/**
	 * The currently played playlist. May often be null if set this way.
	 */
	private ObjectProperty<Playlist> playlist;

	/**
	 * Used when passing from any mode to normal. It will ensure that the next
	 * song is the one that is after the current song.
	 */
	private int actualSortedIndex = 0;

	/**
	 * 
	 */
	private Mp3Player player;

	/**
	 * 
	 * Constructs a playlist player with no default songs to play. If
	 * {@link #play()} is called directly, an exception may be thrown.
	 * 
	 * The player will not be able to play any songs until
	 * {@link #setPlaylist(Playlist)} is called with a proper playlist to play.
	 * 
	 * By default, the playlist is set to the pause state with NORMAL reading
	 * mode.
	 */
	public PlaylistPlayer() {
		this(null);
	}

	/**
	 * Constructs a playlist that will play the songs from the given playlist
	 * when {@link #play()} or {@link #playFrom(Mp3SongInfo)} will be called.
	 * 
	 * By default, the player is set to pause state and have the NORMAL reading
	 * mode.
	 * 
	 * @param playlist
	 *            The playlist to be played in this player.
	 */
	public PlaylistPlayer(Playlist playlist) {

		this(playlist, READING_MODE.NORMAL);

	}

	/**
	 * 
	 * Creates a playlist player that will play the given playlist with the
	 * given reading mode when {@link #play()} or {@link #playFrom(Mp3SongInfo)}
	 * will be called.
	 * 
	 * At this state, player is on the pause state, which makes wait until
	 * method call.
	 * 
	 * @param playlist
	 *            The playlist to the be played
	 * @param mode
	 *            The required reading mode. Cannot be null.
	 */
	public PlaylistPlayer(Playlist playlist, READING_MODE mode) {

		songsToPlay = new LinkedList<Mp3SongInfo>();
		previouslyPlayed = new Stack<Mp3SongInfo>();
		isPlaying = new SimpleBooleanProperty(false);
		playing = new SimpleObjectProperty<>();
		readingMode = new SimpleObjectProperty<>(mode);
		volume = new SimpleDoubleProperty(100);
		this.playlist = new SimpleObjectProperty<>();
		this.player = new JavaFXPlayer();

		organizeReading(mode);
		setPlaylist(playlist);
		init();

		System.out.println(getSongsSorted().size() + " size");

	}

	/**
	 * Initializes the timer service by instantiating its task. This method must
	 * only be called at construction.
	 */
	private void init() {
		timer = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {

					@Override
					protected Void call() throws Exception {

						double actualTimeS = 0;

						while (!Thread.interrupted()) {
							if (playing.get() != null && player != null) {

								// Updates the current progress from the current
								// playback time in the media.
								actualTimeS = (player.getCurrentTime());
								System.out.println(actualTimeS + " current media time");
								if (actualTimeS < playing.get().getLength()) {

									updateProgress(actualTimeS, ((double) playing.get().getLength()));

								}
							} else {
								// 1 sec sleep
								Thread.sleep(500);
							}

							Thread.sleep(1000);
						}
						return null;
					}

				};

			}
		};
	}

	/**
	 * Organize the reading mode in function of the mode parameter.
	 * 
	 * RANDOM - will shuffle the songsToPlay List to make the playlist look
	 * random.
	 * 
	 * NORMAL - will sort the songsToPlayList to make it seems like it follows.
	 * The actualSortedIndex is used to ensure that the song that will follow
	 * will be the right one to be played after the current song
	 * 
	 * REPEAT - Do nothing : the repeat mode is handled in previous and next
	 * methods.
	 * 
	 * @param mode
	 */
	private void organizeReading(READING_MODE mode) {

		switch (mode) {
		case RANDOM:
			Collections.shuffle(songsToPlay);

			break;
		case NORMAL:

			System.out.println(songsToPlay.contains(getPlaying()) + " before");
			if (getPlaying() != null) {
				songsToPlay.add(getPlaying());

			}
			Collections.sort(songsToPlay);
			actualSortedIndex = songsToPlay.indexOf(getPlaying());
			try {
				songsToPlay.remove(getPlaying());
			} catch (IndexOutOfBoundsException e) {
				e.printStackTrace();
				actualSortedIndex = 0;
			}
			System.out.println(songsToPlay.contains(getPlaying()) + " after");

			break;
		default:
			break;
		}
		this.readingMode.set(mode);

	}

	/**
	 * Add the required listeners to the media, so it will play the next song on
	 * the end of this one.
	 */
	private void addMediaListener() {
		player.setOnfinished(new SongEvent() {

			@Override
			public void handle() {
				next();

			}
		});
	}

	/**
	 * Creates a new media that may be started if the player is in the playing
	 * state. This method requests the old media to stop and free its resources
	 * to display the song in parameter as the new playing song.
	 * 
	 * The media created will start to play directly after if needed.
	 * 
	 * @param playSong
	 */
	private synchronized void createNewMedia(Mp3SongInfo playSong) {

		this.playing.set(playSong);

		player.play(playSong.getPathAsURI());

		addMediaListener();

	}

	/**
	 * Used to know if there is at least one single song to play on this player.
	 * Will return true if either the currently playing is not null, or songs
	 * were previously played or songs are still left to play.
	 * 
	 * @return True there is a song to play, false otherwise.
	 */
	private boolean hasSongToPlay() {

		return !songsToPlay.isEmpty() || playing.get() != null || !previouslyPlayed.isEmpty();
	}

	/**
	 * Checks the songs to play in the queue and replaces the songs contained in
	 * {@link #getPlaying()} and {@link #previouslyPlayed} in
	 * {@link #songsToPlay} if it is empty. This will ensure an infinite reading
	 * through the songs even if the player is at the end of its songs.
	 */
	private void checkEmptyQueue() {
		// If there is no song left in the queue
		if (songsToPlay.isEmpty()) {
			if (!previouslyPlayed.isEmpty()) {
				songsToPlay.addAll(previouslyPlayed);
				previouslyPlayed.clear();
			}
			if (getPlaying() != null) {
				songsToPlay.add(getPlaying());
				playing.set(null);
			}
		}

	}

	/**
	 * Switches the media player of state. If the current state was pause, it
	 * will turn to play and vis-versa.
	 */
	public void switchPlaying() {
		if (player.isPlaying()) {
			pause();
		} else {
			play();
		}
	}

	/**
	 * Play the media from the current song set to play.
	 * 
	 * <p>
	 * If the current song to play is null, the first song in the songs to play
	 * is removed and chosen as the song to play
	 * </p>
	 * 
	 * <p>
	 * If there is no song to play in the player (null playlist or empty
	 * playlist), then no action is performed and the player remains on the
	 * pause state.
	 * </p>
	 * 
	 */
	public void play() {
		// if there is at least one song to play
		if (hasSongToPlay()) {

			checkEmptyQueue();

			// If we have no playing, we chose the first
			if (getPlaying() == null) {
				createNewMedia(songsToPlay.remove(0));
				// if we have no media, we create one with the actual playing
			} else if (player == null) {
				createNewMedia(getPlaying());
			}

			player.resume();
			isPlaying.set(true);
			timer.restart();
			addMediaListener();

		}
	}

	/**
	 * Pauses the player. This action will also stop timer being updated and
	 * will set playing state to false. After this method is called, no sounds
	 * should be played anymore.
	 * 
	 * Notice that if the media was currently not playing, no change is
	 * performed.
	 */
	public void pause() {

		if (player != null) {
			player.pause();
			isPlaying.set(false);
			timer.cancel();

		}
	}

	/**
	 * Plays the next song contained in the playlist. About that time, three
	 * things can happen.
	 * 
	 * <p>
	 * <b> The playlist is empty :</b> Nothing happens. No song will be played.
	 * </p>
	 * 
	 * <p>
	 * <b> All the songs have been read and the songs to read are empty:</b> The
	 * songs contained in {@link #previouslyPlayed} are stored back again in the
	 * songs to play. This ensures infinite reading. The next song in the list
	 * is then played.
	 * </p>
	 * 
	 * <p>
	 * <b> The playlist still have songs to play:</b> The next song is simply
	 * popped and played.
	 * </p>
	 */
	public void next() {
		System.out.println(getSongsSorted().size());
		if (getReadingMode() != READING_MODE.REPEAT) {

			// If there is a song to play
			if (hasSongToPlay()) {
				// Check to know if the queue is empty
				checkEmptyQueue();

				// Once we are sure to find a song in the queue, we start the
				// process
				// We push the old song in the previously played stack
				if (playing.get() != null) {
					previouslyPlayed.push(getPlaying());
				}
				// reorganize the reading mode to make sure the right song is
				// played
				organizeReading(getReadingMode());

				try {

					Mp3SongInfo song = songsToPlay.remove(actualSortedIndex);
					if (song == getPlaying()) {
						song = songsToPlay.remove(actualSortedIndex);
					}
					createNewMedia(song);
				} catch (IndexOutOfBoundsException e) {

					createNewMedia(songsToPlay.remove(0));
				}
				if (!isPlaying()) {
					player.pause();
				}
			}
		}
	}

	/**
	 * Plays the previous song contained in the playlist by popping the last
	 * played song from {@link #previouslyPlayed}. About that time, three things
	 * can happen.
	 * 
	 * <p>
	 * <b> The current playback time > 2 seconds :</b> Seeks the media time to
	 * the beginning. Will not play the previous song but start it from the
	 * beginning.
	 * </p>
	 * 
	 * <p>
	 * <b> There is no previous song OR the playlist is empty:</b> Nothing
	 * happens. No song is changed.
	 * </p>
	 * 
	 * <p>
	 * <b> The playlist have songs previously played and CAN play them:</b> The
	 * previous song is popped and played if the player was in the playing
	 * state, and displays it as the currently playing song otherwise.
	 * </p>
	 */
	public void previous() {

		if (player != null) {
			if (player.getCurrentTime() > 2) {

				setTime(0);

			} else if (!previouslyPlayed.isEmpty()) {
				System.out.println(previouslyPlayed.lastElement());
				// add the old playing song
				songsToPlay.add(getPlaying());

				organizeReading(readingMode.get());
				createNewMedia(previouslyPlayed.pop());

				if (!isPlaying()) {
					player.pause();
				}

			}
		}
	}

	/**
	 * Sets the progress of the song between 0 and 1, where 0 is begin time and
	 * 1 end time. If higher than 1, endTime is seek, if lower than 0, 0 is
	 * seek.
	 * 
	 * @see MediaPlayer#seek(Duration)
	 * 
	 * @param progress
	 *            The progress requested for the song.
	 */
	// TODO - LOOK FOR WRONG SEEK TIME
	public void setProgress(double progress) {

		if (getPlaying() != null) {
			player.seek(progress);

		} else
			setTime(0);
	}

	/**
	 * @return A read only property indicating song's progress in the total
	 *         time. This value is updated every second from {@link #timer} and
	 *         is stopped updated on the pause state.
	 */
	public ReadOnlyDoubleProperty songProgressProperty() {
		return DoubleProperty.readOnlyDoubleProperty(timer.progressProperty());
	}

	/**
	 * Starts playing the song in the playlist from the song given in parameter.
	 * From there, two things can happen.
	 * 
	 * <p>
	 * <b>The song is the one currently playing : </b> No change is made
	 * </p>
	 * 
	 * <p>
	 * <b>The song is not the one playing : </b> Player is stopped, song is
	 * switched to the new one and put to play state if it was not done.
	 * </p>
	 * 
	 * @param song
	 *            The song that must be played. This song must absolutely be
	 *            contained in the current playlist, or an exception will be
	 *            thrown.
	 * 
	 * @throws SongNotContainedInPlaylistException
	 *             If the song is not part of the current playlist playing.
	 * 
	 */
	public void playFrom(Mp3SongInfo song) throws SongNotContainedInPlaylistException {

		// We check if the song is playable
		if (hasSongToPlay() && (previouslyPlayed.contains(song) || (getPlaying() != null && getPlaying().equals(song))
				|| songsToPlay.contains(song))) {

			// If this is not the song that is currently playing, we move to
			// this song
			if (!song.equals(getPlaying())) {
				songsToPlay.addAll(previouslyPlayed);

				if (getPlaying() != null)
					songsToPlay.add(getPlaying());

				createNewMedia(song);
				play();
			}
		} else {
			throw new SongNotContainedInPlaylistException();
		}

	}

	/**
	 * Sets the playing song. This method will have absolutely no effect if the
	 * media is currently in the reading state. This method will create a change
	 * if {@link #play()} is called after this method has been called.
	 * 
	 * The song that will be played after this call will be the one set at this
	 * moment.
	 * 
	 * Otherwise, the song currently played keeps playing. However, the value
	 * accessed via {@link #getPlaying()} will be the one set by this method if
	 * it is called.
	 * 
	 * It is highly deprecated to use this method without any knowledge of its
	 * reaction.
	 * 
	 * @param song
	 *            The song that must be set as the playing song.
	 * @throws SongNotContainedInPlaylistException
	 *             If the song is not contained in the current playlist.
	 */
	public void setPlaying(Mp3SongInfo song) throws SongNotContainedInPlaylistException {

		if (song == null) {
			playing.set(null);
		}
		// If the song is contained in the song to play
		if (songsToPlay.remove(song)) {
			playing.set(song);

			// else, if the song is in the previously played stack
		} else if (previouslyPlayed.remove(song)) {
			if (getPlaying() != null) {
				// Puts the currently playing song in the songs to play.
				songsToPlay.add(getPlaying());
			}
			playing.set(song);

			// else the song can only be playing, or it's not contained in the
			// playlist
		} else if (song != getPlaying()) {
			throw new SongNotContainedInPlaylistException();
		}
	}

	/**
	 * Returns the time formatted for the progress given in parameter compared
	 * with the currently playing song. This progress may have any value, but is
	 * computed as if situated between 0 and 1.
	 * 
	 * @param progress
	 *            The progress time for the current song.
	 * @return A String representing the progress time on the total time of the
	 *         current time with the format (hh:)mm:ss where hours are optional.
	 */
	public String getTimeFormattedForProgress(double progress) {
		return getPlaying() != null ? Formatters.formatTime((long) (playing.get().getLength() * progress))
				: Formatters.formatTime(0);
	}

	/**
	 * Makes the player seek the media player to the time sent in parameter.
	 * This time is computed in seconds.
	 * 
	 * @see MediaPlayer#seek(Duration)
	 * @param time
	 *            The requested seek time in seconds.
	 */
	public void setTime(double time) {

		if (player != null) {

			player.seek(time / player.getTotalDuration());

		}
	}

	/**
	 * Resets the media player to the regular reading mode. This will clear the
	 * currently playing song, stop the media and clear its resources. The
	 * playlist is also cleared.
	 * 
	 * This method is usually used to restore the player and close it. However,
	 * it may be possible to reuse after this call by setting a new playlist.
	 */
	public void reset() {
		// Clears all the songs
		this.playing.set(null);
		this.songsToPlay.clear();
		this.previouslyPlayed.clear();

		// stops playing
		this.playlist.set(null);
		organizeReading(READING_MODE.NORMAL);
		isPlaying.set(false);
		if (player != null) {
			player.pause();
			player.dispose();
		}
	}

	/**
	 * Sets the player reading mode to the one in parameter. This reading mode
	 * may not be null, or an exception will occur. The reading mode will sort
	 * player reading order of the songs for the one specified by the mode.
	 * 
	 * @see {@link READING_MODE}
	 * 
	 * @param mode
	 *            The chosen mode, may not be null.
	 */
	public void setReadingMode(READING_MODE mode) {
		organizeReading(mode);
	}

	/**
	 * 
	 * @return All the songs contained in this player in a {@link TreeSet},
	 *         which keeps them constantly sorted.
	 */
	public SortedSet<Mp3SongInfo> getSongsSorted() {

		SortedSet<Mp3SongInfo> retour = new TreeSet<>();

		if (songsToPlay != null) {
			try {
				retour.addAll(songsToPlay);
			} catch (NullPointerException e) {

			}
		}

		retour.addAll(previouslyPlayed);

		if (getPlaying() != null)
			retour.add(getPlaying());

		return retour;
	}

	/**
	 * 
	 * @return A bindable property for the player's volume.
	 */
	public DoubleProperty volumeProperty() {
		return this.volume;
	}

	/**
	 * Sets the player volume to the requested one. For more information about
	 * volume, see mediaPlayer documentation.
	 * 
	 * @see MediaPlayer#setVolume(double)
	 * @param value
	 *            The volume value between 0 and 1 inclusive.
	 */
	public void setVolume(double value) {
		player.setVolume(value);
		volume.set(value);
	}

	/**
	 * @return The song currently played in the player. This song will be the
	 *         one played when {@link #play()} will be called if the media is on
	 *         pause, or the one currently played by the player.
	 * 
	 * @see #setPlaying(Mp3SongInfo)
	 */
	public Mp3SongInfo getPlaying() {
		return this.playing.get();

	}

	/**
	 * 
	 * @return A read only property of the song currently played by the player.
	 */
	public ReadOnlyObjectProperty<Mp3SongInfo> playingProperty() {
		return playing;

	}

	/**
	 * 
	 * @return A read only property of the playing property of the media. This
	 *         property is set to true when the player is playing mp3, and to
	 *         false otherwise.
	 */
	public ReadOnlyBooleanProperty isPlayingProperty() {
		return BooleanProperty.readOnlyBooleanProperty(isPlaying);
	}

	/**
	 * 
	 * @return The value of {@link #isPlaying}
	 */
	public boolean isPlaying() {
		return isPlaying.get();
	}

	/**
	 * @return The current song total time formatted with the format (hh:)mm:ss
	 *         where hours are optional.
	 */
	public String getTotalTimeFormatted() {
		return Formatters.formatTime(playing.get().getLength());
	}

	/**
	 * 
	 * @return Media's current time in seconds. If the media is null, 0 is
	 *         returned.
	 */
	public long getCurrentTime() {
		return (long) (player == null ? 0 : player.getCurrentTime());
	}

	/******************************************************************
	 * * For all the Research things * * *
	 *****************************************************************/
	/**
	 * Precises the research type allowed to be made in the playlist.
	 * 
	 * @author sunny
	 *
	 */
	public enum ResearchType {
		BY_ALBUM, BY_TITLE, BY_ARTIST, ALL
	}

	/******************************************************************
	 * * For all the fast time event things. Fast forward/backward * * *
	 *****************************************************************/
	/**
	 * Values that help to handle pressed/held events on {@link #next} and
	 * {@link #previous} buttons
	 */
	private boolean isPressed = false;
	/**
	 * The last pressed time.
	 */
	private long pressedTime = 0;

	/**
	 * The last release time
	 */
	private long releaseTime = 0;

	/**
	 * The current event being run in the forwardBackwardUpdate. Used to always
	 * have a reference on the actual source of the event.
	 */
	private FastTimeEvent currentEvent;

	public enum FastTimeEvent {
		FORWARD, BACKWARD
	}

	/**
	 * A service that whether check if the mouse is being held by the user, and
	 * react in consequence. If the mouse is held on next or previous button,
	 * the time of the {@link #playlist} will be changed every 500 ms. In the
	 * case that the user doesn't hold the mouse for more than 1 second, it is
	 * considered as a click, and {@link #playlist.next()} or
	 * {@link #playlist.previous()} method from playlist is called by
	 * {@link #controlButtonReleased(MouseEvent)} method.
	 */
	private Service<Void> forwardBackwardUpdate;

	/**
	 * Notifies that the next or previous button is pressed. This method is used
	 * to trigger a varying seek time depending on time that the mouse is held
	 * by the user. Calling this method will ensure that the playback seeks to
	 * the next step time that will depend on how long this method will be
	 * called.
	 * 
	 * No end this playback fast event, call
	 * {@link #forwardOrBackwardReleased()}, which will resume regular play.
	 * 
	 * @param bEvent
	 *            The time event requested by the user.
	 */
	public void forwardOrBackwardPressed(FastTimeEvent bEvent) {

		this.currentEvent = bEvent;
		isPressed = true;

		pressedTime = System.currentTimeMillis();

		if (forwardBackwardUpdate == null) {
			forwardBackwardUpdate = new Service<Void>() {

				@Override
				protected Task<Void> createTask() {
					return new Task<Void>() {

						@Override
						protected Void call() throws Exception {
							try {
								Thread.sleep(300);
							} catch (Exception e1) {

							}

							int timeIncrement = 10;
							int cycleCount = 0;

							// while the mouse is held by the user
							while (isPressed) {

								if (currentEvent == FastTimeEvent.FORWARD) {
									setTime(getCurrentTime() + timeIncrement);
								} else {
									setTime(getCurrentTime() - timeIncrement);
								}
								try {
									// The more the user pressed long, the
									// shorter sleep time is, and the bigger the
									// steps are.
									Thread.sleep(800);
								} catch (Exception e) {

								}

							}
							releaseTime = System.currentTimeMillis();

							// If held for less than 1000 second, next or
							// previous is called
							if (releaseTime - pressedTime < 1000) {

								if (currentEvent == FastTimeEvent.FORWARD) {
									Platform.runLater(new Runnable() {

										@Override
										public void run() {
											next();
										}
									});
								} else {
									Platform.runLater(new Runnable() {

										@Override
										public void run() {
											previous();
										}
									});
								}
							}
							return null;
						}
					};

				}

			};
		}
		if (!forwardBackwardUpdate.isRunning())
			forwardBackwardUpdate.restart();

	}

	/**
	 * Notifies the player that the next or previous seeking step must end. This
	 * method will have no effect if
	 * {@link #forwardOrBackwardPressed(FastTimeEvent)} has not been called
	 * previously.
	 * 
	 * After this method is called, player will resume normally.
	 */
	public void forwardOrBackwardReleased() {
		isPressed = false;

	}

	/**
	 * 
	 * @return A read only object property of the player reading mode.
	 */
	public final ReadOnlyObjectProperty<READING_MODE> readingModeProperty() {
		return this.readingMode;
	}

	/**
	 * 
	 * @return Current player's reading mode.
	 */
	public final READING_MODE getReadingMode() {
		return this.readingModeProperty().get();
	}

	/**
	 * 
	 * @return The current playlist as a read only property
	 */
	public final ReadOnlyObjectProperty<Playlist> playlistProperty() {
		return this.playlist;
	}

	/**
	 * 
	 * @return The current playlist being read.
	 */
	public final Playlist getPlaylist() {
		return this.playlistProperty().get();
	}

	/**
	 * Sets the player playlist. The songs contained in this playlist will be
	 * the next to be played. By default, no song is chosen. It is possible to
	 * chose the first song to play by using {@link #setPlaying(Mp3SongInfo)} if
	 * the player is currently on pause state.
	 * 
	 * If another song was playing in the previous playlist and in was not
	 * contained in the new playlist, it will keep playing until it is changed,
	 * but it will not be possible to play it again when it will change.
	 * 
	 * @param playlist
	 */
	public final void setPlaylist(final Playlist playlist) {

		if (playlist == null || playlist != getPlaylist()) {
			this.playlist.set(playlist);
			previouslyPlayed.clear();
			songsToPlay.clear();
			try {
				setPlaying(null);
			} catch (SongNotContainedInPlaylistException e1) {

			}

			if (playlist != null) {

				// Add the songs to the future song going to be played
				songsToPlay = new ArrayList<>(playlist.getSongs());
				System.out.println(songsToPlay.size() + " after init");
				try {
					System.out.println(songsToPlay);
					createNewMedia(songsToPlay.remove((int) (Math.random() * songsToPlay.size())));

					if (!isPlaying()) {
						player.pause();
					}

				} catch (SongNotContainedInPlaylistException e) {

				} catch (IndexOutOfBoundsException e1) {

				}
				System.out.println(isPlaying() + " " + playlist.getName());

			}
		}
	}

	public void updatePlaylist() {
		Playlist current = getPlaylist();

		if (current != null) {

			previouslyPlayed.clear();
			songsToPlay.clear();

			// Add the songs to the future song going to be played
			songsToPlay = new ArrayList<>(current.getSongs());
			songsToPlay.remove(getPlaying());
		}

	}

	@Override
	public String toString() {

		String retour = "";
		for (Mp3SongInfo song : songsToPlay) {
			retour += song.toString() + "\n";
		}

		return retour;

	}

	/**
	 * Will play the previous song in the playlist without ensuring that the
	 * song has been playing for 2 seconds at least
	 */
	public void forcePrevious() {

		if (!previouslyPlayed.isEmpty()) {
			System.out.println(previouslyPlayed.lastElement());
			// add the old playing song
			songsToPlay.add(getPlaying());

			organizeReading(readingMode.get());
			createNewMedia(previouslyPlayed.pop());

			if (!isPlaying()) {
				player.pause();
			}

		}
	}
}
