package houseDataCenter.mp3Player;

/**
 * Represents a runtime exception thrown by a {@link PlaylistPlayer} when an
 * attempt to play a song fails because the given song does not exist in the
 * currently displayed playlist in the player.
 * 
 * @author sunny
 *
 */
public class SongNotContainedInPlaylistException extends RuntimeException {

	private static final long serialVersionUID = -3507376345757445509L;

}
