package houseDataCenter.mp3Player;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Container keeping together information about a given artist.This keep
 * together songs that has the artist as their given artist. It is possible to
 * add a given song to the album's information using
 * {@link #addSong(Mp3SongInfo)}. You can also removed it via
 * {@link #remove(Mp3SongInfo)}.
 * 
 * @author sunny
 *
 */
public class ArtistInfo implements Comparable<ArtistInfo> {

	/**
	 * The given artist of the songs contained in {@link #songs}
	 */
	private Artist artist;

	/**
	 * All the songs that has {@link #artist} as their artist.
	 */
	private SortedSet<Mp3SongInfo> songs;

	/**
	 * Creates artist information from the given artist.
	 * 
	 * @param artist
	 *            The artist from which information is created. Cannot be null.
	 */
	public ArtistInfo(Artist artist) {
		if (artist == null) {
			throw new NullPointerException("Null artist");
		}
		this.artist = artist;
		this.songs = new TreeSet<>();
	}

	/**
	 * Add the given song to the artist's list of songs. If the song is null,
	 * false is returned. If the song is already contained in the album, false
	 * is also returned.
	 * 
	 * @param song
	 *            The song to be added.
	 * @return True if the song was successfully added, false otherwise.
	 */
	public boolean addSong(Mp3SongInfo song) {

		boolean added = false;

		if (song != null && song.getArtist().equals(artist)) {
			added = songs.add(song);
		}

		return added;
	}

	/**
	 * Removes the given song from artist's list of songs.
	 * 
	 * @param song
	 *            The song to be removed.
	 * @return True if the song was successfully removed, false otherwise.
	 */
	public boolean remove(Mp3SongInfo song) {
		return songs.remove(song);
	}

	/**
	 * 
	 * @return The artist from which information was created.
	 */
	public Artist getArtist() {
		return artist;
	}

	/**
	 * 
	 * @return The songs that have the given artist as their artist.
	 */
	public SortedSet<Mp3SongInfo> getSongs() {
		return songs;
	}

	/**
	 * 
	 * @return The number of songs that have {@link #artist} as their artist.
	 */
	public int getNumberOfSongs() {
		return songs.size();
	}

	/**
	 * By default, artist information are comparable by their artists.
	 * 
	 * @see Artist#compareTo(Artist)
	 * @param artist
	 *            The compared artist
	 * @return See original documentation from {@link Comparable}
	 */
	@Override
	public int compareTo(ArtistInfo artist) {

		return this.artist.compareTo(artist.artist);
	}

}
