package houseDataCenter.commucationData;

public class TransferFlags {

	/********************************************************
	 ********************************************************
	 ** Flags for managing of the server **
	 ********************************************************
	 *******************************************************/
	public static final int ACCESS_GRANTED = -7;
	public static final int ACCESS_REFUSED = -6;
	public static final int NOT_AUTHENTICATED = -5;
	public static final int REQUEST_IDENTIFICATION_DATA = -4;
	public static final int REQUEST_DEVICE_TYPE = -3;
	public static final int ROOM_CONTROLLER = -2;
	public static final int DEVICE_INTELLIGENT = -1;

	/********************************************************
	 ********************************************************
	 ** Flags for music management **
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_MUSIC_FLAGS = 0;
	/**
	 * Tells the server to play the next song in the playlist
	 */
	public static final int FORWARD_OR_BACK_PRESSED = 1;

	/**
	 * Tells the server to play from the song set as object
	 */
	public static final int PLAY_FROM = 2;

	/**
	 * Sets the playlist selected to the one set as object. The playlists are
	 * equals by name. Therefore, sending the name as a string is the key to
	 * change it.
	 */
	public static final int SET_MP3_PLAYLIST = 3;

	/**
	 * 
	 */
	public static final int FORWARD_OR_BACK_RELEASED = 4;

	/**
	 * 
	 */
	public static final int SET_MP3_ALBUM = 5;

	/**
	 * 
	 */
	public static final int SET_MP3_ARTIST = 6;

	/**
	 * 
	 */
	public static final int SET_MP3_ = 7;

	/**
	 * 
	 */
	public static final int PAUSE_MP3 = 8;

	/**
	 * 
	 */
	public static final int PLAY_MP3 = 9;

	/**
	 * 
	 */
	public static final int UPDATE_PLAYING = 10;

	/**
	 * 
	 */
	public static final int UPDATE_TIME = 11;

	/**
	 * 
	 */
	public static final int SET_PLAYING = 12;

	/**
	 * 
	 */
	public static final int SET_READING_MODE = 13;

	/**
	 * 
	 */
	public static final int SET_PROGRESS = 14;

	public static final int SET_MP3_VOLUME = 15;

	public static final int SWITCH_PLAYING = 16;

	public static final int REQUEST_MUSIC_STATUS = 17;

	public static final int NEXT_SONG = 18;

	public static final int PREVIOUS_SONG = 19;

	public static final int REQUEST_MUSIC_READING_MODE = 20;
	public static final int NOTIFY_MEDIA_PLAYING_CHANGED = 21;
	public static final int NOTIFY_MEDIA_STATUS_CHANGED = 22;
	public static final int NOTIFY_READING_MODE_CHANGED = 23;
	public static final int UPLOAD_NEW_MUSIC = 24;
	public static final int FORCE_PREVIOUS_MUSIC = 25;

	public static final int END_MUSIC_FLAGS = 999;

	/********************************************************
	 ********************************************************
	 ** Audio and vocal recognition **
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_VOCAL_REQUEST_FLAG = 1000;

	public static final int AUDIO_DATA_INPUT = 1001;
	public static final int PLAY_AI_ANSWER = 1002;

	public static final int END_VOCAL_REQUEST_FLAG = 1999;

	/********************************************************
	 ********************************************************
	 ** Flags for creation of resident **
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_RESIDENT_MANAGEMENT_FLAGS = 2000;

	public static final int REQUEST_RESIDENTS_INFO = 2001;
	public static final int REQUEST_RESIDENT_CREATION = 2002;
	public static final int REQUEST_MAIL_CONFIRMATION_CODE = 2003;
	public static final int SUCCESS_CREATION = 2004;
	public static final int FAILURE_CREATION_INVALID_CONFIRMATION = 2005;
	public static final int CREATION_CANCELED_PROTOCOL_TIMEOUT = 2006;
	public static final int FAILURE_CREATION_INVALID_PICTURES = 2007;
	public static final int FAILURE_CREATION_INVALID_NAME = 2008;
	public static final int FAILURE_CREATION_INVALID_BORNDATE = 2009;
	public static final int FAILURE_CREATION_INVALID_MAIL = 2010;
	public static final int FAILURE_CREATION_INVALID_PIN = 2011;
	public static final int REQUEST_MAIL_AND_RESIDENT_CONFIRMATION = 2012;
	public static final int FAILURE_ADD_EMAIL_USED = 2013;

	public static final int END_RESIDENT_MANAGEMENT_FLAGS = 2999;

	/********************************************************
	 ********************************************************
	 ** Flags for alarm management **
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_ALARM_MANAGEMENT_FLAGS = 3000;

	public static final int ALARM_RINGING = 3001;
	public static final int REQUEST_ROOM_ALARMS = 3002;
	public static final int CREATE_ALARM = 3003;
	public static final int DEACTIVATE_ALARM = 3004;
	public static final int ACTIVATE_ALARM = 3005;
	public static final int UPDATE_ALARM = 3006;
	public static final int REMOVE_ALARM = 3007;
	public static final int SNOOZE_ALARM = 3008;

	public static final int END_ALARM_MANAGEMENT_FLAGS = 3999;

	/********************************************************
	 ********************************************************
	 ** Room management flags
	 ********************************************************
	 *******************************************************/

	public static final int BEGIN_ROOM_MANAGEMENT_FLAGS = 4000;

	public static final int REQUEST_TEMP_UP = 4001;
	public static final int REQUEST_TEMP_DOWN = 4002;
	public static final int SET_TEMP = 4003;
	public static final int REQUEST_ROOM_TEMP_UNIT = 4004;
	public static final int REQUEST_TEMP = 4005;
	public static final int REQUEST_ROOMS = 4006;
	public static final int NEW_ROOM_CONNECTED = 4007;
	public static final int SET_ALL_ROOMS_LANGUAGE = 4008;
	public static final int SET_LANGUAGE = 4009;
	public static final int REQUEST_SERVER_LANGUAGE = 4010;
	public static final int SET_SERVER_LANGUAGE = 4011;
	public static final int SET_ALL_ROOM_TEMP_DISPLAY = 4012;
	public static final int SET_TEMP_DISPLAY = 4013;
	public static final int SET_ROOM_NAME = 4014;
	public static final int REQUEST_ROOM_NAME = 4015;
	public static final int SET_ROOM_VOLUME = 4016;
	public static final int VOLUME_UP = 4017;
	public static final int VOLUME_DOWN = 4018;
	public static final int SET_ROOM_TEMP_DISPLAY = 4019;


	public static final int END_ROOM_MANAGEMENT_FLAGS = 4999;

	/********************************************************
	 ********************************************************
	 ** System management flags
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_SYSTEM_MANAGEMENT_FLAGS = 5000;

	public static final int REQUEST_SERVER_EXTERNAL_IP = 5001;
	public static final int REQUEST_LANGUAGE = 5002;
	public static final int REQUEST_TEMP_DISPLAY = 5003;

	public static final int END_SYSTEM_MANAGEMENT_FLAGS = 5999;

	/********************************************************
	 ********************************************************
	 ** Camera management flags
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_CAMERA_MANAGEMENT_FLAGS = 6000;

	public static final int REQUEST_CAMERA_STREAMING = 6001;
	public static final int SET_UP_CAMERA_STREAMING = 6002;
	public static final int END_CAMERA_STREAMING = 6003;

	public static final int END_CAMERA_MANAGEMENT_FLAGS = 6999;

	/********************************************************
	 ********************************************************
	 ** Lights management flags
	 ********************************************************
	 *******************************************************/
	public static final int BEGIN_LIGHT_MANAGEMENT_FLAGS = 7000;

	public static final int REQUEST_ROOM_LIGHTS = 7001;
	public static final int SET_LIGHT_COLOR = 7002;
	public static final int SET_LIGHT_LUMINOSITY = 7003;
	public static final int OPEN_ALL_LIGHTS = 7004;
	public static final int CLOSE_ALL_LIGHTS = 7005;
	public static final int RESET_LIGHT_COLOR = 7006;
	public static final int UPDATE_LIGHT_STATUS = 7007;
	public static final int REQUEST_AVAILABLE_LIGHTS = 7008;
	public static final int LIGHT_TEST = 7009;
	public static final int UPDATE_ROOM_LIGHTS = 7010;
	public static final int OPEN_LIGHT = 7011;
	public static final int CLOSE_LIGHT = 7012;
	public static final int RESET_LIGHTS_COLOR = 7013;
	public static final int SET_LIGHTS_LUMINOSITY = 7014;
	public static final int SET_LIGHTS_COLOR = 7015;
	public static final int SET_LIGHT_NAME = 7016;
	public static final int LIGHT_STATE_CHANGED = 7017;
	public static final int MULTIPLE_LIGHTS_STATE_CHANGED = 7018;
	public static final int GET_LIGHT_STATUS = 7019;

	public static final int END_LIGHT_MANAGEMENT_FLAGS = 7999;

}
