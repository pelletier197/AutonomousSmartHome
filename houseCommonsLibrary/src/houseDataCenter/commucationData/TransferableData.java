package houseDataCenter.commucationData;

import java.io.Serializable;

public class TransferableData implements Serializable {



	/**
	 * 
	 */
	private static final long serialVersionUID = -3493325518552057832L;

	private int flag;
	private Object data;

	public TransferableData(int flag, Object data) {

		this.flag = flag;
		this.data = data;

	}

	public TransferableData(int flag) {
		this(flag, null);
	}

	public int getFlag() {
		return flag;
	}

	public Object getData() {
		return data;
	}

	@Override
	public String toString() {
		return "TransferableData [flag=" + flag + ", data=" + data + "]";
	}

}
