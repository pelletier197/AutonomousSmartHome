package houseDataCenter.math;

public enum Constant {
	//Avoid regular letters as representation @formatter:off
	PI("\u03C0", "\u03C0", Math.PI),
	N_A("N<sub>A</sub>", "N",  6.02214179e23),
	E("e", "e", Math.E),
	H("h","h",6.626070040e-34),
	H_BAR("\u210F","\u210F",1.0545718e-34),
	G("G","G",6.67408e-11),
	K("k","k", 8.9875517873681764e9),
	C("c","|",299792458),
	BOLTZMAN("\u03C3","\u03C3",5.67036713e-8),
	FARADAY("e<sub>c</sub>","$",1.6021766e-19);
	
	;
	//@formatter:on

	private String representation;
	private String symbol;
	private double value;

	private Constant(String symbol, String representation, double value) {
		this.representation = representation;
		this.symbol = symbol;
		this.value = value;
	}

	public String getRepresentation() {
		return representation;
	}

	public String getSymbol() {
		return symbol;
	}

	public double getValue() {
		return value;
	}
}
