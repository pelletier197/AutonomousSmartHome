package houseDataCenter.math;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComplexCalculator {

	/**
	 * The calculator's settings
	 */
	private CalculatorSettings settings;

	/**
	 * The simple calculator, solving calculus between 2 variables during
	 * solving phase called by {@link #equals()}
	 */
	private Calculator calculator;

	/**
	 * The current complete expression of the calculator. This expression is
	 * evaluated only when {@link #equals()} is called. When evaluated, the
	 * syntax must match perfectly calculator's syntax, or an exception will be
	 * thrown.
	 */
	private String completeExpression;

	/**
	 * Contains exactly the same expression as {@link #completeExpression}, but
	 * remains formatted as long as operations are computed.
	 */
	private String formattedExpression;

	/**
	 * The current number being written. As soon as a value different than a
	 * number is entered via {@link #enterValue(String)}, this value is restored
	 * to an empty String.
	 */
	private String currentNumber;

	/**
	 * The begin index of {@link #currentNumber} in {@link #completeExpression}.
	 * This value will remain the same until the current number is completely
	 * erased or a value different than a number is inserted in
	 * {@link #enterValue(String)}. When this number is not pointing any current
	 * index in the list, it should always have the value of -1.
	 */
	private int currentBeginIndex;

	/**
	 * The current cursor's position in the complete expression. Every time a
	 * number is inserted, it is inserted at the index indicated by this
	 * variable.
	 */
	private int cursorPosition;

	/**
	 * The last result indicated by the calculator. This variable always has the
	 * value of {@link Double #NaN} when no value is stored in it.
	 */
	private double result;

	/**
	 * The operators allowed to be used when calling {@link #enterValue(String)}
	 * .
	 */
	public static final String[] operators = new String[] { "sin", "cos", "tan", "asin", "acos", "atan", "log", "ln",
			"+", "-", "*", "/", "+/-", "nPr", "nCr", "x!", "\u221A", "1/x", "x^2", "x^3", "x^y", "%", "mod", "(", ")" };

	/**
	 * The patterns associated to the {@link #operators}. The index of a certain
	 * pattern is associated to the operator of the same index in
	 * {@value #operators}.
	 */
	private static final String[] patterns = new String[] { "@()", "@()", "@()", "@()", "@()", "@()", "@()", "@()",
			" @ ", " @ ", " @ ", " @ ", null, "()P()", "()C()", "(@)!", "\u221A(@)", "1/@", "@^(2)", "@^(3)", "@^()",
			"(@)%", "()mod()" };

	/**
	 * A map of constants generated at the calculator's construction. The key is
	 * the symbol of the constant, either a letter or Greek letter.
	 */
	private Map<String, Double> constants;

	/**
	 * A private map of method, which are associated to their method. This map
	 * is generated at construction, and is used when {@link #equals()} is
	 * called.
	 */
	private Map<String, Method> operations;

	public static final String decimal = ".";

	private boolean solved = false;

	private NumberFormat formatter;

	/**
	 * 
	 */
	public ComplexCalculator() {

		this.calculator = new Calculator();

		this.currentNumber = "";
		this.completeExpression = "";
		this.constants = new HashMap<>();
		this.formattedExpression = "";
		this.settings = new CalculatorSettings();

		this.formatter = NumberFormat.getInstance();
		formatter.setMaximumFractionDigits(8);
		formatter.setMinimumFractionDigits(0);
		formatter.setMinimumIntegerDigits(0);

		for (Constant c : Constant.values()) {
			constants.put(c.getRepresentation(), c.getValue());
		}

		generateOperations();
	}

	/**
	 * Generates the map of operators associated to their methods.
	 */
	private void generateOperations() {

		this.operations = new HashMap<>();
		try {

			// Puts the trigonometric operators
			this.operations.put("sin", Calculator.class.getMethod("sin", double.class));
			this.operations.put("cos", Calculator.class.getMethod("cos", double.class));
			this.operations.put("tan", Calculator.class.getMethod("tan", double.class));
			this.operations.put("acos", Calculator.class.getMethod("acos", double.class));
			this.operations.put("asin", Calculator.class.getMethod("asin", double.class));
			this.operations.put("atan", Calculator.class.getMethod("atan", double.class));

			// puts log and ln values
			this.operations.put("log", Calculator.class.getMethod("log10", double.class));
			this.operations.put("ln", Calculator.class.getMethod("ln", double.class));

			// Puts regular operations
			this.operations.put("+", Calculator.class.getMethod("addition", double.class, double.class));
			this.operations.put("-", Calculator.class.getMethod("subtraction", double.class, double.class));
			this.operations.put("*", Calculator.class.getMethod("multiplication", double.class, double.class));
			this.operations.put("/", Calculator.class.getMethod("division", double.class, double.class));
			this.operations.put("mod", Calculator.class.getMethod("modulo", double.class, double.class));
			this.operations.put("P", Calculator.class.getMethod("nPr", double.class, double.class));
			this.operations.put("C", Calculator.class.getMethod("nCr", double.class, double.class));
			this.operations.put("!", Calculator.class.getMethod("factorial", double.class));
			this.operations.put("^", Calculator.class.getMethod("pow", double.class, double.class));
			this.operations.put("\u221A", Calculator.class.getMethod("sqrt", double.class));
			this.operations.put("%", Calculator.class.getMethod("percent", double.class));

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param valueEntered
	 */
	public void enterValue(String valueEntered) {

		// if it is a digit
		if (valueEntered.matches("\\d") || isConstant(valueEntered) || isParentheses(valueEntered)
				|| valueEntered.equals(decimal)) {

			// if it is the decimal, will insert if and only if the current
			// number doesn't contain 1
			if ((currentNumber.length() > 0 && !currentNumber.contains(".")) || !valueEntered.equals(decimal)) {

				// If the calcul was previously solved but never erased, it is
				// done when a value is entered, that is not an operator.
				if (solved) {
					clear();
				}

				// If the number starts to be written, current begin index
				// starts
				// now.
				if (currentNumber.trim().isEmpty()) {
					currentBeginIndex = cursorPosition;
				}

				// First insert the value in the current number at the relative
				// position
				StringBuffer buffer = new StringBuffer(currentNumber);
				try {
					buffer.insert(relativePosition(), valueEntered);
				} catch (Exception e) {
					// When the user came back and there was no number before.
					// The
					// number is started to be written
					buffer.append(valueEntered);

				}
				this.currentNumber = buffer.toString();

				// Then modifies the current expression and insert the number at
				// cursor's position
				buffer = new StringBuffer(completeExpression);
				buffer.insert(cursorPosition, valueEntered);
				this.completeExpression = buffer.toString();

				cursorPosition++;
			}

		} else if (isOperator(valueEntered)) {

			// Computes the expression with the current number
			computeExpression(valueEntered);

		} else {
			throw new InvalidOperatorException(valueEntered + " is not a valid operator");
		}

		// Whatever happened, the equation is not solved anymore
		solved = false;

		// The current value is now the newExpression
		System.out.println(currentNumber + " current");
		System.out.println(formattedExpression + " formatted");
	}

	public void enterValue(Constant c) {
		if (shouldMultiply()) {
			enterValue("*");
		}
		enterValue(c.getRepresentation());
	}

	public void moveCursorRight() {
		// In the matching, the current letter in the one pointed by the cursor.
		// At least, the pointer must not point a letter that is not a constant,
		// when the one on its left, cursor index-1 is also not a constant.
		if (cursorPosition < completeExpression.length()) {

			String cursorChar = "";
			do {

				cursorChar = String.valueOf(completeExpression.charAt(cursorPosition));

				cursorPosition++;

				if (cursorPosition >= completeExpression.length()) {
					break;
				}
			} while (!cursorChar.matches("[\\d-+*/.()]") && !isConstant(cursorChar));
			computeCurrentNumber();
		}
	}

	public void moveCursorLeft() {
		if (cursorPosition > 0) {

			String cursorChar = "";
			do {
				try {
					cursorChar = String.valueOf(completeExpression.charAt(cursorPosition - 2));
				} catch (Exception e) {
					cursorPosition = 0;
					break;
				}
				cursorPosition--;

				if (cursorPosition >= completeExpression.length()) {
					break;
				}

			} while (!cursorChar.matches("[\\d-+*/.()]") && !isConstant(cursorChar));
			computeCurrentNumber();
			System.out.println(cursorChar + " chaaaaaaaaaar");
		}
	}

	public String getActualExpression() {

		return completeExpression;
	}

	public String getCurrentNumber() {
		return currentNumber;
	}

	public int getCursorIndex() {
		return cursorPosition;
	}

	public CalculatorSettings getSettings() {
		return settings;
	}

	/**
	 * 
	 * @param operator
	 */
	private void computeExpression(String operator) {

		// The computation pattern of the object.
		final String pattern = patterns[operatorIndex(operator)];
		final char[] numChar = completeExpression.toCharArray();
		StringBuffer buffer = new StringBuffer(completeExpression);

		if (isBasicOperator(operator)) {
			// Replaces the current number in the complete current expression if
			// the operator sent is a basic operator + - * /
			if (isBasicOperator(String.valueOf(numChar[cursorPosition - 1]))) {

				// Insert the new operator at other's place, and removes the old
				// one
				completeExpression = buffer.replace(cursorPosition - 1, cursorPosition, operator).toString();

			} else {

				// Simply inserts the operator if the operator before is not
				// one.
				completeExpression = buffer.insert(cursorPosition, operator).toString();
				cursorPosition += 1;
			}

			currentNumber = "";
			// As it is going to be incremented after this operation is called.
			currentBeginIndex = cursorPosition;

		} else if (operator.equals("+/-")) {

			// For this special operator, the number is simply converted to
			// negative
			// value
			negateCurrent();

		} else if (pattern.equals("()P()") || pattern.equals("()C()")) {

			// This pattern reserved for combination is special. Insert the old
			// value in parentheses, and position the cursor inside the second
			// parentheses .
			String op = pattern.equals("()P()") ? "P" : "C";

			buffer.replace(currentBeginIndex, currentBeginIndex + currentNumber.length(),
					"(" + currentNumber + ")" + op + "()");
			this.completeExpression = buffer.toString();

			this.currentBeginIndex += currentNumber.length() + 4;
			this.cursorPosition = currentBeginIndex;

			this.currentNumber = "";
		} else if (pattern.equals("()mod()")) {
			buffer.replace(currentBeginIndex, currentBeginIndex + currentNumber.length(),
					"(" + currentNumber + ")" + "mod" + "()");
			this.completeExpression = buffer.toString();

			// moves the cursor from "()mod(".length()
			this.currentBeginIndex += currentNumber.length() + 6;
			this.cursorPosition = currentBeginIndex;

			this.currentNumber = "";
		} else if (pattern.equals("@()")) {

			// Regular computation
			// The operator is simply inserted inside the required position
			// indexed by the @ symbol.

			// If the current number is not empty, then the user wants to
			// multiply by this operator.
			if (shouldMultiply()) {
				enterValue("*");

				// reset the buffer to the new expression
				buffer = new StringBuffer(completeExpression);
			}
			char current;
			char[] patternArray = pattern.toCharArray();
			String result = "";

			for (int i = 0; i < pattern.length(); i++) {
				current = patternArray[i];

				if (current == '@') {
					result += operator;
				} else {
					result += current;
				}
			}
			// insert the pattern in the complete expression
			buffer.insert(cursorPosition, result);
			completeExpression = buffer.toString();

			// Resets the current number
			cursorPosition += result.length() - 1;
			currentBeginIndex = cursorPosition;
			currentNumber = "";

		} else if (pattern.equals("(@)!") || pattern.equals("(@)%")) {

			if (shouldMultiply() && currentNumber.trim().isEmpty()) {
				enterValue("*");
				buffer = new StringBuffer(completeExpression);
			}
			// Simple pattern for factorial and %
			completeExpression = buffer.replace(currentBeginIndex, currentBeginIndex + currentNumber.length(),
					"(" + currentNumber + ")" + pattern.charAt(3)).toString();
			if (!currentNumber.trim().isEmpty()) {
				cursorPosition += 3;
			} else {
				cursorPosition += 1;
			}

			currentNumber = "";
			currentBeginIndex = cursorPosition;

		} else if (pattern.equals("\u221A(@)")) {
			if (shouldMultiply()) {
				enterValue("*");
				buffer = new StringBuffer(completeExpression);
			}
			completeExpression = buffer.insert(cursorPosition, "\u221A()").toString();

			cursorPosition += 2;
			currentBeginIndex = cursorPosition;
			currentNumber = "";
		} else if (pattern.equals("1/@")) {

			// pattern for the 1/x entry.
			if (currentNumber.trim().isEmpty()) {
				completeExpression = buffer.insert(cursorPosition, "1/").toString();
			} else {
				// will display 1/currentNumber
				completeExpression = buffer.replace(currentBeginIndex, currentNumber.length() + currentBeginIndex,
						"1/".concat(currentNumber)).toString();

				// The index of beginning of the number is incremented by 2 for
				// 1/
				currentBeginIndex += 2;
			}
			cursorPosition += 2;

			// Exponent
		} else if (pattern.matches("@\\^((\\(\\d*\\)))")) {
			System.out.println("pattern compiled");

			// Compiles the exponent TODO -look for pattern solution efficiency
			if (currentNumber.trim().isEmpty()) {
				// replaces the cursor for empty number
				currentBeginIndex = cursorPosition;
			}
			String exponent = "";
			// reads the exponent after @^ of regular pattern for exponent
			for (int i = 1; i < pattern.length(); i++) {
				exponent += pattern.charAt(i);
			}
			String expression = "(".concat(currentNumber).concat(")").concat(exponent);

			// replaces the complete expression
			completeExpression = buffer
					.replace(currentBeginIndex, currentBeginIndex + currentNumber.length(), expression).toString();

			// replaces cursor
			if (currentNumber.trim().isEmpty()) {
				// only move from 1 for (
				cursorPosition++;
			} else if (pattern.equals("@^()")) {
				// moves the cursor from 4 for "()^(".length()
				cursorPosition += 4;
			} else {
				System.out.println("allo");
				// move the cursor from ()^(2).length()+1
				cursorPosition += 6;
			}
			currentNumber = "";
			currentBeginIndex = cursorPosition;
		}
	}

	private boolean isParentheses(String operator) {
		// TODO Auto-generated method stub
		return operator.matches("[()]");
	}

	private boolean shouldMultiply() {
		int previous = cursorPosition - 1;
		return previous >= 0 && !isBasicOperator(String.valueOf(completeExpression.charAt(previous)))
				&& completeExpression.charAt(previous) != '(';
	}

	/**
	 * This method is used to get the number currently pointed by the index
	 * {@link #cursorPosition}. The number is searched around from the pointer's
	 * position.
	 * 
	 * For instance of what this method do, take the expression above, where _
	 * will be the cursor's position.
	 * 
	 * <p>
	 * <b>If what is sent is 908.0+34_5.0</b> The result will be 345.0
	 * </p>
	 * 
	 * <p>
	 * <b> If what is sent is 9_08.0+345.0</b> The result is 908.0
	 * </p>
	 * 
	 * <p>
	 * <b> If what is sent is 908.0_+345.0</b> The result is 908.0
	 * </p>
	 * 
	 * <p>
	 * <b> If what is sent is _908.0+345.0</b> The result is null, as there is
	 * nothing before the cursor.
	 * </p>
	 * 
	 * @return Returns an array, where the first value is a String representing
	 *         the number at the cursor's position, and the 2 last values are
	 *         the begin and ending index of the number as integers, or -1 if no
	 *         position is found.
	 */
	private void computeCurrentNumber() {

		Integer beginIndex = 0;
		Integer endIndex = 0;

		boolean rFound = false;
		boolean lFound = false;

		char curCharR = 0;
		char curCharL;

		final char[] curExpression = completeExpression.toCharArray();

		// Iterates over right and left at the same time. Each operator will
		// perform verification for the end of the number, then save the begin
		// or end index when necessary.
		for (int l = cursorPosition - 1, r = cursorPosition; !rFound || !lFound; r++, l--) {

			// The first to operate is the right part
			try {
				if (!rFound) {

					curCharR = curExpression[r];

					// If is part of the number, it it added to the current
					// number
					if (!String.valueOf(curCharR).matches("[\\d.]")) {

						// Reached the end otherwise
						rFound = true;
						endIndex = r;
					}

				}
			} catch (ArrayIndexOutOfBoundsException e) {
				// Meaning the number finishes at the end of expression
				rFound = true;
				endIndex = r;
			}
			try {
				// then look for left part
				if (!lFound) {
					curCharL = curExpression[l];

					// If it is not a number or a miness, or the count of miness
					// is already at one, and the current char is a number
					if (!String.valueOf(curCharL).matches("[\\d.-]")) {

						lFound = true;
						beginIndex = l + 1;

					} else if (curCharL == '-') {

						// If the character found is -, must find the beginning
						// of the number. Can either be x or -x, depending on
						// the number before.
						int previousIndex = l - 1;

						// If the previous character is not a number, then the
						// miness symbol is the miness representation
						if (previousIndex >= 0 && !String.valueOf(curExpression[previousIndex]).matches("[\\d]")) {
							beginIndex = l;
						} else {
							// Else, it is a subtraction, and the index is not
							// counted in the number.
							beginIndex = l + 1;
						}
						lFound = true;
					}
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				// Meaning the number starts at the begin of expression
				lFound = true;
				beginIndex = l + 1;
			}

		}
		System.out.println(beginIndex);

		try {
			currentNumber = completeExpression.substring(beginIndex, endIndex);
		} catch (Exception e) {
			currentNumber = "";
		}
		System.out.println(currentNumber + " resultategvg");

		this.currentBeginIndex = beginIndex;
	}

	/**
	 * This method is used to find the relative position of cursor's position in
	 * the current number described by {@link #currentNumber}. When inserting a
	 * number in {@link #completeExpression}, it is important to also modify the
	 * currently pointed number. This method will therefore be used to retrieve
	 * the index of cursor's position in the current number.
	 * 
	 * For instance, if the current {@link #completeExpression}'s value is
	 * 9345+2334.33x33_1.1, where _ is the cursor, then cursor's position is 15,
	 * but its relative position in the current number (331.1) is 2.
	 * 
	 * @return The relative position of the cursor in the current number, or -1
	 *         if the cursor is not contained in the current number.
	 */
	private int relativePosition() {
		int result = cursorPosition - currentBeginIndex;
		System.out.println(result + " result");
		if (result > completeExpression.length() || result < 0) {
			return -1;
		} else {
			return result;
		}
	}

	/**
	 * 
	 * @return
	 */
	public double equals() {
		if (!solved) {
			// Complete expression with missing parentheses at the end.
			completeExpression();
			String toSolve = replaceConstants(completeExpression);

			// Compiles result and assings it to the current number
			String sResult = solving(toSolve, 0, toSolve.length());

			System.out.println(sResult);

			currentNumber = sResult;
			completeExpression = currentNumber;
			currentBeginIndex = 0;
			cursorPosition = sResult.length();
			result = Double.parseDouble(sResult);
		}
		solved = true;

		return result;
	}

	private String replaceConstants(String expression) {

		String result = expression;

		for (Constant c : Constant.values()) {
			result = result.replaceAll(Pattern.quote(c.getRepresentation()), String.valueOf(c.getValue()));
			System.out.println(result);
		}

		return result;
	}

	/**
	 * Complete the expression with the required number of "(" and ")", when the
	 * numbers
	 */
	private void completeExpression() {

		int open = completeExpression.length() - completeExpression.replaceAll(Pattern.quote("("), "").length();
		int close = completeExpression.length() - completeExpression.replaceAll(Pattern.quote(")"), "").length();

		while (open > close) {
			close++;
			completeExpression = completeExpression.concat(")");
		}
		System.out.println(completeExpression + " completed");

	}

	private String solving(String expression, int from, int to) {

		// Solves the expression from index to index
		String toSolve = expression.substring(from, to);

		while (hasParentheses(toSolve)) {

			// Finds the opening brace index and the closing brace associated
			// index
			int openIndex = toSolve.indexOf("(");
			int closeIndex = findClosingBrace(toSolve, openIndex);

			// This is the thing to solve before being able to solve the rest of
			// the operation
			String currentSolve = toSolve.substring(openIndex, closeIndex + 1);

			System.out.println(currentSolve + " current solve");
			System.out.println(" what will be solved " + toSolve.substring(openIndex + 1, closeIndex));
			// Replaces the old value of parentheses by the solution without
			// parentheses.
			System.out.println(toSolve + "toSolve");

			toSolve = toSolve.replaceAll(Pattern.quote(currentSolve), solving(toSolve, openIndex + 1, closeIndex));
		}

		return simpleSolve(toSolve);
	}

	/**
	 * Finds the closing brace associated to the openIndex's position in the exp
	 * parameter. Returns its index in the exp parameter, or -1 if this closing
	 * brace does not exist in the specified expression.
	 * 
	 * @param exp
	 * @param openIndex
	 * @return
	 */
	private int findClosingBrace(String exp, int openIndex) {

		int openMet = 0;
		final char[] expres = exp.toCharArray();
		char current;

		// Starts at the open index and skip the first, as it is an opening
		// brace
		for (int i = openIndex + 1; i < expres.length; i++) {

			current = expres[i];

			if (current == '(') {
				openMet++;
			} else if (current == ')') {
				if (openMet == 0) {
					return i;
				} else {
					openMet--;
				}
			}

		}

		return -1;
	}

	/**
	 * Tells either the substring from begin to end index of exp parameter
	 * contains parentheses. By definition, parentheses come in pairs.
	 * Therefore, this method will only return true if an opening parenthese is
	 * found in the expression.
	 * 
	 * @param exp
	 *            The expression in which to search for an opening brace
	 * @param begin
	 *            The begin of the substring operation
	 * @param end
	 *            The end of the substring operation
	 * @return True if an opening brace is found.
	 */
	private boolean hasParentheses(String exp) {

		return exp.substring(0, exp.length()).contains("(");
	}

	/**
	 * Simplest solve. Iterates over the expression in parameter to identify
	 * what are the operators to use. At this point, the parameter should
	 * normally have no parentheses inside.
	 * 
	 * If it is not the case, it might occur in certain inappropriate behavior.
	 * 
	 * @return The solution of the simple solve as a String
	 */
	private String simpleSolve(String toSolve) {

		int index = -1;
		// The first result is the equation to solve. In the worst case, the
		// number will simply be returned without any operations.
		String result = new String(toSolve);
		String newResult = "";
		String oldResult = "";

		String before = "";
		String after = "";

		Method callback = null;
		String operator = "";
		try {

			/*
			 * Exponent first
			 */
			// Searches for factorial parameter operators working with
			// multiplications
			while ((index = firstIndexOfAny(result, "!", "%")) != -1) {
				// Finds the operator
				operator = String.valueOf(result.charAt(index));

				// Look for numbers before and after
				before = numberBefore(index, result);

				System.out.println("Before  :  " + before);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = before.concat(operator);

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(before)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}

			// Searches for operators of 3 letters, working with multiplications
			// and
			// number after
			while ((index = firstIndexOfAny(result, "asin", "atan", "acos")) != -1) {

				// Finds the operator
				operator = String.valueOf(result.substring(index, index + 4));

				// Look for numbers before and after
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = operator.concat(after);
				System.out.println("old " + oldResult);

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(after)));

				if (settings.getAngleUnit() == AngleUnity.DEGREES) {
					newResult = String.valueOf(Math.toDegrees(Double.parseDouble(newResult)));
				}

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);
				System.out.println("result " + result);

			}
			// Searches for operators of 3 letters, working with multiplications
			// and
			// number after
			while ((index = firstIndexOfAny(result, "sin", "cos", "tan")) != -1) {

				// Finds the operator
				operator = String.valueOf(result.substring(index, index + 3));

				// Look for numbers before and after
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = operator.concat(after);

				if (settings.getAngleUnit() == AngleUnity.DEGREES) {
					after = String.valueOf(Math.toRadians(Double.parseDouble(after)));
				}

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}

			while ((index = firstIndexOfAny(result, "log")) != -1) {
				// Finds the operator
				operator = String.valueOf(result.substring(index, index + 3));

				// Look for numbers before and after
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = operator.concat(after);

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}
			while ((index = firstIndexOfAny(result, "mod")) != -1) {
				// Finds the operator
				operator = String.valueOf(result.substring(index, index + 3));

				// Look for numbers before and after
				before = numberBefore(index, result);
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = before.concat(operator).concat(after);

				// Calculates the result from the method callback
				newResult = String
						.valueOf(callback.invoke(calculator, Double.parseDouble(before), Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}
			// Searches for operators of 2 letter, working with multiplications
			// and
			// number after
			while ((index = firstIndexOfAny(result, "ln")) != -1) {
				// Finds the operator
				operator = String.valueOf(result.substring(index, index + 2));

				// Look for numbers before and after
				after = numberAfter(index, result);

				// Compiles the current expression
				oldResult = operator.concat(after);

				// Get the current method
				callback = operations.get(operator);

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}

			// Searches for operators of 1 letter, working with
			// multiplications
			// and
			// number after --> sqrt
			while ((index = firstIndexOfAny(result, "\u221A")) != -1) {
				// Finds the operator
				operator = String.valueOf(result.charAt(index));

				// Look for numbers before and after
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression
				operator = Pattern.quote(operator);
				oldResult = operator.concat(after);

				// Calculates the result from the method callback
				newResult = String.valueOf(callback.invoke(calculator, Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}

			// Searches for 2 parameter operators working with multiplications
			while ((index = firstIndexOfAny(result, "^", "C", "P")) != -1) {

				System.out.println(toSolve + " presolve");

				// Finds the operator
				operator = String.valueOf(result.charAt(index));

				// Look for numbers before and after
				before = numberBefore(index, result);
				after = numberAfter(index, result);

				System.out.println("Before  :  " + before);
				System.out.println("After  :  " + after);

				System.out.println(oldResult + " old result");
				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression and pattern to replace the
				// old number
				operator = Pattern.quote(operator);
				oldResult = before.concat(operator).concat(after);

				// Calculates the result from the method callback
				newResult = String
						.valueOf(callback.invoke(calculator, Double.parseDouble(before), Double.parseDouble(after)));
				System.out.println(newResult + " new res" + newResult.length());

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);
				System.out.println(result + " result" + result.length());

				// System.out.println("still passing " + result + " <- result "+
				// " old : " + oldResult);

			}

			// Searches for 2 parameter operators working with multiplications
			while ((index = firstIndexOfAny(result, "*", "/")) != -1) {

				System.out.println(toSolve + " presolve");

				// Finds the operator
				operator = String.valueOf(result.charAt(index));

				// Look for numbers before and after
				before = numberBefore(index, result);
				after = numberAfter(index, result);

				System.out.println("Before  :  " + before);
				System.out.println("After  :  " + after);

				System.out.println(oldResult + " old result");
				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression and pattern to replace the
				// old number
				operator = Pattern.quote(operator);
				oldResult = before.concat(operator).concat(after);

				// Calculates the result from the method callback
				newResult = String
						.valueOf(callback.invoke(calculator, Double.parseDouble(before), Double.parseDouble(after)));
				System.out.println(newResult + " new res" + newResult.length());

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);
				System.out.println(result + " result" + result.length());

				// System.out.println("still passing " + result + " <- result "+
				// " old : " + oldResult);

			}

			// Searches for + and - operator and solve them
			while ((index = firstIndexOfAny(result, "+")) != -1 || (index = firstMinessIndex(result)) != -1) {
				// Finds the operator
				operator = String.valueOf(result.charAt(index));

				// Break if the answer is found enterly
				if (onlyNumberLeft(result)) {
					break;
				}

				// Look for numbers before and after
				before = numberBefore(index, result);
				after = numberAfter(index, result);

				// Get the current method
				callback = operations.get(operator);

				// Compiles the current expression and pattern to replace the
				// old number
				operator = Pattern.quote(operator);
				oldResult = before.concat(operator).concat(after);

				// Calculates the result from the method callback
				newResult = String
						.valueOf(callback.invoke(calculator, Double.parseDouble(before), Double.parseDouble(after)));

				// Replaces the old result by the new result everywhere
				result = result.replaceAll(oldResult, newResult);

				System.out.println("still passing");

			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			throw new InvalidNumberException();
		}
		return result;
	}

	/**
	 * Finds the first miness index in the expression, taking care of not taking
	 * E-** from double constants exponent.
	 * 
	 * @param expression
	 * @return
	 */
	public static int firstMinessIndex(String expression) {
		
		if(expression.equals("-Infinity")){
			return -1;
		}

		final char[] exp = expression.toCharArray();
		final int sz = exp.length;

		for (int i = 0; i < sz; i++) {
			// Made to avoid array out of bound,but should never occur at this
			// point. Either way, the expression would end with a - symbol
			if (exp[i] == '-' && (i == 0 || exp[i - 1] != 'E')) {
				return i;
			}

		}
		return -1;
	}

	public static int firstIndexOfAny(String target, String... expression) {
		if ((target == null) || (expression == null)) {
			return -1;
		}
		int sz = expression.length;

		// String's can't have a MAX_VALUEth index.
		int ret = Integer.MAX_VALUE;

		int tmp = 0;
		for (int i = 0; i < sz; i++) {
			String search = expression[i];
			if (search == null) {
				continue;
			}
			tmp = target.indexOf(search);
			if (tmp == -1) {
				continue;
			}

			if (tmp < ret) {
				ret = tmp;
			}
		}

		return (ret == Integer.MAX_VALUE) ? -1 : ret;
	}

	private boolean onlyNumberLeft(String number) {
		// TODO Auto-generated method stub
		return number.matches("-?[\\d]+\\.?[\\d]*(E-?)?[0-9]*");
	}

	/**
	 * 
	 */
	private String numberAfter(int index, String expression) {
		String research = expression.substring(index + 1, expression.length());

		Pattern p = Pattern.compile("-?[0-9]+\\.?[0-9]*(E-?[0-9]*)?");
		Matcher m = p.matcher(research);

		System.out.println(research + " expression");
		if (m.find()) {
			System.out.println(m.group() + " group");

			return m.group();
		} else {
			throw new InvalidNumberException();
		}

	}

	/**
	 * 
	 * @param index
	 *            The start index of the research
	 * @return
	 */
	private String numberBefore(int index, String expression) {

		String research = expression.substring(0, index);

		// Complex pattern to match numbers wherever they are in the thing
		Pattern p = Pattern.compile("(((?<=([+-/*]))-)?|^-?|(?<=(\\())-?)[0-9]+\\.?[0-9]*(E-?[0-9]*)?$");
		System.out.println(expression + "   ---   " + index);
		System.out.println(research + " researched");
		Matcher m = p.matcher(research);
		if (m.find()) {

			return m.group();
		} else {
			throw new InvalidNumberException();
		}
	}

	/**
	 * 
	 */
	public void clear() {

		cursorPosition = 0;
		currentBeginIndex = 0;

		currentNumber = "";
		completeExpression = "";
		formattedExpression = "";
	}

	public void delete() {

		// get the index of the current selected number in the string
		int beginNumber = relativePosition() - 1;
		int cursorBegin = cursorPosition - 1;

		System.out.println(beginNumber + " relative");

		// Moves the cursor to the left, and delete all the characters that are
		// separated by the begin and end index.
		moveCursorLeft();

		this.completeExpression = completeExpression.substring(0, cursorPosition)
				.concat(completeExpression.substring(cursorBegin + 1, completeExpression.length()));

		if (!this.currentNumber.trim().isEmpty() && beginNumber != -1) {

			this.currentNumber = currentNumber.substring(0, relativePosition())
					.concat(currentNumber.substring(beginNumber, currentNumber.length()));

		}

		computeCurrentNumber();

	}

	/**
	 * Tells either the string sent in parameter represents a valid operator to
	 * be used or not.
	 * 
	 * @param value
	 *            The operator to be checked.
	 * @return True if the value sent in parameter in a valid operator, false
	 *         otherwise.
	 */
	private boolean isOperator(String value) {

		for (int i = 0; i < operators.length; i++) {
			if (value.equals(operators[i])) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 */
	private boolean isBasicOperator(String operator) {

		String value = operator.trim();

		return value.matches("[+-/*]");
	}

	/**
	 * Returns the index of the operator sent in parameter from the Array of
	 * operators. The value returned by this method also represents the index
	 * associated pattern in {@link #patterns}.
	 * 
	 * @param operator
	 *            The operator to be fined in the array.
	 * @return The index of the operator sent in parameter from
	 *         {@link #operators}, as much as its patern's index in
	 *         {@link #patterns}
	 */
	private int operatorIndex(String operator) {

		for (int i = 0; i < operators.length; i++) {
			if (operator.equals(operators[i])) {
				return i;
			}
		}

		return -1;
	}

	private boolean isConstant(String representation) {

		return constants.get(representation) != null;
	}

	private void negateCurrent() {

		String newCurrent = "";
		int cursorOffset = 0;
		if (!currentNumber.trim().isEmpty()) {

			// If was negative, remove it
			if (currentNumber.startsWith("-")) {

				newCurrent = currentNumber.substring(1, currentNumber.length());
				cursorOffset = -1;

			} else {
				// If was positive, negate it.
				newCurrent = "-" + currentNumber;
				cursorOffset = 1;
			}
			try {
				// Replaces it in the current expression
				completeExpression = new StringBuffer(completeExpression)
						.replace(currentBeginIndex, currentBeginIndex + currentNumber.length(), newCurrent).toString();
			} catch (Exception e) {
				// throws an exception when start>end, therefore no number is
				// selected
				throw new NoNumberSelectedException();
			}
			currentNumber = newCurrent;
			cursorPosition += cursorOffset;
		}

	}

	public void answer() {
		String answer = null;

		if (new Double(result).isNaN()) {
			answer = "0";
		} else {
			answer = String.valueOf(result);
		}

		if (shouldMultiply()) {
			enterValue("*");
		}

		for (int i = 0; i < answer.length(); i++) {
			enterValue(String.valueOf(answer.charAt(i)));
		}

	}

}
