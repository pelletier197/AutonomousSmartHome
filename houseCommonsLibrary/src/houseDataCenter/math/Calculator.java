package houseDataCenter.math;

public class Calculator {

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double addition(double first, double second) {
		return first + second;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double subtraction(double first, double second) {
		return first - second;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double multiplication(double first, double second) {
		return first * second;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double modulo(double first, double second) {
		return first % second;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double division(double first, double second) {
		return first / second;
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double max(double first, double second) {
		return Math.max(first, second);
	}

	/**
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public double min(double first, double second) {

		return Math.min(first, second);
	}

	/**
	 * 
	 * @param n
	 * @param r
	 * @return
	 */
	public double nPr(double n, double r) {

		// r always smaller or equal to n
		if (r > n || n < 0 || r < 0) {
			throw new IllegalArgumentException();
		}

		// Equals
		// n(n-1)...r
		double result = n;

		// For example, if n = 5 and r = 2, then must execute 5 * 4. Will
		// multiply from n - r + 1 and stop at n - 1
		double count = n - r + 1;

		while (count < n) {
			result *= count;
			count += 1;
		}

		return result;
	}

	/**
	 * 
	 * @param n
	 * @param r
	 * @return
	 */
	public double nCr(double n, double r) {

		// r always smaller or equal to n
		if (r > n || n < 0 || r < 0) {
			throw new IllegalArgumentException();
		}

		// Equals
		// n(n-1)...r
		double division = 1;
		double result = n / division;

		// For example, if n = 5 and r = 2, then must execute 5 * 4. Will
		// multiply from n - r + 1 and stop at n - 1
		double count = n - r + 1;

		while (count < n) {

			result *= count;
			result /= ++division;
			count += 1;
		}

		return result;
	}

	/**
	 * 
	 * @param number
	 * @return
	 */
	public double factorial(double number) {

		if (number < 0) {
			throw new IllegalArgumentException();
		}

		if (number == 0) {
			return 1;
		}
		double result = number;
		double count = number - 1;

		while (count > 0) {
			result *= count;
			count -= 1;
		}
		return result;
	}

	public double sin(double angle) {
		return Math.sin(angle);
	}

	public double cos(double angle) {
		return Math.cos(angle);
	}

	public double tan(double angle) {
		return Math.tan(angle);
	}

	public double asin(double angle) {
		return Math.asin(angle);
	}

	public double acos(double angle) {
		return Math.acos(angle);
	}

	public double atan(double angle) {
		return Math.atan(angle);
	}

	public double sqrt(double value) {
		return Math.sqrt(value);
	}

	public double log10(double value) {
		return Math.log10(value);
	}

	public double ln(double value) {
		return Math.log(value);
	}

	public double pow(double value, double exp) {
		return Math.pow(value, exp);
	}

	public double percent(double percent) {
		return percent / 100;
	}

}
