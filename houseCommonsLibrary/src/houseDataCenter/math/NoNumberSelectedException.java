package houseDataCenter.math;

public class NoNumberSelectedException extends RuntimeException {
	public NoNumberSelectedException() {
		super();
	}

	public NoNumberSelectedException(String message) {
		super(message);
	}
}
