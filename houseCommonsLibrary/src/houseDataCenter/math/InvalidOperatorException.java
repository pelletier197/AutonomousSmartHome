package houseDataCenter.math;

public class InvalidOperatorException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4204877624731570130L;

	public InvalidOperatorException() {
		super();
	}

	public InvalidOperatorException(String message) {
		super(message);
	}
}
