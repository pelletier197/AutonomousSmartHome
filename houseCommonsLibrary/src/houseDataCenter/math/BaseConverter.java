package houseDataCenter.math;

import java.util.Arrays;

public class BaseConverter {

	private static final char[] BASE_CONVERT_CHAR = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
			'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
			'W', 'X', 'Y', 'Z' };

	/**
	 * To modify
	 * 
	 * @param number
	 * @return
	 */
	private String toHex(int number) {

		return base2PowerConversion(number, 4);
	}

	/**
	 * To modify
	 * 
	 * @param number
	 * @return
	 */
	private String toOctal(int number) {

		return base2PowerConversion(number, 3);
	}

	/**
	 * To modify
	 * 
	 * @param number
	 * @return
	 */
	private String toQuaternary(int number) {

		return base2PowerConversion(number, 2);
	}

	/**
	 * To modify
	 * 
	 * @param number
	 * @return
	 */
	private String toBinary(int number) {

		return base2PowerConversion(number, 1);
	}

	/**
	 * 
	 * @param number
	 * @param from
	 * @param to
	 * @return
	 */
	public String convert(String number, int from, int to) {

		// First criteria. Base cannot be higher than 37, as there is 36
		// characters
		if (from <= 1 || to <= 1 || to > 37) {
			throw new IllegalArgumentException();
		}

		// First convert the number to base 10
		final int numVal = toBase10(number, from);
		String result = "";

		// Try finding base2 conversion
		if (from == 10) {
			// From 10 to 2
			if ((numVal & 2) == 2) {
				result = toBinary(numVal);

				// From 10 to 4
			} else if ((numVal & 4) == 4) {
				result = toQuaternary(numVal);
			}
			// From 10 to 8
			else if ((numVal & 8) == 8) {
				result = toOctal(numVal);
			}
			// From 10 to 16
			else if ((numVal & 16) == 16) {
				result = toHex(numVal);
			}

		} else {

			// For any other than to base 2 conversion
			int rest = numVal;

			// The longest size the number can have is 32
			char[] newNumber = new char[32];
			int position = 31;

			// Converts it to the number
			while (rest != 0) {
				
				newNumber[position] = BASE_CONVERT_CHAR[rest % to];
			
				//The rest becomes the quotient
				rest = rest / to;
				
				//Position moves from 1 to back
				position--;
			}
			result = new String(newNumber, position + 1, 32 - position -1);

		}

		return result;

	}

	/**
	 * 
	 * @param number
	 * @param shift
	 * @return
	 */
	private String base2PowerConversion(int number, int shift) {

		String result = "";

		int mask = (1 << shift) - 1;

		do {
			result = BASE_CONVERT_CHAR[number & mask] + result;
			number >>>= shift;

		} while (number != 0);

		return result;
	}

	/**
	 * 
	 * @param number
	 * @param base
	 * @return
	 */
	private int toBase10(String number, int base) {

		// Return the original parsing for a number equal to 10
		if (base == 10) {
			return Integer.parseInt(number);
		}
		// Restrictions
		final char[] numberChar = number.toCharArray();
		final int length = numberChar.length;
		final int maxAllowed = base - 1;

		//We start at the  first character from the right
		int result = Character.getNumericValue(numberChar[length - 1]);
		int numericValue;
		int basePower = base;

		// Iterates over every character and convert them into required number
		// of the 10 base from the current base.
		for (int i = length - 2; i >= 0; i--) {
			
			numericValue = Character.getNumericValue(numberChar[i]);
			if (numericValue > maxAllowed || numericValue < 0) {
				throw new InvalidNumberException(number + " is not a valid number for base " + base);
			}
			result += numericValue * basePower;
			basePower *= base;
		}
		return result;
	}

}
