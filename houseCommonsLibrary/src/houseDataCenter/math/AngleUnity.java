package houseDataCenter.math;

public enum AngleUnity {

	DEGREES, RADIANS, DMS
}
