package houseDataCenter.math.unitConversion;

public class CTempConverter extends Convertible {

	public CTempConverter() {
		super(Unit.K, Unit.C, Unit.F, Unit.RE, Unit.N, Unit.RA);
	}

	@Override
	public double convert(double qty, Unit source, Unit target) {
		if (source == target) {
			return qty;
		}

		double result = 0;

		switch (source) {
		case K:
			result = fromK(qty, target);
			break;
		case C:
			result = fromC(qty, target);
			break;
		case F:
			result = fromF(qty, target);
			break;
		case RE:
			result = fromRE(qty, target);
			break;
		case N:
			result = fromN(qty, target);
			break;
		case RA:
			result = fromRA(qty, target);
			break;
		default:
			break;

		}
		return result;
	}

	private double fromK(double K, Unit to) {

		double result = 0;

		if (to == Unit.F) {
			result = K * 1.8 - 459.67;
		} else if (to == Unit.C) {
			result = K - 273.15;
		} else if (to == Unit.RA) {
			result = K * 1.8;
		} else if (to == Unit.RE) {
			result = (K - 273.15) * 0.8;
		} else if (to == Unit.N) {
			// Returns Kelvin to celcius to Newton
			result = fromC(fromK(K, Unit.C), Unit.N);
		}

		return result;
	}

	private double fromC(double C, Unit to) {
		double result = 0;

		if (to == Unit.F) {
			result = C * 1.8 + 32;
		} else if (to == Unit.K) {
			result = C + 273.15;
		} else if (to == Unit.RA) {
			result = C * 1.8 + 32 + 459.67;
		} else if (to == Unit.RE) {
			result = C * 0.8;
		} else if (to == Unit.N) {
			result = C * 0.33;
		}

		return result;
	}

	private double fromF(double faran, Unit to) {

		double result = 0;

		if (to == Unit.C) {
			result = (faran - 32) * 1.8;
		} else if (to == Unit.K) {
			result = (faran + 459.67) / 1.8;
		} else if (to == Unit.RA) {
			result = faran + 459.67;
		} else if (to == Unit.RE) {
			result = (faran - 32) / 2.25;
		} else if (to == Unit.N) {
			result = fromC(fromF(faran, Unit.C), Unit.N);
		}

		return result;
	}

	private double fromRE(double Re, Unit to) {
		double result = 0;

		if (to == Unit.F) {
			result = Re * 2.25 + 32;
		} else if (to == Unit.K) {
			result = Re * 1.25 + 273.15;
		} else if (to == Unit.RA) {
			result = Re * 2.25 + 32 + 459.67;
		} else if (to == Unit.C) {
			result = Re * 1.25;
		} else if (to == Unit.N) {
			result = fromC(fromRE(Re, Unit.C), Unit.N);

		}

		return result;
	}

	private double fromN(double N, Unit to) {

		double result = 0;

		if (to == Unit.F) {
			result = fromC(fromN(N, Unit.C), Unit.F);
		} else if (to == Unit.K) {
			result = fromC(fromN(N, Unit.C), Unit.K);
		} else if (to == Unit.RA) {
			result = fromC(fromN(N, Unit.C), Unit.RA);
		} else if (to == Unit.C) {
			result = N / 0.33;
		} else if (to == Unit.RE) {
			result = fromC(fromN(N, Unit.C), Unit.RE);
		}

		return result;
	}

	private double fromRA(double Ra, Unit to) {
		double result = 0;

		if (to == Unit.F) {
			result = Ra - 459.67;
		} else if (to == Unit.K) {
			result = Ra / 1.8;
		} else if (to == Unit.N) {
			result = fromC(fromRA(Ra, Unit.C), Unit.N);
		} else if (to == Unit.C) {
			result = (Ra - 32 - 459.67) / 1.8;
		} else if (to == Unit.RE) {
			result = (Ra - 32 - 459.67) / 2.25;
		}

		return result;
	}

}
