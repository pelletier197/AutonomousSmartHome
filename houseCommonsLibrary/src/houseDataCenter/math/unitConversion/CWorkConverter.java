package houseDataCenter.math.unitConversion;

public class CWorkConverter extends Convertible {

	public CWorkConverter() {
		super(Unit.J, Unit.KJ, Unit.CAL, Unit.KCAL, Unit.WH, Unit.KWH, Unit.BTU, Unit.THM, Unit.FT_LB);
	}
}
