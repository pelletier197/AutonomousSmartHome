package houseDataCenter.math.unitConversion;

public class Tester {
	
	public static void main(String[] args) {
		Unit[] values = Unit.values();

		StringBuilder builder = new StringBuilder();

		for (Unit u : values) {
			builder.append(u.symbol).append(" = \n");
		}
		System.out.println(builder.toString());
	}
}
