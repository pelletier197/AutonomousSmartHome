package houseDataCenter.math.unitConversion;

public class CFrequencyConverter extends Convertible {

	public CFrequencyConverter() {
		super(Unit.HZ, Unit.KHZ, Unit.MHZ, Unit.GHZ);
	}
}
