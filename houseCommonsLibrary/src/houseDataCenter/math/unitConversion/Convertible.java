package houseDataCenter.math.unitConversion;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class Convertible {

	private List<Unit> compatibleUnits;

	public Convertible(Unit... units) {
		if (units == null || units.length == 0) {
			throw new NullPointerException("Cannot create unit converter from those units");
		}

		this.compatibleUnits = Collections.unmodifiableList(Arrays.asList(units));
	}

	public double convert(double qty, Unit source, Unit target) {
		if (compatibleUnits.contains(source) && compatibleUnits.contains(target)) {
			return qty * target.factor / source.factor;
		} else {
			throw new IllegalAccessError("Source or Target parameter is not a valid unit for this conversion");
		}
	}
	
	public List<Unit> getUnits(){
		return compatibleUnits;
	}
}
