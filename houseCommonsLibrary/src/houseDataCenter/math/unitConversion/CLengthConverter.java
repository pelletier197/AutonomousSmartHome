package houseDataCenter.math.unitConversion;

public class CLengthConverter extends Convertible {

	public CLengthConverter() {
		super(Unit.MM, Unit.CM, Unit.METTER, Unit.DM, Unit.KM, Unit.MI, Unit.IN, Unit.FT, Unit.YD);
	}
}
