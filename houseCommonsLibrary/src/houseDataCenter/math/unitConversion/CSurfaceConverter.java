package houseDataCenter.math.unitConversion;

public class CSurfaceConverter extends Convertible {

	public CSurfaceConverter() {
		super(Unit.MM_2, Unit.CM_2, Unit.DM_2, Unit.M_2, Unit.HA, Unit.MI_2, Unit.A, Unit.CA);
	}
}
