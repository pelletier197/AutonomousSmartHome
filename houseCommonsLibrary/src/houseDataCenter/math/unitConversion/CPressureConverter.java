package houseDataCenter.math.unitConversion;

public class CPressureConverter extends Convertible {

	public CPressureConverter() {
		super(Unit.PA, Unit.HPA, Unit.BAR, Unit.MBAR, Unit.TORR, Unit.ATM, Unit.PSI);
	}
}
