package houseDataCenter.math.unitConversion;

public class CDataConverter extends Convertible {

	public CDataConverter() {
		super(Unit.B, Unit.O, Unit.MO, Unit.KO, Unit.GO, Unit.TO, Unit.PO, Unit.EO, Unit.ZO, Unit.YO);
	}
}
