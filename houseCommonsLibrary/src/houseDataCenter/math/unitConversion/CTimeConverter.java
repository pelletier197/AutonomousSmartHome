package houseDataCenter.math.unitConversion;

public class CTimeConverter extends Convertible {

	public CTimeConverter() {
		super(Unit.AS, Unit.FS, Unit.PICOS, Unit.NS, Unit.US, Unit.MS, Unit.SECOND, Unit.MINUT, Unit.HOUR, Unit.DAY,
				Unit.WEEK, Unit.YEAR);
	}
}
