package houseDataCenter.math.unitConversion;

public class CSpeedConverter extends Convertible {

	public CSpeedConverter() {
		super(Unit.MPS, Unit.MPH, Unit.KMPH, Unit.KNOT, Unit.MAC);
	}
}
