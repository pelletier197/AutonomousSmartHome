package houseDataCenter.math.unitConversion;

public class CForceConverter extends Convertible{

	public CForceConverter() {
		super(Unit.CENTI_N, Unit.NEWTON, Unit.DA_N, Unit.KN, Unit.DYN, Unit.G_F, Unit.KG_F, Unit.LB_F);
	}
}
