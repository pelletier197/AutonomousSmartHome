package houseDataCenter.math.unitConversion;

public enum Unit {

	//@formatter:off
	// Weight Unit
	GRAM(1000, "g",false), KG(1, "kg",true), TON(0.001, "t",false), HG(10, "hg",false), DG(10_000, "dg",false), CG(100_000, "cg",false), MG(1_000_000,"mg",false), UG(1_000_000_000, "\u00B5g",false),
	CARAT(5000,"carat",false), GRAIN(15432.358352941,"grain",false), OZ(35.27396194958,"oz",false),LB(2.2046226218478,"lb",false),

	//Temperature unit. They are calculated from functions, so factor is set to 1
	K(1,"\u00B0K",true),C(1,"\u00B0C",false),F(1,"\u00B0F",false),RE(1,"\u00B0Ré",false),N(1,"\u00B0N",false),RA(1,"\u00B0Ra",false),
	
	//Volume units.
	LITTER(1,"L",false),M_3(0.001,"m³",true),DM_3(1,"dm³",false),CM_3(1000,"cm³",false),DL(10,"dl",false),CL(100,"cl",false),ML(1000,"ml",false),
	IN_3(61.023744095,"in³",false),FT_3(0.035314666721,"ft³",false),YD_3(0.0013079506193,"yd³",false), BBL(0.0062898107704,"bbl",false),
	
	//Distance units.
	METTER(1,"m",true),KM(0.001,"km",false),DM(10,"dm",false),CM(100,"cm",false),MM(1000,"mm",false),MI(0.00062137119223733,"mi",false),IN(39.370078740157,"in",false),
	FT(3.2808398950131,"ft",false),YD(1.0936132983377,"yd",false),
	
	//Data Units
	MO(1,"Mo",false),O(1048576,"O",false),B(8388608,"b",true),KO(1024,"Ko",false),GO(0.0009765625,"Go",false), TO(9.5367431640625e-7,"To",false),
	PO(9.3132257461548e-10,"Po",false),EO(9.0949470177293e-13,"Eo",false),ZO(8.8817841970013e-16,"Zo",false),YO(8.673617379884e-19,"Yo",false),
	
	//Surface Units.
	M_2(1,"m²",true),KM_2(1E-6,"km²",false),DM_2(100,"dm²",false),CM_2(10_000,"cm²",false),MM_2(1_000_000,"mm²",false),HA(0.0001,"ha",false),A(0.01,"a",false),CA(1,"ca",false),MI_2(3.8610215854245e-7,"mi²",false),
	IN_2(1550.0031,"in²",false),YD_2(1.196,"yd²",false),FT_2(10.763,"ft²",false),
	
	//Speed Unit
	MPS(1,"m/s",true),KMPH(3.6,"km/h",false),MPH(2.2369362920544,"mi/h",false),KNOT(1.9438444924406 ,"knot",false),MAC(0.0029411764705882 ,"mac",false),
	
	//Time units
	SECOND(1,"s",true),MINUT(0.016666666666667 ,"min",false),HOUR(0.00027777777777778 ,"h",false),DAY(1.1574074074074e-5,"d",false),WEEK(1.6534391534392e-6,"w",false),MS(1000,"ms",false),US(1_000_000,"\u00B5s",false),NS(1e9,"ns",false),
	PICOS(1e12,"ps",false),FS(1e15,"fs",false),AS(1e18,"as",false),YEAR(3.1709791983765e-8,"y",false),
	
	//Frequency units
	HZ(1000,"Hz",true),KHZ(1,"KHz",false),MHZ(0.001,"MHz",false),GHZ(1e-6,"GHz",false),
	
	//Pressure units
	PA(1,"Pa",true),HPA(0.01 ,"hPa",false),BAR(1.0e-5,"bar",false),MBAR(0.01 ,"mbar",false),ATM(9.8692326671601e-6,"atm",false),PSI(0.00014503725635332,"psi",false),TORR(0.0075006168270417 ,"Torr",false),
	
	//Work units
	J(1,"J",true),KJ(0.001,"KJ",false),CAL(0.23912003825921 ,"cal",false),KCAL(0.00023912003825921,"kcal",false),WH(0.00027777777777778 ,"Wh",false),
	KWH(2.7777777777778 ,"kWh",false),BTU(0.00094831673779042 ,"BTU",false),THM(9.4805555555556e-9,"thm",false),FT_LB(0.73756111111111,"ft-lb",false),
	
	//Force units
	NEWTON(1,"N",true),CENTI_N(100,"cN",false),DA_N(0.1,"daN",false),KN(0.001,"kN",false),DYN(100_000,"dyn",false),G_F(101.9716213,"gf",false),KG_F(0.1019716213,"kgf",false),LB_F(0.224809,"lbf",false),
	
	//Power units
	W(1,"W",true),KW(1,"kW",false),MW(1,"MW",false),KCALPS(1,"kcal/s",false),KCALPH(1,"kcal/h",false),HP(1,"HP",false),PS(1,"PS",false),
	
	//Angle units
	DEG(1,"\u00B0",false),GRAD(1.1111116062593 ,"Grad",true),MIL_ANG(109.662 ,"Mil\u00B0",false),MIN_DEG(60,"'",false),SEC_DEG(3600,"\"",false);
	//@formatter:on

	protected final double factor;
	public final String symbol;
	public final boolean isIS;

	private Unit(double factor, String symbol, boolean IS) {
		this.factor = factor;
		this.symbol = symbol;
		this.isIS = IS;

	}

	@Override
	public String toString() {
		return symbol;
	}

}
