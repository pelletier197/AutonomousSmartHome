package houseDataCenter.math.unitConversion;

import houseDataCenter.math.unitConversion.Unit;

public class CWeightConverter extends Convertible {

	public CWeightConverter() {
		super(Unit.UG, Unit.MG, Unit.CG, Unit.GRAM, Unit.DG, Unit.HG, Unit.KG, Unit.TO, Unit.LB, Unit.CARAT, Unit.GRAIN,
				Unit.OZ);
	}
}
