package houseDataCenter.math.unitConversion;

public class CPowerConverter extends Convertible {

	public CPowerConverter() {
		super(Unit.W, Unit.KW, Unit.MW, Unit.KCALPS, Unit.KCALPH, Unit.HP, Unit.PS);

	}
}
