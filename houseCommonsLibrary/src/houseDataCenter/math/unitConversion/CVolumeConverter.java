package houseDataCenter.math.unitConversion;

public class CVolumeConverter extends Convertible {

	public CVolumeConverter() {
		super(Unit.CM_3, Unit.DM_3, Unit.M_3, Unit.ML, Unit.CL, Unit.DL, Unit.LITTER, Unit.IN_3, Unit.FT_3, Unit.YD_3,
				Unit.BBL);
	}
}
