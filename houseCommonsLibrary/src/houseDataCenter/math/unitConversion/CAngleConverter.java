package houseDataCenter.math.unitConversion;

import java.text.DecimalFormat;

public class CAngleConverter extends Convertible {

	private DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();

	public CAngleConverter() {
		super(Unit.DEG, Unit.GRAD, Unit.MIL_ANG, Unit.MIN_DEG, Unit.SEC_DEG);

		df.setMinimumFractionDigits(0);
		df.setMaximumFractionDigits(1);

	}

	public String toDMS(double angle, Unit source) {

		double degPrecision = convert(angle, source, Unit.DEG);
		System.out.println(degPrecision);
		int deg = (int) (Math.floor(degPrecision));
		int min = (int) (Math.floor((degPrecision - deg) * 60));

		// Forces the minute into a double
		double sec = (((degPrecision - deg - (new Integer(min).doubleValue() / 60)) * 3600));

		StringBuilder buffer = new StringBuilder();
		buffer.append(deg).append("\u00B0").append(min).append("'").append(df.format(sec)).append("\"");

		return buffer.toString();
	}
}
