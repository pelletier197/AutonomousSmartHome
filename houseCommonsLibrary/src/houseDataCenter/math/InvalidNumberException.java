package houseDataCenter.math;

public class InvalidNumberException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1861440574338699433L;

	public InvalidNumberException() {
		super();
	}

	public InvalidNumberException(String message) {
		super(message);
	}
}
