package houseDataCenter.math;

import java.io.Serializable;

public class CalculatorSettings implements Serializable {

	private AngleUnity angleUnit;

	public CalculatorSettings() {
		this.angleUnit = AngleUnity.DEGREES;
	}

	public AngleUnity getAngleUnit() {
		return angleUnit;
	}

	public void setAngleUnit(AngleUnity unit) {
		if (unit == null) {
			throw new NullPointerException();
		}
		this.angleUnit = unit;
	}

}
