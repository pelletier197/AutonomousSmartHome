package serverDataManaging;

import custom.CommunicationCenter;

/**
 * Every class implementing this interface will be allowed the right to
 * communicate and send orders to the server.
 * 
 * However, reception will have to be handled outside this interface. It will
 * only allow send of data.
 * 
 * This will be allowed via {@link #setCommunicationCenter(CommunicationCenter)}
 * , which will allow server to access program's communication center and send
 * data directly to the server.
 * 
 * @author sunny
 *
 */
public interface Communicator {

	/**
	 * Sets the class' communication when it is loaded. This should be done
	 * manually when the object implementing this class is generated.
	 * 
	 * @param com
	 *            Object's communication center.
	 */
	public void setCommunicationCenter(CommunicationCenter com);

}
