package serverDataManaging;

import java.util.UUID;

/**
 * A listener for connection lost. When a class implements this interface, it
 * will be notified when a {@link ServerDataSender} attempts to send data to a
 * server but the sending fails throwing an exception.
 * 
 * @author sunny
 *
 */
public interface ConnectionLostListener {

	/**
	 * Notifies the implementer that a connection has been lost with the client
	 * specified in parameter.
	 * 
	 * @param clientID
	 *            The client ID to which connection has been lost.
	 */
	public void connectionLost(UUID clientID);

}
