package serverDataManaging;

import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.UUID;

/**
 * Simple interface providing a method to call when a {@link ConnectionAccepter}
 * receives a new connection. The server or any other structure can therefore
 * handle the connection and data transferring via the socket received during
 * the connection.
 * 
 * @author Sunny Pelletier
 *
 */
public interface ConnectionListener {

	/**
	 * Method called by a {@link ConnectionAccepter} when a new client achieved
	 * to connect to the server. The socket parameter is the socket returned by
	 * {@link ServerSocket#accept()} when connection finally unlocks.
	 * 
	 * @param clientSocket
	 *            The client socket received during connection.
	 */
	public void handleConnection(UUID clientID);
	
	/**
	 * Notifies the implementer that a connection has been lost with the client
	 * specified in parameter.
	 * 
	 * @param clientID
	 *            The client ID to which connection has been lost.
	 */
	public void connectionLost(UUID clientID);

}
