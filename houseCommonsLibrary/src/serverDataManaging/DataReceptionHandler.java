package serverDataManaging;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Interface providing 1 method that allows to a receiver class to handle the
 * reception of Data. When a {@link ClientDataReceiver} or
 * {@link ServerDataReceiver} receives data, it transfers them to th listener
 * classes via {@link #received(Object, int)}.
 * 
 * @author sunny
 *
 */
public interface DataReceptionHandler {

	/**
	 * Handle reception of data from a server or a client. The data is passed in
	 * parameter, and the int value represents the ID of the client that sent
	 * this object. This value can then be used to answer the client from the
	 * receiver, using the same ID.
	 * 
	 * @param obj
	 *            The object received
	 * @param fromID
	 *            The ID of the client, or -1 if the application runs on the
	 *            client side, as the server is not a client.
	 */
	public void received(ByteBuffer data, UUID fromID);
}
