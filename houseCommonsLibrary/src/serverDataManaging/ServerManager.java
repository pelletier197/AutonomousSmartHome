package serverDataManaging;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import com.sun.javafx.iio.ios.IosDescriptor;

import javafx.concurrent.Worker;

/**
 * Class providing implementation of a server connected to many client at the
 * same time. This server is always connected to the localHost via the port
 * given at construction.
 * 
 * It is possible to specify the amount of client desired at construction, or
 * allow an infinite amount of client by using {@link #INFINITE_CLIENT}.
 * 
 * To use this server manager, use {@link #openConnection()}, which will open
 * connection to allow clients connection to the server. Once clients are
 * connected, using {@link #sendToAll(Object)} method will have an effect on
 * clients receptions. It is also possible to send an object to one specific
 * client using {@link #send(Object, int)} when the client's ID is known.
 * 
 * When the server requests to be closed, call {@link #closeConnection()}. This
 * will close all parallel threads running and free sockets' resources.
 * 
 * @author sunny
 *
 */
public class ServerManager implements ConnectionLostListener {

	/**
	 * Constant holding the value of infinite amount of client allowed on the
	 * server.
	 */
	public static final int INFINITE_CLIENT = -1;

	/**
	 * The number of packets that will be allowed to be read before forcing the
	 * reading to be finished in background. This ensure that the server is not
	 * be locked while performing big data transfer, and will ensure equal time
	 * between every client.
	 */
	public static final int READ_BEFORE_BACKGROUND = 2;

	/**
	 * The number of packets written before the process of writing is forced to
	 * be finished in background, so the server does not block while writing is
	 * performed.
	 */
	public static final int WRITE_BEFORE_BACKGROUND = 2;

	/**
	 * The port to bind to
	 */
	@SuppressWarnings("unused")
	private int port = 0;

	/**
	 * The server socket to which clients connect
	 */
	private ServerSocketChannel serverSocket;

	private Selector selector;

	/**
	 * A map holding clients ID with their socket, allowing easy communication
	 * between 1 client to the server.
	 */
	private Map<UUID, SocketWrapper> clientMap;

	private Map<SocketWrapper, UUID> reverseClientMap;

	private Map<SocketChannel, SocketWrapper> socketToWrapper;

	private Map<UUID, Queue<byte[]>> dataToSend;

	/**
	 * An optionnal connection reception listener for outside purpose
	 */
	private ConnectionListener connectListener;

	private int required;
	private int clientCount;

	private DataReceptionWorker recepWorker;

	public final String externalIpAddress;

	private ByteBuffer currentObject;
	private ByteBuffer byteObjLength;
	private IntBuffer objLength;

	/**
	 * Constructs a server manager connected to the port wished that will accept
	 * the required amount of client maximum. To allow an infinite amount of
	 * client, use {@link #INFINITE_CLIENT} constant. The port must also be
	 * free, or an exception will be thrown when {@link #openConnection()} will
	 * be called.
	 * 
	 * @param port
	 *            The requested port
	 * @param required
	 *            The required amount of client (maximum)
	 * @throws IOException
	 *             If any problem occurs during creation of the serverSocket
	 */
	public ServerManager(int port, int required, DataReceptionHandler handler) throws IOException {
		if (required <= 0 && required != INFINITE_CLIENT) {
			throw new IllegalArgumentException("Cannot create a server that accepts 0 or less clients.");
		}
		this.recepWorker = new DataReceptionWorker(handler, this);

		serverSocket = ServerSocketChannel.open();
		serverSocket.configureBlocking(false);
		serverSocket.bind(new InetSocketAddress(port));

		this.selector = Selector.open();
		serverSocket.register(selector, SelectionKey.OP_ACCEPT);

		this.port = port;
		this.clientMap = new HashMap<>();
		this.reverseClientMap = new HashMap<>();
		this.socketToWrapper = new HashMap<>();
		this.dataToSend = new HashMap<>();
		this.required = required;
		this.clientCount = 0;

		this.byteObjLength = ByteBuffer.allocate(4);
		this.objLength = byteObjLength.asIntBuffer();

		// Requests the external IP of the server
		this.externalIpAddress = getExternalIpAddress();

	}

	/**
	 * Opens connection to the server. When this method is called, three
	 * parallel threads are started to ensure connections are received, to send
	 * data and to receive data from different clients.
	 * 
	 * When a new client will connect to the server, a clientID will be
	 * generated and given to this client until it disconnects.
	 */
	public void openConnection() {

		this.recepWorker.start();
		startReadingAndAcceptThread();
		startSendingThread();

	}

	private void startReadingAndAcceptThread() {

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						// Wait for an event one of the registered channels
						selector.select();

						// Iterate over the set of keys for which events are
						// available
						Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();

						while (selectedKeys.hasNext()) {

							SelectionKey key = selectedKeys.next();
							selectedKeys.remove();

							if (!key.isValid()) {
								continue;
							}

							// Check what event is available and deal with it
							if (key.isAcceptable()) {
								accept(key);
							} else if (key.isReadable()) {
								try {
									read(key);
								} catch (IOException e) {
									// Happens when dangerous error occurs while
									// reading, so the stream is closed.

									connectionLost(reverseClientMap.get(socketToWrapper.get(key.channel())));
								} catch (OutOfMemoryError e) {
									e.printStackTrace();
									connectionLost(reverseClientMap.get(socketToWrapper.get(key.channel())));
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();

					}
				}
			}

		}).start();
	}

	private void startSendingThread() {

		new Thread(new Runnable() {

			@Override
			public void run() {

				while (true) {

					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}

					// Checks if there is something to send to a client.
					for (Map.Entry<UUID, Queue<byte[]>> entry : dataToSend.entrySet()) {

						byte[] toSend = entry.getValue().poll();

						if (toSend != null) {
							try {
								final SocketWrapper wrapper = clientMap.get(entry.getKey());

								// The socket must not be currently used for
								// writing, or data will be corrupted. We let
								// the event pass.
								if (!wrapper.blockedReading) {

									final SocketChannel socket = wrapper.channel;

									// Writes the # of bytes to read
									ByteBuffer length = ByteBuffer.allocate(4);
									length.putInt(toSend.length);
									length.flip();
									int written1 = socket.write(length);
									System.out.println("sent " + written1 + " bytes");
									// Prepares the buffer
									ByteBuffer buffer = ByteBuffer.wrap(toSend);
									buffer.put(toSend);
									buffer.position(0);

									int written = 0;
									final int towrite = buffer.capacity();
									int writeCount = 0;

									// Writes down the buffer
									do {
										written += socket.write(buffer);
										writeCount++;
									} while (written != towrite && writeCount != WRITE_BEFORE_BACKGROUND);
									// If the buffer is not empty, the writing
									// is finished in background.
									if (buffer.hasRemaining()) {
										finshSendInBackground(wrapper, buffer);
									}
								} else {
									// Puts the data back on the queue
									entry.getValue().add(toSend);
								}
							} catch (IOException e) {
								e.printStackTrace();
								// Something went wrong, so the connection to
								// the current client is closed.
								connectionLost(entry.getKey());
							}
						}

					}
				}
			}

		}).start();
	}

	private void finshSendInBackground(SocketWrapper wrapper, ByteBuffer buffer) {

		// Blocks the channel, so nothing is written at the same time.
		wrapper.blockedWriting = true;

		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					final SocketChannel channel = wrapper.channel;
					int read = 0;
					do {
						read = channel.write(buffer);
					} while (buffer.hasRemaining() && read != -1);

					System.out.println("Background sending over : " + buffer.capacity() + " bytes");
					// Unlock the channel
					wrapper.blockedWriting = false;

				} catch (IOException e) {
					connectionLost(reverseClientMap.get(wrapper));
				}
			}
		}).start();

	}

	private void accept(SelectionKey key) throws IOException {

		if (this.clientCount < required || required == INFINITE_CLIENT) {
			// For an accept to be pending the channel must be a server socket
			// channel.
			ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

			// Accept the connection and make it non-blocking
			SocketChannel socketChannel = serverSocketChannel.accept();
			socketChannel.configureBlocking(false);

			// Register the new SocketChannel with our Selector, indicating
			// we'd like to be notified when there's data waiting to be read
			socketChannel.register(this.selector, SelectionKey.OP_READ);

			final int id = socketChannel.hashCode();
			handleConnection(socketChannel, id);

			clientCount++;
		}
	}

	private void read(SelectionKey key) throws IOException {

		SocketChannel socketChannel = (SocketChannel) key.channel();
		SocketWrapper wrapper = socketToWrapper.get(socketChannel);

		// The operation is already being performed in background, so it is
		// abort.
		if (!wrapper.blockedReading) {

			// Clear out the object length buffer so it's ready for new data
			this.byteObjLength.clear();

			socketChannel.read(byteObjLength);
			currentObject = ByteBuffer.allocate(objLength.get(0));

			System.out.println("Bytes to read : " + objLength.get(0));

			// Attempt to read off the channel
			int numRead;
			int readCount = 0;
			try {
				do {
					numRead = socketChannel.read(currentObject);
					readCount += 1;

				} while (currentObject.hasRemaining() && numRead != -1 && (readCount != READ_BEFORE_BACKGROUND));

			} catch (IOException e) {
				// The remote forcibly closed the connection, cancel
				// the selection key and close the channel.
				key.cancel();
				socketChannel.close();
				this.connectionLost(reverseClientMap.get(socketToWrapper.get(socketChannel)));
				return;
			}

			if (currentObject.hasRemaining()) {
				finishReadingInBackground(wrapper, currentObject, key);
				return;
			}

			if (numRead == -1) {

				System.out.println("-1 read - normal");

				// Remote entity shut the socket down cleanly. Do the
				// same from our end and cancel the channel.
				this.connectionLost(reverseClientMap.get(socketToWrapper.get(socketChannel)));
				return;
			}
			// Puts cursor back to 0
			currentObject.rewind();
			recepWorker.receive(currentObject, reverseClientMap.get(wrapper));
		}

	}

	private void finishReadingInBackground(final SocketWrapper wrapper, final ByteBuffer currentObject,
			final SelectionKey key) {

		wrapper.blockedReading = true;

		// De-register the socket, so no event is thrown until register again
		key.interestOps(0);

		System.out.println("Data too big - finishing background");

		new Thread(new Runnable() {

			@Override
			public void run() {
				// Attempt to read off the channel
				int numRead;

				final SocketChannel socketChannel = wrapper.channel;
				try {
					do {

						// Blocks and read data until the object buffer is
						// filled.
						numRead = socketChannel.read(currentObject);

					} while (currentObject.hasRemaining() && numRead != -1);

					if (numRead == -1) {

						System.out.println("-1 read");
						// Remote entity shut the socket down cleanly. Do the
						// same from our end and cancel the channel.
						key.channel().close();
						key.cancel();
						connectionLost(reverseClientMap.get(socketToWrapper.get(socketChannel)));
						return;
					}
					// We unlock the wrapper, new data will be read as new
					// objects
					wrapper.blockedReading = false;

					recepWorker.receive(currentObject, reverseClientMap.get(wrapper));

					// Registers to reading again
					key.interestOps(SelectionKey.OP_READ);
					// Wake up the selector for it not to block
					selector.wakeup();

				} catch (IOException e) {
					// The remote forcibly closed the connection, cancel
					// the selection key and close the channel.
					key.cancel();
					connectionLost(reverseClientMap.get(socketToWrapper.get(socketChannel)));

					return;
				}
			}
		}).start();

	}

	/**
	 * Sends an object to all the clients of the server. The object must never
	 * be null.
	 * 
	 * @see {@link ServerDataSender #sendToAll(Object)}.
	 * 
	 * @param data
	 * @throws IOException
	 */
	public void sendToAll(byte[] data) throws IOException {

		sendToAll(data, clientMap.keySet());
	}

	/**
	 * Send the given object to all the clients with the ids in the collection.
	 * 
	 * 
	 * This method is much more efficient then doing the following, because the
	 * object is not serialized every iteration, but reused.
	 * 
	 * <pre>
	 * {@code for(Integer i : ids){
	 *  send(obj, i); 
	 *  }
	 * </pre>
	 * 
	 * @param obj
	 *            The object to send
	 * @param ids
	 *            The ids of the receivers
	 * @throws IOException
	 *             If an error occurs during the serialization or sending
	 *             process.
	 */
	public void sendToAll(byte[] data, Collection<UUID> ids) throws IOException {

		for (UUID i : ids) {
			try {
				dataToSend.get(i).add(data);

			} catch (NullPointerException e) {
			}
		}

	}

	/**
	 * Sends an object to a specific client.
	 * 
	 * @see {@link ServerDataSender #send(Object, int)}
	 * 
	 * @param data
	 *            The object to be sent
	 * @param clientID
	 *            The client that will receive the object.
	 */
	public void send(byte[] data, UUID clientID) {
		dataToSend.get(clientID).add(data);
	}

	/**
	 * Safely closes connection for reception, sending and connection to the
	 * server. Once this method is called on the server side of the application,
	 * no client will be able to send, receive and connect to the server.
	 * 
	 * The sockets already opened are closed and client list are cleared. It
	 * will be possible to reopen the connection by using
	 * {@link #openConnection()}
	 */
	public void closeConnection() {

		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (Map.Entry<UUID, SocketWrapper> entry : clientMap.entrySet()) {
			try {
				entry.getValue().channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		clientMap.clear();
		reverseClientMap.clear();
		dataToSend.clear();
		socketToWrapper.clear();
		clientCount = 0;

	}

	/**
	 * Returns the IP address of the client. If the client is connected on the
	 * same network, the address returned will be a local address. In the other
	 * case, the IP address will be a real remote address.
	 * 
	 * @param clientID
	 *            The current id of the client from which IP is retrieved.
	 * @return The IP of the given client, or null if an error occurs while
	 *         retrieving it.
	 */
	public String getClientIPAddress(UUID clientID) {

		try {
			return ((InetSocketAddress) clientMap.get(clientID).channel.getRemoteAddress()).getAddress()
					.getHostAddress();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	public String getServerExternalIP() {
		System.out.println("External IP = " + externalIpAddress);
		return externalIpAddress;
	}

	private void handleConnection(SocketChannel clientSocket, int id) {

		// Unique ID should be generated from address in memory
		SocketWrapper wrapper = new SocketWrapper(clientSocket);
		UUID clientID = UUID.randomUUID();

		synchronized (clientMap) {
			clientMap.put(clientID, wrapper);

		}
		synchronized (reverseClientMap) {
			reverseClientMap.put(wrapper, clientID);

		}
		synchronized (socketToWrapper) {
			socketToWrapper.put(clientSocket, wrapper);

		}
		synchronized (dataToSend) {
			dataToSend.put(clientID, new ArrayDeque<byte[]>());

		}

		// Notifies outside application of connection
		if (connectListener != null) {
			connectListener.handleConnection(clientID);
		}

	}

	public void setConnectionListener(ConnectionListener listener) {
		this.connectListener = listener;
	}

	@Override
	public void connectionLost(UUID clientID) {

		SocketWrapper chan = null;
		System.out.println("Connection lost called");

		synchronized (clientMap) {
			chan = clientMap.remove(clientID);
		}

		synchronized (reverseClientMap) {
			reverseClientMap.remove(chan);
		}

		synchronized (socketToWrapper) {
			socketToWrapper.remove(chan);
		}

		synchronized (dataToSend) {
			dataToSend.remove(clientID);
		}

		if (chan != null) {
			try {
				chan.channel.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		clientCount--;

		if (connectListener != null) {
			try {
				// Notifies the attached connection lost listener
				connectListener.connectionLost(clientID);
			} catch (Exception e) {
			}
		}
	}

	private String getExternalIpAddress() throws IOException {
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));

		String ip = in.readLine(); // get the IP as a String

		in.close();

		return ip;

	}

	private class SocketWrapper {

		public final SocketChannel channel;

		public boolean blockedReading;
		public boolean blockedWriting;

		public SocketWrapper(SocketChannel channel) {
			this.channel = channel;
			this.blockedReading = false;
			this.blockedWriting = false;
		}

		@Override
		public int hashCode() {
			return channel.hashCode();
		}

	}

	public void setDataReceptionHandler(DataReceptionHandler handler) {
		this.recepWorker.setDataReceptionHandler(handler);
	}

}
