package serverDataManaging;

public class ServerOfflineException extends RuntimeException{

	public ServerOfflineException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServerOfflineException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ServerOfflineException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ServerOfflineException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ServerOfflineException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
