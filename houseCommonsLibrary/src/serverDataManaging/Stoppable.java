package serverDataManaging;

/**
 * An interface used to make a thread stoppable in a way that resources are free
 * when {@link #stop()} is called. Unused streams should be closed and resources
 * free at the end of this call.
 * 
 * @author sunny
 *
 */
public interface Stoppable extends Runnable {

	/**
	 * Tells either the current stoppable application is running. Normally, if
	 * this value is false, calling {@link #stop()} will have no effect on the
	 * state of the class.
	 * 
	 * @return True if the thread is running, false otherwise
	 */
	public boolean isRunning();

	/**
	 * Stops the current thread running. The call of this method should free all
	 * used resources and stop the execution as soon as reading/writing is
	 * finished. After this method is called, the current thread should shortly
	 * ends working.
	 */
	public void stop();
}
