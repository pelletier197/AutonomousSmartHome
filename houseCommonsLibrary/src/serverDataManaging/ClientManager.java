package serverDataManaging;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.UUID;

/**
 * A Client Manager is a class that provides different methods to communicate
 * directly with a server situated on the local host via the port given at
 * construction.
 * 
 * Exchange between server and client starts when
 * {@link #openConnection(DataReceptionHandler)} is called, and the data
 * received will be sent to the listener given in parameter. This calling will
 * attempts to connect the client to the server until connection success,
 * blocking the current thread to this method calling.
 * 
 * It is also possible to close the connection to the server using
 * {@link #closeConnection()}. It will be possible to open it again reusing
 * {@link #openConnection(DataReceptionHandler)}.
 * 
 * @author sunny
 *
 */
public class ClientManager implements ConnectionLostListener {

	private static final String BASE_ADDRESS = "192.168.2.";

	/**
	 * The socket of the client
	 */
	private SocketChannel socket;

	/**
	 * The current port on which the client must connect on localHost
	 */
	private int port;

	private String currentIP;

	private Selector selector;

	private DataReceptionWorker worker;

	private ByteBuffer currentObject;
	private ByteBuffer byteObjLength;
	private IntBuffer objLength;

	/**
	 * Constructs a client manager that will use the port given in parameter for
	 * data sharing between the client and the server. This port will remain
	 * unused
	 * 
	 * @param port
	 *            The port on which to connect on the local host.
	 */
	public ClientManager(int port, DataReceptionHandler listener) {

		if (listener == null) {
			throw new NullPointerException("Cannot use a null listener");
		}
		// Always connect to the localHost.
		this.port = port;

		this.worker = new DataReceptionWorker(listener, this);

		this.byteObjLength = ByteBuffer.allocate(4);
		this.objLength = byteObjLength.asIntBuffer();

	}

	/**
	 * Tells either connection has been canceled or not
	 */
	private boolean connectionCanceled = false;
	/**
	 * Tells either connection succeed or not
	 */
	private boolean connectionSucceded = false;

	private ConnectionLostListener conList;

	/**
	 * Opens the connection with the server, using the listener in parameter to
	 * send reception data.
	 * 
	 * First, the client will attempt to connect to the server until success,
	 * blocking the current thread.
	 * 
	 * Two threads will start after this method is called, and will run until
	 * {@link #closeConnection()} is called.
	 * 
	 * @throws UnknownHostException
	 * 
	 */
	public void openConnection() throws UnknownHostException {

		// reset connection state
		connectionCanceled = false;
		connectionSucceded = false;

		while (!connectionCanceled && !connectionSucceded) {

			String base = BASE_ADDRESS;

			for (int i = 0; i < 255 && !connectionCanceled; i++) {
				try {
					String current = base + i;

					System.out.println("Testing : " + current);

					openConnection(current, 100);

					// If the test is over, stop testing
					if (connectionSucceded) {
						break;
					}
				} catch (IOException e) {
				}

			}

		}
	}

	/**
	 * Opens the connection with the server, using the listener in parameter to
	 * send reception data.
	 * 
	 * First, the client will attempt to connect to the server until success,
	 * blocking the current thread.
	 * 
	 * Two threads will start after this method is called, and will run until
	 * {@link #closeConnection()} is called.
	 * 
	 */
	public void openConnection(String ip, int timeout) throws IOException {

		if (!connectionSucceded) {

			// Once connection is established, the connection
			// attempt is
			// canceled.
			socket = SocketChannel.open();
			socket.configureBlocking(false);

			// Kick off connection establishment
			socket.connect(new InetSocketAddress(ip, this.port));

			// Opens a selector
			selector = Selector.open();

			if (!socket.isConnected()) {
				// Test connection during the given timeout
				socket.register(selector, SelectionKey.OP_CONNECT);

				selector.select(timeout);

				if (selector.selectedKeys().isEmpty()) {
					throw new IOException("Connection failed");
				}
				Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

				while (keys.hasNext()) {
					SelectionKey key = keys.next();
					keys.remove();

					if (!key.isValid()) {
						throw new IOException("Cannot connect");
					}
					if (key.isConnectable()) {
						SocketChannel socketChannel = (SocketChannel) key.channel();
						socketChannel.finishConnect();
					} else {
						throw new IOException("Cannot connect");
					}
				}

			}

			System.out.println("connection succeded");

			// Sets the selector to be notified when reading.
			socket.register(selector, SelectionKey.OP_READ);

			// The ip to which we connected
			this.currentIP = ip;

			this.worker.start();
			initServerThread();

			connectionSucceded = true;
		}
	}

	private void initServerThread() {
		new Thread(new Runnable() {

			@Override
			public void run() {

				boolean running = true;

				while (running) {
					try {

						// Wait for an event one of the registered channels
						selector.select();

						System.out.println("NOTIFIED");

						// Iterate over the set of keys for which events are
						// available
						Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();

						while (selectedKeys.hasNext()) {

							SelectionKey key = selectedKeys.next();
							selectedKeys.remove();

							if (!key.isValid()) {
								continue;
							}

							// Check what event is available and deal with it
							if (key.isReadable()) {
								try {

									read(key);
								} catch (IOException e) {
									running = false;
									connectionLost(null);

								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}).start();

	}

	private void read(SelectionKey key) throws IOException {

		SocketChannel socketChannel = (SocketChannel) key.channel();

		// Clear out the object length buffer so it's ready for new data
		this.byteObjLength.clear();

		socketChannel.read(byteObjLength);
		currentObject = ByteBuffer.allocate(objLength.get(0));

		System.out.println("should read "+currentObject.capacity()+" bytes");
		// Attempt to read off the channel
		int numRead;
		try {
			do {
				// Blocks and read data until the object buffer is filled.
				numRead = socketChannel.read(currentObject);
			} while (currentObject.hasRemaining() || numRead == -1);

		} catch (IOException e) {
			// The remote forcibly closed the connection, cancel
			// the selection key and close the channel.
			key.cancel();
			socketChannel.close();
			return;
		}

		if (numRead == -1)

		{
			// Remote entity shut the socket down cleanly. Do the
			// same from our end and cancel the channel.
			key.channel().close();
			key.cancel();
			return;
		}

		System.out.println("Received client : " + currentObject.capacity());
		worker.receive(currentObject, null);
	}

	/**
	 * Sends an object to a specific client.
	 * 
	 * @see {@link ServerDataSender #send(Object, int)}
	 * 
	 * @param data
	 *            The object to be sent
	 * @param clientID
	 *            The client that will receive the object.
	 */
	public void send(byte[] data) {
		try {

			int written = 0;

			// Converts it to sendable buffer with the length to read
			final ByteBuffer length = ByteBuffer.allocate(4).putInt(data.length);
			length.flip();
			socket.write(length);
			
			final ByteBuffer tosend = ByteBuffer.wrap(data);
			tosend.position(0);

			final int towrite = tosend.capacity();
			do {
				System.out.println("Writing to socket");
				written += socket.write(tosend);
			} while (written != towrite);

		} catch (IOException e) {
			e.printStackTrace();

			connectionLost(null);

		}
	}

	/**
	 * Sends an object to the server via {@link #sender}.
	 * 
	 * @see ClientDataSender
	 * 
	 * @param data
	 *            The object that must be sent
	 */
	public void sendUnshared(byte[] data) {
		send(data);
	}

	public void setDataReceptionHandler(DataReceptionHandler handler) {
		this.worker.setDataReceptionHandler(handler);
	}

	private void restartConnection() throws UnknownHostException {

		closeConnection();

		socket = null;

		openConnection();

	}

	/**
	 * Closes the connection between the server and the client. After this
	 * method is called, no other data will be received via the listener. No new
	 * client will be able to connect again to the server. Also, every parallel
	 * thread is stopped.
	 * 
	 * @throws IOException
	 *             If error occurs during closing
	 */
	public void closeConnection() {

		// closes the socket to free the port
		try {
			connectionSucceded = false;
			this.socket.close();
		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Cancels the connection. This method call has no effect if the client is
	 * already connected to the server. However, it will cancel the connection
	 * attempts if the client is currently trying to join the server.
	 */
	public void cancelConnection() {
		connectionCanceled = true;
	}

	public String getIP() {
		// TODO Auto-generated method stub
		return currentIP;
	}

	public boolean isConnected() {
		return connectionSucceded;
	}

	/**
	 * Use this function to set an event to occur on connection to server lost.
	 * By default, if null, the server will attempt reconnecting to the server
	 * on connection lost, but this function can be overwritten to whatever is
	 * needed.
	 * 
	 * @param listener
	 *            The listener that will be called on connection to server lost.
	 */
	public void setOnConnectionLostListener(ConnectionLostListener listener) {
		this.conList = listener;
	}

	@Override
	public void connectionLost(UUID clientID) {

		closeConnection();

		if (conList == null) {

			try {
				restartConnection();
			} catch (UnknownHostException e) {
				closeConnection();
				e.printStackTrace();
			}
		} else {
			// First closes the connection
			closeConnection();

			conList.connectionLost(clientID);
		}
	}

}
