package serverDataManaging;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;
import java.util.UUID;

public class DataReceptionWorker {

	private DataReceptionHandler handler;

	private Queue<Object[]> dataQueue;

	private ConnectionLostListener connectionListener;

	public DataReceptionWorker(DataReceptionHandler handler, ConnectionLostListener listener) {

		this.handler = handler;
		this.dataQueue = new ArrayDeque<>();

		setConnectionLostListener(listener);
	}

	public void start() {
		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					Object[] data = null;
					ByteBuffer buffer = null;
					UUID id = null;

					while (true) {

						data = dataQueue.poll();

						if (data != null) {

							buffer = (ByteBuffer) data[0];
							id = (UUID) data[1];

							if (buffer.capacity() > 0) {

								if (handler != null) {

									// Rewinds the buffer
									buffer.rewind();

									// Data received. Notify the handler
									handler.received(buffer, id);
								}
							} else {

								// Happens when a channel is closed unsafely.
								connectionListener.connectionLost(id);
							}

						} else {
							try {
								Thread.sleep(25);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}).start();
	}

	public void receive(ByteBuffer data, UUID id) {
		System.out.println("Data receive added to unserialization queue");
		this.dataQueue.add(new Object[] { data, id });

	}

	public void setDataReceptionHandler(DataReceptionHandler handler) {
		this.handler = handler;
	}

	public void setConnectionLostListener(ConnectionLostListener listener) {
		if (listener == null) {
			throw new NullPointerException("Null listener");
		}
		this.connectionListener = listener;
	}

}
