package embedded.controllers.camera;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author sunny
 *
 */
public interface AbstractCamera {

	/**
	 * Opens the camera.
	 * 
	 * @return True if the camera is successfully open
	 */
	public boolean open();

	/**
	 * Capture and returns an image of the camera that will have the exact
	 * {@link #getWidth()} width and {@link #getHeight()} height values.
	 * 
	 * <p>
	 * This image will have the exact {@link #getImageSize()} in byte.
	 * </p>
	 * 
	 * <p>
	 * Note that the image pixel encoding may differ from a camera to an other.
	 * 
	 * @return Returns a raw image image of the camera that will have the exact
	 *         {@link #getImageSize()} length, or null if the camera is closed.
	 */
	public BufferedImage getImage();

	/**
	 * @return True if the camera is open
	 */
	public boolean isOpen();

	/**
	 * Closes the camera
	 * 
	 * @return True if the camera was successfully closed.
	 */
	public boolean close();

	/**
	 * 
	 * @return The with of the image in pixels.
	 */
	public int getWidth();

	/**
	 * 
	 * @return The height of the image in pixels.
	 */
	public int getHeight();

	/**
	 * Gets the id of the given camera
	 * 
	 * @return Camera's ID
	 */
	public String getID();

	/**
	 * Returns a raw image of the camera. The encoding must correspond to the
	 * {@link #getEncoding()} image, and must be of size {@link #getHeight()} x
	 * {@link #getWidth()}.
	 * 
	 * @return
	 */
	byte[] getImageByte();

	/**
	 * Returns the encoding of the raw image retrieved via
	 * {@link #getImageByte()}
	 * 
	 * @return
	 */
	int getEncoding();

	/**
	 * Returns the size of the image in bytes. This correspond to the length of
	 * the array when {@link #getImageByte()} is called.
	 * 
	 * @return The raw image's size in bytes.
	 */
	int getImageSize();
	
	/**
	 * Returns the camera frame rate per second
	 * 
	 * @return The frame rate in frame per second
	 */
	int getFPS();

}
