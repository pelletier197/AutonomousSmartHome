package embedded.controllers.thermometer;

/**
 * An abstract thermometer interface for retrieving the temperature from an
 * embedded thermometer.
 * 
 * @author sunny
 *
 */
public interface AbstractThermometer {

	/**
	 * Retrieves and return the temperature measured from the thermometer in celcius.
	 * 
	 * @return The temperature measured in celcius
	 */
	public double getTemperature();
}
