package embedded.controllers.baseboard;

/**
 * 
 * An abstract baseboard controller. Note that some baseboard may stop heating
 * automatically when a danger is detected.
 * 
 * @author sunny
 *
 */
public interface AbstractBaseboard {

	/**
	 * @return True if the heat successfully started
	 */
	public boolean activateBaseboard();

	/**
	 * @return True if the heat successfully ended.
	 */
	public boolean deactivateBaseboard();

}
