package embedded.controllers.light;

public enum LightEventType {
	OPEN, CLOSE;
}
