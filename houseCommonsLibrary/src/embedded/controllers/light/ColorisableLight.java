package embedded.controllers.light;

/**
 * Interface implemented by light types that can be colorized. This light type
 * implements directly {@link AbstractLight} functions.
 * 
 * @author sunny
 *
 */
public interface ColorisableLight {

	/**
	 * @return The RGB color code of the light.
	 */
	public int getColor();

	/**
	 * Changes the color of the light.
	 * 
	 * @param rgbValue
	 *            The new value of RGB light color.
	 */
	public void setColor(int rgbValue);

	/**
	 * Resets the light color to the original light color
	 */
	public void resetColor();

}
