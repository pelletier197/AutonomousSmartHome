package embedded.controllers.light;

import java.util.List;

public interface LightManager {
	
	public void load();

	public List<AbstractLight> getLights();
	
	public void close();

}
