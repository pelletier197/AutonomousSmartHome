package embedded.controllers.light;

import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.alarm.Tone;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.lang.NullPointerException;
import java.util.Arrays;
import java.util.UUID;

public class LightEvent extends Alarm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9089637877137864042L;
	private String[] aimedLights;
	private LightEventType type;

	public LightEvent(int hour, int minutes, String[] aimedLightsIDs, LightEventType type, AlarmDay... days) {
		super(hour, minutes, new Tone(null, 0), days);

		setAimedLights(aimedLightsIDs);
		setType(type);

	}

	public LightEvent(int hour, int minutes, String[] aimedLightsIDs, LightEventType type) {
		super(hour, minutes, new Tone(null, 0));

		setAimedLights(aimedLightsIDs);
		setType(type);

	}

	public String[] getAimedLights() {
		return aimedLights;
	}

	public LightEventType getType() {
		return type;
	}

	public void setAimedLights(String[] aimedLightsIDs) {

		if (aimedLightsIDs == null || aimedLightsIDs.length == 0) {
			throw new IllegalAccessError("There must be at least one light for the event to be valid.");
		}
		this.aimedLights = aimedLightsIDs;
	}

	public void setType(LightEventType type) {
		if (type == null) {
			throw new NullPointerException("Null type not permitted");
		}
		this.type = type;
	}

	@Override
	public Tone getTone() {
		throw new IllegalAccessError("The light event doesn't have a tone");
	}

	@Override
	public void setTone(Tone tone) {
		throw new IllegalAccessError("The light event doesn't have a tone");
	}

	@Override
	public String toString() {
		return "LightEvent [aimedLights=" + Arrays.toString(aimedLights) + ", type=" + type + ", toString()="
				+ super.toString() + "]";
	}
	
	
}
