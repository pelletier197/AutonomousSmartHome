package embedded.controllers.light;

import java.io.Serializable;

/**
 * 
 * An abstract light controller.
 * 
 * @author sunny
 *
 */
public interface AbstractLight extends Serializable{

	/**
	 * Turns on the light
	 */
	public void open();

	/**
	 * Turns off the light
	 */
	public void close();
	
	/**
	 * Tells either the light is open or not
	 */
	public boolean isOpen();
	
	public String getLightID();
	
	public String getName();
	public void setName(String name);

}
