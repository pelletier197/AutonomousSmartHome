package embedded.controllers.light;

public interface DimmableLight {

	/**
	 * Sets the brightness of the light. Note that not all light systems may
	 * support this operation.
	 * 
	 * @param value
	 *            The brightness of the light, between 0 and 1 included.
	 */
	public void setBrightness(double value);

	/**
	 * @return The brightness value between 0 and 1.
	 */
	public double getBrightness();

}
