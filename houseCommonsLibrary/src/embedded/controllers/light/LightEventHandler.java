package embedded.controllers.light;

import java.util.HashMap;

import embedded.controllers.light.AbstractLight;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmHandler;

/**
 * A class that handles events for lights. Light events may be added to the
 * {@link HouseLightManager}, and the events will be managed by this class.
 * 
 * <p>
 * An event for a light represent a programmed light event. The type of the
 * event is scheduled by {@link LightEventType} type.
 * 
 * @author sunny
 *
 */
public class LightEventHandler implements AlarmHandler {

	private HashMap<String, AbstractLight> idLightMap;

	@Override
	public void handleAlarm(Alarm alarm) {

		LightEvent event = (LightEvent) alarm;

		final String[] ids = event.getAimedLights();
		final AbstractLight[] lights = getLights(ids);

		switch (event.getType()) {
		case OPEN:
			openAll(lights);
			break;
		case CLOSE:
			closeAll(lights);
			break;

		}

	}

	private void closeAll(AbstractLight[] lights) {
		for (AbstractLight li : lights) {
			try {
				li.close();
			} catch (Exception e) {

			}
		}

	}

	private void openAll(AbstractLight[] lights) {
		for (AbstractLight li : lights) {
			try {
				li.open();
			} catch (Exception e) {

			}
		}

	}

	private AbstractLight[] getLights(String[] ids) {
		AbstractLight[] lights = new AbstractLight[ids.length];

		for (int i = 0; i < ids.length; i++) {
			try {
				lights[i] = idLightMap.get(ids[i]);
			} catch (Exception e) {
				// May occur if the light was not added.
				e.printStackTrace();
			}
		}
		return lights;
	}

}
