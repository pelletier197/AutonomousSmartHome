package stream;

import utils.Compresser;

/**
 * Data uncompresser using GZIP algorithm 
 * 
 * @author sunny
 *
 */
public class GZIPCompressor implements DataCompressor {

	@Override
	public byte[] compress(byte[] data) {
		return Compresser.compressGZIP(data);
	}

	@Override
	public byte[] uncompress(byte[] data) {
		return Compresser.uncompressGZIP(data);
	}

}
