package stream;

public interface ConnectionListener {

	public void onConnectionSucceded();

	public void onConnectionClosed();

}
