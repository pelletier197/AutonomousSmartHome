package stream;

import java.nio.ByteBuffer;
import java.util.UUID;

import server.client.ClientManager;
import server.transport.ConnectionListener;
import server.transport.ConnectionLostListener;
import server.transport.DataReceptionHandler;

public class ClientStreamManager {

	private DataCompressor compresser;

	private DataReceptionHandler handler;

	private ClientManager man;

	public ClientStreamManager(int port, DataReceptionHandler handler, ConnectionLostListener listener) {
		this(port, null, handler, listener);
	}

	public ClientStreamManager(int port, DataCompressor compresser, DataReceptionHandler handler,
			ConnectionLostListener listener) {

		if (compresser == null) {
			compresser = new NoCompressor();
		}

		this.compresser = compresser;
		this.handler = handler;

		this.man = new ClientManager(port);
		this.man.addOnConnectionLostListener(listener);
	}

	private DataReceptionHandler generateDataReceptionListener() {
		return new DataReceptionHandler() {

			@Override
			public void receive(ByteBuffer data, UUID clientId) {
				// TODO Auto-generated method stub

			}
		};
	}

	public void setOnConnectionLostListener(ConnectionLostListener listener) {
		man.addOnConnectionLostListener(listener);
	}

	public void send(byte[] data) {
		// TODO Auto-generated method stub
		man.send(compresser.compress(data));
	}

}
