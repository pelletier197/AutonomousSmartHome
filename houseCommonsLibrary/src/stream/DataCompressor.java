package stream;

public interface DataCompressor {

	/**
	 * Calls the class to compress the given data into smaller data array.
	 * 
	 * @param data The data to compress
	 * @return The compressed byte array
	 */
	public byte[] compress(byte[] data);

	/**
	 * Calls the class to uncompress the given data.
	 * 
	 * @param data The data to uncompress
	 * @return The uncompressed byte array
	 */
	public byte[] uncompress(byte[] data);

}
