package stream;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import server.communication.Request;
import server.dispatch.AbstractDataDispatcher;
import server.dispatch.DispatchOverloadSelector;
import server.serialization.AbstractSerializationManager;
import server.transport.ConnectionListener;
import server.transport.DataReceptionHandler;
import server.transport.ServerManager;

public class ServerStreamManager {

	private DataCompressor compresser;

	private ServerManager man;
	private DataReceptionHandler handler;

	public ServerStreamManager(int port, int required, ConnectionListener connectionListener,
			DataReceptionHandler handler) throws IOException {
		this(port, required, connectionListener, handler, null);
	}

	public ServerStreamManager(int port, int required, ConnectionListener connectionListener,
			DataReceptionHandler handler, DataCompressor compresser) throws IOException {

		if (connectionListener == null || handler == null) {
			throw new NullPointerException("Handler and listener can't be null");
		}

		if (compresser == null) {
			compresser = new NoCompressor();
		}

		man = new ServerManager(port, required);
		man.getDataReceiver().setSerializationManager(generateSerializationManager());
		man.addConnectionListener(connectionListener);

		this.handler = handler;
		this.compresser = compresser;
	}

	private AbstractSerializationManager generateSerializationManager() {
		return new AbstractSerializationManager() {

			@Override
			public byte[] serialize(Object data, byte serType) throws IOException {
				return (byte[]) data;
			}

			@Override
			public Object deserialize(byte[] data, byte serType) throws IOException {
				return data;
			}
		};
	}

	private AbstractDataDispatcher generateDataDispatcher() {
		return new AbstractDataDispatcher() {

			@Override
			public void dispatch(Request request) {

			}

			@Override
			public void addDataReceiver(Object receiver) {
			}

			@Override
			public void setDispatchOverloadSelector(DispatchOverloadSelector selector) {
				// TODO Auto-generated method stub

			}
		};
	}

	public void sendToAll(byte[] data) throws IOException {
		man.sendToAll(compresser.compress(data));
	}

	public void sendToAll(byte[] data, Collection<UUID> ids) throws IOException {
		man.sendToAll(compresser.compress(data), ids);
	}

	public void send(byte[] data, UUID clientID) {
		man.send(compresser.compress(data), clientID);
	}
}
