package stream;

/**
 * Basic compressor that does not compress data packets. It only returns the
 * data packets directly without any compression.
 * 
 * @author sunny
 *
 */
public class NoCompressor implements DataCompressor {


	/**
	 * Returns directly the data
	 */
	@Override
	public byte[] compress(byte[] data) {
		return data;
	}

	/**
	 * Returns directly the data
	 */
	@Override
	public byte[] uncompress(byte[] data) {
		return data;
	}

}
