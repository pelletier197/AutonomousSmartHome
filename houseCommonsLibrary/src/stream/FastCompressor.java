package stream;

import utils.Compresser;

/**
 * Fast but non efficient data compresser.
 * 
 * @author sunny
 *
 */
public class FastCompressor implements DataCompressor {

	@Override
	public byte[] compress(byte[] data) {
		return Compresser.compressFast(data);
	}

	
	@Override
	public byte[] uncompress(byte[] data) {
		return Compresser.uncompress(data);
	}

}
