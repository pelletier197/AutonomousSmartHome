package usbUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.usb4java.BufferUtils;
import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;
import org.usb4java.Transfer;

/**
 * Threaded manager of multiple connections to USB devices detected on the OS.
 * Using this class, the connection can only be threaded.
 * 
 * <p>
 * When using this manager, first add new connection information via
 * {@link #addUSBConnection(USBConnecterInfo)},
 * {@link #addUSBConnection(String, int, int, byte)} or
 * {@link #addUSBConnection(short, short, int, int, byte)}. Then, call
 * {@link #connect()}, which will start the background thread after having set
 * the receiver class via {@link #setDataReceiver(USBDataReceiver)}. The data
 * received via USB connections will be sent to the receiver class directly. The
 * reception timeout between the next USB device check is set to the
 * {@link #receptionTimeout} value.
 * </p>
 * 
 * <p>
 * To stop the execution of the thread, use {@link #close()}. After this method
 * call, note that the manager may ot be used anymore.
 * </p>
 * 
 * <b>Note that this manager uses bulk transfer method to receive and send the
 * data.</b>
 * 
 * @see {@link USBConnecterInfo}, {@link USBConnectionBase},
 *      {@link BulkTransferer}
 * 
 * @author sunny
 *
 */
public class MultipleUSBConnectionManager extends USBConnectionBase {

	/**
	 * Default timeout for reception if none is given.
	 */
	public static final long DEFAULT_RECEPTION_TIMEOUT = 1000L;

	/**
	 * Application's specific reception timeout between the next device check
	 * during a threaded usage.
	 */
	private long receptionTimeout = DEFAULT_RECEPTION_TIMEOUT;

	/**
	 * The list of connection checked fr data reception.
	 */
	private List<USBConnecterInfo> infos;

	/**
	 * The transfer manager, which transfers the data using a bulk transfer
	 */
	private BulkTransferer transferManager;

	/**
	 * Creates a multiple connection manager with an empty list of connection.
	 * At this state, the manager has an empty list of connection, and is not
	 * yet receiving data. To start the execution, use {@link #connect()}
	 */
	public MultipleUSBConnectionManager() {
		super();

		this.infos = new ArrayList<>();
		this.transferManager = new BulkTransferer();

	}

	/**
	 * Creates and add a USB connection that has the given device name in its
	 * descriptor/dump. This method will claim the interface associated to the
	 * interface number, and will set the size of bytes that this connection may
	 * send. The connection will also be made to the given endpoint.
	 * 
	 * @see {@linkplain USBConnecterInfo}
	 * 
	 * @param deviceName
	 *            The string reference contained in the descriptor.
	 * @param interfaceNumber
	 *            The interface to claim. // * @param byteLength The size of the
	 *            data array that will be sent/received from the device.
	 * @param endPoint
	 *            The endpoint value of the device. This value has special bits
	 *            reserved. See documentation about this.
	 */
	public synchronized void addUSBConnection(String deviceName, int interfaceNumber, int byteLength, byte endPoint) {

		this.infos.add(new USBConnecterInfo(deviceName, interfaceNumber, byteLength, endPoint, getDeviceList()));

	}

	/**
	 * Creates and add a USB connection that has the given vendor ID and product
	 * ID in its descriptor. This method will claim the interface associated to
	 * the interface number, and will set the size of bytes that this connection
	 * may send. The connection will also be made to the given endpoint.
	 * 
	 * @see {@linkplain USBConnecterInfo}
	 * 
	 * @param idVendor
	 *            The vendor ID attributed b USB.org
	 * @param idProduct
	 *            The product ID attributed by the manufacturer.
	 * @param interfaceNumber
	 *            The interface to claim.
	 * @param byteLength
	 *            The size of the data array that will be sent/received from the
	 *            device.
	 * @param endPoint
	 *            The endpoint value of the device. This value has special bits
	 *            reserved. See documentation about this.
	 */
	public synchronized void addUSBConnection(short idVendor, short idProduct, int interfaceNumber, int byteLength,
			byte endPoint) {

		this.infos
				.add(new USBConnecterInfo(idVendor, idProduct, interfaceNumber, byteLength, endPoint, getDeviceList()));

	}

	/**
	 * Add the given connection to the device in the manager. It will then be
	 * possible to communicate with this device via bulk transfer after
	 * {@link #connect()} is called.
	 * 
	 * Note that this method is synchronized and ca be called from any thread,
	 * even if the connection is launched.
	 * 
	 * @param info
	 *            The connection information.
	 */
	public synchronized void addUSBConnection(USBConnecterInfo info) {

		if (info == null) {
			throw new NullPointerException("Cannot add a null USB connection");
		}
		this.infos.add(info);
	}

	/**
	 * Removes the given usb connection from the manager. Calling this method
	 * will remove the given device from the connection list. It will no longer
	 * be possible to receive data from this device.
	 * 
	 * @param info
	 *            The connection to remove.
	 */
	public synchronized void removeUSBConnection(USBConnecterInfo info) {

		this.infos.remove(info);

	}

	/**
	 * 
	 * @return A List of all current connection on this manager. Note that this
	 *         list is not modifiable, but the information can be modified
	 *         without any problem (endpoint, sizem etc.)
	 */
	public List<USBConnecterInfo> getUSBConnectionInfo() {
		return Collections.unmodifiableList(infos);
	}

	/**
	 * Method called every frame to check reception from any device. Do not call
	 * this method outside the current class.
	 */
	@Override
	public void checkRecetion() {

		for (USBConnecterInfo info : infos) {
			System.out.println("Checking");

			// Creates a buffer for the number of byte to receive
			ByteBuffer buffer = transferManager.receive(info, receptionTimeout, info.getSize());

			if (buffer != null) {
				getReceiver().receive(buffer, info);
			}
		}
	}

	/**
	 * Sends the given lot of bytes to a connected device detected by the OS.
	 * CIt is allowed to send data to a device that is not added in the transfer
	 * list, but this device will not have the possibility to answer via the
	 * current thread.
	 * 
	 * @param transfer
	 *            The data to send
	 * @param info
	 *            The information about the connection.
	 * @param timeOut
	 *            The allowed timeout to send the data.
	 */
	public void send(byte[] transfer, USBConnecterInfo info, long timeOut) {
		transferManager.send(transfer, info, timeOut);
	}

	/**
	 * Handles the close of the connection. This method must not be called
	 * outside the current class.
	 */
	@Override
	public void handleClose() {
		for (USBConnecterInfo info : infos) {
			info.close();
		}

		// Clear information to release memory
		infos.clear();

		System.gc();

	}
}
