package usbUtil;

import java.nio.ByteBuffer;
import java.util.Arrays;

import org.usb4java.DeviceDescriptor;

public class Tester implements USBDataReceiver {

	public static void main(String[] args) throws Exception {
		MultipleUSBConnectionManager manager = new MultipleUSBConnectionManager();
		
			manager.setDataReceiver(new Tester());
			for (DeviceDescriptor s : manager.getAvailableDevicesDescritor()) {
				System.out.println(s + " /////////////////////");
			}
			USBConnecterInfo info = new USBConnecterInfo((short) 0x1c7a, (short) 0x0603, 0, 79360, (byte) (2),
					manager.getDeviceList());
			manager.addUSBConnection(info);
			//manager.connect();
			manager.send(new byte[] { 1 }, manager.getUSBConnectionInfo().get(0), 5000);
		
			manager.close();
			
			
			
		
		// Configuration configuration = new Configuration();
		/*
		 * configuration.setAcousticModelPath(
		 * "resource:/models/fr-ca/cmusphinx-fr-5.2");
		 * configuration.setDictionaryPath("resource:/models/fr-ca/fr.dict");
		 * configuration.setLanguageModelPath(
		 * "resource:/models/fr-ca/fr-small.lm.bin");
		 */
		/*
		 * configuration.setAcousticModelPath(
		 * "resource:/edu/cmu/sphinx/models/en-us/en-us");
		 * configuration.setDictionaryPath(
		 * "resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
		 * configuration.setLanguageModelPath(
		 * "resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");
		 * 
		 * LiveSpeechRecognizer recognizer = new
		 * LiveSpeechRecognizer(configuration); InputStream stream = new
		 * FileInputStream(new
		 * File(Tester.class.getResource("/audio/test0.wav").getPath()));
		 * 
		 * recognizer.startRecognition(true); System.out.println("Speak now");
		 * SpeechResult result; while ((result = recognizer.getResult()) !=
		 * null) { System.out.println("stop now"); System.out.format(
		 * "Hypothesis: %s\n", result.getHypothesis()); }
		 * recognizer.stopRecognition();
		 */
	}

	@Override
	public void receive(ByteBuffer buffer, USBConnecterInfo connecter) {

		byte[] bytes = new byte[buffer.remaining()];
		buffer.get(bytes);

		System.out.println(Arrays.toString(bytes) + " reception");
	}
}
