package usbUtil;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.usb4java.BufferUtils;
import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceHandle;
import org.usb4java.DeviceList;
import org.usb4java.Interface;
import org.usb4java.LibUsb;
import org.usb4java.Transfer;

/**
 * A threaded single USB connection to a single device. This class may be used
 * when a program needs a constant checking of data reception, or if the program
 * only contain one single USB device.
 * 
 * <p>
 * Note that the reception check is checked to indefinite timeout, as there is
 * only one device.
 * </p>
 * 
 * @author sunny
 *
 */
public class SingleUSBConnection extends USBConnectionBase {

	public static final long DEFAULT_RECEPTION_TIMEOUT = 0L;

	private long receptionTimeout = DEFAULT_RECEPTION_TIMEOUT;

	private USBConnecterInfo infos;

	public SingleUSBConnection(short idVendor, short idProduct, int interfaceNumber, int byteLength, byte endPoint) {
		super();
		this.infos = new USBConnecterInfo(idVendor, idProduct, interfaceNumber, byteLength, endPoint, getDeviceList());

	}

	public SingleUSBConnection(String deviceName, int interfaceNumber, int byteLength, byte endPoint) {
		super();
		this.infos = new USBConnecterInfo(deviceName, interfaceNumber, byteLength, endPoint, getDeviceList());

	}

	@Override
	public void checkRecetion() {
		// Creates a buffer for the number of byte to receive
		ByteBuffer buffer = BufferUtils.allocateByteBuffer(infos.getSize());

		// Creates the intBuffer to read data
		IntBuffer transfered = BufferUtils.allocateIntBuffer();

		// https://github.com/usb4java/usb4java-examples/blob/master/src/main/java/org/usb4java/examples/SyncBulkTransfer.java
		int result = LibUsb.bulkTransfer(infos.getHandle(), infos.getEndPoint(), buffer, transfered, 0);

		System.out.println(buffer.toString());
		if (result == LibUsb.SUCCESS && getReceiver() != null) {
			getReceiver().receive(buffer, infos);
		}
	}

	@Override
	public void handleClose() {
		infos.close();
		infos = null;
	}

}
