package usbUtil;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.usb4java.BufferUtils;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;

/**
 * Simple class providing methods to send and receive data from a USB device to
 * the program, using Bulk Transfer method.
 * 
 * @author sunny
 *
 */
public class BulkTransferer {

	/**
	 * The receiver method using bulk transfer this method will attempt to
	 * receive data during the given timeout in milliseconds. the reception MAY
	 * fail for any reason, most likely, either no data is received, or device's
	 * peripheral is used by the OS. In any of these cases, the return result
	 * will be null.
	 * 
	 * To free device's peripheral, use Zadig tool on your device, then restart
	 * the OS.
	 * 
	 * @param info
	 *            The USB connection information for bulk transfer between
	 *            device and OS.
	 * @param timeout
	 *            The allowed timeout to process the transfer. Note that this
	 *            method will block for the given timeout if no data is
	 *            received.
	 * @param size
	 *            The quantity of byte to receive. The quantity contained in the
	 *            info is ignored.
	 * @return The byte received from the device in a byte buffer, or null if no
	 *         data is received.
	 */
	public ByteBuffer receive(USBConnecterInfo info, long timeout, int size) {

		// Creates a buffer for the number of byte to receive
		ByteBuffer buffer = BufferUtils.allocateByteBuffer(size);

		// Creates the intBuffer to read data. This int buffer will contain the
		// number of bytes transfered
		IntBuffer transfered = IntBuffer.allocate(1);

		System.out.println(info.getHandle() + " " + info.getEndPoint());
		// https://github.com/usb4java/usb4java-examples/blob/master/src/main/java/org/usb4java/examples/SyncBulkTransfer.java
		int result = LibUsb.bulkTransfer(info.getHandle(), (byte) (LibUsb.ENDPOINT_IN | info.getEndPoint()), buffer,
				transfered, timeout);
		System.out.println(result + " result");

		// On success, return the data received
		if (result >= 0) {
			return buffer;

		}
		return null;

	}

	/**
	 * Processes a bulk transfer to the given device using connection
	 * information. The given bytes will be transfered with an allowed timeout.
	 * 
	 * @param transfer
	 *            The data to be transfered to the device.
	 * @param info
	 *            The connection information to the USB device.
	 * @param timeOut
	 *            The timeout allowed for the transfer. Note that this method
	 *            may block until timeout value if the transfer fails.
	 */
	public void send(byte[] transfer, USBConnecterInfo info, long timeOut) {
		// The data to be sent
		ByteBuffer buffer = ByteBuffer.allocateDirect(transfer.length);
		buffer.put(transfer);

		// Amount of bytes transfered
		IntBuffer transfered = IntBuffer.allocate(1);

		// Only takes bits 0-4
		int intResult = LibUsb.bulkTransfer(info.getHandle(), (byte) (info.getEndPoint() & 0xF), buffer, transfered,
				timeOut);
		System.out.println(Integer.toBinaryString(info.getEndPoint() & 0xF));

		try {

			if (intResult < LibUsb.SUCCESS) {
				throw new LibUsbException("Builk transfer failed", intResult);
			}
			if (transfered.get(0) != transfer.length) {
				throw new LibUsbException("Control transfer failed, Not all bytes were transfered", intResult);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
