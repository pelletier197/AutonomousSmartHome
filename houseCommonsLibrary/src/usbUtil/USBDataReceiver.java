package usbUtil;

import java.nio.ByteBuffer;

/**
 * Interface that must implement a receiver class when using a threaded // * USB
 * connection. This interface will allow a given class to receive the bytes sent
 * from the USB device associated to a connection.
 * 
 * @author sunny
 *
 */
public interface USBDataReceiver {

	/**
	 * The method called when a specific device receives data from a connected
	 * USB device. The class implementing this method will be notified of
	 * reception by this method.
	 * 
	 * @param buffer
	 *            The buffer in which byes received are stored.
	 * @param connecter
	 *            The connection information. The data were received from this
	 *            connection.
	 */
	public void receive(ByteBuffer buffer, USBConnecterInfo connecter);
}
