package usbUtil;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.usb4java.BufferUtils;
import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;

/**
 * <p>
 * Represents Threaded USB connection. This class is used to form a base to
 * single and multiple USB connections. It allow to a given application to
 * always check for data reception, with a given timeout between every check. It
 * is important that classes extending this class implement
 * {@link #checkRecetion()} carefully, because it is where data reception should
 * be handled. The received data are then sent to {@link #receiver} via
 * {@link USBDataReceiver#receive(ByteBuffer, USBConnecterInfo)}.
 * </p>
 * 
 * <p>
 * Note that the receiver of this class must be set via
 * {@link #setReceiver(USBDataReceiver)} before {@link #connect()} is called, or
 * an exception will be thrown.
 * </p>
 * 
 * <p>
 * Also note that this class uses LibUsb's methods and classes, which means that
 * the library will already be loaded if required by the implementing classes
 * requires it to work.
 * </p>
 * 
 * @author Sunny
 *
 */
public abstract class USBConnectionBase {

	/**
	 * The default wait timeout between every check. This time is set in
	 * milliseconds.
	 */
	public static final long DEFAULT_NEXT_CHECK_TIMEOUT = 300L;

	/**
	 * The thread in which {@link #checkRecetion()} is called. Excluding
	 * execution time of the method, {@link #checkRecetion()} should be called
	 * every {@link #nextCheckTimeout}'s value in milliseconds
	 */
	private Thread thread;

	/**
	 * Private varible indicating the thread to stop running.
	 */
	private boolean stopped;

	/**
	 * The data receiver of the class. Every data received during execution of
	 * the thread will be sent to this class via
	 * {@link USBDataReceiver#receive(ByteBuffer, USBConnecterInfo)}
	 */
	private USBDataReceiver receiver;

	/**
	 * The LibUsb's context for runnng the app.
	 */
	private Context context;

	/**
	 * The elapsed time between every call of {@link #checkRecetion()}. Small
	 * values may occur in program's speed decrease.
	 */
	private long nextCheckTimeout = DEFAULT_NEXT_CHECK_TIMEOUT;

	/**
	 * Construct a USB connection to the given USB device using the given byte
	 * length for data reception size. The flags are used to request information
	 * from the given device, and the idVendor is the parameter used to identify
	 * the given device between the deviceList. Note that if many devices have
	 * the same isVendor, only the first one will be used.
	 */
	public USBConnectionBase() {

		initLib();

	}

	/**
	 * Sets the data receiver of the USB connection. The data received after
	 * {@link #connect()} is called will be sent to this object. This method
	 * must imperatively be called for attempting to connect, or an exception
	 * will be thrown.
	 * 
	 * @param receiver
	 *            The receiver of the USB connection. Cannot be null.
	 */
	public void setDataReceiver(USBDataReceiver receiver) {
		if (receiver == null) {
			throw new NullPointerException("Cannot set a null receiver");
		}
		this.receiver = receiver;
	}

	/**
	 * Opens connection with the USB connection. Data can now be sent and
	 * received between the 2 sides of the hardware.
	 * 
	 * <p>
	 * After this method is called, the background thread searching for data
	 * reception is launched, and {@link #checkRecetion()} will be called every
	 * {@link #nextCheckTimeout}'s value in milliseconds.
	 * </p>
	 * <p>
	 * This background thread will be running until {@link #close()} is called,
	 * or the application stops working. (Deamon thread)
	 * </p>
	 * <p>
	 * Note that exceptions will be thrown if the connection is already open or
	 * if the receiver is null. It might also occur if a given USB connection is
	 * impossible to open.
	 * </p>
	 * 
	 * 
	 */
	public void connect() {

		if (receiver == null) {
			throw new NullPointerException("No data receiver as been set");
		}
		if (thread != null && thread.isAlive()) {
			throw new IllegalStateException("The connection is already open");
		}
		stopped = false;

		thread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					checkRecetion();

					try {

						Thread.sleep(nextCheckTimeout);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		thread.start();
	}

	/**
	 * Abstract method that must be implemented by extending classes. This
	 * method is called every frame of the thread. Data reception and reception
	 * handling must be executed in this part using LibUsb's methods.
	 * 
	 * @see <a href=
	 *      "Exemples LibUsb" > https://github.com/usb4java/usb4java-examples/
	 *      blob/master/src/main /java/
	 *      org/usb4java/examples/SyncBulkTransfer.java</a>
	 * 
	 */
	public abstract void checkRecetion();

	/**
	 * Closes USB connection with every USB devices attached to this connection.
	 * The connection may not be restarted again using {@link #connect()}. The
	 * LibUsb library will be exited in the current context.
	 */
	public void close() {
		stopped = true;
		handleClose();
		LibUsb.exit(context);
	}

	/**
	 * Notifies extending classes to handle {@link #close()} call. This method
	 * should simply close every single {@link USBConnecterInfo} via
	 * {@link USBConnecterInfo.close()}
	 */
	public abstract void handleClose();

	/**
	 * Initializes the library an throws an exception if the data are not
	 * reachable.
	 */
	private void initLib() {
		context = new Context();
		int result = LibUsb.init(context);

		if (result != LibUsb.SUCCESS) {
			throw new IllegalAccessError("Cannot access the USB devices");
		}
	}

	/**
	 * Returns a DeviceList created by LibUsb that contains all accessible USB
	 * devices for the program. Even though they are accessible through this
	 * list doesn't mean they will necessarily be usable for a connection.
	 * Exception may occur later in the process.
	 * 
	 * @return A device list of all the available devices on the OS.
	 */
	public DeviceList getDeviceList() {
		DeviceList devices = new DeviceList();

		int result = LibUsb.getDeviceList(context, devices);

		if (result < 0) {
			throw new IllegalAccessError("Cannot access the USB devices");
		}

		return devices;
	}

	/**
	 * Returns the descriptor of all the available devices contained in the
	 * device list returned by {@link #getDeviceList()}.
	 * 
	 * @return A list containing all the descriptors of the devices found on the
	 *         OS
	 */
	public List<DeviceDescriptor> getAvailableDevicesDescritor() {

		// Creates the context.
		DeviceList devices = getDeviceList();

		// Looks for device descriptor
		List<DeviceDescriptor> answer = new ArrayList<>();
		try {

			for (Device device : devices) {
				DeviceDescriptor descriptor = new DeviceDescriptor();
				int result = LibUsb.getDeviceDescriptor(device, descriptor);
				if (result == LibUsb.SUCCESS) {
					answer.add(descriptor);
				}
			}
		} finally {
			LibUsb.freeDeviceList(devices, true);
		}
		return answer;
	}

	/**
	 * Returns the dump of all the available devices contained in the device
	 * list returned by {@link #getDeviceList()}.
	 * 
	 * @return A list containing all the descriptors of the devices found on the
	 *         OS
	 */

	public List<String> getAvailableDevicesDump() {
		// Creates the context.

		DeviceList devices = getDeviceList();

		// Looks for device descriptor
		List<String> answer = new ArrayList<>();
		try {

			for (Device device : devices) {

				DeviceDescriptor descriptor = new DeviceDescriptor();
				int result = LibUsb.getDeviceDescriptor(device, descriptor);

				if (result == LibUsb.SUCCESS) {
					answer.add(descriptor.dump());
				}
			}
		} finally {
			LibUsb.freeDeviceList(devices, true);
		}
		return answer;
	}

	/**
	 * Returns the receiver object of this class. This receiver is the one that
	 * will be notified of data reception via
	 * {@link USBDataReceiver#receive(ByteBuffer, USBConnecterInfo)}.
	 * 
	 * @return The data receiver of the application.
	 */
	public USBDataReceiver getReceiver() {
		return receiver;
	}

	/**
	 * Returns the timeout used between every {@link #checkRecetion()}. This
	 * timeout does not include execution time of the method, it is simply a
	 * sleep
	 * 
	 * @return
	 */
	public long getNextCheckTimeout() {
		return nextCheckTimeout;
	}

	/**
	 * Sets the timeout between the next call of {@link #checkRecetion()}. This
	 * timeout value does not include execution time of this method.
	 * 
	 * @param retryTimeout
	 */
	public void setNextCheck(long retryTimeout) {
		if (retryTimeout < 0) {
			throw new IllegalArgumentException("Timeout value must be >= 0");
		}
		this.nextCheckTimeout = retryTimeout;
	}
}
