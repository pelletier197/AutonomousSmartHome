package usbUtil;

import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceHandle;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;

/**
 * USB connection information container class. With the current implementation,
 * this class only contain data to allow a bulk transfer, but this may be
 * changed in future versions.
 * 
 * <p>
 * When creating the object via any of the constructor, the class will attemt to
 * load the device handle and will claim the interface requested. Therefore,
 * make sure to initialize the LibUsb library.
 * </p>
 * 
 * <p>
 * Finally, to close connection, make sure to call {@link #close()} when the
 * connection with the device must be closed. This will not deinitialize the
 * library, but will close device handle and release the interface previously
 * claimed.
 * <p>
 * 
 * 
 * @author sunny
 *
 */
public class USBConnecterInfo {

	/**
	 * The usbDevice to which connection is made.
	 */
	private Device USBDevice;

	/**
	 * The devicd handle to communicate with the device.
	 */
	private DeviceHandle handle;

	/**
	 * The interface number to claim.
	 */
	private int interfaceNumber;

	/**
	 * The size of the data to be sent/receive
	 */
	private int size;

	/**
	 * Connection endpoint address. Note that bits 0-3 are for endpoint number,
	 * 4-6 are reserved and 7 is for input or output (1 for input, for output)
	 */
	private byte endPoint;

	/**
	 * Memory space allowed for user data. Where an object is attached to its
	 * connection.
	 */
	private Object UserData;

	/**
	 * Boolean telling to reatach kernel driver at the close.
	 */
	private boolean kernelDriverActivated;

	/**
	 * Creates connection information to the device described by the idVendor
	 * and idProduct.
	 * 
	 * <p>
	 * This constructor will claim the interface associated to the interface
	 * number, and will open connection to the device. Connection will be
	 * created to the given endpoint, and the amount of data to read will be set
	 * by the byteLength parameter.
	 * </p>
	 * 
	 * @param idVendor
	 *            The vendor ID attributed b USB.org
	 * @param idProduct
	 *            The product ID attributed by the manufacturer.
	 * @param interfaceNumber
	 *            The interface to claim.
	 * @param byteLength
	 *            The size of the data array that will be sent/received from the
	 *            device.
	 * @param endPoint
	 *            The endpoint value of the device. Note that only bits 0-3 are
	 *            requested for this construction. Other bits are handled for
	 *            transfer direction
	 * @param targetDevices
	 *            The devices that will be iterated to find the given device.
	 */
	public USBConnecterInfo(short idVendor, short idProduct, int interfaceNumber, int byteLength, byte endPoint,
			DeviceList targetDevices) {

		this.interfaceNumber = interfaceNumber;
		this.size = byteLength;
		this.endPoint = endPoint;

		for (Device device : targetDevices)

		{
			DeviceDescriptor descriptor = new DeviceDescriptor();
			int found = LibUsb.getDeviceDescriptor(device, descriptor);

			if (found == LibUsb.SUCCESS && descriptor.idVendor() == idVendor && descriptor.idProduct() == idProduct) {
				this.USBDevice = device;
				break;
			}
		}
		initDeviceHandleAndInterface();
	}

	/**
	 * Creates connection information to the device described by specified
	 * device name. This name must be contained in the device descriptor.
	 * 
	 * <p>
	 * This constructor will claim the interface associated to the interface
	 * number, and will open connection to the device. Connection will be
	 * created to the given endpoint, and the amount of data to read will be set
	 * by the byteLength parameter.
	 * </p>
	 * 
	 * @param deviceName
	 *            The string that must be contained in the device descriptor.
	 * @param interfaceNumber
	 *            The interface to claim.
	 * @param byteLength
	 *            The size of the data array that will be sent/received from the
	 *            device.
	 * @param endPoint
	 *            The endpoint value of the device. Note that bits 0-3 are for
	 *            endpoint number, 4-6 are reserved and 7 is for input or output
	 *            (1 for input, for output)
	 * @param targetDevices
	 *            The devices that will be iterated to find the given device.
	 */
	public USBConnecterInfo(String deviceName, int interfaceNumber, int byteLength, byte endPoint,
			DeviceList targetDevices) {

		this.interfaceNumber = interfaceNumber;
		this.size = byteLength;
		this.endPoint = endPoint;

		for (Device device : targetDevices) {

			DeviceDescriptor descriptor = new DeviceDescriptor();
			int found = LibUsb.getDeviceDescriptor(device, descriptor);

			if (found == LibUsb.SUCCESS && descriptor.dump().contains(deviceName)) {
				this.USBDevice = device;
				break;
			}
		}
		initDeviceHandleAndInterface();
	}

	/**
	 * Common method to all constructors. Claim the interface requested and open
	 * connection with the device handle. It will also detach the kernel driver
	 * if the OS supports it.
	 */
	private void initDeviceHandleAndInterface() {

		if (USBDevice == null) {
			throw new NullPointerException("The device was not found");
		}

		handle = new DeviceHandle();

		// Opens a connection with the device
		int open = LibUsb.open(USBDevice, handle);

		if (open != LibUsb.SUCCESS) {
			throw new LibUsbException("Cannot open the connection", open);
		}

		open = LibUsb.claimInterface(handle, interfaceNumber);

		if (open != LibUsb.SUCCESS) {
			throw new LibUsbException("Cannot reach the interface requested", open);
		}

		kernelDriverActivated = (LibUsb.kernelDriverActive(handle, interfaceNumber) == 1);
		if (kernelDriverActivated) {
			open = LibUsb.detachKernelDriver(handle, interfaceNumber);
			if (open != LibUsb.SUCCESS)
				throw new LibUsbException("Unable to detach kernel driver", open);
		}
	}

	/**
	 * 
	 * @return The interface number claimed by this connection.
	 */
	public int getInterfaceNumber() {
		return interfaceNumber;
	}

	/**
	 * The size of the data that will be received/sent.
	 * 
	 * @return The size of the data that will be received.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * The endpoint address of this connection. Note that bits 0-3 are for
	 * endpoint number, 4-6 are reserved and 7 is for input or output (1 for
	 * input, for output)
	 * 
	 * // * @return The endpoint address of the connection.C
	 */
	public byte getEndPoint() {
		return endPoint;
	}

	/**
	 * Returns the LibUsb device associated to the connection.
	 * 
	 * @return The LibUsb device associated to the connection.
	 */
	protected Device getUSBDevice() {
		return USBDevice;
	}

	/**
	 * Returns the LibUsb device handle associated to the connection.
	 * 
	 * @return The LibUsb device handle associated to the connection.
	 */
	protected DeviceHandle getHandle() {
		return handle;
	}

	/**
	 * Returns the user data allowed for memory storage in the connection
	 * information.
	 * 
	 * @return The user data.
	 */
	public Object getUserData() {
		return UserData;
	}

	/**
	 * Sets the user data associated to the connection. Note that the classes
	 * from this package do not use this user data.
	 * 
	 * @param userData
	 *            The user data to set.
	 */
	public void setUserData(Object userData) {
		UserData = userData;
	}

	/**
	 * Releases the connection with the previously requested interface and
	 * closes the device handle. After this call, this object will no longer
	 * have any utilities.
	 */
	public void close() {
		
		int result = 0;
		
		// Re attach kernel drive
		if (kernelDriverActivated) {
			result = LibUsb.attachKernelDriver(handle, interfaceNumber);

			if (result != LibUsb.SUCCESS) {
				throw new LibUsbException("Cannot reatach kernel driver.", result);

			}
		}

		result = LibUsb.releaseInterface(handle, interfaceNumber);

		if (result != LibUsb.SUCCESS) {
			throw new LibUsbException("Cannot release the interface requested.", result);

		}

		LibUsb.close(handle);

		System.out.println("Connection closed successfuly");
	}

}
