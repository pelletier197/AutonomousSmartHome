package utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import javax.imageio.ImageIO;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;

import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;
import src.main.java.com.mpatric.mp3agic.InvalidDataException;
import src.main.java.com.mpatric.mp3agic.UnsupportedTagException;

public class Mp3Utils {

	/**
	 * 
	 * @param repository
	 * @return
	 */
	public static List<Mp3SongInfo> getSongInfoFromVirginRepository(String repository) {

		return getMP3FromRepository(new File(repository));
	}

	/**
	 * 
	 * @param repository
	 * @param songs
	 * @return
	 * @throws UnsupportedTagException
	 * @throws InvalidDataException
	 * @throws IOException
	 */
	public static List<Mp3SongInfo> getSongInfoNotContainedFromRepository(File repository,
			Collection<Mp3SongInfo> songs) {

		return (List<Mp3SongInfo>) CollectionUtils.subtract(getMP3FromRepository(repository), songs);

	}

	/**
	 * 
	 * @param file
	 * @param size
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static List<Mp3SongInfo> readSongInfoFromFile(File file) throws IOException, ClassNotFoundException {

		return (List<Mp3SongInfo>) readObject(file);
	}

	/**
	 * 
	 * @param file
	 * @param songs
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void writeSongsInfoToFile(File file, Collection<Mp3SongInfo> songs)
			throws IOException, ClassNotFoundException {

		writeObject(file, songs);
	}

	/**
	 * 
	 * @param f
	 * @return
	 */
	public static List<Mp3SongInfo> getMP3FromRepository(File f) {

		List<Mp3SongInfo> mp3List = new ArrayList<Mp3SongInfo>();

		// Check whether the file is a directory or not. If it is, all the
		// files
		// from this directory are watched.
		if (f.isDirectory()) {

			for (File file : f.listFiles()) {
				if (file.isDirectory()) {

					mp3List.addAll(getMP3FromRepository(file));

				} else {

					if (validFileExtention(file.getName(), ".mp3")) {
						try {
							mp3List.add(new Mp3SongInfo(file.getPath()));
						} catch (UnsupportedTagException | InvalidDataException | IOException e) {
							System.out.println(file.getName());
						}
					}

				}
			}
		} else {
			if (validFileExtention(f.getPath(), ".mp3")) {
				try {
					mp3List.add(new Mp3SongInfo(f.getPath()));
				} catch (UnsupportedTagException | InvalidDataException | IOException e) {
					System.out.println(f.getName());
				}
			}
		}

		return mp3List;
	}

	/**
	 * 
	 * @param f
	 * @param extention
	 * @return
	 */
	private static List<File> getFilesFromRepository(File f, String extention) {
		// Creates the empty list
		List<File> list = new ArrayList<File>();

		// Check whether the file is a directory or not. If it is, all the files
		// from this directory are watched.
		if (f.isDirectory()) {

			for (File file : f.listFiles()) {
				if (file.isDirectory()) {

					list.addAll(getFilesFromRepository(file, extention));

				} else {

					if (validFileExtention(file.getName(), extention)) {

						list.add(file);
					}

				}
			}
		} else {
			if (validFileExtention(f.getPath(), ".mp3")) {
				list.add(f);
			}
		}
		return list;
	}

	/**
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] readByteArrayImageFromFile(File file) throws IOException {

		// Buffers from files
		BufferedImage image = ImageIO.read(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// writes of the byteArrayOutputStream
		ImageIO.write(image, getFileExtentionNoDot(file.getPath()), baos);

		// Convert image to byteArray
		baos.flush();
		byte[] retour = baos.toByteArray();
		baos.close();

		return retour;

	}

	/**
	 * 
	 * @param filePath
	 * @return
	 */
	private static String getFileExtentionNoDot(String filePath) {
		return filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length() - 1);
	}

	/**
	 * 
	 * @param fileName
	 * @param extention
	 * @return
	 */
	private static boolean validFileExtention(String fileName, String extention) {

		return fileName.endsWith(extention);

	}

	@SuppressWarnings("unchecked")
	public static List<Playlist> readPlaylists(File file) throws IOException, ClassNotFoundException {

		return (List<Playlist>) readObject(file);
	}

	/**
	 * 
	 * @param file
	 * @param playlists
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void writePlaylists(File file, Collection<Playlist> playlists) throws IOException {

		writeObject(file, playlists);
	}

	private static void writeObject(File file, Object obj) throws IOException {
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fileStream = new FileOutputStream(file);
		ObjectOutputStream stream = new ObjectOutputStream(fileStream);

		stream.writeObject(obj);

		stream.flush();
		stream.close();
	}

	private static Object readObject(File file) throws IOException, ClassNotFoundException {
		Object obj = null;
		FileInputStream fileStream = new FileInputStream(file);
		ObjectInputStream stream = null;
		try {
			stream = new ObjectInputStream(fileStream);
			obj = stream.readObject();
			stream.close();

		} catch (EOFException e) {

		}

		return null;
	}

	public static String generateSongName(){
		return String.valueOf(System.currentTimeMillis()).concat(".mp3");
	}

}
