package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;

public class FileUtils {

	private static String workingDirectory;

	public static String getAppDataDirectory() {

		if (workingDirectory == null) {
			// here, we assign the name of the OS, according to Java, to a
			// variable...
			final String OS = (System.getProperty("os.name")).toUpperCase();
			// to determine what the workingDirectory is.
			// if it is some version of Windows
			if (OS.contains("WIN")) {
				// it is simply the location of the "AppData" folder
				workingDirectory = System.getenv("AppData");
			}
			// Otherwise, we assume Linux or Mac
			else {
				// in either case, we would start in the user's home directory
				workingDirectory = System.getProperty("user.home");
				// if we are on a Mac, we are not done, we look for "Application
				// Support"
				workingDirectory += "/Library/Application Support";
			}
			workingDirectory += "/HouseController";

			File f = new File(workingDirectory);
			if (!f.exists()) {
				System.out.println(f.mkdirs() + "AAAAAAAAAAAAAAH");
			}

		}
		return workingDirectory;
	}

	public static String isolateFileBaseName(String path) {
		return FilenameUtils.getBaseName(path);
	}

	public static void writeBytesToFile(byte[] bytes, String file) throws IOException {

		File f = new File(file);

		if (!f.exists()) {
			f.createNewFile();
		}
		FileOutputStream stream = new FileOutputStream(f);
		stream.write(bytes);
		stream.flush();
		stream.close();
	}
}
