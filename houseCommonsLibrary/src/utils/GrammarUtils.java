package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import houseDataCenter.mp3Player.Mp3SongInfo;

public class GrammarUtils {
	/**
	 * 
	 * @param writer
	 * @param grammarName
	 * @throws IOException
	 */
	private static void writeJSGFHeader(BufferedWriter writer, String grammarName) throws IOException {

		writer.write("#JSGF V1.0;");
		writer.newLine();
		writer.write("grammar " + grammarName + ";");
		writer.newLine();

	}

	/**
	 * 
	 * @param songs
	 * @param path
	 * @return
	 */
	public static boolean writeMusicGrammar(Collection<Mp3SongInfo> songs, String path) {

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));

			// Writes standard JSGF header
			writeJSGFHeader(writer, "musics");
			writer.write("public<musicName> = ");

			final int indexToReach = songs.size() - 1;
			int currentIndex = 0;

			writer.write('(');
			if (songs.isEmpty()) {
				// Make sure the grammar is not empty, but not pronounceable
				// either
				writer.write("1232342384234uvfdfgdf");
			} else {

				for (Mp3SongInfo song : songs) {
					if (currentIndex != indexToReach) {
						writer.write(song.getTitle() + " | ");
					} else {
						writer.write(song.getTitle());
					}

					currentIndex++;
				}
			}
			writer.write(')');
			writer.write(';');
			writer.flush();
			writer.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
