package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;

import org.apache.commons.compress.utils.IOUtils;

public class Compresser {

	/**
	 * Compress the given byte array using the GZIP compression algorithm.
	 * 
	 * <p>
	 * To uncompress, use {@link #uncompressGZIP(byte[])} <\p>
	 * 
	 * @param content
	 *            The data to compress
	 * @return The compressed data
	 */
	public static byte[] compressGZIP(byte[] content) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
			gzipOutputStream.write(content);
			gzipOutputStream.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.printf("Compressiono %f\n", (1.0f * content.length / byteArrayOutputStream.size()));
		return byteArrayOutputStream.toByteArray();
	}

	/**
	 * Uncompress the given byte array that has been compressed using the GZIP
	 * compression algorithm.
	 * 
	 * @param content
	 *            The data to uncompress
	 * @return The uncompressed data
	 */
	public static byte[] uncompressGZIP(byte[] contentBytes) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(contentBytes)), out);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return out.toByteArray();
	}

	/**
	 * Uses the fastest compression algorithm to compress the given data.
	 * 
	 * @param data
	 *            The data to compress
	 * @return The compressed data
	 */
	public static byte[] compressFast(byte[] data) {
		return compress(data, Deflater.BEST_SPEED);

	}

	/**
	 * Uses the highest compression ratio algorithm to compress the data into a
	 * smaller byte array.
	 * 
	 * @param data
	 *            The data to compress
	 * @return The compressed data
	 */
	public static byte[] compressMaximum(byte[] data) {
		return compress(data, Deflater.BEST_COMPRESSION);

	}

	/**
	 * Compresses the data using the given data compression flag.
	 * 
	 * @param data
	 *            The data to compress
	 * @param flag
	 *            The flag of compression type
	 * @return The compressed data
	 */
	private static byte[] compress(byte[] data, int flag) {
		if (data != null) {
			Deflater def = new Deflater(flag);
			def.setInput(data);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
			def.finish();
			byte[] buffer = new byte[1024];

			while (!def.finished()) {

				int count = def.deflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			byte[] output = outputStream.toByteArray();

			return output;
		}
		return null;
	}

	/**
	 * Decompresses the given byte array.
	 * 
	 * @param data
	 * @return
	 */
	public static byte[] uncompress(byte[] data) {
		if (data != null) {
			Inflater inflater = new Inflater();
			inflater.setInput(data);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
			byte[] buffer = new byte[1024];
			while (!inflater.finished()) {
				int count = 0;
				try {
					count = inflater.inflate(buffer);
				} catch (DataFormatException e) {

				}
				outputStream.write(buffer, 0, count);
			}
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			byte[] output = outputStream.toByteArray();
			return output;
		}
		return null;
	}
}
