package utils;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.FilenameUtils;

import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;

public class Formatters {

	private static NumberFormat formatter = NumberFormat.getInstance();

	/**
	 * @param timeInSeconds
	 * @return
	 */
	public static String formatTime(long timeInSeconds) {

		int h = (int) (timeInSeconds / 3600);
		int m = (int) ((timeInSeconds % 3600) / 60);
		int s = (int) (timeInSeconds % 60);

		StringBuilder r = new StringBuilder();

		if (h > 0) {
			if (h < 10) {
				r.append("0").append(h).append(":");
			} else {
				r.append(h).append(":");
			}
		}
		if (m > 0) {
			if (m < 10) {
				if (h > 0) {
					r.append("0").append(m).append(":");
				} else {
					r.append(m).append(":");
				}
			} else {
				r.append(m).append(":");
			}
		} else {
			r.append("0:");
		}
		if (s >= 0) {
			if (s < 10) {
				r.append("0").append(s);
			} else {
				r.append(s);
			}
		}
		return r.toString();
	}

	public static String getDaysFormatted(List<AlarmDay> days) {
		StringBuilder builder = new StringBuilder();
		days.sort(AlarmDay.getDayComparator());

		if (days.contains(AlarmDay.ALL)) {
			builder.append(BundleWrapper.getString(BundleKey.all_days));
		} else {

			for (int i = 0; i < days.size(); i++) {
				switch (days.get(i)) {
				case MONDAY:
					builder.append(BundleWrapper.getString(BundleKey.monday));
					break;
				case TUESDAY:
					builder.append(BundleWrapper.getString(BundleKey.tuesday));
					break;
				case WEDNESDAY:
					builder.append(BundleWrapper.getString(BundleKey.wednesday));
					break;
				case THURSDAY:
					builder.append(BundleWrapper.getString(BundleKey.thursday));
					break;
				case FRIDAY:
					builder.append(BundleWrapper.getString(BundleKey.friday));
					break;
				case SATURDAY:
					builder.append(BundleWrapper.getString(BundleKey.saturday));
					break;
				case SUNDAY:
					builder.append(BundleWrapper.getString(BundleKey.sunday));
					break;
				default:
					break;
				}
				if (i != days.size() - 1) {
					builder.append(", ");
				}

			}
		}
		return builder.toString();
	}

	/**
	 * 
	 * @param dateMillis
	 * @return
	 */
	public static String millisToFormattedDateDay(long dateMillis) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(dateMillis);

		StringBuilder builder = new StringBuilder();

		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);

		completeZero(year, builder);
		builder.append("/");
		completeZero(month, builder);
		builder.append("/");
		completeZero(day, builder);

		return builder.toString();
	}

	/**
	 * 
	 * @return
	 */
	public static String getFormatedDate() {

		final Calendar calendar = Calendar.getInstance();

		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);

		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH) + 1;
		int year = calendar.get(Calendar.YEAR);

		StringBuilder builder = new StringBuilder();

		completeZero(year, builder);
		builder.append("/");
		completeZero(month, builder);
		builder.append("/");
		completeZero(day, builder);
		builder.append(" - ");

		completeZero(hour, builder);
		builder.append(":");
		completeZero(min, builder);
		builder.append(":");
		completeZero(sec, builder);

		return builder.toString();

	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static long localDateToMillis(LocalDate date) {
		if (date != null) {
			Calendar calendar = Calendar.getInstance();

			calendar.set(Calendar.YEAR, date.getYear());
			calendar.set(Calendar.MONTH, date.getMonthValue());
			calendar.set(Calendar.DAY_OF_MONTH, date.getDayOfMonth());

			System.out.println("current time :  " + System.currentTimeMillis() + ", local date time : "
					+ calendar.getTimeInMillis());

			return calendar.getTimeInMillis();
		} else {
			return 0;
		}
	}

	/**
	 * Returns the formatted time of a certain progress in a specified and
	 * finished amount of time.
	 * 
	 * For example, {@code formatTime(0.5, 2)} would return 00:01 which is 1
	 * second.
	 * 
	 * @param progress
	 *            The progress in the proposed amount of time, between 0 and 1
	 *            include
	 * @param lengthSec
	 *            The amount of time in which progress is occurring.
	 * @return A formatted time
	 */
	public static String formatTime(double progress, long lengthSec) {
		if (progress < 0) {
			return "0";
		} else if (progress > 1) {
			return String.valueOf(lengthSec);
		}

		return formatTime((long) (progress * lengthSec));

	}

	public static String formatHourMinute(int hour, int minute) {
		String h = hour < 10 ? "0" + hour : String.valueOf(hour);
		String m = minute < 10 ? "0" + minute : String.valueOf(minute);

		StringBuilder builder = new StringBuilder();
		builder.append(h).append(':').append(m);

		return builder.toString();
	}

	/**
	 * 
	 * @param number
	 * @param minFracDigit
	 * @param maxFracDigit
	 * @return
	 */
	public static String doubleFormat(double number, int minFracDigit, int maxFracDigit) {

		formatter.setMinimumFractionDigits(minFracDigit);
		formatter.setMaximumFractionDigits(maxFracDigit);

		return formatter.format(number);
	}

	/**
	 * Formats the given number to the requested fraction digits. Calling this
	 * method has the same effect as calling
	 * {@code doubleFormat(double, fracDigit,fracDigit)}
	 * 
	 * @param number
	 * @param fracDigit
	 * @return
	 */
	public static String doubleFormat(double number, int fracDigit) {

		return doubleFormat(number, fracDigit, fracDigit);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static String formatAsInteger(int value) {
		return String.valueOf(value);
	}

	/**
	 * 
	 * @param value
	 * @param builder
	 */
	private static void completeZero(int value, StringBuilder builder) {

		if (value < 10) {
			builder.append("0").append(value);
		} else {
			builder.append(value);
		}

	}

}
