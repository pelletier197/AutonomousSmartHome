package utils;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.opencv_core.Mat;

import houseDataCenter.alarm.Alarm;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.residents.ResidentBuilder;
import tests.houseDataCenter.house.HouseParameter;
import utils.Formatters;
import utils.OpenCVUtils;

public class HouseUtils {

	/**
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static Object readObject(String path) throws IOException, EOFException, ClassNotFoundException {

		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(new File(path)));

		Object read = stream.readObject();

		System.out.println(read.getClass());

		stream.close();

		return read;

	}

	/**
	 * 
	 * @param obj
	 * @param path
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void writeObject(Object obj, String path) throws FileNotFoundException, IOException {
		File f = new File(path);
		if (!f.exists()) {

			File parent = f.getParentFile();

			if (parent != null && !parent.exists()) {
				parent.mkdirs();
			}

			f.createNewFile();
		}
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(f));

		stream.writeObject(obj);

		stream.flush();
		stream.close();
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static HouseParameter readHouseParams(String path) {
		try {
			return (HouseParameter) readObject(path);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {

		}
		return new HouseParameter();
	}

	/**
	 * 
	 * @param param
	 * @param path
	 * @return
	 */
	public static boolean writeHouseParams(HouseParameter param, String path) {
		try {
			writeObject(param, path);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 */
	public static byte[] getDeviceUUID() {
		try {
			byte[] result = NetworkInterface.getByInetAddress(InetAddress.getLocalHost()).getHardwareAddress();
			System.out.println(Arrays.toString(result) + "UUID generated");
			return result;

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param roomDataPath
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Collection<Room> readRoomData(String roomDataPath) {

		try {
			return (Collection<Room>) readObject(roomDataPath);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {

		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<Room>();
	}

	/**
	 * 
	 * @param alarmsDataPath
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Collection<Alarm> readAlarms(String alarmsDataPath) {
		try {
			return (Collection<Alarm>) readObject(alarmsDataPath);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {

		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<Alarm>();
	}

	/**
	 * 
	 * @param alarms
	 * @param path
	 * @return
	 */
	public static boolean writeAlarms(Collection<Alarm> alarms, String path) {

		try {
			writeObject(alarms, path);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param rooms
	 * @param path
	 * @return
	 */
	public static boolean writeRoomData(Collection<Room> rooms, String path) {

		try {
			System.out.println("Saving rooms : " + rooms);
			writeObject(rooms, path);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param residents
	 * @param path
	 * @return
	 */
	public static boolean writeResidentData(Collection<Resident> residents, String path) {

		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(new File(path)));

			for (Resident r : residents) {
				writeResident(out, r);
			}
			out.flush();
			out.close();
			System.out.println("Write SUCCESSFULL");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param stream
	 * @param r
	 * @throws IOException
	 */
	private static void writeResident(ObjectOutputStream stream, Resident r) throws IOException {

		stream.writeObject(r.getName());
		stream.writeLong(r.getBornDate());
		stream.writeObject(r.getEmail());
		stream.writeObject(r.getPIN());

	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static List<Resident> readResidentData(String path) {

		List<Resident> deserialized = new ArrayList<>();
		ObjectInputStream in = null;

		try {
			ResidentBuilder builder = new ResidentBuilder();
			System.out.println(path);
			in = new ObjectInputStream(new FileInputStream(path));

			while (true) {

				builder.setName((String) in.readObject());
				builder.setBornDate(in.readLong());
				builder.setEmail((String) in.readObject());

				String pin = (String) in.readObject();
				builder.setPIN(pin);
				builder.setPINConfirm(pin);

				deserialized.add(builder.build());
			}

		} catch (EOFException e) {
			try {
				in.close();
			} catch (Exception e1) {
			}

			System.out.println("READ SUCCESSFULL : " + deserialized.size() + " residents found");

			return deserialized;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public static String getFormatedDate() {
		return Formatters.getFormatedDate();
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public static long localDateToMillis(LocalDate date) {
		return Formatters.localDateToMillis(date);
	}

	/**
	 * 
	 * @param dateMillis
	 * @return
	 */
	public static String millisToFormattedDateDay(long dateMillis) {

		return Formatters.millisToFormattedDateDay(dateMillis);
	}

	public static String[] isolateIDs(List<LightData> lights) {
		String[] res = new String[lights.size()];

		for (int i = 0; i < res.length; i++) {
			res[i] = lights.get(i).getId();
		}
		return res;
	}

	public static void writeToFile(byte[] data, File f) throws IOException {
		FileUtils.writeByteArrayToFile(f, data);
	}

}
