package utils;

import static org.bytedeco.javacpp.opencv_imgcodecs.imencode;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.opencv_core.Mat;

public class OpenCVUtils {

	public static byte[] toJPGImage(Mat mat) {

		// Converts mat image to jpg format image as byte array
		BytePointer buffer = new BytePointer();

		imencode(".jpg", mat, buffer);

		byte[] img = new byte[(int) (buffer.limit() - buffer.position())];

		buffer.get(img);

		return img;

	}

	public static byte[] toByteArray(Mat mat) {

		// Converts mat image to jpg format image as byte array
		BytePointer buffer = new BytePointer(mat);
		System.out.println("test1");
		byte[] img = new byte[(int) mat.rows()*mat.cols()];
		buffer.get(img);
		System.out.println("test2");

		
		System.out.println(img.length + " length");

		return img;

	}

}
