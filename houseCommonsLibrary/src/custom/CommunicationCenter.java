package custom;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.UnknownHostException;

import server.client.ClientComManager;
import server.communication.Request;
import server.communication.TransferData;
import server.transport.DataReceptionHandler;

/**
 * This class represents a client communication center to a given server
 * connected on a default port, {@value port}. From this port, both application
 * side can send and receive data that must be handled in the other side of the
 * application.
 * 
 * The send a specific type of data, use {@link #send(int)},
 * {@link #send(int, Serializable)}, {@link #send(Request)}, where the
 * transferable data is a simple data to which a flag is associated and a given
 * object that can be null. This allow easy communication between both
 * application side.
 * 
 * The receiver of data from the server will be given at construction by the
 * {@link DataReceptionHandler}
 * 
 * @author sunny
 *
 */
public class CommunicationCenter {

	/**
	 * The client manager allowing object transmission.
	 */
	private ClientComManager com;

	/**
	 * The default port of the application
	 */
	public static final int PORT = 10246;

	public static final String CONNECTION_CONFIG = CommunicationCenter.class.getResource("/config/connection.config")
			.getFile();;

	/**
	 * Constructs a communication center that will connect to the default port of
	 * the application. The communication center will retrieve reception data to the
	 * listener in parameters.
	 * 
	 * @param listener
	 *            The reception of data handler
	 */
	public CommunicationCenter() {

		this.com = new ClientComManager(PORT);

	}

	public void addDataReceiver(Object dataReceiver) {
		com.addDataReceiver(dataReceiver);
	}

	public ClientComManager getCommunicationManager() {
		return com;
	}

	public void openConnection() {
		String configs = readConfig();

		if (configs == null) {
			try {
				com.openConnection();
				writeConfig();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			System.out.println("establishing new connection");

		} else {
			// Allow enormous time. If the connection fails, either connection
			// ip changed, or server is offline.

			try {
				com.openConnection(configs, 10_000);
			} catch (IOException e) {
				e.printStackTrace();
			}

			System.out.println("Connecting to known server : " + configs);

			// Look up for a new connection
			if (!com.isConnected()) {
				try {
					com.openConnection();
					writeConfig();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private String readConfig() {

		ObjectInputStream in = null;
		try {

			in = new ObjectInputStream(new FileInputStream(CONNECTION_CONFIG));
			String ip = (String) in.readObject();

			in.close();

			return ip;
		} catch (EOFException e1) {

		} catch (IOException | ClassNotFoundException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	private void writeConfig() {

		new Thread(new Runnable() {

			@Override
			public void run() {

				ObjectOutputStream out = null;
				try {
					out = new ObjectOutputStream(new FileOutputStream(CONNECTION_CONFIG));
					out.writeObject(com.getIP());

					out.flush();
					out.close();
					System.out.println("writing new ip to file : " + com.getIP());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

	/**
	 * Sends the given transferable data to the server. This transferable data might
	 * contain a valid flag that the server can read.
	 * 
	 * @param data
	 *            The data that will be transfered and read on the server.
	 */
	public void send(TransferData data) {
		com.send(data);
	}

	/**
	 * Sends a transferable data that will have the given flag and object to the
	 * server. The flag must be a valid flag that can be read on the server, and the
	 * object can be null.
	 * 
	 * @param flag
	 *            The flag of the sending telling the server what to do.
	 * @param data
	 *            The optionnal data, can be null. To avoid useless parameter, use
	 *            {@link #send(int)}
	 */
	public void send(int flag, Serializable data) {
		send(new TransferData(flag, data));
	}

	/**
	 * Sends a transferable data that will have the given flag and a null data part.
	 * The flag must be a valid flag that the server can read.
	 * 
	 * @param flag
	 *            The flag telling the server what to do.
	 */
	public void send(int flag) {
		send(flag, null);
	}

}
