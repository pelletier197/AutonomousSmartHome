package tests.houseDataCenter.alarm;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmDay;

public class AlarmTest {
	Alarm alarm1 = null;
	Alarm alarm2 = null;
	Calendar cal = null;
	AlarmDay today = null;
	AlarmDay tomorrow = null;

	private int hour = 0;
	private int min = 0;
	private int day = 0;

	@Before
	public void setUp() throws Exception {
		cal = Calendar.getInstance();
		hour = cal.get(Calendar.HOUR_OF_DAY);
		min = cal.get(Calendar.MINUTE) + 1;
		day = cal.get(Calendar.DAY_OF_WEEK);
		alarm1 = new Alarm(hour, min, null);

		today = associatedDay(day);
		tomorrow = associatedDay((day % 7) + 1);

		alarm2 = new Alarm(hour, min, null, today, tomorrow);

	}

	private AlarmDay associatedDay(int dayNum) {

		for (AlarmDay d : AlarmDay.values()) {
			if (d.day == dayNum) {
				return d;
			}
		}
		return null;
	}

	@Test
	public void testAlarmIntInt() {
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(-1, 1, null);
			System.out.println(alarm1);
			fail();
		} catch (Exception e) {

		}
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(1, -1, null);
			fail();
		} catch (Exception e) {

		}
		// Tests an alarm that should only ring once (-1,-1)
		try {
			alarm1 = new Alarm(-1, -1, null);
			fail();
		} catch (Exception e) {

		}
		// Tests an alarm that should only ring once (midnight)
		try {
			alarm1 = new Alarm(0, 0, null);

		} catch (Exception e) {
			fail();

		}
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(24, 23, null);

		} catch (Exception e) {
			fail();
		}
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(23, 24, null);

		} catch (Exception e) {
			fail();
		}
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(24, 24, null);

		} catch (Exception e) {
			fail();
		}
		// Tests an alarm that should only ring once
		try {
			alarm1 = new Alarm(25, 23, null);
			fail();
		} catch (Exception e) {

		}
	
	}

	@Test
	public void testAlarmIntIntAlarmDayArray() {
		// Tests an alarm that should only ring once
		try {
			alarm2 = new Alarm(0, 0, null, new AlarmDay[0]);

		} catch (Exception e) {
			fail();
		}
		// Tests an alarm that should only ring once
		try {
			alarm2 = new Alarm(24, 60, null, today);

		} catch (Exception e) {
			fail();
		}
		// Tests an alarm that should only ring once
		try {
			alarm2 = new Alarm(23, 61, null, today);
			fail();
		} catch (Exception e) {

		}
		// Tests an alarm that should only ring once
		try {
			alarm2 = new Alarm(0, 0, null, today);

		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testVerify() throws InterruptedException {
		alarm2.setDays(tomorrow);
		System.out.println(tomorrow);
		System.out.println(alarm2);
		Thread.sleep(61 * 1000);

		// Rings today in one minute
		assertTrue(alarm1.verify());
		// Rings tomorrow
		assertFalse(alarm2.verify());
	}

	@Test
	public void testIsUseOnce() throws InterruptedException {
		Thread.sleep(61 * 1000);

		// Rings today in one minute
		assertTrue(alarm1.verify());

		// Asserts the alarm is used once and is not activated after in rang
		assertTrue(alarm1.isUseOnce());
		assertFalse(alarm1.isActive());

		alarm1.setDays(tomorrow);
		assertFalse(alarm1.isUseOnce());
		assertFalse(alarm2.isUseOnce());
		assertFalse(alarm1.isActive());

		alarm1.setActive(true);
		assertTrue(alarm1.isActive());

	}

	@Test
	public void testHasRang() throws InterruptedException {
		assertFalse(alarm1.hasRang());
		assertTrue(alarm1.timeBeforeNext() > 0);
		Thread.sleep(61 * 1000);
		assertFalse(alarm1.hasRang());
		assertTrue(alarm1.timeBeforeNext() == -2);
		assertTrue(alarm1.verify());
		assertTrue(alarm1.hasRang());

	}

	@Test
	public void testIsActive() throws InterruptedException {
		alarm1.setDays();
		alarm2.setDays(today);
		assertTrue(alarm1.isActive());
		assertTrue(alarm2.isActive());
		assertTrue(alarm1.timeBeforeNext() < 61 * 1000 && alarm1.timeBeforeNext() > 0);
		assertTrue(alarm2.timeBeforeNext() < 61 * 1000 && alarm2.timeBeforeNext() > 0);
		Thread.sleep(61 * 1000);

		assertTrue(alarm1.verify());
		assertTrue(alarm2.verify());

		// Alarm 1 is deactivated, because no alarm day is assigned
		assertFalse(alarm1.isActive());

		// Alarm2 should ring in 7 days, next week
		assertTrue(alarm2.isActive());

		// Assert the next alarm will ring in 7 days, we give 60 second of
		// range.
		System.out.println(alarm2.timeBeforeNext() + "  " + 7 * 86_400_000L);
		assertTrue(alarm2.timeBeforeNext() < 60 * 60 * 1000 * 24 * 7
				&& alarm2.timeBeforeNext() > 60 * 60 * 1000 * 24 * 7 - 60_000);

		alarm1.setDays(AlarmDay.ALL);
		assertTrue(alarm1.timeBeforeNext() == -1);

		alarm1.setActive(true);

		// Assert the next alarm will ring in 1 days, we give 60 second of
		// range.
		assertTrue(alarm1.timeBeforeNext() < 60 * 60 * 1000 * 24 * 1
				&& alarm1.timeBeforeNext() > 60 * 60 * 1000 * 24 * 1 - 60_000);

	}

	@Test
	public void testSetActive() throws InterruptedException {
		assertTrue(alarm1.isActive());
		alarm1.setActive(false);
		assertFalse(alarm1.isActive());
		Thread.sleep(61 * 1000);

		assertFalse(alarm1.verify());
		assertFalse(alarm1.isActive());

	}

	@Test
	public void testSetNextAlarmTime() {
		alarm1.setNextAlarmTime(hour, min + 1);
		assertTrue(alarm1.timeBeforeNext() < 60 * 1000 * 2 + 1 && alarm1.timeBeforeNext() > 60 * 1000 * 2 - 60_000);

	}

	@Test
	public void testIsAllDay() {

		alarm1.setDays(AlarmDay.ALL);
		assertTrue(alarm1.isAllDay());

		alarm1.setDays();
		assertFalse(alarm1.isAllDay());

		// Sets it for all days of week
		alarm1.setDays(AlarmDay.MONDAY, AlarmDay.TUESDAY, AlarmDay.WEDNESDAY, AlarmDay.THURSDAY, AlarmDay.FRIDAY,
				AlarmDay.SATURDAY, AlarmDay.SUNDAY);
		assertTrue(alarm1.isAllDay());

		// Sets it for all days of week except sunday
		alarm1.setDays(AlarmDay.MONDAY, AlarmDay.TUESDAY, AlarmDay.WEDNESDAY, AlarmDay.THURSDAY, AlarmDay.FRIDAY,
				AlarmDay.SATURDAY);
		assertFalse(alarm1.isAllDay());
	}
}
