package tests.houseDataCenter.house;

import houseDataCenter.house.exceptions.InvalidBornDate;
import houseDataCenter.house.exceptions.InvalidEmailException;
import houseDataCenter.house.exceptions.InvalidNameException;
import houseDataCenter.house.exceptions.InvalidPINException;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.residents.ResidentBuilder;

public class TestUtils {

	public static Resident generateResident1() {

		// We do not use all images from database, because sizes and quality are
		// not all the same. We only keep images that are reliable after
		// resizing. In the original usage of this class, the pictures might all
		// be taken from the same spot in the house e.i, will increase
		// recognition rate.

		// Builds resident 1
		ResidentBuilder builder = createIdentity();

		builder.setName("Léo");
		builder.setPIN("12312");
		builder.setPINConfirm("12312");
		builder.setEmail("ss@gmail.com");
		try {
			return builder.build();
		} catch (InvalidBornDate e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPINException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidEmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static Resident generateResident2() {

		// We do not use all images from database, because sizes and quality are
		// not all the same. We only keep images that are reliable after
		// resizing. In the original usage of this class, the pictures might all
		// be taken from the same spot in the house e.i, will increase
		// recognition rate.

		// Builds resident 1
		ResidentBuilder builder = createIdentity();

		builder.setName("Mariah");
		builder.setPIN("12345");
		builder.setPINConfirm("12345");
		builder.setEmail("mariah@gmail.com");
		try {
			return builder.build();
		} catch (InvalidBornDate e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPINException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidEmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static Resident generateResident3() {

		// We do not use all images from database, because sizes and quality are
		// not all the same. We only keep images that are reliable after
		// resizing. In the original usage of this class, the pictures might all
		// be taken from the same spot in the house e.i, will increase
		// recognition rate.

		// Builds resident 1
		ResidentBuilder builder = createIdentity();

		builder.setName("Johnny");
		builder.setPIN("54321");
		builder.setPINConfirm("54321");
		builder.setEmail("john@gmail.com");
		try {
			return builder.build();
		} catch (InvalidBornDate e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidNameException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidPINException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidEmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	private static ResidentBuilder createIdentity() {

		ResidentBuilder builder = new ResidentBuilder();
		builder.setBornDate(1);
		builder.setName("gdfgd");

		return builder;
	}
}
