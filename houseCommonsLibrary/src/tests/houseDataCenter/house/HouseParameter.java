package tests.houseDataCenter.house;

import java.io.Serializable;

import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.house.data.TempDisplay;

public class HouseParameter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8540583091075552876L;

	private static final Language DEFAULT_LANG = Language.ENGLISH_US;
	private static final TempDisplay DEFAULT_TEMP_DISPLAY = TempDisplay.TEMP_CELCIUS;

	private int clientCount;

	private Language lang;

	private TempDisplay tempDisplay;

	public HouseParameter() {

		this(DEFAULT_LANG, DEFAULT_TEMP_DISPLAY);

	}

	public HouseParameter(Language lang, TempDisplay tempDisplay) {
		setLanguage(lang);
		this.clientCount = 0;
	}

	public Language getLanguage() {
		return lang;
	}

	public void setLanguage(Language lang) {
		if (lang == null) {
			throw new NullPointerException("Null language not permitted");
		}

		this.lang = lang;
	}

	public TempDisplay getTempDisplay() {
		return tempDisplay;
	}

	public void setTempDisplay(TempDisplay tempDisplay) {
		if (lang == null) {
			throw new NullPointerException("Null temp display not permitted");
		}
		this.tempDisplay = tempDisplay;
	}

	public int getClientCount() {
		return clientCount;
	}

	public void addClient() {
		clientCount++;
	}

	public void removeClient() {
		if (clientCount > 1) {
			clientCount--;
		}
	}

}
