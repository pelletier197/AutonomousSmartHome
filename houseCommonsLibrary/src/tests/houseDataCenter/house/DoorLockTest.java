package tests.houseDataCenter.house;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.house.exceptions.AccessRefusedError;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.security.DoorLock;

public class DoorLockTest {

	private DoorLock lock;

	@Before
	public void setUp() throws Exception {

		lock = new DoorLock();

	}

	@Test
	public void testManualUnlock() throws InterruptedException, AccessRefusedError {

		Resident r = TestUtils.generateResident1();

		// Cannot open when no nip
		assertNull(lock.unlock());

		// Cannot open when trying a nip that is not there
		lock.nextPIN(r.getPIN());
		assertNull(lock.unlock());

		lock.addUnlocker(r);
		assertNull(lock.unlock());

		lock.nextPIN(r.getPIN());
		assertNotNull(lock.unlock());

		assertTrue(lock.isUnlocked());
		Thread.sleep(DoorLock.DEFAULT_UNLOCK + 1000);
		assertFalse(lock.isUnlocked());

		lock.nextPIN("77777");
		lock.unlock();
		lock.nextPIN("77777");
		lock.unlock();
		lock.nextPIN("77777");
		lock.unlock();

		try {
			// Should fail, even though nip is null, as attempt is passed.
			lock.unlock();

			fail();

		} catch (AccessRefusedError e) {

		}

		try {

			// Must fail, as attempt number is passed.
			lock.nextPIN("77777");
			lock.unlock();

			fail();

		} catch (AccessRefusedError e) {

		}

		assertFalse(lock.isUnlocked());
		lock.manualUnlock();

		assertFalse(lock.isUnlocked());

		// Should throw exception
		lock.nextPIN("77777");
		lock.unlock();

	}

}
