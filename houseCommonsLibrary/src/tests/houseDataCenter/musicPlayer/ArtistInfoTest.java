package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Artist;
import houseDataCenter.mp3Player.ArtistInfo;

public class ArtistInfoTest {

	private ArtistInfo infos;
	private Artist artist;

	@Before
	public void before() {
		Tests.initialize();
		this.artist = new Artist();
		this.infos = new ArtistInfo(artist);
	}

	@Test
	public void testArtistInfo() {
		try {
			infos = new ArtistInfo(null);
			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testAddSong() {

		// All different artists
		assertFalse(infos.addSong(Tests.song1));
		assertFalse(infos.addSong(Tests.song2));
		assertTrue(infos.addSong(Tests.song3));
		assertFalse(infos.addSong(Tests.song4));
		assertFalse(infos.addSong(Tests.song5));

		infos = new ArtistInfo(Tests.song1.getArtist());
		// song 2 is added because it is the same artist
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));
		assertFalse(infos.addSong(Tests.song3));
	}

	@Test
	public void testRemove() {

		infos = new ArtistInfo(Tests.song1.getArtist());
		// song 2 is added because it is the same artist
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));
		assertFalse(infos.addSong(Tests.song3));

		assertFalse(infos.remove(Tests.song3));
		assertTrue(infos.remove(Tests.song1));
		assertTrue(infos.remove(Tests.song2));

		// ensure we can't remove it 2 times
		assertFalse(infos.remove(Tests.song1));
	}

	@Test
	public void testGetSongs() {
		infos = new ArtistInfo(Tests.song1.getArtist());
		// song 2 is added because it is the same artist
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));
		assertFalse(infos.addSong(Tests.song3));

		assertTrue(infos.getSongs().size() == 2 && infos.getSongs().contains(Tests.song1)
				&& infos.getSongs().contains(Tests.song2));
	}

}
