package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.AlbumInfo;
import houseDataCenter.mp3Player.ArtistInfo;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;
import houseDataCenter.mp3Player.PlaylistManager;

public class PlaylistManagerTests {

	private Playlist playlist;
	private PlaylistManager manager;

	@Before
	public void before() {
		Tests.initialize();
		playlist = new Playlist();
		manager = new PlaylistManager(playlist);
	}

	@Test
	public void testPlaylistManager() {
		try {
			manager = new PlaylistManager();

		} catch (Exception e) {
			fail();
		}
		try {
			manager = new PlaylistManager(new Playlist(null));

		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testGetMainPlaylist() {

		// Assert it is the object itself
		assertTrue(manager.getMainPlaylist() == playlist);

	}

	@Test
	public void testGetSubPlaylists() {
		try {
			// doesn't exist
			manager.getSubPlaylist(0);
			fail();
		} catch (IndexOutOfBoundsException e) {

		}
		manager.addSubPlaylist(new Playlist());

		assertTrue(manager.getSubPlaylist(0) != null);
	}

	@Test
	public void testGetSubPlaylist() {
		assertTrue(manager.getSubPlaylists().size() == 0);

		manager.addSubPlaylist(new Playlist());
		manager.addSubPlaylist(new Playlist());
		manager.addSubPlaylist(new Playlist());

		assertTrue(manager.getSubPlaylists().size() == 3);
	}

	@Test
	public void testAddSongToSubPlaylist() {

		// At this point, no song is added to the manager
		manager.addSubPlaylist(new Playlist());
		// We add all these songs to the sub playlist. They should be added to
		// the main playlist
		manager.addSongToSubPlaylists(Tests.songs, 0);

		// Assert they are contained and only once
		assertTrue(manager.getMainPlaylist().getSongs().containsAll(Tests.songs)
				&& manager.getMainPlaylist().getNumberOfSongs() == Tests.song_length);
	}

	@Test
	public void testRemoveSong() {

		// add songs to manager
		manager.addSongs(Tests.songs);
		assertTrue(manager.getMainPlaylist().getNumberOfSongs() == Tests.song_length);

		// add song to playlist
		manager.addSubPlaylist(new Playlist());
		assertTrue(manager.getMainPlaylist().getNumberOfSongs() == Tests.song_length);
		manager.addSongToSubPlaylist(Tests.song1, 0);
		assertTrue(manager.getSubPlaylist(0).contains(Tests.song1));

		assertTrue(manager.getMainPlaylist().getNumberOfSongs() == Tests.song_length);

		// removes it from manager. Should remove from playlist and main
		// playlist too
		manager.removeSong(Tests.song1);
		assertTrue(manager.getMainPlaylist().getNumberOfSongs() == Tests.song_length - 1);
		assertFalse(manager.getSubPlaylist(0).contains(Tests.song1));

	}

	@Test
	public void testGetAllSongsSorted() {
		manager.addSongs(Tests.songs);
		assertTrue(manager.getAllSongsSorted().size() == Tests.song_length);
	}

	@Test
	public void testGetAllAlbumInfo() {
		assertTrue(manager.getAlbumInfo().size() == 0);

		manager.addSongs(Tests.songs);

		final Set<AlbumInfo> infos = manager.getAlbumInfo();

		// As there are 2 with the same album 4 elements instead of 5
		assertTrue(infos.size() == 4);

		boolean achieved = false;

		// Ensure that all album are contained from the songs
		for (AlbumInfo info : infos) {
			achieved = false;
			for (Mp3SongInfo song : Tests.songs) {
				if (song.getAlbum().equals(info.getAlbum())) {
					achieved = true;
				}
			}
			assertTrue(achieved);
		}

	}

	@Test
	public void testGetAllArtistInfo() {
		assertTrue(manager.getAlbumInfo().size() == 0);

		manager.addSongs(Tests.songs);

		final Set<ArtistInfo> infos = manager.getArtistInfo();

		// As there are 2 with the same album 3 elements instead of 5
		assertTrue(infos.size() == 3);

		boolean achieved = false;

		// Ensure that all album are contained from the songs
		for (ArtistInfo info : infos) {
			achieved = false;
			for (Mp3SongInfo song : Tests.songs) {
				if (song.getArtist().equals(info.getArtist())) {
					achieved = true;
				}
			}
			assertTrue(achieved);
		}
	}

}
