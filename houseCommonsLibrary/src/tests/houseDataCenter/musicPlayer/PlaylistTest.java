package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;

public class PlaylistTest {

	private Playlist playlist;
	private String unknown;

	@Before
	public void before() {
		Tests.initialize();
		playlist = new Playlist();
		unknown = "";
	}

	@Test
	public void testPlaylist() {
		try {
			playlist = new Playlist();
		} catch (Exception e) {
			fail();
		}
		try {
			playlist = new Playlist(null);
		} catch (Exception e) {
			fail();
		}
		try {
			playlist = new Playlist(null, null);
			fail();
		} catch (Exception e) {

		}
		try {
			playlist = new Playlist(new ArrayList<>(), "allo");
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testSetName() {
		playlist.setName(null);
		assertTrue(playlist.getName().equals(unknown));

		playlist.setName("allow");
		assertTrue(playlist.getName().equals("allow"));

		playlist.setName("           ");
		assertTrue(playlist.getName().equals(unknown));
	}

	@Test
	public void testContains() {
		assertFalse(playlist.contains(Tests.song1));

		playlist.addSong(Tests.song1);
		assertTrue(playlist.contains(Tests.song1));
	}

	@Test
	public void testSetSongs() {
		playlist.setSongs(Tests.songs);

		for (Mp3SongInfo song : Tests.songs)
			assertTrue(playlist.contains(song));
		try {
			playlist.setSongs(null);
			fail();
		} catch (NullPointerException e) {

		}
	}

	@Test
	public void testAddSong() {

		// can add it as much as desired
		assertTrue(playlist.addSong(Tests.song1));
		assertTrue(playlist.addSong(Tests.song1));
		assertTrue(playlist.addSong(Tests.song1));
	}

	@Test
	public void testAddSongs() {

		playlist.addSongs(Tests.songs);
		for (Mp3SongInfo song : Tests.songs)
			assertTrue(playlist.contains(song));

	}

	@Test
	public void testRemoveSong() {
		assertFalse(playlist.removeSong(null));
		assertFalse(playlist.removeSong(Tests.song1));

		assertTrue(playlist.addSong(Tests.song1));
		assertTrue(playlist.removeSong(Tests.song1));
	}

	@Test
	public void testRemoveAllSongs() {
		playlist.addSongs(Tests.songs);

		// assert that the length is computed
		long count = 0;
		for (Mp3SongInfo songs : Tests.songs)
			count += songs.getLength();
		assertTrue(playlist.getLength() == count);

		playlist.removeAllSongs(Tests.songs);

		assertTrue(playlist.getSongs().size() == 0);
		assertTrue(playlist.getLength() == 0);
	}

	@Test
	public void testGetLength() {
		playlist.addSong(Tests.song1);
		playlist.addSong(Tests.song2);

		assertTrue(playlist.getLength() == Tests.song1.getLength() + Tests.song2.getLength());

		playlist.removeSong(Tests.song3);
		playlist.removeSong(Tests.song2);
		assertTrue(playlist.getLength() == Tests.song1.getLength());
	}

	@Test
	public void testGetSongs() {

		playlist.addSong(Tests.song1);
		playlist.addSong(Tests.song2);

		assertTrue(playlist.getSongs().size() == 2 && playlist.getSongs().contains(Tests.song1));

		playlist.removeSong(Tests.song1);
		assertTrue(playlist.getSongs().size() == 1 && !playlist.getSongs().contains(Tests.song1));
	}

}
