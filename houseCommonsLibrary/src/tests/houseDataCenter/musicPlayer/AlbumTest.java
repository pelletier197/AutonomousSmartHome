package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Album;


public class AlbumTest {

	private Album album;
	private String unknown;

	@Before
	public void before() {
		
		Tests.initialize();
		unknown = "";
		
		album = new Album();
		
	}

	@Test
	public void testAlbum() {
		try {
			album = new Album(null);
			assertTrue(unknown != null && unknown.equals(album.getName()));
		} catch (Exception e) {
			fail();
		}
		try {
			album = new Album();
			assertTrue(unknown != null && unknown.equals(album.getName()));
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void testSetName() {
		album.setName("hey");
		assertTrue(album.getName().equals("hey"));
		
		album.setName("       ");
		assertTrue(album.getName().equals(unknown));
		
		album.setName(null);
		assertTrue(album.getName().equals(unknown));
	}

}
