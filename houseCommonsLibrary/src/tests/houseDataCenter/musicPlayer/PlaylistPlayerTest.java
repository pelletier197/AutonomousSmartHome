package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseDataCenter.mp3Player.PlaylistPlayer.ResearchType;
import houseDataCenter.mp3Player.SongNotContainedInPlaylistException;
import javafx.application.Application;
import javafx.stage.Stage;

public class PlaylistPlayerTest extends Application {

	private Playlist playlist;
	private PlaylistManager manager;
	private List<Mp3SongInfo> subplayist;

	private PlaylistPlayer player;

	@Before
	public void before() {

		Tests.initialize();
		playlist = new Playlist(Tests.songs, "playlist");
		manager = new PlaylistManager(playlist);
		subplayist = new ArrayList<>(playlist.getSongs().subList(0, 2));

		manager.addSubPlaylist(new Playlist(subplayist, "sublist"));

		player = new PlaylistPlayer(manager.getMainPlaylist());

		// ensure volume stay close
		player.setVolume(0);
	}

	@After
	public void beforeStop() {
		player.pause();
		
			try {
				player.setPlaying(null);
			} catch (SongNotContainedInPlaylistException e) {
				e.printStackTrace();
			}
		
	}

	@Test
	public void testPlaylistPlayer() {
		try {
			player = new PlaylistPlayer(null);

		} catch (Exception e) {
			fail();
		}
		try {
			player = new PlaylistPlayer(new Playlist(), null);
			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testSwitchPlaying() {

		// Will always fail as javaFx toolkit is never initialized
		/*
		 * 
		 *
		 * player.isPlayingProperty().addListener((value,old,newv)->
		 * System.out.println(newv)); if (!player.isPlaying()) {
		 * System.out.println(player.getPlaying()); player.switchPlaying();
		 * 
		 * if (player.isPlaying()) { player.switchPlaying();
		 * assertFalse(player.isPlaying()); } else { fail(); } }
		 */

	}

	@Test
	public void testPlay() {
		// Will always fail as javaFx toolkit is never initialized.
		// Must be tested graphically
	}

	@Test
	public void testPlayFrom() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPlaying() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTimeFormattedForProgress() {
		fail("Not yet implemented");
	}

	@Test
	public void testPause() {
		fail("Not yet implemented");
	}

	@Test
	public void testNext() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTime() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetProgress() {
		fail("Not yet implemented");
	}

	@Test
	public void testPrevious() {
		fail("Not yet implemented");
	}

	@Test
	public void testReset() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetReadingMode() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetSongsSorted() {
		fail("Not yet implemented");
	}

	@Test
	public void testVolumeProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetVolume() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPlaying() {
		fail("Not yet implemented");
	}

	@Test
	public void testPlayingProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsPlayingProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalTimeFormatted() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCurrentTime() {
		fail("Not yet implemented");
	}

	@Test
	public void testSongProgressProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testCurrentTimeAsStringProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testForwardOrBackwardPressed() {
		fail("Not yet implemented");
	}

	@Test
	public void testForwardOrBackwardReleased() {
		fail("Not yet implemented");
	}

	@Test
	public void testToString() {
		fail("Not yet implemented");
	}

	@Test
	public void testReadingModeProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetReadingMode() {
		fail("Not yet implemented");
	}

	@Test
	public void testPlaylistProperty() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPlaylist() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPlaylist() {
		fail("Not yet implemented");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub

	}

}
