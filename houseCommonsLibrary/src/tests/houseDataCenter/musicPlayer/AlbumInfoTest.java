package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Album;
import houseDataCenter.mp3Player.AlbumInfo;


public class AlbumInfoTest {
	private String unkown;
	private AlbumInfo infos;
	private Album album;

	@Before
	public void before() {
		Tests.initialize();
		unkown = "";
		this.album = new Album();
		this.infos = new AlbumInfo(album);
	}

	@Test
	public void testAlbumInfo() {
		try {
			infos = new AlbumInfo(null);
			fail();
		} catch (Exception e) {

		}
	}

	@Test
	public void testAddSong() {
		// Songs should not be added, because album doesn't match empty album
		assertFalse(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song3));

		infos = new AlbumInfo(Tests.song1.getAlbum());

		// Should add 1 and 2 has they have the same album
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));

		// should not add this one
		assertFalse(infos.addSong(Tests.song3));
	}

	@Test
	public void testRemoveSong() {
		infos = new AlbumInfo(Tests.song1.getAlbum());

		// Should add 1 and 2 has they have the same album
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));

		// should not add this one
		assertFalse(infos.addSong(Tests.song3));

		assertTrue(infos.remove(Tests.song1));
		assertTrue(infos.remove(Tests.song2));

		// ensure true is not always returned
		assertFalse(infos.remove(Tests.song3));

		// ensure they are really removed
		assertFalse(infos.remove(Tests.song1));
	}

	@Test
	public void testGetSongs() {

		infos = new AlbumInfo(Tests.song1.getAlbum());

		// Should add 1 and 2 has they have the same album
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));

		// should not add this one
		assertFalse(infos.addSong(Tests.song3));

		assertTrue(infos.getSongs().contains(Tests.song1));
		assertTrue(infos.getSongs().contains(Tests.song2));
		assertFalse(infos.getSongs().contains(Tests.song3));
	}

	@Test
	public void testGetLength() {
		infos = new AlbumInfo(Tests.song1.getAlbum());

		// Should add 1 and 2 has they have the same album
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));

		// should not add this one
		assertFalse(infos.addSong(Tests.song3));
		assertTrue(infos.getLength() == Tests.song1.getLength() + Tests.song2.getLength());

		// Should not remove, so length should remain
		infos.remove(Tests.song3);
		assertTrue(infos.getLength() == Tests.song1.getLength() + Tests.song2.getLength());

		// should reduce the time
		infos.remove(Tests.song1);
		assertTrue(infos.getLength() == Tests.song2.getLength());

		// should reset the time
		infos.remove(Tests.song2);
		assertTrue(infos.getLength() == 0L);
	}

	@Test
	public void testGetNumberOfSongs() {

		infos = new AlbumInfo(Tests.song1.getAlbum());

		// Should add 1 and 2 has they have the same album
		assertTrue(infos.addSong(Tests.song1));
		assertTrue(infos.addSong(Tests.song2));

		// should not add this one
		assertFalse(infos.addSong(Tests.song3));

		assertTrue(infos.getNumberOfSongs() == 2);
	}
}
