package tests.houseDataCenter.musicPlayer;

import java.io.File;
import java.util.List;

import houseDataCenter.mp3Player.Mp3SongInfo;
import utils.Mp3Utils;

public class Tests {

	public static Mp3SongInfo song1;
	public static Mp3SongInfo song2;
	public static Mp3SongInfo song3;
	public static Mp3SongInfo song4;
	public static Mp3SongInfo song5;

	public static int song_length = 0;

	public static List<Mp3SongInfo> songs;

	private final static String fs = File.separator;
	private final static String base = "C:" + fs + "Users" + fs + "sunny" + fs + "Documents" + fs + "HouseMonitors" + fs
			+ "bin" + fs + "tests" + fs + "music" + fs;

	/**
	 * Initialize the songs with the specified characteristics. This method must
	 * be called before calling the objects, or null values will be pointed by
	 * them.
	 * 
	 * <p>
	 * <li>song1 has the same artist and album than song2</li>
	 * <li>song3 has the same artist than song2 and song1, but not the same
	 * album</li>
	 * <li>artist and album of song3 are unknown</li>
	 * <li>song4 and 5 have different artist, album</li>
	 */
	public static void initialize() {

		songs = Mp3Utils.getMP3FromRepository(new File(Tests.class.getResource("/music").getPath()));
		song_length = songs.size();
		
		song1 = songs.get(0);
		song2 = songs.get(1);
		song3 = songs.get(2);
		song4 = songs.get(3);
		song5 = songs.get(4);
	
		System.out.println(songs);
	}

}
