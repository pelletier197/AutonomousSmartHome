package tests.houseDataCenter.musicPlayer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import houseDataCenter.mp3Player.Artist;

public class ArtistTest {
	private String unknown;

	@Before
	public void before() {
		Tests.initialize();
		unknown = "";
	}

	@Test
	public void testArtist() {
		Artist artist = new Artist();
		assertTrue(unknown != null && artist.getName().equals(unknown));

		artist = new Artist("hey");
		assertTrue(artist.getName().equals("hey"));

		artist = new Artist("          ");
		assertTrue(artist.getName().equals(unknown));
	}

	@Test
	public void testSetName() {
		Artist artist = new Artist();

		artist.setName("hey");
		assertTrue(artist.getName().equals("hey"));

		artist.setName("        ");
		assertTrue(artist.getName().equals(unknown));
	}

}
