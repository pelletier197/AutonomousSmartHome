Multi-project repository containing the server, commons, client and mobile app (android) projects for home controlling purpose. 

## What it does
The house incorporates multiple consoles installed in every room
and every one of them have its own set of devices and cameras connected to it, working under the principle of [IOT](https://en.wikipedia.org/wiki/Internet_of_things). Every room also have their own music controller, alarm manager, temperature controller and many other utilities that are all managed by a central server. The access to the house is protected by an account access system where the user willing to create the account must absolutely be connected on the same network than the server and the account must be confirmed by another resident.

## The goal
The project's goal is to ensure the security, notifying the users when an illegal access is made, when a fire or any other type of danger happens in the house.  It is first of all a tool to help the residents and to connect everything together. Every device of the house is accessible from a single mobile application and can therefore be accessed from everywhere.