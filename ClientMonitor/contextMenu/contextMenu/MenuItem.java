package contextMenu;

import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

public class MenuItem {

	private static final URL MENU_ITEM_VIEW = MenuController.class.getResource("/contextMenu/MenuItem.fxml");

	private Handler handler;

	private String text;

	private String iconPath;

	protected Node view;

	public MenuItem(String text, String iconPath, Handler selectionHandler) {

		if (text == null) {
			text = "";
		}
		this.text = text;
		this.iconPath = iconPath;
		this.handler = selectionHandler;

		MenuItemController controller = new MenuItemController();

		FXMLLoader loader = new FXMLLoader(MENU_ITEM_VIEW);
		loader.setController(controller);

		try {

			view = loader.load();
			controller.setItem(this);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Handler getHandler() {
		return handler;
	}

	public String getText() {
		return text;
	}

	public String getIconPath() {
		return iconPath;
	}

	public Node getView() {
		return view;
	}

	public boolean isDisable() {
		return handler == null;
	}

}
