package contextMenu;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

public class ContextMenu {

	public static final double DEFAULT_WIDTH = MenuController.MENU_WIDTH;

	public static final double TOP_MENU_HEIGHT = MenuController.TOP_HEIGHT;

	private Pane view;
	private MenuController controller;
	private Pane center;

	public ContextMenu() {

		controller = new MenuController();

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/contextMenu/ContextMenu.fxml"));
		loader.setController(controller);
		try {
			view = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setCenter(Pane view) {
		if (center == null) {
			this.view.getChildren().add(view);
		} else {
			this.view.getChildren().set(2, view);
		}
		this.center = view;
		
		this.center.setMaxHeight(Double.MAX_VALUE);
		this.center.setMaxWidth(Double.MAX_VALUE);
		
		this.center.prefWidthProperty().bind(this.view.widthProperty());
		this.center.prefHeightProperty().bind(this.view.heightProperty().subtract(TOP_MENU_HEIGHT));
		this.center.setTranslateY(TOP_MENU_HEIGHT);
		
		this.center.toBack();

	}

	public void setTitle(String title) {
		controller.setTitle(title);
	}

	public void setIconPath(String path) {
		controller.setIcon(path);
	}

	public void addMenuItem(MenuItem item){
		controller.addItem(item);
	}
	public void extend(boolean withAnimation) {
		controller.extend(withAnimation);
	}

	public void contract(boolean withAnimation) {
		controller.collapse(withAnimation);
	}

	public Parent getView() {
		return view;
	}

}
