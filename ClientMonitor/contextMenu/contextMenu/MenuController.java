package contextMenu;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import com.sun.glass.ui.Menu;

import javafx.animation.Animation.Status;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import utils.HouseUtils;

public class MenuController implements Initializable {

	public static final double TOP_HEIGHT = 84;

	public static final double MENU_WIDTH = 350;

	public static final double ANIM_DURATION = 300;

	@FXML
	private ImageView iconBar;

	@FXML
	private ImageView iconMenu;

	@FXML
	private VBox menu;

	@FXML
	private HBox menuBar;

	@FXML
	private Label title;

	@FXML
	private Label date;
	
    @FXML
    private VBox menuItem;

	private Timeline extendAnim;

	private Timeline collapseAnim;

	private boolean isExtended;

	private List<MenuItem> items;

	@FXML
	private void collapse(MouseEvent event) {
		collapse(true);
	}

	public void collapse(boolean withAnim) {

		// Ensure animation is allowed, or exception will be thrown
		if (isExtended && collapseAnim.getStatus() != Status.RUNNING && extendAnim.getStatus() != Status.RUNNING) {
			if (withAnim) {

				collapseAnim.playFromStart();

			} else {
				menu.setTranslateX(-MENU_WIDTH);
				menu.setOpacity(0);
				menu.setDisable(true);
			}
			isExtended = false;
		}
	}

	public void addItem(MenuItem item) {

		this.menuItem.getChildren().add(item.view);
		this.items.add(item);
	}

	public void removeItem(MenuItem item) {

		int index = items.indexOf(item);

		items.remove(index);
		// Skip title + image box
		menuItem.getChildren().remove(index);
	}

	public void removeItem(int index) {

		items.remove(index);
		// Skip title + image box
		menuItem.getChildren().remove(index);
	}

	@FXML
	private void extend(MouseEvent event) {
		extend(true);
	}

	public void extend(boolean withAnim) {

		// Ensure animation is allowed, or exception will be thrown
		if (!isExtended && collapseAnim.getStatus() != Status.RUNNING && extendAnim.getStatus() != Status.RUNNING) {

			menu.setDisable(false);
			menu.setOpacity(1);

			if (withAnim) {
				extendAnim.playFromStart();
			} else {
				menu.setTranslateX(MENU_WIDTH);
			}
			isExtended = true;
		}
	}
 
	public void setTitle(String title) {
		this.title.setText(title);
	}

	public void setIcon(String path) {

		Image image = new Image(path);

		iconBar.setImage(image);
		iconMenu.setImage(image);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.items = new ArrayList<>();

		// Initializes animations
		extendAnim = new Timeline(
				new KeyFrame(Duration.millis(0), new KeyValue(menu.translateXProperty(), -MENU_WIDTH)),
				new KeyFrame(Duration.millis(ANIM_DURATION), new KeyValue(menu.translateXProperty(), 0)));

		collapseAnim = new Timeline(new KeyFrame(Duration.millis(0), new KeyValue(menu.translateXProperty(), 0)),
				new KeyFrame(Duration.millis(ANIM_DURATION), new KeyValue(menu.translateXProperty(), -MENU_WIDTH)));

		collapseAnim.setOnFinished((event) -> {
			menu.setOpacity(0);
			menu.setDisable(true);
		});

		isExtended = true;

		// The menu is initialized closed
		collapse(false);

		System.out.println(menu.getStyleClass());

		Task<Void> timeUpdater = new Task<Void>() {

			@Override
			protected Void call() throws Exception {

				while (!Thread.interrupted()) {
					Platform.runLater(() -> date.setText(HouseUtils.getFormatedDate()));
					Thread.sleep(500);
				}

				return null;
			}
		};
		new Thread(timeUpdater).start();
	}
}
