package contextMenu;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class MenuItemController {

	@FXML
	private AnchorPane background;

	@FXML
	private ImageView icon;

	@FXML
	private Label text;

	private MenuItem item;

	@FXML
	private void itemClicked(MouseEvent event) {
		if (!item.isDisable()) {
			item.getHandler().handleSelection();
		}
	}

	@FXML
	private void itemEntered(MouseEvent event) {
		if (!item.isDisable()) {
			background.setOpacity(0.8);
		}
	}

	@FXML
	private void itemExited(MouseEvent event) {
		if (!item.isDisable()) {
			background.setOpacity(1);
		}
	}

	public void setItem(MenuItem item) {
		this.item = item;
		init();
	}

	private void init() {
		text.setText(item.getText());

		if (item.getIconPath() != null) {
			icon.setImage(new Image(item.getIconPath()));
		}

	}

}
