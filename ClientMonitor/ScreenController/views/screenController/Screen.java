package views.screenController;

import java.net.URL;

/**
 * An enum that represents the screens that can be displayed in the program. The
 * enum will allow to get an URL corresponding to the Screen's position on the
 * disk.
 * 
 * @author sunny
 *
 */
public enum Screen {

	/**
	 * From a program to another, the enum is the only part to change. The files
	 * are represented as strings, with the path /package/fxmlFileName.fxml
	 */

	// @Formatter :off
	MUSIC_LIST_VIEW("/houseMonitorViews/musicPlayer/researchListView.fxml"), MUSIC_PLAYING(
			"/houseMonitorViews/musicPlayer/MusicPlaying.fxml"), CONVERTER(
					"/houseMonitorViews/converter/Converter.fxml"), CALCULATOR(
							"/houseMonitorViews/calculator/Calculator.fxml"), RESIDENT_MODIFICATION(
									"/houseMonitorViews/control/ResidentModification.fxml"), ROOM_INFO(
											"/houseMonitorViews/control/RoomInfo.fxml"), RESIDENT_CREATION(
													"/houseMonitorViews/control/ResidentCreation.fxml"), RESIDENT_CONFIRMATION(
															"/houseMonitorViews/control/ResidentConfirmation.fxml"), ROOM_MODIFICATION(
																	"/houseMonitorViews/control/RoomModification.fxml"), ALARM(
																			"/houseMonitorViews/control/Alarm.fxml"), ADD_LIGHTS(
																					"/houseMonitorViews/control/AddLights.fxml"), PIN_CONFIRM(
																							"/houseMonitorViews/control/PINConfirm.fxml"), LIGHT_OPTIONS(
																									"/houseMonitorViews/control/SingleLightControl.fxml"), EDIT_ALARM(
																											"/houseMonitorViews/control/EditAlarm.fxml"), MUSIC_SELECTOR(
																													"/houseMonitorViews/musicPlayer/MusicSelector.fxml"),
	ALARM_RINGING(
			"/houseMonitorViews/control/AlarmRinging.fxml");
	// @Formatter :on
	private URL fxmlReferencePath;

	/**
	 * The constructor of a screen, with the String representing the package and
	 * the file's Name
	 * 
	 * @param fxmlName
	 */
	private Screen(String fxmlName) {

		fxmlReferencePath = this.getClass().getResource(fxmlName);

	}

	/**
	 * Returns an URL that corresponds to the screen's position on the disk.
	 */
	public URL getFxmlUrl() {
		System.out.println(fxmlReferencePath);
		System.out.println("---------------------------");
		return fxmlReferencePath;
	}
}
