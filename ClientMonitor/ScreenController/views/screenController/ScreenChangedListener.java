package views.screenController;

public interface ScreenChangedListener {

	public void changed(Screen old, Screen newv);
}
