package house;

public interface AlarmRingingDecisionListener {

	public void onSnooze();
	
	public void onStop();
}
