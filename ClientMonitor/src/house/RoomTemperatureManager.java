package house;

import embedded.ArduinoTemperatureEmbeddedController;

public class RoomTemperatureManager {

	/**
	 * The rooms allow {@value #DELTA_T_ALLOWED} degrees difference before
	 * starting to heat.
	 */
	private static final double DELTA_T_ALLOWED = 1;

	/**
	 * The time given to wait before checking what is the new temperature.
	 */
	private static final long TIME_BETWEEN_CHECKS = 30_000;

	private double lastTemp;
	private double lastHumidity;

	private RoomParams params;
	private boolean activated;
	private ArduinoTemperatureEmbeddedController controller;

	private boolean hasError;

	public RoomTemperatureManager(ArduinoTemperatureEmbeddedController embeddedController) {
		this.params = ClientParameters.getParams();
		controller = embeddedController;
	}

	public void begin() {

		activated = true;

		new Thread(() -> {
			while (activated) {
				try {
					Thread.sleep(TIME_BETWEEN_CHECKS);
				} catch (Exception e) {
				}
				final double currentTemp = params.getTemp();

				try {
					float[] tempHumi = controller.getTemperatureAndHumitity();
					lastTemp = tempHumi[0];
					lastHumidity = tempHumi[1];

					if (!isAcceptableTemperature(lastTemp)) {
						// Starts heating if too cold
						if (lastTemp < currentTemp) {
							controller.activateBaseboard();
						} else {
							// Stops heating when too hot
							controller.deactivateBaseboard();
						}
					}
					hasError = false;
				} catch (Exception e) {
					e.printStackTrace();
					// Catch anything. It would be a connection problem.
					hasError = true;
				}

			}
		}).start();
	}

	/**
	 * Checks if the given temperature is an acceptable temperature comparing
	 * with room's target temperature and the value of {@link #DELTA_T_ALLOWED}
	 * 
	 * @param temperature
	 *            The temperature to watch
	 * @return True if the temperature is okay, false otherwise.
	 */
	private boolean isAcceptableTemperature(double temperature) {
		final double target = params.getTemp();
		return temperature >= target - DELTA_T_ALLOWED;
	}

	/**
	 * Returns the last registered temperature. This temperature is fetched
	 * every {@value #TIME_BETWEEN_CHECKS} milliseconds.
	 * 
	 * @return The last fetched temperature of the room in celcius
	 */
	public double getLastTemp() {
		return lastTemp;
	}

	/**
	 * Returns the last registered humidity. This temperature is fetched every
	 * {@value #TIME_BETWEEN_CHECKS} milliseconds.
	 * 
	 * @return The last fetched humidity of the room in %
	 */
	public double getLastHumidity() {
		return lastHumidity;
	}

	/**
	 * 
	 * @return True if there is an error with the connection to the micro
	 *         controller, false otherwise
	 */
	public boolean hasError() {
		return hasError;
	}

}
