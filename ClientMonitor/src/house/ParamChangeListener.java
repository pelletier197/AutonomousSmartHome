package house;

import houseDataCenter.commucationData.TransferFlags;

public interface ParamChangeListener {

	public void languageChanged();

	public void displayTemperatureChanged();
}
