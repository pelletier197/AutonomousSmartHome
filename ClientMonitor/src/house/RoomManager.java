package house;

import static houseDataCenter.commucationData.TransferFlags.NOTIFY_MEDIA_PLAYING_CHANGED;
import static houseDataCenter.commucationData.TransferFlags.NOTIFY_MEDIA_STATUS_CHANGED;
import static houseDataCenter.commucationData.TransferFlags.NOTIFY_READING_MODE_CHANGED;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import bridge.Bridge;
import custom.CommunicationCenter;
import embedded.ArduinoTemperatureEmbeddedController;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.Room;
import houseDataCenter.house.data.MusicStatus;
import houseDataCenter.mp3Player.JavaFXPlayer;
import houseDataCenter.mp3Player.Mp3Player;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlayerState;
import houseDataCenter.mp3Player.Playlist;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseDataCenter.mp3Player.PlaylistPlayer.READING_MODE;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import src.main.java.com.mpatric.mp3agic.InvalidDataException;
import src.main.java.com.mpatric.mp3agic.UnsupportedTagException;
import util.IOUtils;
import utils.FileUtils;
import utils.HouseUtils;
import utils.Mp3Utils;

/**
 * This class is used to execute basic commands around the room and uses
 * specific managers for executing these operations.
 * 
 * <p>
 * It's main task is to load everything when the console is started, including
 * language of the application, musics, temperature manager, voice recognition
 * engine and server connection engine.
 * </p>
 * 
 * @author sunny
 *
 */
public class RoomManager {

	private static final long SAVING_TIMEOUT = 120_000;
	private static final String STRINGS_KEY = "strings";

	private static final String PLAYLISTS_PATH = FileUtils.getAppDataDirectory() + "/config/playlists.config";
	private static final String SONGS_PATH = FileUtils.getAppDataDirectory() + "/music";
	private static final double VOLUME_WHILE_AI_SPEAKING = 0.1;

	/**
	 * Bridge to communicate with the view.
	 */
	private Bridge bridge;

	/**
	 * Managers of the room
	 */
	private PlaylistManager playlists;
	private PlaylistPlayer player;
	private CommunicationCenter center;
	private ClientRequestManager man;
	private ClientSpeechRecognizer rec;
	private RoomTemperatureManager temperature;
	private ArduinoTemperatureEmbeddedController embedded;

	/**
	 * Tells if the room is currently saving
	 */
	private boolean saving;

	/**
	 * Creates and load the room manager associated to the given bridge view. This
	 * class is completely independent of the view, but however uses bridge's
	 * functions.
	 * 
	 * @param b
	 *            The bridge to the view
	 */
	public RoomManager(Bridge b) {

		if (b == null) {
			throw new NullPointerException("Null bridge");
		}
		this.bridge = b;

		loadParameters();
		loadLanguage();
		loadEmbeddedController();
		loadTemperatureManager();
		loadMusicManager();
		loadCommunicationCenter();
		loadRequestManager();
		loadSpeechRecognizer();

		initBridge();
		initBackgroundSaving();

		center.openConnection();

		bridge.displayRoomController();
		save();
	}

	/**
	 * Loads the speech recognizer of the room.
	 */
	private void loadSpeechRecognizer() {
		rec = new ClientSpeechRecognizer(center);
		rec.startRecording();
	}

	/**
	 * Loads the parameters of the room, or create new ones if they are not found.
	 * This function is temporary before the manager fetches them from the server.
	 */
	private void loadParameters() {

		RoomParams params = IOUtils.readClientParameters();

		if (params == null) {

			// Parameters with default value
			params = new RoomParams();
		}

		ClientParameters.setParameters(params);

		params.setParamChangeListener(new ParamChangeListener() {

			@Override
			public void languageChanged() {
				loadLanguage();
				// Notifies the view to refresh the contents
				bridge.languageChanged();
			}

			@Override
			public void displayTemperatureChanged() {
				center.send(TransferFlags.SET_ROOM_TEMP_DISPLAY, ClientParameters.getParams().getTempDisplay());
			}
		});

	}

	/**
	 * Loads application's resource bundle from language in parameter.
	 */
	private void loadLanguage() {

		RoomParams params = ClientParameters.getParams();
		Language lang = params.getLanguage();

		// Locale, so only the path from strings are taken.
		Locale loc = new Locale("", "");
		ResourceBundle rb = ResourceBundle.getBundle(lang.getProperty(STRINGS_KEY), loc);

		// Sets the default language of the application.
		BundleWrapper.setResourceBundle(rb);

	}

	/**
	 * Loads the embedded controller that communicates with the micro controller.
	 */
	private void loadEmbeddedController() {
		this.embedded = new ArduinoTemperatureEmbeddedController();
	}

	/**
	 * Loads the temperature manager of the room.
	 */
	private void loadTemperatureManager() {
		this.temperature = new RoomTemperatureManager(embedded);
		temperature.begin();
	}

	/**
	 * Loads the music manager, its songs and its playlists
	 */
	private void loadMusicManager() {

		List<Mp3SongInfo> songs = null;
		List<Playlist> playlists = null;

		File path = new File(SONGS_PATH);

		if (!path.exists()) {
			path.mkdirs();
		}

		songs = Mp3Utils.getMP3FromRepository(path);

		try {
			playlists = Mp3Utils.readPlaylists(new File(PLAYLISTS_PATH));
		} catch (FileNotFoundException e) {
			new File(PLAYLISTS_PATH).mkdirs(); // Creates the dir
		} catch (IOException | ClassNotFoundException e) {
			playlists = new ArrayList<>();
			e.printStackTrace();
		}

		// Loads the music playlist
		this.playlists = new PlaylistManager(songs);

		// Successfully read
		if (playlists != null) {
			// Loads the sub-playlists
			for (Playlist p : playlists) {
				this.playlists.addSubPlaylist(p);
			}
		}

		this.player = new PlaylistPlayer(this.playlists.getMainPlaylist());

		// Will notify on music playing status changed
		player.isPlayingProperty().addListener((value, old, newv) -> center.send(NOTIFY_MEDIA_PLAYING_CHANGED));
		player.playingProperty().addListener((value, old, newv) -> center.send(NOTIFY_MEDIA_STATUS_CHANGED));
		player.readingModeProperty().addListener((value, old, newv) -> center.send(NOTIFY_READING_MODE_CHANGED));
		player.setVolume(ClientParameters.getParams().getVolume());
	}

	/**
	 * Loads the communication center to communicate with the server
	 */
	private void loadCommunicationCenter() {
		this.center = new CommunicationCenter();
	}

	/**
	 * Loads the request manager that will threat requests from the server using the
	 * bridge and the current class.
	 */
	private void loadRequestManager() {
		this.man = new ClientRequestManager(this, center);
		this.center.addDataReceiver(this.man);
	}

	/**
	 * Initializes the bridge with the playlist player and the playlist manager
	 */
	private void initBridge() {
		this.bridge.init(center, player, playlists);
	}

	/**
	 * Initializes a thread that saves application's data every
	 * {@value #SAVING_TIMEOUT} ms.
	 */
	private void initBackgroundSaving() {

		new Thread(() -> {
			while (true) {

				save();

				try {
					Thread.sleep(SAVING_TIMEOUT);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}).start();
	}

	/**
	 * Save application's data
	 */
	private synchronized void save() {
		if (!saving) {
			saving = true;

			IOUtils.writeClientParameters(ClientParameters.getParams());
			try {
				File path = new File(PLAYLISTS_PATH);

				if (!path.exists()) {
					path.createNewFile();
				}

				Mp3Utils.writePlaylists(path, playlists.getSubPlaylists());
			} catch (IOException e) {
				e.printStackTrace();
			}
			saving = false;
		}
	}

	/**
	 * Updates room temperature to the given temperature this function must only be
	 * called after a server request.
	 */
	public void updateTemp(double temp) {
		if (temp < Room.MIN_TEMP || temp > Room.MAX_TEMP) {
			throw new IllegalArgumentException("Invalid temperature");
		}

		// The temperature manager updates automatically
		ClientParameters.getParams().setTemp(temp);
	}

	/**
	 * Returns a new music status from the player's information
	 */
	public MusicStatus getMusicStatus() {
		return new MusicStatus(player.getPlaying(), player.isPlaying(), player.getReadingMode());
	}

	/**
	 * Plays the next song in the player
	 */
	public void nextSong() {
		player.next();
	}

	/**
	 * Plays the previous song in the player
	 */
	public void previousSong() {
		player.previous();

	}

	public void switchPlaying() {
		player.switchPlaying();
	}

	public READING_MODE getReadingMode() {
		return player.getReadingMode();
	}

	public void setReadingMode(READING_MODE mode) {
		player.setReadingMode(mode);
	}

	public void uploadMusic(String name, byte[] data) {

		File f = new File(SONGS_PATH + "/" + name);

		try {
			// Writes the new song to the file
			HouseUtils.writeToFile(data, f);

			// Adds the music to the playlist
			Mp3SongInfo createdSong = new Mp3SongInfo(f.getPath());

			System.out.println(createdSong);

			playlists.addSong(createdSong);

			player.setPlaylist(playlists.getMainPlaylist());

			// Notify the player to update the playlist
			player.updatePlaylist();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedTagException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadLanguage(Language lang) {
		Language cur = ClientParameters.getParams().getLanguage();

		if (cur != lang) {
			ClientParameters.getParams().setLanguage(lang);
			loadLanguage();
			bridge.languageChanged();
		}

	}

	public void playSongEqualTo(Mp3SongInfo song) {
		Mp3SongInfo real = playlists.findSong(song);

		if (real != null) {
			player.setPlaylist(playlists.getMainPlaylist());
			player.playFrom(real);
		}
	}

	public void playMusic() {
		player.play();
	}

	public void pauseMusic() {
		player.pause();
	}

	public void forcePrevious() {

		player.forcePrevious();
	}

	public void playAiAnswer(String file) {
		// Saves and set music volume
		PlayerState state = PlayerState.generatePlayerState(player);

		player.setVolume(VOLUME_WHILE_AI_SPEAKING);

		// Play the sound
		Mp3Player player = new JavaFXPlayer();
		player.play(Paths.get(file).toUri().toString());

		// Restore music volume
		player.setOnfinished(() -> {
			player.dispose();
			state.restore();
		});
	}

	public void alarmRinging(Alarm a) {

		final BooleanProperty isStopped = new SimpleBooleanProperty(false);

		PlayerState state = PlayerState.generatePlayerState(player);

		Mp3Player player = new JavaFXPlayer();
		String path = Paths.get(a.getTone().getTonePath()).toUri().toString();
		Platform.runLater(() -> this.player.pause()); // Pauses music

		player.play(path);
		player.setVolume(a.getTone().getToneVolume());

		final AlarmRingingDecisionListener listener = new AlarmRingingDecisionListener() {

			@Override
			public void onStop() {
				if (player != null && isStopped != null && !isStopped.get()) {
					isStopped.set(true);
					player.pause();
					player.dispose();
					state.restore();
				}
			}

			@Override
			public void onSnooze() {
				if (player != null && isStopped != null && !isStopped.get()) {
					isStopped.set(true);
					player.pause();
					player.dispose();
					state.restore();
					center.send(TransferFlags.SNOOZE_ALARM, a.getId());
				}
			}
		};

		bridge.displayAlarmRinging(a, listener);

		new Thread(() -> {
			try {
				// 30 seconds of waiting
				Thread.sleep(30000);

				// User has not stopped it
				if (!isStopped.get()) {
					listener.onSnooze();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	public void setVolume(double volume) {
		player.setVolume(volume);
		bridge.volumeChanged();

		// Saves the volume
		RoomParams params = ClientParameters.getParams();
		params.setVolume(volume);
		IOUtils.writeClientParameters(params);
	}

}
