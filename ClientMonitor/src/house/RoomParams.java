package house;

import java.io.Serializable;
import java.util.Arrays;

import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.house.data.TempDisplay;

public class RoomParams implements Serializable {

	private static final long serialVersionUID = 5791754472054716210L;

	// DEFAULT VALUES
	private static final Language DEFAULT_LANGUAGE = Language.FRENCH;
	private static final TempDisplay DEFAULT_DISPLAY_TEMP = TempDisplay.TEMP_CELCIUS;
	private static final double DEFAULT_TEMPERATURE = 15.0;
	private static final double DEFAULT_VOLUME = 0.2;

	// SPECIFIC VALUES
	private TempDisplay tempDisplay;
	private Language language;
	private double temperature;
	private String roomName;
	private byte[] UUID;
	private double volume;

	private transient ParamChangeListener listener;

	public RoomParams() {

		this.language = DEFAULT_LANGUAGE;
		this.tempDisplay = DEFAULT_DISPLAY_TEMP;
		this.temperature = DEFAULT_TEMPERATURE;
		this.roomName = "";
		this.volume = DEFAULT_VOLUME;
	}

	public void setTempDisplay(TempDisplay unit) {
		if (unit == null) {
			throw new NullPointerException();
		}

		boolean changed = unit != tempDisplay;
		
		this.tempDisplay = unit;
		
		if(changed && listener != null){
			listener.displayTemperatureChanged();
		}
	}

	public TempDisplay getTempDisplay() {
		return tempDisplay;
	}

	public void setTemp(double temp) {
		temperature = temp;
	}

	public double getTemp() {
		return temperature;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language lang) {
		if (lang == null) {
			throw new NullPointerException();
		}
		boolean changed = language != lang;
		this.language = lang;

		if (listener != null && changed) {
			listener.languageChanged();
		}
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public byte[] getUUID() {
		return UUID;
	}

	public void setUUID(byte[] UUID) {
		this.UUID = UUID;
	}
	
	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	protected void setParamChangeListener(ParamChangeListener listener) {
		this.listener = listener;
	}

	@Override
	public String toString() {
		return "RoomParams [tempDisplay=" + tempDisplay + ", language=" + language + ", temperature=" + temperature
				+ ", roomName=" + roomName + ", UUID=" + Arrays.toString(UUID) + "]";
	}
}
