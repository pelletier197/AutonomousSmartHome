package house;

public class ClientParameters {

	private static RoomParams params = new RoomParams();

	public static void setParameters(RoomParams param) {
		if (params == null) {
			throw new NullPointerException("Null params");
		}
		params = param;
	}

	public static RoomParams getParams() {
		return params;
	}
}
