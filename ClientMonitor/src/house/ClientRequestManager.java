package house;

import static houseDataCenter.commucationData.TransferFlags.*;
import static houseServer.HouseServerManager.ROOM_OVERLOAD;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import bridge.Bridge;
import custom.CommunicationCenter;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.MusicStatus;
import houseDataCenter.house.data.TempDisplay;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistPlayer.READING_MODE;
import javafx.application.Platform;
import server.client.TransferDataClientRequest;
import server.communication.Request;
import server.communication.TransferData;
import server.dispatch.SocketMethod;
import server.dispatch.SocketParameter;
import server.dispatch.TransferDataRequest;
import utils.Compresser;
import utils.FileUtils;
import utils.HouseUtils;

public class ClientRequestManager {

	private RoomManager manager;
	private CommunicationCenter center;

	public ClientRequestManager(RoomManager manager, CommunicationCenter center) {
		if (manager == null || center == null) {
			throw new NullPointerException("Null manager | center | bridge");
		}
		this.manager = manager;
		this.center = center;
	}

	@SocketMethod(flag = REQUEST_IDENTIFICATION_DATA)
	public void getIdentificationData(TransferDataClientRequest request) {

		while (center == null) {
			// Happens when object finishes construction after connection.
			// Handle by waiting.
			System.out.println("Infinite ?");
		}
		if (ClientParameters.getParams().getUUID() == null) {
			ClientParameters.getParams().setUUID(HouseUtils.getDeviceUUID());
		}
		System.out.println(center);
		request.respond(REQUEST_IDENTIFICATION_DATA,
				new Object[] { ClientParameters.getParams().getUUID(), TransferFlags.ROOM_CONTROLLER });
		System.out.println("UUID sent  : " + Arrays.toString(HouseUtils.getDeviceUUID()));
	}

	@SocketMethod(flag = REQUEST_DEVICE_TYPE)
	public void getDeviceType(TransferDataClientRequest request) {
		request.respond(ROOM_CONTROLLER);

	}

	@SocketMethod(flag = SET_TEMP)
	public void setRoomTemp(@SocketParameter double data, TransferDataClientRequest request) {
		manager.updateTemp(data);
	}

	@SocketMethod(flag = REQUEST_MUSIC_STATUS)
	public void getMusicStatic(@SocketParameter Object[] data, TransferDataClientRequest request) {
		Platform.runLater(() -> {

			MusicStatus status = null;

			status = manager.getMusicStatus();
			// Send the status to the server, which will redirect the data
			// to
			// the client, which's id is "obect"
			request.respond(new Object[] { status, data[0] });
		});
	}

	@SocketMethod(flag = NEXT_SONG)
	public void nextSong(TransferDataClientRequest request) {
		Platform.runLater(() -> manager.nextSong());

	}

	@SocketMethod(flag = PREVIOUS_SONG)
	public void previousSong(TransferDataClientRequest request) {
		Platform.runLater(() -> manager.previousSong());
	}

	@SocketMethod(flag = SWITCH_PLAYING)
	public void switchPlaying(TransferDataClientRequest request) {
		Platform.runLater(() -> manager.switchPlaying());
	}

	@SocketMethod(flag = REQUEST_MUSIC_READING_MODE)
	public void getMusicReadingMode(@SocketParameter UUID toID, TransferDataClientRequest request) {

		request.respond(new Object[] { manager.getReadingMode(), toID });
	}

	@SocketMethod(flag = SET_READING_MODE)
	public void setReadingMode(@SocketParameter READING_MODE mode, TransferDataClientRequest request) {
		Platform.runLater(() -> manager.setReadingMode(mode));
	}

	@SocketMethod(flag = SET_ROOM_VOLUME)
	public void setRoomVolume(@SocketParameter double volume, TransferDataClientRequest request) {
		manager.setVolume(volume);
	}

	@SocketMethod(flag = UPLOAD_NEW_MUSIC)
	public void uploadMusic(@SocketParameter Object[] data, TransferDataClientRequest request) {
		String name = (String) data[0];
		final byte[] b = (byte[]) data[1];
		manager.uploadMusic(name, Compresser.uncompress(b));
	}

	@SocketMethod(flag = SET_ROOM_NAME)
	public void setRoomName(@SocketParameter String name, TransferDataClientRequest request) {
		ClientParameters.getParams().setRoomName(name);
	}

	@SocketMethod(flag = REQUEST_ROOM_NAME)
	public void getRoomName(TransferDataClientRequest request) {
		request.respond(ClientParameters.getParams().getRoomName());
	}

	@SocketMethod(flag = REQUEST_TEMP_DISPLAY)
	public void getTempDisplay(TransferDataClientRequest request) {
		request.respond(ClientParameters.getParams().getTemp());
	}

	@SocketMethod(flag = SET_TEMP_DISPLAY)
	public void setTempDisplay(@SocketParameter TempDisplay display, TransferDataClientRequest request) {
		ClientParameters.getParams().setTempDisplay(display);
	}

	@SocketMethod(flag = REQUEST_LANGUAGE)
	public void getLanguage(TransferDataClientRequest request) {
		request.respond(ClientParameters.getParams().getLanguage());
	}

	@SocketMethod(flag = SET_LANGUAGE)
	public void setLanguage(@SocketParameter Language language, TransferDataClientRequest request) {
		manager.loadLanguage(language);
	}

	@SocketMethod(flag = ALARM_RINGING)
	public void setLanguage(@SocketParameter Alarm a, TransferDataClientRequest request) {
		manager.alarmRinging(a);
	}

	@SocketMethod(flag = SET_PLAYING)
	public void setLanguage(@SocketParameter boolean playing, TransferDataClientRequest request) {

		Platform.runLater(() -> {

			if (playing) {
				manager.playMusic();
			} else {
				manager.pauseMusic();

			}
		});
	}

	@SocketMethod(flag = FORCE_PREVIOUS_MUSIC)
	public void forcePreviousMusic(TransferDataClientRequest request) {
		Platform.runLater(() -> manager.forcePrevious());

	}

	public void receive(int flag, Object data) {
		if (flag == FORCE_PREVIOUS_MUSIC) {
		} else if (flag == PLAY_AI_ANSWER) {
			String file = "temp.wav";
			byte[] bytes = (byte[]) data;

			try {
				FileUtils.writeBytesToFile(bytes, file);
				manager.playAiAnswer(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (flag == SET_LANGUAGE) {
			Language l = (Language) data;
			ClientParameters.getParams().setLanguage(l);
		} else if (flag == SET_TEMP_DISPLAY) {
			TempDisplay d = (TempDisplay) data;
			ClientParameters.getParams().setTempDisplay(d);
		}

	}

}
