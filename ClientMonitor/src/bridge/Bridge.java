package bridge;

import custom.CommunicationCenter;
import house.AlarmRingingDecisionListener;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;

public interface Bridge {

	public void dataReceived(int flag, Object data);

	public void init(CommunicationCenter center, PlaylistPlayer player, PlaylistManager man);

	public void displayAllSongs();

	public void languageChanged();

	public void displayRoomController();

	public void displayAlarmRinging(Alarm a, AlarmRingingDecisionListener listener);

	public void volumeChanged();

}
