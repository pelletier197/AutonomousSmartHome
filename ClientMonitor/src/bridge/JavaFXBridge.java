package bridge;

import java.util.Set;

import custom.CommunicationCenter;
import house.AlarmRingingDecisionListener;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import houseMonitorControllers.control.AlarmRingingController;
import houseMonitorControllers.musicPlayer.MusicEngine;
import houseMonitorControllers.musicPlayer.MusicPlayingController;
import houseMonitorControllers.musicPlayer.ResearchListViewController;
import javafx.application.Platform;
import javafx.stage.PopupWindow.AnchorLocation;
import server.transport.Communicator;
import views.screenController.Screen;
import views.screenController.ScreenController;
import views.screenController.ScreenController.Animations;

public class JavaFXBridge implements Bridge {

	private ScreenController menu;

	public JavaFXBridge(ScreenController menu) {

		if (menu == null) {
			throw new NullPointerException();
		}

		this.menu = menu;

	}

	@Override
	public void dataReceived(int flag, Object data) {

		Object controller = menu.getController(menu.getCurrentScreen());

		// Notifies the current screen of data reception
		if (controller instanceof ReturnCodeListener) {
			Platform.runLater(() -> ((ReturnCodeListener) controller).returnCodeReceived(flag, data));
		}
	}

	@Override
	public void init(CommunicationCenter center, PlaylistPlayer player, PlaylistManager man) {
		menu.addScreenChangedListener((old, newv) -> {

			System.out.println("CHange screen listener");

			Object controller = menu.getController(newv);

			if (controller instanceof Communicator) {
				((Communicator) menu.getController(newv)).setCommunicationCenter(center);
			}

			if (controller instanceof MusicEngine) {
				((MusicEngine) controller).setPlayers(man, player);
			}

		});
	}

	@Override
	public void displayAllSongs() {

		Object contr = menu.getController(Screen.MUSIC_LIST_VIEW);

		if (contr != null) {
			((ResearchListViewController) contr).displayAllSongs();
		}
	}

	@Override
	public void languageChanged() {
		// Run the changes in the FXApplication thread.
		Platform.runLater(() -> {
			Set<Screen> screens = menu.getLoadedScreens();
			Object currentController = null;

			for (Screen sc : screens) {
				currentController = menu.getController(sc);

				if (currentController != null && currentController instanceof Universalisable) {
					((Universalisable) currentController).languageChanged();
				}
			}
		});
	}

	@Override
	public void displayRoomController() {
		menu.setScreen(Screen.ROOM_INFO, Animations.FADE_IN);
	}

	@Override
	public void displayAlarmRinging(Alarm a, AlarmRingingDecisionListener listener) {
		menu.setScreen(Screen.ALARM_RINGING, Animations.FADE_IN);
		AlarmRingingController contr = (AlarmRingingController) menu.getController(Screen.ALARM_RINGING);
		contr.setAlarm(a, listener);
	}
 
	@Override
	public void volumeChanged() {
		// Run the changes in the FXApplication thread.
				Platform.runLater(() -> {
					Set<Screen> screens = menu.getLoadedScreens();
					Object currentController = null;

					for (Screen sc : screens) {
						currentController = menu.getController(sc);

						if (currentController != null && currentController instanceof MusicEngine) {
							((MusicEngine) currentController).playerVolumeChanged();
						}
					}
				});		
	}

}
