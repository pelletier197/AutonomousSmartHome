package houseMonitorViews.musicPlayer;

import java.util.Collection;
import java.util.List;

import houseDataCenter.mp3Player.Mp3SongInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CheckableMp3Info {
	
	public boolean checked;
	public final Mp3SongInfo song;

	public CheckableMp3Info(Mp3SongInfo song) {
		this.song = song;
		this.checked = false;
		
	}
	
	public static ObservableList<CheckableMp3Info> generateCheckForSongs(Collection<Mp3SongInfo> songs){
		ObservableList<CheckableMp3Info> ch = FXCollections.observableArrayList();
		
		for(Mp3SongInfo song : songs){
			ch.add(new CheckableMp3Info(song));
		}
		return ch;
	}
	

}
