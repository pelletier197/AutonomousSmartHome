package houseMonitorViews.musicPlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import houseDataCenter.mp3Player.AlbumInfo;
import houseDataCenter.mp3Player.ArtistInfo;
import houseMonitorControllers.musicPlayer.AlbumViewController;
import houseMonitorControllers.musicPlayer.ArtistViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class ArtistListView extends ListView<ArtistInfo> {
	public ArtistListView() {
		this(new ArrayList<>());
	}

	public ArtistListView(List<ArtistInfo> artists) {
		if (artists == null) {
			throw new NullPointerException();
		}

		this.getStyleClass().add("music-list-view");
		this.setMinWidth(300);

		this.setCellFactory(new Callback<ListView<ArtistInfo>, ListCell<ArtistInfo>>() {

			@Override
			public ListCell<ArtistInfo> call(ListView<ArtistInfo> param) {
				return new ArtistCellFactory();
			}
		});
	}

	public void setSeenArtists(Collection<ArtistInfo> items) {
		this.getItems().setAll(items);
	}

	private class ArtistCellFactory extends ListCell<ArtistInfo> {

		@Override
		protected void updateItem(ArtistInfo item, boolean empty) {
			super.updateItem(item, empty);

			if (!empty && item != null) {
				FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/houseMonitorViews/musicPlayer/ArtistView.fxml"));
				try {
					this.setGraphic(loader.load());
				} catch (IOException e) {
					e.printStackTrace();
				}
				((ArtistViewController) loader.getController()).setArtistInfo(item);
			} else {
				setGraphic(null);
			}
		}

	}
}
