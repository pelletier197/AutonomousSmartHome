package houseMonitorViews.musicPlayer;

import java.util.Collection;
import java.util.List;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.Mp3SongInfo;
import javafx.collections.FXCollections;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.util.Callback;
import utils.Formatters;

public class MusicListView extends ListView<Mp3SongInfo> {

	public MusicListView(Collection<Mp3SongInfo> songs) {
		if (songs == null) {
			throw new NullPointerException();
		}

		this.getStyleClass().add("music-list-view");
		this.setMinWidth(300);
		setSeenMusics(songs);

		this.setCellFactory(new Callback<ListView<Mp3SongInfo>, ListCell<Mp3SongInfo>>() {

			@Override
			public ListCell<Mp3SongInfo> call(ListView<Mp3SongInfo> param) {
				// TODO Auto-generated method stub
				return new MusicCellFactory();
			}
		});
	}

	public void setSeenMusics(Collection<Mp3SongInfo> items) {
		this.getItems().setAll(items);
	}

	private class MusicCellFactory extends ListCell<Mp3SongInfo> {

		@Override
		protected void updateItem(Mp3SongInfo item, boolean empty) {
			super.updateItem(item, empty);

			if (!empty && item != null) {

				String artist = item.getArtistName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownArtist)
						: item.getArtistName();

				String album = item.getAlbumName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
						: item.getAlbumName();

				this.getStyleClass().add("music-list-cell");

				AnchorPane pane = new AnchorPane();
				pane.getStyleClass().add("list-cell-background");
				Label song = new Label(item.getTitle());
				AnchorPane.setRightAnchor(song, 150d);
				song.getStyleClass().add("list-cell-songNameLabel");
				Label ArtistAlbum = new Label(artist + " - " + album);

				ArtistAlbum.setMaxWidth(200);
				ArtistAlbum.setFont(new Font(10));
				Label time = new Label(Formatters.formatTime(item.getLength()));
				time.setPrefWidth(Control.USE_COMPUTED_SIZE);

				pane.getChildren().addAll(song, ArtistAlbum, time);
				AnchorPane.setTopAnchor(song, (double) 0);
				AnchorPane.setLeftAnchor(song, (double) 10);

				AnchorPane.setBottomAnchor(ArtistAlbum, (double) 0);
				AnchorPane.setLeftAnchor(ArtistAlbum, (double) 10);

				AnchorPane.setBottomAnchor(time, (double) 0);
				AnchorPane.setRightAnchor(time, (double) 15);

				pane.setPrefHeight(40);

				pane.setMinWidth(300);

				setGraphic(pane);
			} else {
				setGraphic(null);
			}
		}

	}

}
