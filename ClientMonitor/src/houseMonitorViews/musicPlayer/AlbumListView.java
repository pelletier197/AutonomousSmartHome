package houseMonitorViews.musicPlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import houseDataCenter.mp3Player.Album;
import houseDataCenter.mp3Player.AlbumInfo;
import houseMonitorControllers.musicPlayer.AlbumViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class AlbumListView extends ListView<AlbumInfo> {
	public AlbumListView() {
		this(new ArrayList<>());
	}

	public AlbumListView(List<AlbumInfo> albums) {
		if (albums == null) {
			throw new NullPointerException();
		}

		this.getStyleClass().add("music-list-view");
		this.setMinWidth(300);

		this.setCellFactory(new Callback<ListView<AlbumInfo>, ListCell<AlbumInfo>>() {

			@Override
			public ListCell<AlbumInfo> call(ListView<AlbumInfo> param) {
				return new AlbumCellFactory();
			}
		});
	}

	public void setSeenAlbum(Collection<AlbumInfo> items) {
		this.getItems().setAll(items);
	}

	private class AlbumCellFactory extends ListCell<AlbumInfo> {

		@Override
		protected void updateItem(AlbumInfo item, boolean empty) {
			super.updateItem(item, empty);

			if (!empty && item != null) {
				FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/houseMonitorViews/musicPlayer/AlbumView.fxml"));
				try {
					this.setGraphic(loader.load());
				} catch (IOException e) {
					e.printStackTrace();
				}
				((AlbumViewController) loader.getController()).setAlbumInfo(item);
			} else {
				setGraphic(null);
			}
		}

	}
}
