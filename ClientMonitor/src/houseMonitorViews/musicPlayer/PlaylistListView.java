package houseMonitorViews.musicPlayer;

import java.util.Collection;
import java.util.List;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.Playlist;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.util.Callback;
import utils.Formatters;

public class PlaylistListView extends ListView<Playlist> {

	public PlaylistListView(Collection<Playlist> playlist) {
		if (playlist == null) {
			throw new NullPointerException();
		}
		this.getItems().addAll(playlist);

		this.getStyleClass().add("music-list-view");
		this.setMinWidth(300);

		this.setCellFactory(new Callback<ListView<Playlist>, ListCell<Playlist>>() {

			@Override
			public ListCell<Playlist> call(ListView<Playlist> param) {
				// TODO Auto-generated method stub
				return new PlaylistCellFactory();
			}
		});
	}

	private class PlaylistCellFactory extends ListCell<Playlist> {

		@Override
		protected void updateItem(Playlist item, boolean empty) {
			super.updateItem(item, empty);
			
			if (!empty && item != null) {
				
				this.getStyleClass().add("music-list-cell");
				
				AnchorPane pane = new AnchorPane();
				pane.getStyleClass().add("list-cell-background");
				Label name = new Label(item.getName());
				name.setMaxWidth(280);
				name.getStyleClass().add("list-cell-songNameLabel");
				Label numberOfSongs = new Label(
						item.getNumberOfSongs() + " " + BundleWrapper.getString(BundleKey.songs));

				numberOfSongs.setMaxWidth(200);
				numberOfSongs.setFont(new Font(10));
				Label time = new Label(Formatters.formatTime(item.getLength()));
				time.setPrefWidth(Control.USE_COMPUTED_SIZE);

				pane.getChildren().addAll(name, numberOfSongs, time);
				AnchorPane.setTopAnchor(name, (double) 0);
				AnchorPane.setLeftAnchor(name, (double) 10);

				AnchorPane.setBottomAnchor(numberOfSongs, (double) 0);
				AnchorPane.setLeftAnchor(numberOfSongs, (double) 10);

				AnchorPane.setBottomAnchor(time, (double) 0);
				AnchorPane.setRightAnchor(time, (double) 15);

				pane.setPrefHeight(40);

				pane.setMinWidth(300);

				setGraphic(pane);
			} else {
				setGraphic(null);
			}
		}
	}
}
