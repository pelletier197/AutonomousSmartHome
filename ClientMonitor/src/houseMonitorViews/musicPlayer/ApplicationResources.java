package houseMonitorViews.musicPlayer;

import javafx.scene.image.Image;

public class ApplicationResources {

	public static final Image[] SONG_IMAGES = {
			
			new Image(ApplicationResources.class.getResource("/resources/images/music.png").toExternalForm()),
			new Image(ApplicationResources.class.getResource("/resources/images/music.jpg").toExternalForm())
	};

	public static Image chooseRandomImage(Image[] images) {
		return images[(int) (Math.random() * images.length)];
	}

}
