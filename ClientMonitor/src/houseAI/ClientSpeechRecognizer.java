package houseAI;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import custom.CommunicationCenter;
import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.Microphone;
import edu.cmu.sphinx.api.SpeechSourceProvider;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import server.transport.ServerOfflineException;

public class ClientSpeechRecognizer {

	private CommunicationCenter com;

	private boolean stopped;

	private boolean running;

	private Microphone mic;

	public ClientSpeechRecognizer(CommunicationCenter communicator) {

		if (communicator == null) {
			// throw new NullPointerException("Null communicator");
		}

		mic = new Microphone(16000, 16, true, false);
		// Init the configuration from the language property defined in the file

		this.com = communicator;
		stopped = false;
		running = false;

	}

	public void startRecording() {
		if (!running) {

			stopped = false;

			Thread th = new Thread(() -> {

				mic.startRecording();
				InputStream micstream = mic.getStream();
				byte[] input = null;

				while (!stopped) {

					try {
						// Big instances of array to be sure we send the most
						// mic data at the same time. This will enforce
						// recognition rate.
						input = new byte[64_000];

						try {
							micstream.read(input);
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println("still capturing audio");
						com.send(TransferFlags.AUDIO_DATA_INPUT, input);

					} catch (ServerOfflineException e2) {
						// Server is offline, so just do nothing
						e2.printStackTrace();
					}

				}

			});
			th.start();
			running = true;
		}

	}

	public void stopRecording() {
		stopped = true;
		mic.stopRecording();

	}

}
