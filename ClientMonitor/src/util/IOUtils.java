package util;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import house.RoomParams;
import utils.FileUtils;

public class IOUtils {

	private static final String CLIENT_PARAMS_PATH = "/config/client_parameters.config";

	public static void writeClientParameters(RoomParams params) {
		try {
			writeObject(params, new File(FileUtils.getAppDataDirectory() + CLIENT_PARAMS_PATH));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static RoomParams readClientParameters() {
		try {
			return (RoomParams) readObject(new File(FileUtils.getAppDataDirectory() + CLIENT_PARAMS_PATH));

		} catch (EOFException e) {

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		return null;
	}

	/**
	 * 
	 * @param obj
	 * @param path
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void writeObject(Object obj, File f) throws FileNotFoundException, IOException {

		if (!f.exists()) {
			// Creates dir for parent
			File parent = f.getParentFile();

			if (!parent.exists()) {
				parent.mkdirs();
			}

			// Creates file
			f.createNewFile();
		}
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(f));

		stream.writeObject(obj);

		stream.flush();
		stream.close();
	}

	/**
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static Object readObject(File f) throws IOException, EOFException, ClassNotFoundException {
		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(f));

		Object read = stream.readObject();

		System.out.println(read.getClass());

		stream.close();

		return read;

	}

}
