package main;

import utils.GrammarUtils;
import utils.Mp3Utils;

import java.io.File;
import java.util.List;

import houseDataCenter.mp3Player.Mp3SongInfo;;

public class Tester {

	public static void main(String[] args) {
		List<Mp3SongInfo> songs = Mp3Utils
				.getMP3FromRepository(new File(Tester.class.getResource("/music/").getFile()));

		GrammarUtils.writeMusicGrammar(songs, "test.gram");

		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
