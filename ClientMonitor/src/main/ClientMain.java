package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import bridge.JavaFXBridge;
import contextMenu.ContextMenu;
import contextMenu.MenuItem;
import house.RoomManager;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseMonitorControllers.musicPlayer.MusicSelectorController;
import javafx.application.Application;
import javafx.css.Styleable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;
import utils.Mp3Utils;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class ClientMain extends Application {

	private RoomManager manager;
	private ScreenController menu;
	private ContextMenu mainPane;
 
	public static void main(String[] args) {
		launch();

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// Loads the screen controller
		menu = new ScreenController();
		menu.setScreen(Screen.CALCULATOR);

		// Loads the room manager
		this.manager = new RoomManager(new JavaFXBridge(menu));

		loadUI();

		Scene scene = new Scene(mainPane.getView());
		addAppStyleSheets(scene);
		primaryStage.setScene(scene);

		System.out.println("I AM : " + InetAddress.getLocalHost());

		primaryStage.show();

	}
	
	public static void addAppStyleSheets(Alert a){
		DialogPane dialogPane = a.getDialogPane();
		dialogPane.getStylesheets().add("/resources/styles/DarkTheme.css");
		dialogPane.getStylesheets().add("/resources/styles/custom.css");
		dialogPane.setStyle("-fx-font-size : 18px;");
	}

	private static void addAppStyleSheets(Scene n) {
		n.getStylesheets().add("/resources/styles/DarkTheme.css");
		n.getStylesheets().add("/resources/styles/custom.css");
	}

	private void loadUI() {
		loadMenu();
	}

	private void loadMenu() {

		// Loads main pane containing the menu
		mainPane = new ContextMenu();
		mainPane.setCenter(menu);
		mainPane.getView().getStylesheets().add("/resources/styles/context-menu.css");
		mainPane.setIconPath(getClass().getResource("/resources/images/menu-icon.png").toExternalForm());

		mainPane.addMenuItem(new MenuItem("Convertisseur", toResourcePath("/resources/images/convert.gif"), () -> {
			menu.setScreen(Screen.CONVERTER);
			mainPane.contract(true);
		}));

		mainPane.addMenuItem(new MenuItem("Musique", toResourcePath("/resources/images/songIcon.png"), () -> {
			menu.setScreen(Screen.MUSIC_LIST_VIEW);
			mainPane.contract(true);
		}));

		mainPane.addMenuItem(new MenuItem("Calculatrice", toResourcePath("/resources/images/calculator.gif"), () -> {
			menu.setScreen(Screen.CALCULATOR);
			mainPane.contract(true);
		}));

		mainPane.addMenuItem(new MenuItem("Résidents", toResourcePath("/resources/images/residents_icon.gif"), () -> {
			menu.setScreen(Screen.RESIDENT_MODIFICATION);
			mainPane.contract(true);
		}));

		mainPane.addMenuItem(new MenuItem("Pièce", toResourcePath("/resources/images/room.gif"), () -> {
			menu.setScreen(Screen.ROOM_INFO);
			mainPane.contract(true);
		}));
	}

	private String toResourcePath(String base) {
		return ClientMain.class.getResource(base).toExternalForm();
	}

}
