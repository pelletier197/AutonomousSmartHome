package houseMonitorControllers;

public interface ReturnCodeListener {

	public void returnCodeReceived(int flag, Object data);
	
	
}
