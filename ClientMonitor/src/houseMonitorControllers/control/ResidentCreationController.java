package houseMonitorControllers.control;

import static houseDataCenter.commucationData.TransferFlags.FAILURE_ADD_EMAIL_USED;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_BORNDATE;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_MAIL;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_NAME;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_PICTURES;
import static houseDataCenter.commucationData.TransferFlags.FAILURE_CREATION_INVALID_PIN;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import com.github.sarxos.webcam.Webcam;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.residents.Resident;
import houseDataCenter.house.residents.ResidentBuilder;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import server.transport.Communicator;
import utils.HouseUtils;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class ResidentCreationController
		implements Universalisable, ReturnCodeListener, ControlledScreen, Initializable, Communicator {

	@FXML
	private ImageView webcamView;

	private ScreenController controller;

	private CommunicationCenter com;

	private ResidentBuilder builder;

	@FXML
	private Label nameLabel;

	@FXML
	private TextField name;

	@FXML
	private DatePicker date;

	@FXML
	private Label bornLabel;

	@FXML
	private Label failureReason;

	@FXML
	private TextField mail;

	@FXML
	private Label emailLabel;

	@FXML
	private PasswordField nip;

	@FXML
	private Label NIPLabel;

	@FXML
	private PasswordField nipConfirm;

	@FXML
	private Label confirmNIPLabel;

	private static final String INVALID_STYLE = "-fx-background-color:red;-fx-opacity:0.8;";

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		languageChanged();

		builder = new ResidentBuilder();
	}

	@FXML
	private void finish(ActionEvent event) {

		setAllNormalStyle();

		builder.setName(name.getText());
		builder.setBornDate(HouseUtils.localDateToMillis(date.getValue()));
		builder.setEmail(mail.getText());
		builder.setPIN(nip.getText());
		builder.setPINConfirm(nipConfirm.getText());

		// The builder is reused. It has to be sent as a new object every time.
		com.sendUnshared(TransferFlags.REQUEST_RESIDENT_CREATION, builder);

		System.out.println("Sent to server : " + builder);

	}

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	private void setAllNormalStyle() {
		Node[] views = new Node[] { name, date, mail, nip, nipConfirm };

		for (Node n : views) {
			n.setOpacity(1);
			n.setStyle("");
		}
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {

		builder.clear();
		clearViews();
		setCalendarDates();

	}

	private void setCalendarDates() {

		final LocalDate now = LocalDate.now();

		final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
			@Override
			public DateCell call(final DatePicker datePicker) {
				return new DateCell() {
					@Override
					public void updateItem(LocalDate item, boolean empty) {
						super.updateItem(item, empty);
						if (item.isAfter(now)) {
							setDisable(true);
							setStyle("-fx-background-color: #ffc0cb;");
						}
					}
				};
			}
		};
		date.setDayCellFactory(dayCellFactory);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	private void clearViews() {
		name.clear();
		mail.clear();
		date.setValue(LocalDate.now());
		nip.clear();
		nipConfirm.clear();
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void returnCodeReceived(int flag, Object data) {
		System.out.println("Return code on controller : " + flag);

		if (flag == FAILURE_CREATION_INVALID_BORNDATE) {
			date.setStyle(INVALID_STYLE);
			failureReason.setText(BundleWrapper.getString(BundleKey.creation_failure_borndate));

		} else if (flag == FAILURE_CREATION_INVALID_MAIL) {
			mail.setStyle(INVALID_STYLE);
			failureReason.setText(BundleWrapper.getString(BundleKey.creation_failure_mail));

		} else if (flag == FAILURE_CREATION_INVALID_NAME) {
			name.setStyle(INVALID_STYLE);
			failureReason.setText(BundleWrapper.getString(BundleKey.creation_failure_name));

		} else if (flag == FAILURE_CREATION_INVALID_PIN) {
			nip.setStyle(INVALID_STYLE);
			nipConfirm.setStyle(INVALID_STYLE);

			failureReason.setText(BundleWrapper.getString(BundleKey.creation_failure_pin));

		} else if (flag == FAILURE_ADD_EMAIL_USED) {
			mail.setStyle(INVALID_STYLE);

			failureReason.setText(BundleWrapper.getString(BundleKey.creation_failure_mail_used));

		} else if (flag == TransferFlags.REQUEST_MAIL_CONFIRMATION_CODE
				|| flag == TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION) {
			controller.setScreen(Screen.RESIDENT_CONFIRMATION);

			ResidentConfirmationController contr = (ResidentConfirmationController) controller
					.getController(Screen.RESIDENT_CONFIRMATION);

			contr.returnCodeReceived(flag, null);
		}
	}

	@Override
	public void languageChanged() {
		emailLabel.setText(BundleWrapper.getString(BundleKey.mail));
		NIPLabel.setText(BundleWrapper.getString(BundleKey.pin));
		nipConfirm.setText(BundleWrapper.getString(BundleKey.pin_confirm));
		bornLabel.setText(BundleWrapper.getString(BundleKey.born_date_small));
		nameLabel.setText(BundleWrapper.getString(BundleKey.name));
	}

}
