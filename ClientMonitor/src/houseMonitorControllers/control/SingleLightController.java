package houseMonitorControllers.control;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.LightData;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import houseMonitorControllers.musicPlayer.MusicPlayingController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import server.communication.Request;
import server.transport.Communicator;
import views.screenController.ControlledScreen;
import views.screenController.ScreenController;

public class SingleLightController
		implements Universalisable, Initializable, ControlledScreen, Communicator, ReturnCodeListener {

	@FXML
	private ListView<LightData> lightListView;

	@FXML
	private ImageView lightIcon;

	@FXML
	private TextField lightName;

	@FXML
	private ToggleButton lightSwitch;

	@FXML
	private ColorPicker lightColor;

	@FXML
	private Slider lightBrightness;

	private static final Image OPEN = new Image(
			MusicPlayingController.class.getResource("/resources/images/light_icon_open.png").toExternalForm());
	private static final Image CLOSE = new Image(
			MusicPlayingController.class.getResource("/resources/images/light_icon_close.png").toExternalForm());

	private ScreenController controller;
	private CommunicationCenter com;

	private LightData selectedLight;

	@FXML
	private void switchLight(ActionEvent event) {

		int flag = selectedLight.isOpen() ? TransferFlags.CLOSE_LIGHT : TransferFlags.OPEN_LIGHT;
		com.send(flag, selectedLight.getId());

		// The open light status changed
		selectedLight.updateOpen(!selectedLight.isOpen());

		updateSelectedLight();
	}

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@FXML
	private void saveName(ActionEvent event) {

		com.send(new Request(TransferFlags.SET_LIGHT_NAME,
				new Object[] { selectedLight.getId(), lightName.getText() }));

		// Updates the name in the listview
		selectedLight.updateName(lightName.getText());
		LightData saveSelected = selectedLight;
		setLights(lightListView.getItems());
		this.selectedLight = saveSelected;
		lightListView.getSelectionModel().select(selectedLight);
	}

	private void changeOpenImage() {

		lightIcon.setImage(selectedLight.isOpen() ? OPEN : CLOSE);
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		setAll(true);
		com.send(TransferFlags.REQUEST_ROOM_LIGHTS);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.lightListView.setCellFactory(new Callback<ListView<LightData>, ListCell<LightData>>() {

			@Override
			public ListCell<LightData> call(ListView<LightData> param) {
				// TODO Auto-generated method stub
				return new ListCell<LightData>() {

					@Override
					protected void updateItem(LightData data, boolean empty) {
						super.updateItem(data, empty);

						if (data != null && !empty) {

							String name = data.getName() != null ? data.getName()
									: BundleWrapper.getString(BundleKey.no_name_light);
							Label l = new Label(name);
							l.setFont(Font.font("system", FontWeight.BOLD, 20));

							setGraphic(l);
						} else {
							setGraphic(null);
						}
					}

				};
			}
		});

		this.lightListView.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {

			if (old != null && newv != old) {
				old.updateName(lightName.getText());
			}

			this.selectedLight = newv;
			if (newv != null) {
				updateSelectedLight();
			}

		});

		this.lightColor.valueProperty().addListener((value, old, newv) -> {

			int color = 0;
			color |= (int) (newv.getBlue() * 255);
			color |= (((int) (newv.getGreen() * 255)) << 8);
			color |= (((int) (newv.getRed() * 255)) << 16);

			com.send(TransferFlags.SET_LIGHT_COLOR, new Object[] { selectedLight.getId(), color });
			selectedLight.updateColor(color);
		});

		this.lightBrightness.valueChangingProperty().addListener((value, old, newv) -> {

			// stopped changing
			if (!newv) {
				com.send(TransferFlags.SET_LIGHT_LUMINOSITY,
						new Object[] { selectedLight.getId(), lightBrightness.getValue() });

			}
		});
	}

	private void updateSelectedLight() {

		int color = selectedLight.getColor();
		int b = color & 0xff;
		int g = (color >> 8) & 0xff;
		int r = (color >> 16) & 0xff;

		lightColor.setValue(Color.rgb(r, g, b, 1));
		lightColor.setDisable(!(selectedLight.isColorisable() && selectedLight.isOpen()));

		this.lightName.setText(selectedLight.getName());

		this.lightBrightness.setValue(selectedLight.getLuminosity());
		lightBrightness.setDisable(!(selectedLight.isDimmable() && selectedLight.isOpen()));

		lightSwitch.setSelected(selectedLight.isOpen());
		changeOpenImage();
	}

	private void setAll(boolean disable) {

		lightSwitch.setDisable(disable);
		lightName.setDisable(disable);
		lightBrightness.setDisable(disable);
		lightColor.setDisable(disable);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void returnCodeReceived(int flag, Object data) {

		if (flag == TransferFlags.REQUEST_ROOM_LIGHTS) {
			setLights((List<LightData>) data);
		} else if (flag == TransferFlags.LIGHT_STATE_CHANGED) {
			LightData dat = (LightData) data;
			if (selectedLight != null) {
				selectedLight = dat;
				if (dat.getId().equals(selectedLight.getId())) {
					updateSelectedLight();
				}
			}
		}
	}

	private void setLights(List<LightData> data) {
		this.lightListView.setItems(null);
		this.lightListView.setItems(FXCollections.observableArrayList(data));
		if (!data.isEmpty()) {
			lightListView.getSelectionModel().select(0);
			setAll(false);
		}
	}

	@Override
	public void languageChanged() {
		// TODO Auto-generated method stub

	}

}
