
package houseMonitorControllers.control;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.alarm.Tone;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistManager;
import houseMonitorControllers.Universalisable;
import houseMonitorControllers.musicPlayer.MusicSelectorController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import server.transport.Communicator;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import src.main.java.com.mpatric.mp3agic.InvalidDataException;
import src.main.java.com.mpatric.mp3agic.UnsupportedTagException;
import utils.Formatters;
import utils.Mp3Utils;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class EditAlarmController implements Initializable, Universalisable, ControlledScreen, Communicator {

	@FXML
	private Spinner<Integer> hour;

	@FXML
	private Spinner<Integer> minute;

	@FXML
	private Label timeLabel;

	@FXML
	private Label selectedMusicName;

	@FXML
	private ToggleButton mon;

	@FXML
	private ToggleButton tue;

	@FXML
	private ToggleButton wed;

	@FXML
	private ToggleButton thu;

	@FXML
	private ToggleButton fri;

	@FXML
	private ToggleButton sat;

	@FXML
	private ToggleButton sun;

	@FXML
	private CheckBox all;

	@FXML
	private Button music;

	@FXML
	private Label volumeLabel;

	@FXML
	private Slider volume;

	@FXML
	private Label percentVolume;

	private ToggleButton[] buttons;

	@FXML
	private RadioButton am;

	@FXML
	private RadioButton pm;

	@FXML
	private Label or;

	@FXML
	private Button alarm;

	private Alarm alarmGenerated;

	private ScreenController controller;

	private Mp3SongInfo selectedTone;

	private Mp3SongInfo defaultTone;

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@FXML
	private void changeAlarm(ActionEvent event) {
		settingMusic = true;
		controller.setScreen(Screen.MUSIC_SELECTOR);

		MusicSelectorController contr = (MusicSelectorController) controller.getController(Screen.MUSIC_SELECTOR);

		PlaylistManager man = new PlaylistManager(loadDefaultAlarms());

		contr.setPlaylistManager(man);

		contr.onSongSetListener(() -> {
			setSong(contr.getSelectedSong());
		});
	}

	private List<Mp3SongInfo> defaultAlarms;

	private CommunicationCenter com;

	private boolean settingMusic;

	private List<Mp3SongInfo> loadDefaultAlarms() {
		if (defaultAlarms == null) {
			defaultAlarms = Mp3Utils.getMP3FromRepository(new File(getClass().getResource("/alarms/").getFile()));
		}

		return defaultAlarms;
	}

	@FXML
	private void changeMusic(ActionEvent event) {
		settingMusic = true;
		controller.setScreen(Screen.MUSIC_SELECTOR);
		MusicSelectorController contr = (MusicSelectorController) controller.getController(Screen.MUSIC_SELECTOR);

		contr.onSongSetListener(() -> {
			setSong(contr.getSelectedSong());
		});
	}

	private void setSong(Mp3SongInfo selectedSong) {
		this.selectedTone = selectedSong;
		selectedMusicName.setText(selectedSong.getTitle());
	}

	@FXML
	private void save(ActionEvent event) {

		if (selectedTone == null) {
			selectedTone = defaultTone;
		}

		Tone t = new Tone(selectedTone.getPath(), volume.getValue());

		if (alarmGenerated == null) {
			// New alarm
			alarmGenerated = new Alarm(getRealHourTime(), minute.getValue(), t);
			alarmGenerated.setDays(getSelctedDaysOfWeek());
			com.send(TransferFlags.CREATE_ALARM, alarmGenerated);
			System.out.println("Creating alarms : " + alarmGenerated);
		} else {
			// Update alarm
			alarmGenerated.setNextAlarmTime(getRealHourTime(), minute.getValue());
			alarmGenerated.setTone(t);
			alarmGenerated.setDays(getSelctedDaysOfWeek());
			com.send(TransferFlags.UPDATE_ALARM, alarmGenerated);

			System.out.println("Updating alarms : " + alarmGenerated);
		}

		controller.setPreviousScreen();

	}

	private List<AlarmDay> getSelctedDaysOfWeek() {

		List<AlarmDay> days = new ArrayList<>();

		if (all.isSelected()) {
			days.add(AlarmDay.ALL);
		} else {
			if (mon.isSelected())
				days.add(AlarmDay.MONDAY);
			if (tue.isSelected())
				days.add(AlarmDay.TUESDAY);
			if (wed.isSelected())
				days.add(AlarmDay.WEDNESDAY);
			if (thu.isSelected())
				days.add(AlarmDay.THURSDAY);
			if (fri.isSelected())
				days.add(AlarmDay.FRIDAY);
			if (sat.isSelected())
				days.add(AlarmDay.SATURDAY);
			if (sun.isSelected())
				days.add(AlarmDay.SUNDAY);
		}
		return days;
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		if (!settingMusic) {
			alarmGenerated = null;
			selectedTone = defaultTone;
		}

		settingMusic = false;
	}

	@Override
	public void removedFromScreen() {

	}

	@Override
	public void languageChanged() {
		music.setText(BundleWrapper.getString(BundleKey.select_music));
		alarm.setText(BundleWrapper.getString(BundleKey.select_alarm));
		or.setText(BundleWrapper.getString(BundleKey.or));
		all.setText(BundleWrapper.getString(BundleKey.all_days));
	}

	private void setDaysOfWeek(boolean disable) {
		for (ToggleButton b : buttons) {
			b.setDisable(disable);
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		hour.setValueFactory(new IntegerSpinnerValueFactory(1, 12));
		minute.setValueFactory(new IntegerSpinnerValueFactory(0, 59));

		buttons = new ToggleButton[] { sun, mon, tue, wed, thu, fri, sat };

		// When all days of week is selected, buttons become unselectable
		this.all.selectedProperty().addListener((value, old, newv) -> setDaysOfWeek(newv));

		this.percentVolume.textProperty().bind(volume.valueProperty().multiply(100).asString("%.0f%%"));

		ToggleGroup group = new ToggleGroup();
		am.setToggleGroup(group);
		pm.setToggleGroup(group);
		group.selectToggle(am);

		group.selectedToggleProperty().addListener((value, old, newv) -> updateTimeText());
		hour.valueProperty().addListener((value, old, newv) -> updateTimeText());
		minute.valueProperty().addListener((value, old, newv) -> updateTimeText());

		updateTimeText();

		defaultTone = loadDefaultAlarms().get(0);
		setSong(defaultTone);

		languageChanged();
	}

	private int getRealHourTime() {
		int hour = this.hour.getValue();

		// Set value for pm is added
		hour = pm.isSelected() ? hour + 12 : hour;

		return hour;
	}

	private void updateTimeText() {
		int hour = getRealHourTime();
		int minute = this.minute.getValue();
		String text = Formatters.formatHourMinute(hour, minute);

		timeLabel.setText(text);
	}

	public void setAlarm(Alarm alarm) {
		this.alarmGenerated = alarm;

		this.hour.getValueFactory().setValue(alarm.getHour() % 12); // Between 1
																	// and 11
		this.minute.getValueFactory().setValue(alarm.getMin() % 60);

		// Case for 12 and 24
		if (alarm.getHour() % 12 == 0) {
			hour.getValueFactory().setValue(12);
		}
		// Select PM if the alarm rings in the afternoon
		pm.setSelected(alarm.getHour() > 12);

		selectDays();
		// Sets the music info
		volume.setValue(alarm.getTone().getToneVolume());
		try {
			Mp3SongInfo song = new Mp3SongInfo(alarm.getTone().getTonePath());
			setSong(song);
		} catch (UnsupportedTagException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void selectDays() {
		// unselect all the buttons
		setAllButtonsToggle(false);

		if (alarmGenerated.isAllDay()) {
			all.setSelected(true);
		} else {
			List<AlarmDay> days = alarmGenerated.getAlarmDays();
			all.setSelected(false);
			days.sort(AlarmDay.getDayComparator());
			for (AlarmDay day : days) {
				// Since the buttons are sorted and aunday is day #1 in the
				// button
				// array and in the alarm day enum
				buttons[day.day - 1].setSelected(true);
			}
		}
	}

	private void setAllButtonsToggle(boolean toggle) {
		for (ToggleButton b : buttons) {
			b.setSelected(toggle);
		}
	}

}