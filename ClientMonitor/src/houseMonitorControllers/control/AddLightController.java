package houseMonitorControllers.control;

import java.net.URL;
import java.nio.channels.SelectionKey;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.LightData;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Skin;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import server.communication.Request;
import server.transport.Communicator;
import utils.HouseUtils;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class AddLightController
		implements ReturnCodeListener, ControlledScreen, Communicator, Initializable, Universalisable {

	@FXML
	private Label infoText;

	@FXML
	private Label addLabel;

	@FXML
	private ListView<LightData> availableLights;

	@FXML
	private Label removeLabel;

	@FXML
	private ListView<LightData> currentLights;

	private ScreenController sc;
	private CommunicationCenter com;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		availableLights.setCellFactory(call);
		availableLights.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		availableLights.getSelectionModel().selectedItemProperty().addListener(lightListener);

		currentLights.setCellFactory(call);
		currentLights.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		currentLights.getSelectionModel().selectedItemProperty().addListener(lightListener);

		languageChanged();

	}

	@FXML
	private void done(ActionEvent event) {
		ObservableList<LightData> add = availableLights.getSelectionModel().getSelectedItems();
		ObservableList<LightData> remove = currentLights.getSelectionModel().getSelectedItems();

		// Nothing changed
		if (add.isEmpty() && remove.isEmpty()) {
			sc.setPreviousScreen();
		} else {
			sc.setScreen(Screen.PIN_CONFIRM);
			PINConfirmController contr = (PINConfirmController) sc.getController(Screen.PIN_CONFIRM);

			// Notifies the class to add or remove the lights
			contr.setData(TransferFlags.UPDATE_ROOM_LIGHTS,
					new Object[] { HouseUtils.isolateIDs(add), HouseUtils.isolateIDs(remove) });

		}

	}

	@FXML
	private void previous(ActionEvent event) {
		sc.setPreviousScreen();
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.sc = SC;
	}

	@Override
	public void displayedToScreen() {

		com.send(TransferFlags.REQUEST_ROOM_LIGHTS);
		com.send(TransferFlags.REQUEST_AVAILABLE_LIGHTS);

		currentLights.setItems(null);
		availableLights.setItems(null);

	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	private ChangeListener<LightData> lightListener = new ChangeListener<LightData>() {

		@Override
		public void changed(ObservableValue<? extends LightData> observable, LightData oldValue, LightData newValue) {

			if (newValue != null) {
				com.send(TransferFlags.LIGHT_TEST, newValue.getId());
			}
		}
	};
	private Callback<ListView<LightData>, ListCell<LightData>> call = new Callback<ListView<LightData>, ListCell<LightData>>() {

		@Override
		public ListCell<LightData> call(ListView<LightData> param) {
			// TODO Auto-generated method stub
			return new ListCell<LightData>() {

				@Override
				protected void updateItem(LightData dat, boolean empty) {
					super.updateItem(dat, empty);

					System.out.println("LIGHT : + " + dat);
					Label l = new Label();
					l.setFont(Font.font("system", FontWeight.BOLD, 20));
					setGraphic(l);
					if (empty || dat == null) {
						l.setText("");
					} else {
						String name = dat.getName();
						if (name == null || name.trim().isEmpty()) {
							l.setText(BundleWrapper.getString(BundleKey.no_name_light));
						} else {
							l.setText(name);
						}
					}
				}

			};
		}
	};

	@SuppressWarnings("unchecked")
	@Override
	public void returnCodeReceived(int flag, Object data) {

		if (flag == TransferFlags.REQUEST_AVAILABLE_LIGHTS) {
			availableLights.setItems(FXCollections.observableArrayList((List<LightData>) data));
		} else if (flag == TransferFlags.REQUEST_ROOM_LIGHTS) {
			currentLights.setItems(FXCollections.observableArrayList((List<LightData>) data));
		}
	}

	@Override
	public void languageChanged() {
		addLabel.setText(BundleWrapper.getString(BundleKey.add_lights));
		removeLabel.setText(BundleWrapper.getString(BundleKey.remove_lights));
	}

}
