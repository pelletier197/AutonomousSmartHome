package houseMonitorControllers.control;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.commucationData.TransferFlags;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import server.transport.Communicator;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class AlarmController
		implements Initializable, Universalisable, ControlledScreen, Communicator, ReturnCodeListener {

	@FXML
	private ListView<Alarm> alarmList;
	private CommunicationCenter com;
	private ScreenController controller;

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@FXML
	private void newAlarm(ActionEvent event) {
		controller.setScreen(Screen.EDIT_ALARM);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		this.alarmList.setCellFactory(new Callback<ListView<Alarm>, ListCell<Alarm>>() {

			@Override
			public ListCell<Alarm> call(ListView<Alarm> param) {
				// TODO Auto-generated method stub
				return new ListCell<Alarm>() {

					@Override
					protected void updateItem(Alarm item, boolean empty) {
						super.updateItem(item, empty);

						if (!empty && item != null) {
							FXMLLoader loader = new FXMLLoader(
									getClass().getResource("/houseMonitorViews/control/AlarmDisplay.fxml"));

							try {
								setGraphic(loader.load());
								AlarmDisplayController cont = loader.getController();
								cont.setAlarm(item);
								cont.setCommunicationCenter(com);
								cont.setScreenController(controller);

							} catch (IOException e) {
								e.printStackTrace();
								setGraphic(null);
							}

						} else {
							setGraphic(null);
						}
					}

				};
			}
		});
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		com.send(TransferFlags.REQUEST_ROOM_ALARMS);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void languageChanged() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void returnCodeReceived(int flag, Object data) {
		if (flag == TransferFlags.REQUEST_ROOM_ALARMS) {
			List<Alarm> alarms = (List<Alarm>) data;
			System.out.println("ALARMS CLIENT : " + alarms);
			alarmList.setItems(null); // Forces the list to get empty
			alarmList.setItems(FXCollections.observableArrayList(alarms));
		}
	}

}
