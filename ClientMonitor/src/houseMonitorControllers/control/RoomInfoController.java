package houseMonitorControllers.control;

import java.net.URL;
import java.text.NumberFormat;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import house.ClientParameters;
import house.RoomParams;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.data.TempDisplay;
import houseDataCenter.math.unitConversion.CTempConverter;
import houseDataCenter.math.unitConversion.Unit;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.musicPlayer.MusicPlayingController;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import server.communication.Request;
import server.transport.Communicator;
import utils.Formatters;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class RoomInfoController implements ControlledScreen, Initializable, Communicator, ReturnCodeListener {

	@FXML
	private ToggleButton lightSwitch;

	@FXML
	private ImageView lightImage;

	@FXML
	private Label tempLabel;

	@FXML
	private Label name;

	@FXML
	private Button upTemp;

	@FXML
	private Button downTemp;

	@FXML
	private Button alarm;

	@FXML
	private ColorPicker lightsColor;

	@FXML
	private Slider lightsLuminosity;

	@FXML
	private Button resetColor;

	private CTempConverter converter;

	private ScreenController controller;

	private CommunicationCenter com;

	private List<LightData> lights;
	private boolean open;
	private int color;
	private double luminosity;
	private boolean dimmable;
	private boolean colorisable;

	private static final Image OPEN = new Image(
			MusicPlayingController.class.getResource("/resources/images/light_icon_open.png").toExternalForm());
	private static final Image CLOSE = new Image(
			MusicPlayingController.class.getResource("/resources/images/light_icon_close.png").toExternalForm());
	private static final String CELCIUS = "°C", FARANEITH = "°F";

	private static ChangeListener<Color> colorList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		converter = new CTempConverter();

		colorList = (value, old, newv) -> {
			int color = 0;
			color |= (int) (newv.getBlue() * 255);
			color |= (((int) (newv.getGreen() * 255)) << 8);
			color |= (((int) (newv.getRed() * 255)) << 16);

			com.send(TransferFlags.SET_LIGHTS_COLOR, color);

			this.color = color;

		};

		lightsColor.valueProperty().addListener(colorList);

		lightsLuminosity.valueChangingProperty().addListener((value, old, newv) -> {
			// Value stops changing
			if (!newv) {
				com.send(TransferFlags.SET_LIGHTS_LUMINOSITY, lightsLuminosity.getValue());
				this.luminosity = lightsLuminosity.getValue();
			}
		});

	}

	@FXML
	private void alarm(ActionEvent event) {
		controller.setScreen(Screen.ALARM);
	}

	@FXML
	private void options(ActionEvent event) {
		controller.setScreen(Screen.ROOM_MODIFICATION);
	}

	@FXML
	private void openLightOptions(ActionEvent event) {
		controller.setScreen(Screen.LIGHT_OPTIONS);
	}

	@FXML
	private void restoreLightColor(ActionEvent event) {
		com.send(TransferFlags.RESET_LIGHTS_COLOR);
	}

	@FXML
	private void tempDown(ActionEvent event) {
		com.send(TransferFlags.REQUEST_TEMP_DOWN);

	}

	@FXML
	private void tempUp(ActionEvent event) {
		com.send(TransferFlags.REQUEST_TEMP_UP);

	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		com.send(TransferFlags.REQUEST_ROOM_LIGHTS);
		com.send(TransferFlags.REQUEST_TEMP);
		com.send(TransferFlags.REQUEST_ROOM_NAME);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@FXML
	private void manageLights(ActionEvent event) {
		controller.setScreen(Screen.ADD_LIGHTS);
	}

	@FXML
	private void switchLight(ActionEvent event) {
		com.send(open ? TransferFlags.CLOSE_ALL_LIGHTS : TransferFlags.OPEN_ALL_LIGHTS);
		open = !open;
		setLightState();
	}

	private void computeLights(List<LightData> data) {

		System.out.println("COMPUTING LIGHTS");
		this.lights = data;

		open = false;
		dimmable = false;
		colorisable = false;

		// Checks for one open light
		for (LightData dat : lights) {
			if (dat.isOpen()) {
				open = true;
			}
			if (dat.isDimmable()) {
				dimmable = true;
				luminosity = dat.getLuminosity();
			}
			if (dat.isColorisable()) {
				colorisable = true;
				color = dat.getColor();
			}
		}

		setLightState();

	}

	private void setLightState() {

		System.out.println(colorisable + "  " + Integer.toHexString(color) + dimmable);

		lightSwitch.setSelected(open);
		lightImage.setImage(open ? OPEN : CLOSE);

		int b = color & 0xff;
		int g = (color >> 8) & 0xff;
		int r = (color >> 16) & 0xff;

		System.out.println(r + " " + g + " " + b);
		lightsColor.setDisable(!(colorisable && open));
		lightsColor.setValue(Color.rgb(r, g, b, 1));
		resetColor.setDisable(lightsColor.isDisabled());

		lightsLuminosity.setDisable(!(dimmable && open));
		lightsLuminosity.setValue(luminosity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void returnCodeReceived(int flag, Object data) {

		if (flag == TransferFlags.SET_TEMP) {

			double tempCelcius = (double) data;

			if (ClientParameters.getParams().getTempDisplay() == TempDisplay.TEMP_CELCIUS) {

				tempLabel.setText(Formatters.doubleFormat(tempCelcius, 1) + CELCIUS);
			} else {
				double tempFara = converter.convert(tempCelcius, Unit.C, Unit.F);
				tempLabel.setText(Formatters.doubleFormat(tempFara, 1) + FARANEITH);
			}
		} else if (flag == TransferFlags.REQUEST_ROOM_LIGHTS || flag == TransferFlags.UPDATE_LIGHT_STATUS) {
			computeLights((List<LightData>) data);
		} else if (flag == TransferFlags.REQUEST_ROOM_NAME) {
			this.name.setText((String) data);
		} else if (flag == TransferFlags.LIGHT_STATE_CHANGED) {
			LightData dat = (LightData) data;
			int index = lights.indexOf(dat);
			if (index != -1) {
				lights.set(index, dat);
			}
			computeLights(lights);
		}
	}

}
