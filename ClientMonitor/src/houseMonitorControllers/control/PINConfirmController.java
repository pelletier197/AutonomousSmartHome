package houseMonitorControllers.control;

import java.net.URL;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import server.communication.Request;
import server.transport.Communicator;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class PINConfirmController
		implements Universalisable, ReturnCodeListener, ControlledScreen, Communicator, Initializable {

	public interface PINConfirmListener {
		public void onPINConfirm(String pin);
	}

	@FXML
	private Label infoText;

	@FXML
	private PasswordField PINBox;

	@FXML
	private Label errorText;

	@FXML
	private Button finish;

	private Object data;
	private int flag;

	private boolean dataSet;

	private Screen nextScreen;

	private ScreenController controller;
	private CommunicationCenter com;

	private PINConfirmListener listener;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		languageChanged();
	}

	@FXML
	private void finish(ActionEvent event) {

		if (dataSet) {
			errorText.setText("");
			finish.setDisable(true);
			Object[] dat = null;

			if (data instanceof Object[]) {
				Object[] arr = ((Object[]) data);
				dat = new Object[arr.length + 1];
				System.arraycopy(arr, 0, dat, 1, arr.length);

			} else {
				dat = new Object[2];
				dat[1] = data;
			}
			dat[0] = PINBox.getText();

			com.send(flag, dat);
		}
		if (listener != null) {
			listener.onPINConfirm(PINBox.getText());
		}
	}

	public void setData(int flag, Object data) {
		this.flag = flag;
		this.data = data;
		dataSet = true;

	}

	public void setPINConfirmListener(PINConfirmListener listener) {
		this.listener = listener;
	}

	public void setNextScreenOnSuccess(Screen screen) {
		this.nextScreen = screen;
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;

	}

	@Override
	public void displayedToScreen() {
		dataSet = false;
	}

	@Override
	public void removedFromScreen() {
		data = null;
		dataSet = false;
		flag = 0;
		PINBox.setText("");
		listener = null;
	}

	@Override
	public void returnCodeReceived(int flag, Object data) {

		if (flag == TransferFlags.ACCESS_REFUSED) {
			errorText.setText(BundleWrapper.getString(BundleKey.invalid_pin));
		} else if (flag == TransferFlags.ACCESS_GRANTED) {
			errorText.setText("");

			if (nextScreen == null) {
				controller.setScreen(Screen.ROOM_INFO);
			} else {
				controller.setScreen(nextScreen);
			}
		}

		finish.setDisable(false);
	}

	@FXML
	private void previous(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@Override
	public void languageChanged() {
		infoText.setText(BundleWrapper.getString(BundleKey.confirm_resident_pin));
	}

}
