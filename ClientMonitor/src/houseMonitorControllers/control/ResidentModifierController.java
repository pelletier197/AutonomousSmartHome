package houseMonitorControllers.control;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.ResidentData;
import houseDataCenter.house.residents.Resident;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Callback;
import server.communication.Request;
import server.transport.Communicator;
import utils.HouseUtils;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class ResidentModifierController
		implements Universalisable, Initializable, ControlledScreen, Communicator, ReturnCodeListener {

	private static final Image RESIDENT_ICON = new Image(
			ResidentModifierController.class.getResource("/resources/images/user_icon.gif").toExternalForm());
	@FXML
	private Button newResidentButton;

	@FXML
	private VBox background;

	@FXML
	private ListView<ResidentData> residentList;

	@FXML
	private ImageView residentView;

	@FXML
	private Label name;

	@FXML
	private Label born;

	@FXML
	private Label mail;

	@FXML
	private Label pin;

	private ScreenController controller;

	private CommunicationCenter com;

	private String nameS, bornS, mailS, pinS;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		languageChanged();


		residentList.setCellFactory(new Callback<ListView<ResidentData>, ListCell<ResidentData>>() {

			@Override
			public ListCell<ResidentData> call(ListView<ResidentData> param) {
				// TODO Auto-generated method stub
				return new ListCell<ResidentData>() {

					@Override
					protected void updateItem(ResidentData info, boolean empty) {
						super.updateItem(info, empty);

						if (info != null && !empty) {

							ImageView view = new ImageView(RESIDENT_ICON);
							view.setFitWidth(40);
							view.setFitHeight(40);

							setGraphic(view);
							setText(info.name);

						} else {
							setGraphic(null);
							setText("");
						}
					}

				};
			}
		});

		residentList.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {
			if (newv != null) {
				name.setText(nameS + newv.name);
				born.setText(bornS + HouseUtils.millisToFormattedDateDay(newv.bornDate));
				mail.setText(mailS + newv.mail);
			}
		});

		// The pin is not visible and has a length of 5, so we only put 5 stars
		pin.setText(pinS + "*****");

		selectFirstOrInvisible();
	}

	private void selectFirstOrInvisible() {
		if (!residentList.getItems().isEmpty()) {
			background.setVisible(true);
			residentList.getSelectionModel().select(0);
		} else {
			background.setVisible(false);
		}
	}

	@FXML
	private void addResident(ActionEvent event) {
		controller.setScreen(Screen.RESIDENT_CREATION);
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@FXML
	private void enterView(MouseEvent event) {

		Node source = (Node) event.getSource();

		source.setScaleX(1.1);
		source.setScaleY(1.1);

	}

	@FXML
	private void exitView(MouseEvent event) {

		Node source = (Node) event.getSource();

		source.setScaleX(1d);
		source.setScaleY(1d);
	}

	@FXML
	private void modifyBorn(MouseEvent event) {

	}

	@FXML
	private void modifyMail(MouseEvent event) {

	}

	@FXML
	private void modifyName(MouseEvent event) {

	}

	@FXML
	private void modifyPIN(MouseEvent event) {

	}

	public void setResidentList(List<ResidentData> residents) {
		Platform.runLater(() -> {
			residentList.setItems(FXCollections.observableArrayList(residents));
			selectFirstOrInvisible();
		});
	}

	@Override
	public void displayedToScreen() {
		// Requests info from the server
		com.send(TransferFlags.REQUEST_RESIDENTS_INFO);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void returnCodeReceived(int flag, Object data) {

		if (flag == TransferFlags.REQUEST_RESIDENTS_INFO) {
			setResidentList((List<ResidentData>) data);
		}
	}

	@Override
	public void languageChanged() {

		nameS = BundleWrapper.getString(BundleKey.name) + " : ";
		bornS = BundleWrapper.getString(BundleKey.born_date) + " : ";
		mailS = BundleWrapper.getString(BundleKey.mail) + " : ";
		pinS = BundleWrapper.getString(BundleKey.pin) + " : ";

		newResidentButton.setText(BundleWrapper.getString(BundleKey.new_resident));
	}

}
