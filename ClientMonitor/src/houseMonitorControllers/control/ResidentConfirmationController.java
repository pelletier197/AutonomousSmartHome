package houseMonitorControllers.control;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import custom.CommunicationCenter;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import server.transport.Communicator;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class ResidentConfirmationController
		implements Universalisable, ReturnCodeListener, Initializable, Communicator, ControlledScreen {

	@FXML
	private Label confirmation;

	@FXML
	private TextField mail;

	@FXML
	private PasswordField resident;

	@FXML
	private Button send;

	private CommunicationCenter com;

	private ScreenController controller;

	private int creationFlag;

	@FXML
	private Label info;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		languageChanged();
		mail.focusedProperty().addListener((value, old, newv) -> {
			if (newv)
				showMailInfo();
		});
		resident.focusedProperty().addListener((value, old, newv) -> {
			if (newv)
				showResidentInfo();
		});

		mail.requestFocus();
	}

	private void showMailInfo() {
		info.setStyle("");
		info.setText(BundleWrapper.getString(BundleKey.mail_tooltip));
	}

	private void showResidentInfo() {
		info.setStyle("");
		info.setText(BundleWrapper.getString(BundleKey.resident_tooltip));

	}

	private void showErrorConfirmationText() {

		String text = BundleWrapper.getString(BundleKey.error_mail);

		if (creationFlag == TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION) {
			text += (BundleWrapper.getString(BundleKey.error_resident_pin));
		}

		info.setStyle("-fx-text-fill:red");
		info.setText(text);
	}

	private void showTimeoutText() {
		info.setStyle("-fx-text-fill:red");
		info.setText(BundleWrapper.getString(BundleKey.creation_timeout_error));
	}

	@FXML
	private void send(ActionEvent event) {

		// Decides either we send mail and resident code, or just mail confirm,
		// depending on the request of the server.
		if (creationFlag == TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION) {
			com.send(creationFlag, new String[] { mail.getText(), resident.getText() });
		} else {
			com.send(creationFlag, mail.getText());
		}
	}


	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void returnCodeReceived(int flag, Object data) {
		if (flag == TransferFlags.REQUEST_MAIL_AND_RESIDENT_CONFIRMATION) {
			this.creationFlag = flag;
			resident.setVisible(true);

		} else if (flag == TransferFlags.REQUEST_MAIL_CONFIRMATION_CODE) {
			this.creationFlag = flag;
			resident.setVisible(false);

		} else if (flag == TransferFlags.FAILURE_CREATION_INVALID_CONFIRMATION) {

			showErrorConfirmationText();

		} else if (flag == TransferFlags.CREATION_CANCELED_PROTOCOL_TIMEOUT) {
			showTimeoutText();
		} else if (flag == TransferFlags.SUCCESS_CREATION) {
			controller.setScreen(Screen.RESIDENT_MODIFICATION);
		}
	}

	@Override
	public void languageChanged() {
		confirmation.setText(BundleWrapper.getString(BundleKey.resident_validation));
		mail.setPromptText(BundleWrapper.getString(BundleKey.mail_validation));
		resident.setPromptText(BundleWrapper.getString(BundleKey.resident_confirmation));
	}
}