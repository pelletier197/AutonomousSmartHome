package houseMonitorControllers.control;

import java.net.URL;
import java.util.ResourceBundle;

import javax.naming.ldap.Control;

import custom.CommunicationCenter;
import house.AlarmRingingDecisionListener;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseMonitorControllers.musicPlayer.MusicEngine;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.CacheHint;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import server.transport.Communicator;
import views.screenController.ControlledScreen;
import views.screenController.ScreenController;

public class AlarmRingingController implements Initializable, ControlledScreen {

	@FXML
	private ImageView icon;

	@FXML
	private Button snooze;

	@FXML
	private Button stop;

	private AlarmRingingDecisionListener listener;

	private ScreenController controller;

	private Animation animation;

	private Alarm alarm;

	private boolean isStopped;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initRingingAnimation();
	}

	private void initRingingAnimation() {
		animation = new Timeline(
				new KeyFrame(Duration.millis(0), new KeyValue(icon.rotateProperty(), 0)),
				new KeyFrame(Duration.millis(0), new KeyValue(icon.translateYProperty(), 0)),
				new KeyFrame(Duration.millis(150), new KeyValue(icon.rotateProperty(), 30)),
				new KeyFrame(Duration.millis(150), new KeyValue(icon.translateYProperty(), -3)),
				new KeyFrame(Duration.millis(300), new KeyValue(icon.rotateProperty(), -32)),
				new KeyFrame(Duration.millis(300), new KeyValue(icon.translateYProperty(), -8)),
				new KeyFrame(Duration.millis(450), new KeyValue(icon.rotateProperty(), 34)),
				new KeyFrame(Duration.millis(450), new KeyValue(icon.translateYProperty(), -15)),
				new KeyFrame(Duration.millis(600), new KeyValue(icon.rotateProperty(), -36)),
				new KeyFrame(Duration.millis(600), new KeyValue(icon.translateYProperty(), -20)),
				new KeyFrame(Duration.millis(750), new KeyValue(icon.rotateProperty(), 38)),
				new KeyFrame(Duration.millis(750), new KeyValue(icon.translateYProperty(), -25)),
				new KeyFrame(Duration.millis(900), new KeyValue(icon.rotateProperty(), -38)),
				new KeyFrame(Duration.millis(900), new KeyValue(icon.translateYProperty(), -35)),
				new KeyFrame(Duration.millis(1050), new KeyValue(icon.rotateProperty(), 38)),
				new KeyFrame(Duration.millis(1050), new KeyValue(icon.translateYProperty(), -43)),
				new KeyFrame(Duration.millis(1200), new KeyValue(icon.rotateProperty(), -38)),
				new KeyFrame(Duration.millis(1200), new KeyValue(icon.translateYProperty(), -48)),
				new KeyFrame(Duration.millis(1350), new KeyValue(icon.rotateProperty(), 38)),
				new KeyFrame(Duration.millis(1350), new KeyValue(icon.translateYProperty(), -52)),
				new KeyFrame(Duration.millis(1550), new KeyValue(icon.rotateProperty(), 0, Interpolator.EASE_IN)),
				new KeyFrame(Duration.millis(1550), new KeyValue(icon.translateYProperty(), 0, Interpolator.EASE_IN)),
				new KeyFrame(Duration.millis(1700),
						new KeyValue(icon.translateYProperty(), -35, Interpolator.EASE_OUT)),
				new KeyFrame(Duration.millis(1850), new KeyValue(icon.translateYProperty(), 0, Interpolator.EASE_IN)),
				new KeyFrame(Duration.millis(2100), new KeyValue(icon.translateYProperty(), 0)));
		animation.setCycleCount(Animation.INDEFINITE);
		icon.setCache(true);
		icon.setCacheHint(CacheHint.SPEED);
	}

	@FXML
	private void snooze(ActionEvent event) {
		listener.onSnooze();
		isStopped = true;
		controller.setPreviousScreen();
	}

	public void setAlarm(Alarm a, AlarmRingingDecisionListener listener) {
		this.alarm = a;
		this.listener = listener;
	}

	@FXML
	private void stop(ActionEvent event) {
		listener.onStop();
		isStopped = true;
		controller.setPreviousScreen();
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		isStopped = false;
		animation.playFromStart();
	}

	@Override
	public void removedFromScreen() {
		animation.stop();

		if (!isStopped && listener != null) {
			listener.onStop(); // Exit the screen without touching a button //
								// automatically stops the alarm
		}

	}

}
