package houseMonitorControllers.control;

import java.util.List;

import custom.CommunicationCenter;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseMonitorControllers.musicPlayer.MusicPlayingController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import main.ClientMain;
import utils.Formatters;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class AlarmDisplayController {

	private static final Image ACTIVE = new Image(
			MusicPlayingController.class.getResource("/resources/images/alarm_active.png").toExternalForm());
	private static final Image NOT_ACTIVE = new Image(
			MusicPlayingController.class.getResource("/resources/images/alarm.png").toExternalForm());

	@FXML
	private Label time;

	@FXML
	private Label days;

	@FXML
	private ImageView active;

	private Alarm alarm;
	private CommunicationCenter center;
	private ScreenController controller;

	@FXML
	private void edit(ActionEvent event) {
		controller.setScreen(Screen.EDIT_ALARM);

		EditAlarmController edit = (EditAlarmController) controller.getController(Screen.EDIT_ALARM);
		edit.setAlarm(alarm);
	}

	@FXML
	private void delete(ActionEvent event) {
		// Called on delete pressed
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setHeaderText(null);
		alert.setContentText(BundleWrapper.getString(BundleKey.confirmation_delete_alarm));
		ClientMain.addAppStyleSheets(alert);
		alert.showAndWait();

		ButtonType type = alert.getResult();

		if (type == ButtonType.OK) {
			center.send(TransferFlags.REMOVE_ALARM, alarm.getId());
			center.send(TransferFlags.REQUEST_ROOM_ALARMS);
		}
	}

	@FXML
	private void switchActive(ActionEvent event) {
		if (alarm.isActive()) {
			center.send(TransferFlags.DEACTIVATE_ALARM, alarm.getId());
			alarm.setActive(false);
		} else {
			center.send(TransferFlags.ACTIVATE_ALARM, alarm.getId());
			alarm.setActive(true);
		}
		setAlarmIcon();
	}

	private void setAlarmIcon() {

		if (alarm.isActive()) {
			active.setImage(ACTIVE);
		} else {
			active.setImage(NOT_ACTIVE);
		}
	}

	public void setCommunicationCenter(CommunicationCenter center) {
		this.center = center;
	}

	public void setScreenController(ScreenController controller) {
		this.controller = controller;
	}

	public void setAlarm(Alarm alarm) {
		this.alarm = alarm;
		this.time.setText(alarm.getHour() + ":" + (alarm.getMin() < 10 ? "0" + alarm.getMin() : alarm.getMin()));
		setAlarmIcon();
		setDays();
	}

	private void setDays() {
		if (alarm.isUseOnce()) {
			this.days.setText(BundleWrapper.getString(BundleKey.once));

		} else {

			if (!alarm.isAllDay()) {
				List<AlarmDay> days = alarm.getAlarmDays();
				this.days.setText(Formatters.getDaysFormatted(days));
			} else {
				this.days.setText(BundleWrapper.getString(BundleKey.all_days));
			}
		}

	}

}
