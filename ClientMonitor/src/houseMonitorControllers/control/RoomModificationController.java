package houseMonitorControllers.control;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import house.ClientParameters;
import house.RoomParams;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.applicationRessourceBundle.Language;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.house.data.TempDisplay;
import houseMonitorControllers.ReturnCodeListener;
import houseMonitorControllers.Universalisable;
import houseMonitorControllers.control.PINConfirmController.PINConfirmListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import main.ClientMain;
import server.communication.Request;
import server.transport.Communicator;
import sun.nio.ch.IOUtil;
import tests.houseDataCenter.house.HouseParameter;
import util.IOUtils;
import utils.HouseUtils;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class RoomModificationController
		implements Universalisable, Initializable, ControlledScreen, Communicator, ReturnCodeListener {

	@FXML
	private Label nameLabel;

	@FXML
	private TextField nameField;

	@FXML
	private Label langLabel;

	@FXML
	private ComboBox<Language> language;

	@FXML
	private Label tempUnitLabel;

	@FXML
	private CheckBox languageAllRooms;

	@FXML
	private CheckBox tempUnitAllRooms;

	@FXML
	private ToggleButton tempF;

	@FXML
	private ToggleButton tempC;

	@FXML
	private Label serverLanguageLabel;

	@FXML
	private ComboBox<Language> serverLanguage;
	private Language serverLanguageBefore;

	private ScreenController controller;

	private CommunicationCenter com;

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@FXML
	private void done(ActionEvent event) {

		RoomParams params = ClientParameters.getParams();

		List<Request> nipRequiredData = new ArrayList<>();

		// The name
		String name = nameField.getText();
		params.setRoomName(name);
		com.send(TransferFlags.SET_ROOM_NAME, name);

		// The temp display unit
		if (tempUnitAllRooms.isSelected()) {
			nipRequiredData
					.add(new Request(TransferFlags.SET_ALL_ROOM_TEMP_DISPLAY, getTempDisplaySelected()));
		} else {
			params.setTempDisplay(getTempDisplaySelected());
		}

		// The language
		if (languageAllRooms.isSelected()) {
			nipRequiredData.add(new Request(TransferFlags.SET_ALL_ROOMS_LANGUAGE, language.getValue()));
		} else {
			params.setLanguage(language.getValue());
		}

		// Language of server
		if (serverLanguage.getValue() != serverLanguageBefore) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setHeaderText(null);
			alert.setContentText(BundleWrapper.getString(BundleKey.confirmation_text_change_server_lang));
			ClientMain.addAppStyleSheets(alert);
			alert.showAndWait();

			// Confirm the choice
			if (alert.getResult() == ButtonType.OK) {
				nipRequiredData.add(new Request(TransferFlags.SET_SERVER_LANGUAGE, serverLanguage.getValue()));
			}
		}
		// Saves the changes locally
		IOUtils.writeClientParameters(params);

		if (nipRequiredData.isEmpty()) {
			controller.setPreviousScreen();
		} else {
			sendPinRequiredData(nipRequiredData);
		}

		System.out.println(params + " = new params");
	}

	private void sendPinRequiredData(List<Request> nipRequiredData) {

		controller.setScreen(Screen.PIN_CONFIRM);

		PINConfirmController contr = (PINConfirmController) controller.getController(Screen.PIN_CONFIRM);

		contr.setPINConfirmListener(new PINConfirmListener() {

			@Override
			public void onPINConfirm(String pin) {

				// Send all data to the server
				for (Request dat : nipRequiredData) {
					com.send(dat.getFlag(), new Object[] { pin, dat.getData() });
				}
			}
		});
	}

	private TempDisplay getTempDisplaySelected() {

		if (tempF.isSelected()) {
			return TempDisplay.TEMP_FARA;
		} else {
			return TempDisplay.TEMP_CELCIUS;
		}
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		RoomParams params = ClientParameters.getParams();
		com.send(TransferFlags.REQUEST_SERVER_LANGUAGE);
		nameField.setText(params.getRoomName());
		language.setValue(params.getLanguage());
		selectTempUnit(params.getTempDisplay());
	}

	private void selectTempUnit(TempDisplay tempDisplay) {

		switch (tempDisplay) {
		case TEMP_CELCIUS:
			tempC.setSelected(true);
			break;
		case TEMP_FARA:
			tempF.setSelected(true);
			break;

		}
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		languageChanged();

		ToggleGroup group = new ToggleGroup();
		tempC.setToggleGroup(group);
		tempF.setToggleGroup(group);

		ObservableList<Language> langValues = FXCollections.observableArrayList(Arrays.asList(Language.values()));

		serverLanguage.setItems(langValues);
		language.setItems(langValues);
	}

	@Override
	public void returnCodeReceived(int flag, Object data) {
		if (flag == TransferFlags.REQUEST_SERVER_LANGUAGE) {
			Language lang = (Language) data;
			serverLanguage.setValue(lang);
			serverLanguageBefore = lang;
		}
	}

	@Override
	public void languageChanged() {
		nameLabel.setText(BundleWrapper.getString(BundleKey.room_namme));
		langLabel.setText(BundleWrapper.getString(BundleKey.language_displayed));
		tempUnitLabel.setText(BundleWrapper.getString(BundleKey.temp_unit));
		serverLanguageLabel.setText(BundleWrapper.getString(BundleKey.server_language));
	}

}