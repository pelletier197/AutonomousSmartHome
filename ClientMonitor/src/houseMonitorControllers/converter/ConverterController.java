package houseMonitorControllers.converter;

import java.util.Formatter;
import java.util.List;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.math.unitConversion.CAngleConverter;
import houseDataCenter.math.unitConversion.CDataConverter;
import houseDataCenter.math.unitConversion.CForceConverter;
import houseDataCenter.math.unitConversion.CFrequencyConverter;
import houseDataCenter.math.unitConversion.CLengthConverter;
import houseDataCenter.math.unitConversion.CPowerConverter;
import houseDataCenter.math.unitConversion.CPressureConverter;
import houseDataCenter.math.unitConversion.CSpeedConverter;
import houseDataCenter.math.unitConversion.CSurfaceConverter;
import houseDataCenter.math.unitConversion.CTempConverter;
import houseDataCenter.math.unitConversion.CTimeConverter;
import houseDataCenter.math.unitConversion.CVolumeConverter;
import houseDataCenter.math.unitConversion.CWeightConverter;
import houseDataCenter.math.unitConversion.CWorkConverter;
import houseDataCenter.math.unitConversion.Convertible;
import houseDataCenter.math.unitConversion.Unit;
import houseMonitorControllers.Universalisable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import main.NumberField;
import utils.Formatters;
import views.screenController.ControlledScreen;
import views.screenController.ScreenController;

public class ConverterController implements Universalisable, ControlledScreen {

	@FXML
	private Label unitLabel;

	@FXML
	private Button convert;

	@FXML
	private Label currentConversion;

	@FXML
	private ComboBox<Unit> fromUnit;

	@FXML
	private ListView<String> output;

	private Convertible currentConverter;

	private List<Unit> targetUnits = null;

	@FXML
	private NumberField input;

	private ScreenController controller;

	@FXML
	private ComboBox<ConvertibleTitleWrapper> unitChoices;

	@FXML
	private void initialize() {

		languageChanged();

		/**
		 * The unit choices
		 */
		unitChoices.setCellFactory(lt -> createUnitChoiceCell());
		unitChoices.setButtonCell(createUnitChoiceCell());
		unitChoices.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {
			if (newv != null) {
				currentConverter = newv.converter;
				targetUnits = currentConverter.getUnits();
				fromUnit.setItems(FXCollections.observableArrayList(targetUnits));
				selectIS();
				updateConversions();
			}
		});

		/**
		 * The from unit
		 */
		fromUnit.setCellFactory(new Callback<ListView<Unit>, ListCell<Unit>>() {

			@Override
			public ListCell<Unit> call(ListView<Unit> param) {
				return new ListCell<Unit>() {

					@Override
					protected void updateItem(Unit unit, boolean empty) {
						super.updateItem(unit, empty);

						if (unit != null && !empty) {

							StringBuilder builder = new StringBuilder();
							String symbol = unit.symbol;
							builder.append(BundleWrapper.getString(symbol)).append(" (").append(symbol).append(")");
							setText(builder.toString());

							if (unit.isIS) {
								setStyle("-fx-background-color:lightgreen");
							} else {
								setStyle(null);
							}
						}
					}

				};
			}
		});

		input.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				updateConversions();
			}
		});

		input.focusedProperty().addListener((value, old, newv) -> {
			if (newv) {
				input.clear();
			}
		});
		fromUnit.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {
			if (newv != null) {
				updateConversions();
			}
		});

		// Selects the first item
		unitChoices.getSelectionModel().selectFirst();
	}

	private ListCell<ConvertibleTitleWrapper> createUnitChoiceCell() {
		return new ListCell<ConvertibleTitleWrapper>() {
			@Override
			protected void updateItem(ConvertibleTitleWrapper item, boolean empty) {
				super.updateItem(item, empty);

				if (item != null && !empty) {
					setText(item.text);
				} else {
					setText("");
				}
			}
		};
	}

	private void updateConversions() {

		StringBuilder builder = null;
		ObservableList<String> conversions = FXCollections.observableArrayList();
		double value = input.getValue();

		System.out.println("Source : " + fromUnit.getValue() + " Target : " + targetUnits);
		for (Unit u : targetUnits) {
			builder = new StringBuilder();
			builder.append(Formatters.doubleFormat(currentConverter.convert(value, fromUnit.getValue(), u), 1, 8))
					.append(" ").append(u.symbol);

			conversions.add(builder.toString());
		}

		if (currentConverter instanceof CAngleConverter) {
			conversions.add(((CAngleConverter) currentConverter).toDMS(value, fromUnit.getValue()));
		}
		output.setItems(conversions);

	}

	private void selectIS() {

		ObservableList<Unit> items = fromUnit.getItems();

		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).isIS) {
				fromUnit.getSelectionModel().select(i);
				break;
			}
		}
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void languageChanged() {
		convert.setText(BundleWrapper.getString(BundleKey.convert));

		loadUnitChoices();
	}

	private void loadUnitChoices() {
		ObservableList<ConvertibleTitleWrapper> units = FXCollections.observableArrayList();

		units.add(new ConvertibleTitleWrapper(new CLengthConverter(), BundleWrapper.getString(BundleKey.distance)));
		units.add(new ConvertibleTitleWrapper(new CSurfaceConverter(), BundleWrapper.getString(BundleKey.surface)));
		units.add(new ConvertibleTitleWrapper(new CVolumeConverter(), BundleWrapper.getString(BundleKey.volume)));
		units.add(new ConvertibleTitleWrapper(new CSpeedConverter(), BundleWrapper.getString(BundleKey.speed)));
		units.add(new ConvertibleTitleWrapper(new CWeightConverter(), BundleWrapper.getString(BundleKey.weight)));
		units.add(new ConvertibleTitleWrapper(new CForceConverter(), BundleWrapper.getString(BundleKey.force)));
		units.add(new ConvertibleTitleWrapper(new CFrequencyConverter(), BundleWrapper.getString(BundleKey.frequency)));
		units.add(new ConvertibleTitleWrapper(new CPowerConverter(), BundleWrapper.getString(BundleKey.power)));
		units.add(new ConvertibleTitleWrapper(new CTimeConverter(), BundleWrapper.getString(BundleKey.time)));
		units.add(new ConvertibleTitleWrapper(new CWorkConverter(), BundleWrapper.getString(BundleKey.work)));
		units.add(new ConvertibleTitleWrapper(new CAngleConverter(), BundleWrapper.getString(BundleKey.angle)));
		units.add(new ConvertibleTitleWrapper(new CDataConverter(), BundleWrapper.getString(BundleKey.data)));
		units.add(new ConvertibleTitleWrapper(new CPressureConverter(), BundleWrapper.getString(BundleKey.pressure)));

		unitChoices.setItems(units);
	}

	private class ConvertibleTitleWrapper {

		public final Convertible converter;
		public final String text;

		public ConvertibleTitleWrapper(Convertible converter, String text) {

			this.converter = converter;
			this.text = text;
		}

	}
}
