package houseMonitorControllers.calculator;

import java.net.URL;
import java.util.ResourceBundle;

import houseDataCenter.math.AngleUnity;
import houseDataCenter.math.ComplexCalculator;
import houseDataCenter.math.Constant;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Callback;
import views.screenController.ControlledScreen;
import views.screenController.ScreenController;

public class CalculatorController implements Initializable, ControlledScreen {

	@FXML
	private TextField EqBox;

	private ComplexCalculator calculator;

	private ScreenController controller;

	@FXML
	private ToggleButton deg;

	@FXML
	private Label constantLabel;

	@FXML
	private ListView<Constant> constantList;

	@FXML
	private ToggleButton rad;

	@FXML
	private Button root;

	@FXML
	private void answer(ActionEvent event) {
		calculator.answer();
		replaceAll();
	}

	@FXML
	private void clear(ActionEvent event) {

		calculator.clear();
		replaceAll();
	}

	@FXML
	private void equals(ActionEvent event) {
		try {

			Double result = calculator.equals();

			String answer = String.valueOf(result);

			// Infinite key
			if (result.isInfinite()) {
				if (result < 0) {
					answer = "-" + "\u221E";
				} else {
					answer = "\u221E";
				}

			}

			EqBox.setText(answer);

		} catch (Exception e) {
			e.printStackTrace();
			EqBox.setText("Erreur");
			EqBox.setStyle("-fx-font-size:26px;-fx-font-weight:bold;-fx-text-fill:red");
			calculator.clear();
		}
	}

	@FXML
	private void left(ActionEvent event) {

		calculator.moveCursorLeft();
		replaceAll();

	}

	@FXML
	private void right(ActionEvent event) {

		calculator.moveCursorRight();
		replaceAll();

	}

	@FXML
	private void erease(ActionEvent event) {
		calculator.delete();
		replaceAll();
	}

	@FXML
	private void regularHandle(ActionEvent event) {

		calculator.enterValue(((Button) event.getSource()).getText());
		replaceAll();

	}

	private void replaceAll() {
		EqBox.setText(calculator.getActualExpression());
		EqBox.requestFocus();
		EqBox.positionCaret(calculator.getCursorIndex());
		EqBox.setStyle("-fx-font-size:20px;-fx-font-weight:bold;-fx-text-fill:black;");

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("INIT");
		calculator = new ComplexCalculator();
		root.setText("\u221A");

		ToggleGroup group = new ToggleGroup();
		deg.setToggleGroup(group);
		rad.setToggleGroup(group);

		/*
		 * Creates the toggle group for degree buttons
		 */
		group.selectedToggleProperty().addListener((value, old, newv) -> {
			if (newv == null) {
				group.selectToggle(old);
			} else {
				if (newv == deg) {
					calculator.getSettings().setAngleUnit(AngleUnity.DEGREES);
				} else {
					calculator.getSettings().setAngleUnit(AngleUnity.RADIANS);
				}
			}
		});
		initListView();
	}

	private void initListView() {
		constantList.setCellFactory(new Callback<ListView<Constant>, ListCell<Constant>>() {

			@Override
			public ListCell<Constant> call(ListView<Constant> param) {
				// TODO Auto-generated method stub
				return new ListCell<Constant>() {

					@Override
					protected void updateItem(Constant c, boolean empty) {
						super.updateItem(c, empty);

						if (c != null && !empty) {
							// The box
							VBox box = new VBox();

							Label symbol = new Label(c.getSymbol());
							symbol.setFont(new Font(20));

							Label value = new Label(String.valueOf(c.getValue()));
							value.setFont(new Font(12));

							box.getChildren().addAll(symbol, value);
							box.setAlignment(Pos.CENTER);

							this.setGraphic(box);

						}
					}

				};
			}
		});
		constantList.setItems(FXCollections.observableArrayList(Constant.values()));

		constantList.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {
			if (newv != null) {
				calculator.enterValue(newv);
				replaceAll();
				Platform.runLater(() -> constantList.getSelectionModel().clearSelection());
			}
		});
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	

}
