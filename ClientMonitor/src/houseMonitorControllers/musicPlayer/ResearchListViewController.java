package houseMonitorControllers.musicPlayer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.AlbumInfo;
import houseDataCenter.mp3Player.ArtistInfo;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.Playlist;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseDataCenter.mp3Player.PlaylistPlayer.FastTimeEvent;
import houseMonitorViews.musicPlayer.AlbumListView;
import houseMonitorViews.musicPlayer.ApplicationResources;
import houseMonitorViews.musicPlayer.ArtistListView;
import houseMonitorViews.musicPlayer.CheckableMp3Info;
import houseMonitorViews.musicPlayer.MusicListView;
import houseMonitorViews.musicPlayer.PlaylistListView;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import utils.Formatters;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;
import views.screenController.ScreenController.Animations;

public class ResearchListViewController implements MusicEngine, ControlledScreen, MusicCheckListener {

	@FXML
	private VBox background;

	@FXML
	private TabPane tab;

	@FXML
	private Tab musicTab;

	@FXML
	private Tab albumTab;

	@FXML
	private Tab artistTab;

	@FXML
	private Tab playlistTab;

	@FXML
	private Tab newPlaylist;

	@FXML
	private VBox controlBackground;

	@FXML
	private TextField newPlaylistName;

	@FXML
	private ListView<CheckableMp3Info> songsToAdd;

	@FXML
	private Button buildButton;

	@FXML
	private ProgressBar progressSong;

	@FXML
	private ImageView albumImage;

	@FXML
	private Label songNameLabel;

	@FXML
	private Label artistAlbumNameLabel;

	@FXML
	private Button previous;

	@FXML
	private Button pause;

	@FXML
	private Button next;

	@FXML
	private ToggleButton anchorToggle;

	@FXML
	private Label newPlaylistDuration;

	private long currentNewPlaylistLength;
	private List<Mp3SongInfo> currentNewPlaylist;

	private PlaylistManager manager;

	private MusicListView musicListView;
	private AlbumListView albumListView;
	private ArtistListView artistListView;
	private PlaylistListView playlistListView;

	private ChangeListener<Mp3SongInfo> albumListener;
	private ChangeListener<Mp3SongInfo> artistListener;
	private ChangeListener<Mp3SongInfo> playlistListener;

	private Pane insideWatcher;
	private InsideWatcherController insideWatcherController;

	private ScreenController controller;

	private ChangeListener<Mp3SongInfo> listViewChangeListener;

	private Mp3SongInfo currentDisplay;

	private PlaylistPlayer player;

	private boolean init = false;

	private void init() {
		if (manager != null && player != null && !init) {

			System.out.println("Initiated");
			this.currentNewPlaylist = new ArrayList<>();
			initNewPlaylist();

			init = true;
			player.playingProperty().addListener((value, old, newv) -> {
				chargeDataInFields(newv);
			});

			/*
			 * Instantiate the listviews
			 */
			this.albumListView = new AlbumListView();
			this.albumListView.setSeenAlbum(manager.getAlbumInfo());
			this.albumListView.setMaxWidth(Double.MAX_VALUE);
			this.albumListView.setMaxHeight(Double.MAX_VALUE);

			this.artistListView = new ArtistListView();
			this.artistListView.setSeenArtists(manager.getArtistInfo());
			this.artistListView.setMaxWidth(Double.MAX_VALUE);
			this.artistListView.setMaxHeight(Double.MAX_VALUE);

			this.musicListView = new MusicListView(manager.getAllSongsSorted());
			this.musicListView.setMaxWidth(Double.MAX_VALUE);
			this.musicListView.setMaxHeight(Double.MAX_VALUE);

			this.playlistListView = new PlaylistListView(Arrays.asList(new Playlist[] { manager.getMainPlaylist() }));
			this.playlistListView.setMaxWidth(Double.MAX_VALUE);
			this.playlistListView.setMaxHeight(Double.MAX_VALUE);

			/***********************************************
			 * Loads the inside watcher, when an album or an artist is clicked.
			 ***********************************************/
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/houseMonitorViews/musicPlayer/InsideWatcher.fxml"));
			try {
				this.insideWatcher = loader.load();
			} catch (IOException e) {
			}
			this.insideWatcherController = loader.getController();
			/*
			 * Handler called when the button back is pressed on the
			 * insideWatcher
			 */
			this.insideWatcherController.backPressedProperty().addListener((value, old, newv) -> {
				if (newv) {
					// If we are in the album tab
					if (tab.getSelectionModel().getSelectedItem() == this.albumTab) {
						this.albumTab.setContent(albumListView);
						this.albumListView.getSelectionModel().clearSelection();

						// If we are in the artist tab
					} else if (tab.getSelectionModel().getSelectedItem() == this.artistTab) {
						this.artistTab.setContent(artistListView);
						this.artistListView.getSelectionModel().clearSelection();

						// If we are in the playlist tab
					} else if (tab.getSelectionModel().getSelectedItem() == this.playlistTab) {
						this.playlistTab.setContent(playlistListView);
						this.playlistListView.getSelectionModel().clearSelection();
					}
				}
			});

			this.progressSong.progressProperty().bind(player.songProgressProperty());

			/*
			 * Creates a listener on the selected item from the album listview
			 */
			albumListView.getSelectionModel().selectedItemProperty().addListener(((value, old, newv) -> {
				if (newv != null) {

					// removes the inside watcher from other tabs
					isolateInsideWatcher();
					// isolate the listView, so the view can reuse it.
					isolateMusicListView();

					// If the listener is not created yet, we create it.

					albumListener = ((values, olds, newvs) -> {

						// Tells the server to change the playlist to the
						// album's songs
						player.setPlaylist(new Playlist(newv.getSongs(), "temp"));

						try {

							player.playFrom(newvs);

						} catch (Exception e) {
							e.printStackTrace();
						}
					});

					musicListView.setSeenMusics(newv.getSongs());
					musicListView.getSelectionModel().selectedItemProperty().addListener(albumListener);

					this.insideWatcherController.setListView(musicListView);

					this.albumTab.setContent(insideWatcher);
				}
			}

			));

			/*
			 * Creates a listener on the selected item of the artist view
			 */
			artistListView.getSelectionModel().selectedItemProperty().addListener(((value, old, newv) -> {
				if (newv != null) {

					// removes the inside watcher from other tabs
					isolateInsideWatcher();
					// isolate the listView, so the view can reuse it.
					isolateMusicListView();

					// If the listener is not created yet, we create it.

					artistListener = ((values, olds, newvs) -> {

						player.setPlaylist(new Playlist(newv.getSongs(), "temp"));

						try {
							player.playFrom(newvs);
						} catch (Exception e) {
							e.printStackTrace();
						}
					});

					musicListView.setSeenMusics(newv.getSongs());
					musicListView.getSelectionModel().selectedItemProperty().addListener(artistListener);

					this.insideWatcherController.setListView(musicListView);

					this.artistTab.setContent(insideWatcher);
				}

			}));
			/*
			 * Creates a listener on the playlist listview's selected item
			 */
			this.playlistListView.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {
				if (newv != null) {

					// removes the inside watcher from other tabs
					isolateInsideWatcher();
					// isolate the listView, so the view can reuse it.
					isolateMusicListView();

					// If the listener is not created yet, we create it.

					playlistListener = ((values, olds, newvs) -> {
						player.setPlaylist(new Playlist(newv.getSongs(), "temp"));

						try {
							player.playFrom(newvs);
						} catch (Exception e) {
							e.printStackTrace();
						}
					});

					musicListView.setSeenMusics(newv.getSongs());
					musicListView.getSelectionModel().selectedItemProperty().addListener(playlistListener);

					this.insideWatcherController.setListView(musicListView);

					this.playlistTab.setContent(insideWatcher);
				}

			});

			/*
			 * Creates the toggleButton listener on the research category
			 * buttons
			 */
			tab.getSelectionModel().selectedItemProperty().addListener((value, old, newv) -> {

				// reset the tabs to their initial content
				resetTabs();

				if (newv == null) {
					// Always a value selected
					tab.getSelectionModel().select(old);

				}

				if (newv == musicTab) {
					// If back to the music tab, gives the musiclistView the
					// full list of songs
					System.out.println("happened");
					musicListView.setItems(FXCollections.observableArrayList(manager.getAllSongsSorted()));
				} else if (newv == playlistTab) {
					playlistListView.setItems(FXCollections.observableArrayList(manager.getSubPlaylists()));
				} else if (newv == newPlaylist) {
					SortedSet<Mp3SongInfo> songs = manager.getAllSongsSorted();
					if (songsToAdd.getItems().size() != songs.size()) {
						songsToAdd.setItems(CheckableMp3Info.generateCheckForSongs(songs));
					}
				} else if (newv == albumTab) {
					Set<AlbumInfo> infos = manager.getAlbumInfo();
					if (albumListView.getItems().size() != infos.size()) {
						albumListView.setItems(FXCollections.observableArrayList(infos));
					}
				} else if (newv == artistTab) {
					Set<ArtistInfo> infos = manager.getArtistInfo();
					if (artistListView.getItems().size() != infos.size()) {
						artistListView.setItems(FXCollections.observableArrayList(infos));
					}
				}

			});

			/***********************************************
			 * Set the image event handler on mouseHover/mouseExcited On the
			 * album's image
			 ***********************************************/
			albumImage.setOnMouseEntered((event) -> {
				albumImage.setScaleX(1.1);
				albumImage.setScaleY(1.1);
			});
			albumImage.setOnMouseExited((event) -> {
				albumImage.setScaleX(1);
				albumImage.setScaleY(1);
			});

			/*
			 * Creates the listeners on isPlayingProperty from the
			 * playlistPlayer.
			 */

			player.isPlayingProperty().addListener((value, old, newv) -> {
				System.out.println("FUUUUUUUUUUUUUUUUUUUUUU " + newv);
				Platform.runLater(() -> {
					if (newv) {
						// The playlist is playing

						pause.getStyleClass().removeAll("playButton");
						pause.getStyleClass().add("pauseButton");

					} else {
						pause.getStyleClass().removeAll("pauseButton");
						pause.getStyleClass().add("playButton");

					}
				});
			});

			if (player.isPlaying()) {
				pause.getStyleClass().add("pauseButton");
			} else {
				pause.getStyleClass().add("playButton");
			}

			// Sets the text to the center of the bar
			progressSong.setMaxWidth(Double.MAX_VALUE);
			background.setMinHeight(0);
			background.setMaxHeight(Double.MAX_VALUE);

			/*
			 * Instantiate the listener on the selection model. Often removed
			 * and put back again.
			 */
			listViewChangeListener = new ChangeListener<Mp3SongInfo>() {

				@Override
				public void changed(ObservableValue<? extends Mp3SongInfo> observable, Mp3SongInfo oldValue,
						Mp3SongInfo newValue) {

					// When the searchBar is expended and the text is not empty,
					// we
					// reset the view (a research has been made)

					if (!anchorToggle.isSelected() && oldValue != newValue && newValue != null) {

						displayPlaying(null);
					}
					// If we're on the music tab. Else, it is handled by other
					// listeners.
					if (newValue != null && newValue != oldValue) {

						player.setPlaylist(manager.getMainPlaylist());
						try {

							try {
								player.playFrom(newValue);
							} catch (Exception e) {
								e.printStackTrace();
							}

						} catch (IndexOutOfBoundsException e) {
							e.printStackTrace();
						}

					}

				}
			};
			// Gives the listener
			musicListView.getSelectionModel().selectedItemProperty().addListener(listViewChangeListener);
			chargeDataInFields(player.getPlaying());
		}
		// Select the song toggle from the list by default
		tab.getSelectionModel().select(musicTab);

		// set all the default contents.
		resetTabs();

	}

	private void initNewPlaylist() {

		this.songsToAdd.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		this.songsToAdd.setCellFactory(new Callback<ListView<CheckableMp3Info>, ListCell<CheckableMp3Info>>() {

			@Override
			public ListCell<CheckableMp3Info> call(ListView<CheckableMp3Info> param) {
				// TODO Auto-generated method stub
				return new ListCell<CheckableMp3Info>() {

					@Override
					protected void updateItem(CheckableMp3Info ch, boolean empty) {
						super.updateItem(ch, empty);

						if (ch != null && !empty) {
							FXMLLoader loader = new FXMLLoader(
									getClass().getResource("/houseMonitorViews/musicPlayer/CheckableMusicView.fxml"));
							try {
								Node n = loader.load();
								CheckableMusicItem contr = loader.getController();

								n.setOnMouseClicked((event) -> {
									contr.switchSelected();
								});

								contr.setCheckableSong(ch);
								contr.setMusicCheckListener(ResearchListViewController.this);
								setGraphic(n);

							} catch (IOException e) {
								e.printStackTrace();
							}

						} else {
							setGraphic(null);
						}
					}

				};
			};
		});

		songsToAdd.setItems(CheckableMp3Info.generateCheckForSongs(manager.getAllSongsSorted()));

	}

	/**
	 * 
	 */
	private void resetTabs() {

		isolateMusicListView();
		this.musicTab.setContent(musicListView);
		this.albumTab.setContent(albumListView);
		this.artistTab.setContent(artistListView);
		this.playlistTab.setContent(playlistListView);

		if (musicListView != null) {
			musicListView.getSelectionModel().clearSelection();
			albumListView.getSelectionModel().clearSelection();
			artistListView.getSelectionModel().clearSelection();
			playlistListView.getSelectionModel().clearSelection();

		}
	}

	/**
	 * 
	 */
	private void isolateInsideWatcher() {

		if (artistTab.getContent() == insideWatcher) {
			artistTab.setContent(artistListView);
		} else if (albumTab.getContent() == insideWatcher) {
			albumTab.setContent(albumListView);
		} else if (musicTab.getContent() == insideWatcher) {
			musicTab.setContent(null);
		} else if (playlistTab.getContent() == insideWatcher) {
			playlistTab.setContent(playlistListView);
		}
	}

	@FXML
	private void buildPlaylist(ActionEvent event) {

		String name = newPlaylistName.getText();
		Playlist playlist = new Playlist(currentNewPlaylist, name);
		manager.addSubPlaylist(playlist);

		System.out.println("NEW PLAYLIST BUILT");
		clearNewPlaylist();

		tab.getSelectionModel().select(playlistTab);

	}

	private void clearNewPlaylist() {
		newPlaylistName.setText("");
		newPlaylistDuration.setText("00:00");
		currentNewPlaylistLength = 0;
		currentNewPlaylist.clear();
		songsToAdd.setItems(FXCollections.observableArrayList());
		songsToAdd.setItems(CheckableMp3Info.generateCheckForSongs(manager.getAllSongsSorted()));
	}

	/**
	 * Removes the musicListView from any views it could be contained in. As the
	 * listView is reused from a view to another.
	 */
	private void isolateMusicListView() {
		if (artistTab.getContent() == musicListView) {
			artistTab.setContent(null);
		} else if (albumTab.getContent() == musicListView) {
			albumTab.setContent(null);
		} else if (musicTab.getContent() == musicListView) {
			musicTab.setContent(null);
		} else if (playlistTab.getContent() == musicListView) {
			playlistTab.setContent(null);
		}
		if (insideWatcherController != null) {
			insideWatcherController.setListView(null);
		}
		if (albumListener != null)
			musicListView.getSelectionModel().selectedItemProperty().removeListener(albumListener);
		if (artistListener != null)
			musicListView.getSelectionModel().selectedItemProperty().removeListener(artistListener);
		if (playlistListener != null)
			musicListView.getSelectionModel().selectedItemProperty().removeListener(playlistListener);

	}

	/**
	 * 
	 */
	public void chargeDataInFields(Mp3SongInfo song) {

		if (song != null) {

			this.currentDisplay = song;

			// Chose the appropriate artist name.
			String artist = song.getArtistName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownArtist)
					: song.getArtistName();

			String album = song.getAlbumName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
					: song.getAlbumName();

			artistAlbumNameLabel.setText(artist + " - " + album);
			songNameLabel.setText(song.getTitle());

			if (song.getAlbumCover() != null) {
				ByteArrayInputStream bais = new ByteArrayInputStream(song.getAlbumCover());
				albumImage.setImage(new Image(bais));
			} else {
				albumImage.setImage(ApplicationResources.chooseRandomImage(ApplicationResources.SONG_IMAGES));
			}
		} else {
			artistAlbumNameLabel.setText("");
			songNameLabel.setText("");
			albumImage.setImage(ApplicationResources.chooseRandomImage(ApplicationResources.SONG_IMAGES));
		}

	}

	/**
	 * 
	 * @param event
	 */
	@FXML
	private void controlButtonPressed(MouseEvent event) {
		if (event.getSource() == pause) {
			player.switchPlaying();
		} else if (event.getSource() == next) {
			player.forwardOrBackwardPressed(FastTimeEvent.FORWARD);

		} else if (event.getSource() == previous) {
			player.forwardOrBackwardPressed(FastTimeEvent.BACKWARD);
		}
	}

	/**
	 * 
	 * @param event
	 */
	@FXML
	private void displayPlaying(MouseEvent event) {
		controller.setScreen(Screen.MUSIC_PLAYING, Animations.TRANSLATE_BOTTOM_TO_CENTER);
	}

	/**
	 * 
	 * @param event
	 */
	@FXML
	private void controlButtonReleased(MouseEvent event) {
		player.forwardOrBackwardReleased();
	}

	/**
	 * 
	 */
	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;

	}

	/**
	 * 
	 */
	@Override
	public void displayedToScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMusicChecked(CheckableMusicItem item, Mp3SongInfo song) {

		System.out.println("Checked");
		this.currentNewPlaylistLength += song.getLength();
		this.currentNewPlaylist.add(song);
		this.newPlaylistDuration.setText(Formatters.formatTime(currentNewPlaylistLength));
	}

	@Override
	public void onMusicUnchecked(CheckableMusicItem item, Mp3SongInfo song) {
		System.out.println("Unchecked");

		this.currentNewPlaylistLength -= song.getLength();
		this.currentNewPlaylist.remove(song);
		this.newPlaylistDuration.setText(Formatters.formatTime(currentNewPlaylistLength));

	}

	public void displayAllSongs() {

		tab.getSelectionModel().select(musicTab);
	}

	@Override
	public void setPlayers(PlaylistManager manager, PlaylistPlayer player) {
		this.player = player;
		this.manager = manager;
		init();
	}

	@Override
	public void playerVolumeChanged() {
		// TODO Auto-generated method stub
		
	}

}
