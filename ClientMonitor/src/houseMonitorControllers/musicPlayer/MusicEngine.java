package houseMonitorControllers.musicPlayer;

import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;

public interface MusicEngine {

	public void setPlayers(PlaylistManager manager, PlaylistPlayer player);
	
	public void playerVolumeChanged();
}
