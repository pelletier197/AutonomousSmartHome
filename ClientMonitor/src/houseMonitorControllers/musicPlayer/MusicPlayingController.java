package houseMonitorControllers.musicPlayer;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import custom.CommunicationCenter;
import house.ClientParameters;
import house.RoomParams;
import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseDataCenter.mp3Player.PlaylistPlayer.FastTimeEvent;
import houseDataCenter.mp3Player.PlaylistPlayer.READING_MODE;
import houseMonitorViews.musicPlayer.ApplicationResources;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import server.communication.Request;
import server.transport.Communicator;
import util.IOUtils;
import utils.Formatters;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;
import views.screenController.ScreenController.Animations;

public class MusicPlayingController implements Communicator, Initializable, ControlledScreen, MusicEngine {

	private Image originalImage;

	@FXML
	private VBox background;

	@FXML
	private ImageView songImage;

	@FXML
	private ImageView soundImage;

	@FXML
	private Label labelSong;

	@FXML
	private Label labelArtistAlbum;

	@FXML
	private Label labelActualTime;

	@FXML
	private Label labelTotalTime;

	@FXML
	private Label soundVolumeSlider;

	@FXML
	private Slider progressSong;

	@FXML
	private Slider soundSlider;

	@FXML
	private Button previous;

	@FXML
	private Button pause;

	@FXML
	private Button next;

	@FXML
	private ToggleButton normal;

	@FXML
	private ToggleButton random;

	@FXML
	private ToggleButton repeat;

	private ScreenController controller;

	private ToggleGroup playlistToggle;

	private Mp3SongInfo currentDisplay;

	private PlaylistPlayer player;

	private static final Image mute = new Image(
			MusicPlayingController.class.getResource("/resources/images/sound-mute.png").toExternalForm());

	private static final Image sound_on = new Image(
			MusicPlayingController.class.getResource("/resources/images/sound-on.png").toExternalForm());

	private boolean init = false;

	private ChangeListener<Number> progressListener = (value, old, newv) -> {
		labelActualTime.setText(Formatters.formatTime(player.getCurrentTime()));
	};

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		originalImage = songImage.getImage();

		pause.getStyleClass().add("playButton");
		songImage.fitWidthProperty().bind(background.widthProperty());

		init();

	}

	private void init() {

		if (player != null && !init) {

			init = true;

			this.player.isPlayingProperty().addListener((value, old, newv) -> {
				setControlButtonStyle();
				System.out.println("controll button style changed");
			});

			player.playingProperty().addListener((value, old, newv) -> {
				chargeDataInFields(newv);
			});

			this.progressSong.valueProperty().addListener(progressListener);
			this.progressSong.valueProperty().bind(player.songProgressProperty());

			playlistToggle = new ToggleGroup();
			normal.setToggleGroup(playlistToggle);
			random.setToggleGroup(playlistToggle);
			repeat.setToggleGroup(playlistToggle);

			selectCorrespondingToggle(READING_MODE.NORMAL);

			playlistToggle.selectedToggleProperty().addListener((value, old, newv) -> {

				if (newv == normal) {
					player.setReadingMode(READING_MODE.NORMAL);

				} else if (newv == random) {
					player.setReadingMode(READING_MODE.RANDOM);

				} else if (newv == repeat) {
					player.setReadingMode(READING_MODE.REPEAT);

				} else if (newv == null) {
					playlistToggle.selectToggle(old);
				}

			});

			player.readingModeProperty().addListener((value, old, newv) -> {
				selectCorrespondingToggle(newv);
			});

			soundImage.getStyleClass().add("soundIcon-on");
			soundSlider.setOpacity(0);

			soundSlider.valueProperty().addListener((value, old, newv) -> {
				if (newv.doubleValue() == 0) {
					soundImage.setImage(mute);

				} else if (old.doubleValue() == 0d) {
					soundImage.setImage(sound_on);
				}
			});

			soundSlider.valueProperty().addListener((value, old, newv) -> {

				player.setVolume(newv.doubleValue());

			});

			soundSlider.setOnMouseReleased(new EventHandler<MouseEvent>() {

				@Override
				public void handle(MouseEvent event) {

					RoomParams params = ClientParameters.getParams();
					params.setVolume(soundSlider.getValue());
					IOUtils.writeClientParameters(params);
					com.send(new Request(TransferFlags.SET_ROOM_VOLUME, soundSlider.getValue()));
				}
			});
			
			soundSlider.setValue(player.volumeProperty().get());
			
			soundVolumeSlider.textProperty()
					.bind(soundSlider.valueProperty().multiply(100).asString("%1.0f").concat("%"));
			soundVolumeSlider.opacityProperty().bind(soundSlider.opacityProperty());
			chargeDataInFields(player.getPlaying());
			this.setControlButtonStyle();
		}
	}

	private void setControlButtonStyle() {
		if (player.isPlaying()) {

			System.out.println("isplaying true");
			// The playlist is playing
			pause.getStyleClass().removeAll("playButton");
			pause.getStyleClass().add("pauseButton");

		} else {

			System.out.println("isplaying false");

			pause.getStyleClass().removeAll("pauseButton");
			pause.getStyleClass().add("playButton");

		}

	}

	private void selectCorrespondingToggle(READING_MODE mode) {
		switch (mode) {
		case NORMAL:
			playlistToggle.selectToggle(normal);
			break;
		case RANDOM:
			playlistToggle.selectToggle(random);
			break;
		case REPEAT:
			playlistToggle.selectToggle(repeat);
			break;
		}
	}

	public void chargeDataInFields(Mp3SongInfo song) {

		if (song != null) {

			this.currentDisplay = song;
			// Chose the appropriate artist name.
			String artist = song.getArtistName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownArtist)
					: song.getArtistName();

			String album = song.getAlbumName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
					: song.getAlbumName();

			labelArtistAlbum.setText(artist + " - " + album);
			labelSong.setText(song.getTitle());
			labelTotalTime.setText(Formatters.formatTime(currentDisplay.getLength()));

			if (song.getAlbumCover() != null) {
				ByteArrayInputStream bais = new ByteArrayInputStream(song.getAlbumCover());
				songImage.setImage(new Image(bais));
			} else {

				songImage.setImage(ApplicationResources.chooseRandomImage(ApplicationResources.SONG_IMAGES));
			}
		} else {
			labelArtistAlbum.setText("");
			labelSong.setText("");
			labelTotalTime.setText(Formatters.formatTime(0));
			songImage.setImage(ApplicationResources.chooseRandomImage(ApplicationResources.SONG_IMAGES));
		}

	}

	private Service<Void> sliderHider;

	@FXML
	private void handleSoundImageClicked(MouseEvent event) {
		System.out.println("allo");
		if (!soundSlider.isVisible()) {
			soundSlider.setVisible(true);
			soundSlider.setMinHeight(0);
			soundSlider.setPrefHeight(0);
			final Timeline animation = new Timeline(
					new KeyFrame(Duration.millis(500), new KeyValue(soundSlider.opacityProperty(), 1)),
					new KeyFrame(Duration.millis(500), new KeyValue(soundSlider.minHeightProperty(), 140)));
			animation.play();

		}
		if (sliderHider == null) {
			sliderHider = new Service<Void>() {

				@Override
				protected Task<Void> createTask() {

					return new Task<Void>() {

						@Override
						protected Void call() throws Exception {
							Thread.sleep(4000);
							final Timeline animation = new Timeline();
							soundSlider.setMaxHeight(150);
							soundSlider.setPrefHeight(150);
							animation.getKeyFrames()
									.setAll(new KeyFrame(Duration.millis(500),
											new KeyValue(soundSlider.opacityProperty(), 0)),
									new KeyFrame(Duration.millis(500),
											new KeyValue(soundSlider.maxHeightProperty(), 0)));

							animation.setOnFinished((event) -> {
								soundSlider.setVisible(false);
							});

							animation.play();
							return null;
						}
					};
				}
			};
		}
		soundSlider.valueProperty().addListener((value, old, newv) -> {
			sliderHider.restart();
		});

		sliderHider.restart();

	}

	private boolean grabbed = false;

	private CommunicationCenter com;

	@FXML
	private void progressMouseClicked(MouseEvent event) {
		progressSong.valueProperty().unbind();
		// Makes sure text will not be updated while showing current seek time.
		player.songProgressProperty().removeListener(progressListener);

		/*
		 * Simple calculus to set the value of the progressSong depending on
		 * where the user clicked.
		 * 
		 * (valueX of the click in scene)/((maxX of slider in scene)-(minX of
		 * slider in scene)) = progress from 0 to 1
		 */
		progressSong.setValue(event.getX() / (progressSong.localToScene(progressSong.getBoundsInLocal()).getMaxX()
				- (progressSong.localToScene(progressSong.getBoundsInLocal()).getMinX())));

		progressSong.valueProperty().addListener((value, old, newv) -> {

			labelActualTime.setText(Formatters.formatTime(newv.doubleValue(), currentDisplay.getLength()));

		});

		grabbed = true;

	}

	/**
	 * Called by the view when the slider from the view is released.
	 * 
	 * It will automatically change the progress of the song to the value
	 * specified by {@link #progressSong}.
	 * 
	 * The value of the slider is binded again
	 * 
	 * @param event
	 */
	@FXML
	private void progressMouseReleased(MouseEvent event) {

		player.setProgress(progressSong.getValue());
		player.songProgressProperty().addListener(progressListener);
		this.grabbed = false;

		progressSong.valueProperty().bind(player.songProgressProperty());

	}

	/**
	 * Method called by the view in the event that the button forward or
	 * backward are pressed/held. This method will start a thread that will be
	 * running until the method {@link #controlButtonReleased(MouseEvent)} gets
	 * called by the view. This method will set the value of {@link #isPressed}
	 * to false, and the thread will stop.
	 * 
	 * At the end of the thread, if it has been running for more than 1 second,
	 * it's considered as a held from the user, and the time will have gone
	 * forward or backward from a few seconds, depending on the event.
	 * 
	 * In another hand, if the button is simply clicked, the method
	 * {@link #handleReleased(MouseEvent)} is called, to handle the button
	 * release and set the values to false.
	 * 
	 * 
	 * 
	 * @param event
	 */
	@FXML
	private void backOrForwardPressed(MouseEvent event) {

		if (event.getSource() == next) {
			player.forwardOrBackwardPressed(FastTimeEvent.FORWARD);
		} else if (event.getSource() == previous) {
			player.forwardOrBackwardPressed(FastTimeEvent.BACKWARD);
		}

	}

	/**
	 * Called by the view when the control button is released. Set the value of
	 * isPressed and wasDragged to false after the method. wasDragged value is
	 * used to know if the buttons were dragged, or simply clicked.
	 * 
	 * If the event's source is the pause Button, the method handleReleased is
	 * directly called, because the view won't call it from the thread contained
	 * in the {@link #handleReleased(MouseEvent)} method}}
	 * 
	 * @param event
	 */
	@FXML
	private void controlButtonReleased(MouseEvent event) {
		if (event.getSource() == pause) {
			player.switchPlaying();
		} else {
			player.forwardOrBackwardReleased();
		}
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;

	}

	@Override
	public void displayedToScreen() {
		// TODO Auto-generated method stub

	}

	@FXML
	private void showListView(ActionEvent event) {
		controller.setScreen(Screen.MUSIC_LIST_VIEW, Animations.TRANSLATE_TOP_TO_CENTER);
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setPlayers(PlaylistManager manager, PlaylistPlayer player) {
		this.player = player;
		init();
	}

	@Override
	public void setCommunicationCenter(CommunicationCenter com) {
		this.com = com;
	}

	@Override
	public void playerVolumeChanged() {
		soundSlider.setValue(player.volumeProperty().get());
	}
}
