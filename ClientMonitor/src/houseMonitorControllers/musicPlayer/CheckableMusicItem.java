package houseMonitorControllers.musicPlayer;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.Mp3SongInfo;
import houseMonitorViews.musicPlayer.CheckableMp3Info;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import utils.Formatters;

public class CheckableMusicItem {

	@FXML
	private CheckBox checkBox;

	@FXML
	private Label name;

	@FXML
	private Label artistAlbum;

	@FXML
	private Label time;

	private CheckableMp3Info ch;
	private Mp3SongInfo song;

	public void setCheckableSong(CheckableMp3Info ch) {

		this.ch = ch;
		this.song = ch.song;
		name.setText(song.getTitle());

		String artist = song.getArtistName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownArtist)
				: song.getArtistName();

		String album = song.getAlbumName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
				: song.getAlbumName();

		this.artistAlbum.setText(artist + " - " + album);
		time.setText(Formatters.formatTime(song.getLength()));
		this.checkBox.setSelected(ch.checked);

	}

	public void setMusicCheckListener(MusicCheckListener listener) {

		checkBox.selectedProperty().addListener((value, old, newv) -> {
			if (newv) {
				listener.onMusicChecked(CheckableMusicItem.this, song);
				ch.checked = true;

			} else {
				listener.onMusicUnchecked(CheckableMusicItem.this,song);
				ch.checked = false;
			}
		});
	}

	public void switchSelected() {
		// Reverse the selected value
		checkBox.setSelected(!checkBox.isSelected());
	}

	public void setSelected(boolean value) {
		checkBox.setSelected(value);
	}

}
