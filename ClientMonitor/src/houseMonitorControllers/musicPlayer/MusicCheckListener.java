package houseMonitorControllers.musicPlayer;

import houseDataCenter.mp3Player.Mp3SongInfo;

public interface MusicCheckListener {

	public void onMusicChecked(CheckableMusicItem item, Mp3SongInfo song);

	public void onMusicUnchecked(CheckableMusicItem item, Mp3SongInfo song);
}
