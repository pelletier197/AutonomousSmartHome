package houseMonitorControllers.musicPlayer;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.ArtistInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class ArtistViewController {
	@FXML
	private Label artistName;

	@FXML
	private Label numberOfSongs;

	public void setArtistInfo(ArtistInfo infos) {

		String artistName = infos.getArtist().getName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
				: infos.getArtist().getName();

		this.artistName.setText(artistName);
		this.numberOfSongs.setText(infos.getNumberOfSongs() + " " + BundleWrapper.getString(BundleKey.songs));
	}

}
