package houseMonitorControllers.musicPlayer;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class InsideWatcherController {

	@FXML
	private VBox background;

	@FXML
	private Button back;

	private BooleanProperty backPressed = new SimpleBooleanProperty();

	@FXML
	private void backPressed(MouseEvent event) {
		backPressed.set(true);
	}

	@FXML
	private void backReleased(MouseEvent event) {
		backPressed.set(false);
	}

	public ReadOnlyBooleanProperty backPressedProperty() {
		return backPressed;
	}

	/**
	 * Sets the listView that this view displays on the second space of the
	 * VBox. This parameter can be null. A null parameter will simply remove any
	 * children from the listView's position.
	 * 
	 * @param listView
	 *            The listView that is placed in the view. Can be null.
	 */
	public void setListView(ListView<? extends Object> listView) {
		if (listView == null && background.getChildren().size() == 2) {
			background.getChildren().remove(1);
		} else if (listView != null && background.getChildren().size() == 1) {
			this.background.getChildren().add(1, listView);
		} else if (listView != null) {
			this.background.getChildren().set(1, listView);
		}
		if (listView != null) {
			listView.setMaxHeight(Double.MAX_VALUE);
			listView.setMaxHeight(Double.MAX_VALUE);
			VBox.setVgrow(listView, Priority.ALWAYS);
		}
	}
}
