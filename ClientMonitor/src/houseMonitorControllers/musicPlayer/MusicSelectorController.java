package houseMonitorControllers.musicPlayer;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import houseDataCenter.mp3Player.Mp3SongInfo;
import houseDataCenter.mp3Player.PlaylistManager;
import houseDataCenter.mp3Player.PlaylistPlayer;
import houseMonitorViews.musicPlayer.CheckableMp3Info;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import views.screenController.ControlledScreen;
import views.screenController.Screen;
import views.screenController.ScreenController;

public class MusicSelectorController implements Initializable, ControlledScreen, MusicCheckListener, MusicEngine {

	@FXML
	private ListView<CheckableMp3Info> songsView;

	private CheckableMusicItem lastSelected;
	private Mp3SongInfo lastSong;
	private Screen next;

	private ScreenController controller;

	private Runnable onSongSet;

	private PlaylistPlayer player;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		initMusicSelector();
	}

	private void initMusicSelector() {

		handleNext = true;

		this.songsView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		this.songsView.setCellFactory(new Callback<ListView<CheckableMp3Info>, ListCell<CheckableMp3Info>>() {

			@Override
			public ListCell<CheckableMp3Info> call(ListView<CheckableMp3Info> param) {
				// TODO Auto-generated method stub
				return new ListCell<CheckableMp3Info>() {

					@Override
					protected void updateItem(CheckableMp3Info ch, boolean empty) {
						super.updateItem(ch, empty);

						if (ch != null && !empty) {
							FXMLLoader loader = new FXMLLoader(
									getClass().getResource("/houseMonitorViews/musicPlayer/CheckableMusicView.fxml"));
							try {
								Node n = loader.load();
								CheckableMusicItem contr = loader.getController();

								n.setOnMouseClicked((event) -> {
									// If its not the same then before
									contr.setSelected(true);

								});

								contr.setCheckableSong(ch);
								contr.setMusicCheckListener(MusicSelectorController.this);
								setGraphic(n);

								// It is the song selected by default
								if (ch.checked) {
									lastSelected = contr;
									lastSong = ch.song;
								}

							} catch (IOException e) {
								e.printStackTrace();
							}

						} else {
							setGraphic(null);
						}
					}

				};
			};
		});
	}

	public void setPlaylistManager(PlaylistManager manager) {

		Set<Mp3SongInfo> songs = manager.getAllSongsSorted();
		ObservableList<CheckableMp3Info> info = CheckableMp3Info.generateCheckForSongs(songs);

		if (!info.isEmpty()) {
			// Checks the first
			info.get(0).checked = true;
		}

		songsView.setItems(info);
		songsView.refresh();
		songsView.getSelectionModel().selectFirst();
	}

	public void setNextScreen(Screen s) {
		next = s;
	}

	public Mp3SongInfo getSelectedSong() {
		return lastSong;
	}

	@FXML
	private void back(ActionEvent event) {
		controller.setPreviousScreen();
	}

	@FXML
	private void next(ActionEvent event) {

		onSongSet.run();

		if (next != null) {
			controller.setScreen(next);
		} else {
			controller.setPreviousScreen();
		}
	}

	@Override
	public void setScreenController(ScreenController SC) {
		this.controller = SC;
	}

	@Override
	public void displayedToScreen() {
		next = null;
		lastSelected = null;
		lastSong = null;
		onSongSet = null;
	}

	@Override
	public void removedFromScreen() {
		// TODO Auto-generated method stub

	}

	private boolean handleNext;

	@Override
	public void onMusicChecked(CheckableMusicItem item, Mp3SongInfo song) {
		if (handleNext) {
			if (lastSelected != null) {
				handleNext = false;
				lastSelected.setSelected(false);
			}
			lastSelected = item;
			lastSong = song;
		}
		handleNext = true;
	}

	@Override
	public void onMusicUnchecked(CheckableMusicItem item, Mp3SongInfo song) {
		if (handleNext) {
			if (song == lastSong && lastSelected != null) {
				handleNext = false;
				lastSelected.setSelected(true);
			}
		}
		handleNext = true;
	}

	public void onSongSetListener(Runnable runnable) {
		onSongSet = runnable;
	}

	@Override
	public void setPlayers(PlaylistManager manager, PlaylistPlayer player) {
		this.player = player;
		setPlaylistManager(manager);
	}

	@Override
	public void playerVolumeChanged() {
		// TODO Auto-generated method stub
		
	}
}
