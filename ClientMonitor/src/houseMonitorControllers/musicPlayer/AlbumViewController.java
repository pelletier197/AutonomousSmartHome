package houseMonitorControllers.musicPlayer;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import houseDataCenter.applicationRessourceBundle.BundleKey;
import houseDataCenter.applicationRessourceBundle.BundleWrapper;
import houseDataCenter.mp3Player.Album;
import houseDataCenter.mp3Player.AlbumInfo;
import houseMonitorViews.musicPlayer.ApplicationResources;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import utils.Formatters;

public class AlbumViewController implements Initializable {

	@FXML
	private HBox background;

	@FXML
	private ImageView albumImage;

	@FXML
	private Label albumName;

	@FXML
	private Label songCount;

	@FXML
	private Label length;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		albumImage.fitHeightProperty().bind(background.heightProperty());
		albumImage.fitWidthProperty().bind(albumImage.fitHeightProperty());
		albumImage.setPreserveRatio(true);

	}

	public void setAlbumInfo(AlbumInfo album) {
		try {
			this.albumImage.setImage(new Image(new ByteArrayInputStream(album.getAlbum().getCover())));
		} catch (Exception e) {
			this.albumImage.setImage(ApplicationResources.chooseRandomImage(ApplicationResources.SONG_IMAGES));
		}

		String albumName = album.getAlbum().getName().isEmpty() ? BundleWrapper.getString(BundleKey.unknownAlbum)
				: album.getAlbum().getName();

		this.albumName.setText(albumName);

		songCount.setText(
				Formatters.formatAsInteger(album.getNumberOfSongs()) + " " + BundleWrapper.getString(BundleKey.songs));
		length.setText(Formatters.formatTime(album.getLength()));
	}

}
