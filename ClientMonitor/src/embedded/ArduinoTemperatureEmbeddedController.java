package embedded;

import java.io.IOException;
import java.nio.ByteBuffer;

import embedded.controllers.baseboard.AbstractBaseboard;
import embedded.controllers.thermometer.AbstractThermometer;

public class ArduinoTemperatureEmbeddedController implements AbstractBaseboard, AbstractThermometer{

	private static final String PATH_TEMP_SENSOR = "/arduino/getTemperature.ino.standard.hex";
	private static final String PATH_ACTIVATE_BASEBOARD = "/arduino/activateBaseboard.ino.standard.hex";
	private static final String PATH_DEACTIVATE_BASEBOARD = "/arduino/deactivateBaseboard.ino.standard.hex";

	private ArduinoController controller;

	public ArduinoTemperatureEmbeddedController() {
		this.controller = new ArduinoController();
	}

	/**
	 * Activates the baseboard of the room.
	 * 
	 * <p>
	 * Take note that this operation may be extremely long as the program has to
	 * be loaded to the micro-controller every time it is runned.
	 * </p>
	 * 
	 * 
	 * @return True if the operation worked, false otherwise.
	 */
	@Override
	public boolean activateBaseboard() {
		try {
			controller.executeProgram(toResourcePath(PATH_ACTIVATE_BASEBOARD));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Deactivates the baseboard of the room.
	 * 
	 * <p>
	 * Take note that this operation may be extremely long as the program has to
	 * be loaded to the micro-controller every time it is runned.
	 * </p>
	 * 
	 * 
	 * @return True if the operation worked, false otherwise.
	 */
	@Override
	public boolean deactivateBaseboard() {
		try {
			controller.executeProgram(toResourcePath(PATH_DEACTIVATE_BASEBOARD));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Reads the temperature and the humidity of the room directly from the
	 * micro-controller and returns their values in an array where data order is
	 * [temperature (C), Humidity (%)]
	 * 
	 * <p>
	 * Take note that this operation may be extremely long as the program has to
	 * be loaded to the micro-controller every time it is runned.
	 * </p>
	 * 
	 * @return The values of temperature and humidity of the room
	 * @throws If any error occurs
	 * @throws IOException 
	 */
	public float[] getTemperatureAndHumitity() throws Exception {
		// 8 bytes because we read 2 floats of 4 bytes each
		byte[] result = controller.executeProgramReturnByteArray(toResourcePath(PATH_TEMP_SENSOR), 8);

		ByteBuffer buffer = ByteBuffer.wrap(result);

		float[] tempHumiValues = new float[2];
		tempHumiValues[0] = ArduinoController.readNextFloat(buffer);
		tempHumiValues[1] = ArduinoController.readNextFloat(buffer);

		return tempHumiValues;
	}

	private String toResourcePath(String path) {
		System.out.println("path : " + getClass().getResource(path).getFile());
		return getClass().getResource(path).getFile();
	}

	@Override
	public double getTemperature() {
		// TODO Auto-generated method stub
		return 0;
	}

}
