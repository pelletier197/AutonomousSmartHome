package embedded;

public interface EmbeddedController {

	public int executeProgramReturnInt(String path) throws Exception;
	
	public double executeProgramReturnFloat(String path) throws Exception;
	
	public byte[] executeProgramReturnByteArray(String path, int count) throws Exception;
}
