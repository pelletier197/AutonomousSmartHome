package embedded;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class ArduinoController implements EmbeddedController {

	private static final String[] args = new String[] { "C:/Program Files (x86)/Arduino/hardware/tools/avr/bin/avrdude",
			"-CC:/Program Files (x86)/Arduino/hardware/tools/avr/etc/avrdude.conf", "-v", "-patmega328p", "-carduino",
			"-PCOM3", "-b115200", "-D", "-Uflash:w:" };
	private static final int insertPathIndex = 8;
	private static final String PORT_NAME = "COM3";

	// Communication data
	private static final int DATA_RATE = 9600;
	private static final int TIME_OUT = 3000;

	@Override
	public int executeProgramReturnInt(String path) throws Exception {

		byte[] bytes = executeProgramReturnByteArray(path, 4);

		// Default transfer is litle endian
		ByteBuffer buf = ByteBuffer.wrap(bytes);
		buf.order(ByteOrder.LITTLE_ENDIAN);

		return buf.getInt();
	}

	@Override
	public double executeProgramReturnFloat(String path) throws Exception {
		byte[] bytes = executeProgramReturnByteArray(path, 4);

		ByteBuffer buf = ByteBuffer.wrap(bytes);
		buf.order(ByteOrder.LITTLE_ENDIAN);

		return buf.getFloat();
	}

	/**
	 * Executes the Arduino program given from the path, and read the given
	 * number of bytes from the serial port.
	 * 
	 * <p>
	 * It is really important to know that the Arduino.write method writes bytes
	 * backward for each value, and that the result array will also be returned
	 * backward. So for example, if you write
	 * <li>[[1,2,3,4], [5,6,7,8]]</li> where the brackets delimiter a number
	 * written on an Arduino controller, the received data from this method will
	 * be :
	 * <li>[[8,7,6,5], [4,3,2,1]]</li>. To counter this problem, wrap the result
	 * array in a #{@link ByteBuffer}, and call
	 * {@link #readNextFloat(ByteBuffer)} or {@link #readNextInt(ByteBuffer)}.
	 * Method to retrieve the data in order
	 * </p>
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@Override
	public byte[] executeProgramReturnByteArray(String path, int count) throws IOException, InterruptedException {
		// Format the path
		path = correctPath(path);

		String[] argsCopy = Arrays.copyOf(args, args.length);
		argsCopy[insertPathIndex] += path + ":i";

		Process p = Runtime.getRuntime().exec(argsCopy);
		p.waitFor();

		byte[] bytes = readFromSerialPort(count);

		return bytes;

	}

	public void executeProgram(String path) throws Exception {
		// Format the path
		path = correctPath(path);

		String[] argsCopy = Arrays.copyOf(args, args.length);
		argsCopy[insertPathIndex] += path + ":i";

		Process p = Runtime.getRuntime().exec(argsCopy);

		p.waitFor();

		System.out.println("Program executed");

	}

	private String correctPath(String path) {
		if (path.startsWith("/")) {
			path = path.replaceFirst("/", "");
		}

		System.out.println("Execute program : " + path);
		return path;
	}

	private byte[] readFromSerialPort(int byteCount) {

		final SerialPort port = openSerialPort();

		System.out.println(port);
		if (port == null) {
			throw new NullPointerException("Unable to open the port");
		}
		try {

			byte[] bytes = new byte[byteCount];
			applyEvent(port, bytes);

			synchronized (bytes) {
				// Waits for bytes to be read
				bytes.wait(TIME_OUT);

			}

			port.removeEventListener();
			port.close();
			System.out.println(Arrays.toString(bytes) + " READ");
			return bytes;
		} catch (InterruptedException e) {
			port.removeEventListener();
			port.close();
			e.printStackTrace();
		}

		return null;
	}

	private void applyEvent(final SerialPort port, final byte[] bytes) {
		try {
			port.addEventListener(new SerialPortEventListener() {

				@Override
				public void serialEvent(SerialPortEvent event) {
					if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
						try {
							InputStream s = port.getInputStream();
							int read = 0;

							while ((read += s.read(bytes, read, bytes.length - read)) != bytes.length) {

								try {
									Thread.sleep(10);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

						} catch (IOException e) {

							e.printStackTrace();
						}

					}
				}
			});
			port.notifyOnDataAvailable(true);
		} catch (TooManyListenersException e) {
			e.printStackTrace();
		}
	}

	private SerialPort openSerialPort() {
		// System.out.println(java.library.path);
		CommPortIdentifier serialPortId = getSerialPortId();

		System.out.println(serialPortId + " port");
		SerialPort serialPort = null;

		try {
			// Wait for port to be released
			while (true) {
				try {

					// open serial port, and use class name for the appName.
					serialPort = (SerialPort) serialPortId.open(this.getClass().getName(), TIME_OUT);

					System.out.println(serialPort);
					// set port parameters
					serialPort.setSerialPortParams(DATA_RATE, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
							SerialPort.PARITY_NONE);

					return serialPort;

				} catch (PortInUseException e) {
					// Wait for port to be freed
					System.out.println("used");
					Thread.sleep(10);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private CommPortIdentifier getSerialPortId() {

		CommPortIdentifier serialPortId;

		Enumeration<?> enumComm;

		enumComm = CommPortIdentifier.getPortIdentifiers();
		while (enumComm.hasMoreElements()) {
			serialPortId = (CommPortIdentifier) enumComm.nextElement();
			System.out.println(serialPortId + " test");
			if (serialPortId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				if (PORT_NAME.equals(serialPortId.getName())) {
					return serialPortId;
				}
			}
		}
		return null;
	}

	public static float readNextFloat(ByteBuffer buffer) {

		byte[] nextDouble = new byte[4];
		buffer.get(nextDouble);
		reverse(nextDouble);
		ByteBuffer next = ByteBuffer.wrap(nextDouble);
		next.order(ByteOrder.BIG_ENDIAN);

		System.out.println("GET :" + Arrays.toString(nextDouble));
		return next.getFloat();
	}

	public static int readNextInt(ByteBuffer buffer) {

		byte[] nextInt = new byte[4];
		buffer.get(reverse(nextInt));
		ByteBuffer next = ByteBuffer.wrap(nextInt);
		next.order(ByteOrder.LITTLE_ENDIAN);

		return next.getInt();
	}

	private static byte[] reverse(byte[] data) {

		for (int i = 0; i < data.length / 2; i++) {
			byte temp = data[i];
			data[i] = data[data.length - i - 1];
			data[data.length - i - 1] = temp;
		}
		return data;
	}
}
