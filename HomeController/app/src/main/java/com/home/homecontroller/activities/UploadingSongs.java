package com.home.homecontroller.activities;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.util.Log;
import android.widget.TextView;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import utils.Compresser;
import utils.Mp3Utils;


/**
 * Created by sunny on 2017-01-04.
 */

public class UploadingSongs extends Activity {

    private TextView text;
    private CharSequence base;

    private String[] musicPaths;

    private boolean stopped = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.uploading_music);

        musicPaths = (getIntent().getStringArrayExtra("musicPaths"));
        System.out.println(Arrays.toString(musicPaths));
        base = getText(R.string.uploading_song);

        text = (TextView) findViewById(R.id.upload_view);
        text.setText(base + "0/" + String.valueOf(musicPaths.length));

        new Thread(() -> {
            for (int i = 0; i < musicPaths.length && !stopped; i++) {

                String path = musicPaths[i];

                Log.d("Test", path);

                InputStream iStream = null;

                try {
                    Uri uri = Uri.parse(path);
                    iStream = getContentResolver().openInputStream(uri);

                    { // Makes sure bytes are removed from memory every step
                        byte[] compressed = Compresser.compressMaximum(getBytes(iStream));

                        Log.d("Tests", String.valueOf(compressed.length));

                        // Compresses at maximum the data and send them to the server
                        MainAppData.com.send(new TransferableData(TransferFlags.UPLOAD_NEW_MUSIC, new Object[]{findSongDisplayName(path), compressed}));
                    }
                    final int loaded = i + 1;

                    // Update the number of song sent
                    runOnUiThread(() -> text.setText(base + String.valueOf(loaded) + "/" + musicPaths.length));

                    // Clears memory
                    System.gc();

                    // Sleeps to release charges on the process
                    try {
                        Thread.yield();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


            // We finished to load, go back to old screen
            runOnUiThread(() -> super.onBackPressed());
        }).start();

    }

    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private String findSongDisplayName(String path) {


        String displayName = null;
        if (path.startsWith("content://")) {
            Cursor cursor = null;
            Uri uri = Uri.parse(path);

            try {
                cursor = getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)) + ".mp3";
                }
            } finally {
                cursor.close();
            }
        } else if (path.startsWith("file://")) {

            File f = new File(path);
            displayName = f.getName() + ".mp3";

            return displayName;
        } else {
            // Should not happen
            displayName = Mp3Utils.generateSongName();
        }
        return displayName;
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        stopped = true;
    }

    @Override
    public void onBackPressed() {
        // Cannot cancel this operation
    }
}
