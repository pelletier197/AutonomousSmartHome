package com.home.homecontroller.customsComponents;

import houseDataCenter.alarm.Alarm;

/**
 * Created by sunny on 2017-08-24.
 */

public interface AlarmSwitchListener {
    void onAlarmSelected(Alarm a);
}
