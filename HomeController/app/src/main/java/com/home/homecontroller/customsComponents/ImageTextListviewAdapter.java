package com.home.homecontroller.customsComponents;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.home.homecontroller.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sunny on 2016-12-23.
 *
 * <p>
 *     An adapter listviews with an image on the left side of the view, and a
 * </p>
 */

public class ImageTextListviewAdapter extends ArrayAdapter<ImageTextItem> {

    private List<ImageTextItem> items;

    public ImageTextListviewAdapter(Context context, List<ImageTextItem> items) {
        super(context, R.layout.image_text_item);
        this.items = items;
    }

    public ImageTextListviewAdapter(Context context, ImageTextItem... items) {
        this(context, Arrays.asList(items));
    }

    @Override
    public ImageTextItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {

        return items.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // New view
            view = inflater.inflate(R.layout.image_text_item, null);
        }

        ImageView img = (ImageView) view.findViewById(R.id.listview_image);
        TextView txt = (TextView) view.findViewById(R.id.listview_text);

        ImageTextItem item = items.get(position);

        img.setImageResource(item.getImageRes());
        txt.setText(item.getText());

        return view;
    }


}
