package com.home.homecontroller.customsComponents;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.util.List;

import houseDataCenter.house.data.LightData;

/**
 * Created by sunny on 2017-03-05.
 */

public class LightListAdapter extends ArrayAdapter<LightData> {

    private List<LightData> items;
    private LightSwitchListener listener;
    private int selected;
    private int listviewDrawable;

    public LightListAdapter(Context context, List<LightData> items, LightSwitchListener listener) {
        super(context, R.layout.light_layout);

        if (listener == null || items == null) {
            throw new NullPointerException("Null switch listener or items");
        }

        this.listener = listener;
        this.items = items;

        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        listviewDrawable = outValue.resourceId;
    }

    @Override
    public LightData getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {

        return items.size();
    }

    public void setSelected(int i) {
        this.selected = i;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // New view
            view = inflater.inflate(R.layout.light_layout, null);

        }

        if (position == selected) {
            view.setBackgroundResource(R.color.small_accent_light);
        } else {
            view.setBackgroundResource(listviewDrawable);
        }

        TextView name = (TextView) view.findViewById(R.id.light_name);
        Switch swi = (Switch) view.findViewById(R.id.light_switch);


        final LightData item = items.get(position);
        name.setText(item.getName());
        swi.setOnCheckedChangeListener(null);
        swi.setChecked(item.isOpen());

        // Sets the state change listener
        swi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                MainAppData.selectedLight = item;
                listener.selectedLightChanged(item);
                listener.onSwitchStateChanged(b);
                setSelected(position);
                notifyDataSetChanged();
            }
        });
        System.out.println("ITEM : " + item);

        return view;
    }

    public void setItems(List<LightData> lights) {
        this.items = lights;
    }

    public List<LightData> getItems() {
        return items;
    }

    public void setItemAt(int index, LightData light) {

        this.items.set(index, light);
    }
}
