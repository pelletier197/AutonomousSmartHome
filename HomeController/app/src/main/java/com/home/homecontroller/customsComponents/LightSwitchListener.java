package com.home.homecontroller.customsComponents;


import houseDataCenter.house.data.LightData;

/**
 * Created by sunny on 2017-03-05.
 */

public interface LightSwitchListener {

    void onSwitchStateChanged(boolean isOpen);
    void selectedLightChanged(LightData newLight);
}
