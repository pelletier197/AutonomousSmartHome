package com.home.homecontroller.customsComponents;

/**
 * Created by sunny on 2016-12-23.
 * <p>
 * <p>
 * A component to use with an {@link ImageTextListviewAdapter}. It contains the text data and the image resource ID.
 * </p>
 */

public class ImageTextItem {


    /**
     * The image id
     */
    private int image;

    /**
     * The text to display
     */
    private CharSequence text;

    /**
     * Creates a listview element that will have the given image id and the given text to display.
     *
     * @param imageRes The image resource
     * @param text The text to display
     */
    public ImageTextItem(int imageRes, CharSequence text) {
        this.image = imageRes;
        this.text = text;
    }

    /**
     *
     * @return The image's resourc ID
     */
    public int getImageRes() {
        return image;
    }

    /**
     *
     * @return The text to display.
     */
    public CharSequence getText() {
        return text;
    }


}
