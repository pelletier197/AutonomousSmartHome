package com.home.homecontroller.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.home.homecontroller.DrawerRoomLayoutActivity;
import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;
import com.home.homecontroller.controlTabs.CameraTab;
import com.home.homecontroller.controlTabs.ControlTab;
import com.home.homecontroller.controlTabs.DevicesTab;
import com.home.homecontroller.customsComponents.MenuFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import houseDataCenter.house.data.RoomData;

/**
 * Created by sunny on 2016-12-31.
 */

public class RoomView extends DrawerRoomLayoutActivity implements DecodedDataReceptionHandler {

    private TabLayout tabs;
    private ViewPager pager;
    private RecyclerView roomList;
    private List<RoomData> rooms;
    private ControlTab control;
    private CameraTab camera;
    private DevicesTab devices;
    private RoomChangedListener currentSelection;
    private RoomNameAdapter roomAdapter;
    private RoomViewHolder selectedView;
    private LinearLayoutManager roomViewLayoutManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainAppData.com.setDataReceptionHandler(this);

        setContentView(R.layout.room_view);

        initTabLayout();
        initRoomListView();

        MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_ROOMS));

    }

    private void initTabLayout() {

        tabs = (TabLayout) findViewById(R.id.tab_layout);
        pager = (ViewPager) findViewById(R.id.viewpager);

        ImageButton button = (ImageButton) findViewById(R.id.drawer_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDrawerLayout().openDrawer(Gravity.LEFT);
            }
        });

        this.control = new ControlTab();
        this.camera = new CameraTab();
        this.devices = new DevicesTab();

        pager.setAdapter(new MenuFragmentPagerAdapter(getSupportFragmentManager(), control, camera, devices));
        tabs.setupWithViewPager(pager);

        final TabLayout.Tab controlTab = tabs.getTabAt(0);
        controlTab.setText(getText(R.string.control));
        controlTab.setIcon(ContextCompat.getDrawable(this, R.drawable.control));

        final TabLayout.Tab cameraTab = tabs.getTabAt(1);
        cameraTab.setText(getText(R.string.camera));
        cameraTab.setIcon(ContextCompat.getDrawable(this, R.drawable.camera));


        final TabLayout.Tab devicesTab = tabs.getTabAt(2);
        devicesTab.setText(getText(R.string.devices));
        devicesTab.setIcon(ContextCompat.getDrawable(this, R.drawable.iot_symbol));

        // The default tab
        this.currentSelection = control;


        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab == controlTab) {
                    currentSelection = control;
                }
                if (tab == cameraTab) {
                    currentSelection = camera;
                }
                if (tab == devicesTab) {
                    currentSelection = devices;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (MainAppData.selectedRoomData != null) {
            currentSelection.notifyRoomChanged();
        }

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (MainAppData.selectedRoomData != null) {
                        MainAppData.com.send(TransferFlags.VOLUME_UP, MainAppData.selectedRoomData.currentID);
                    }
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    if (MainAppData.selectedRoomData != null) {
                        MainAppData.com.send(TransferFlags.VOLUME_DOWN, MainAppData.selectedRoomData.currentID);
                    }
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {

        super.receive(o, uuid, b);

        TransferableData data = (TransferableData) o;

        final Object obj = data.getData();
        final int flag = data.getFlag();

        if (flag == TransferFlags.REQUEST_ROOMS) {

            MainAppData.rooms.clear();
            MainAppData.rooms.addAll((List<RoomData>) obj);

            System.out.print(MainAppData.rooms);

            if (!MainAppData.rooms.isEmpty()) {
                MainAppData.selectedRoomData = MainAppData.rooms.get(0);
            }

            runOnUiThread(() -> setRoomListView());
        } else {
            super.receive(o, uuid, b);
        }

    }

    private void initRoomListView() {

        roomViewLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        roomList = (RecyclerView) findViewById(R.id.room_listview);
        roomList.setLayoutManager(roomViewLayoutManager);
        roomAdapter = new RoomNameAdapter(new ArrayList<>(), new OnRoomClickListener() {
            @Override
            public void handleClick(RoomData data) {

                Log.d(data.name, data.name);
                if (data != null && MainAppData.selectedRoomData != data) {

                    // Changes the selected room
                    MainAppData.selectedRoomData = data;

                    // Notify the current tab of the change
                    currentSelection.notifyRoomChanged();
                }
            }
        });

        roomList.setAdapter(roomAdapter);


    }


    /**
     * Sets up the room listview of all the rooms
     */
    private void setRoomListView() {

        this.rooms = MainAppData.rooms;

        if (roomAdapter != null) {
            roomAdapter.setRooms(rooms);
            roomAdapter.notifyItemRangeChanged(0, rooms.size());
        }

        if (!rooms.isEmpty()) {
            // selects the first element and simulate a click at this position on the view.
            MainAppData.selectedRoomData = rooms.get(0);

            if (currentSelection != null) {
                currentSelection.notifyRoomChanged();
            }
        }


    }


    /**
     * For when a room view is clicked
     */
    private interface OnRoomClickListener {
        void handleClick(RoomData data);
    }

    /**
     * Adapter for an horizontal listview
     */
    private class RoomNameAdapter extends RecyclerView.Adapter<RoomViewHolder> {

        private List<RoomData> rooms;
        private OnRoomClickListener listener;

        public RoomNameAdapter(List<RoomData> data, OnRoomClickListener listener) {
            super();
            this.rooms = data;
            this.listener = listener;

        }

        @Override
        public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RoomViewHolder holder = new RoomViewHolder(LayoutInflater.from(getApplicationContext()).inflate(R.layout.room_name_view, null));
            holder.setOnClick(listener);
            System.out.print("tppppppppppppppppp");
            return holder;
        }

        @Override
        public void onBindViewHolder(RoomViewHolder holder, int position) {

            holder.setRoom(rooms.get(position));
            Log.d("test", holder.text.toString());
            holder.text.setText(holder.dat.name);

            if (position == 0) {
                holder.background.setSelected(true);
            }

        }

        public void setRooms(List<RoomData> dat) {
            this.rooms = dat;
        }


        @Override
        public int getItemCount() {
            return rooms.size();
        }
    }

    /**
     * Adapter for a room view holder
     */
    private class RoomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public RoomData dat;
        public TextView text;
        private OnRoomClickListener onClick;
        private LinearLayout background;

        public RoomViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            text = (TextView) view.findViewById(R.id.room_name_label);
            background = (LinearLayout) view.findViewById(R.id.room_name_view_background);

        }


        public void setRoom(RoomData dat) {
            this.dat = dat;
        }


        @Override
        public void onClick(View view) {
            background.setSelected(true);

            if (selectedView != null) {
                selectedView.background.setSelected(false);
            }
            selectedView = this;

            onClick.handleClick(dat);

        }

        public void setOnClick(OnRoomClickListener onClick) {
            this.onClick = onClick;
        }


    }
}
