package com.home.homecontroller.serverAndroidAdapter;

import android.os.StrictMode;
import android.util.Log;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import communication.JavaObjectCommunicator;
import custom.CommunicationCenter;
import houseDataCenter.commucationData.TransferableData;
import serverDataManaging.ClientManager;
import serverDataManaging.ConnectionListener;
import serverDataManaging.ConnectionLostListener;

/**
 * Implements a Communication center that will only try to connect when
 * {@link #openConnection()} is called. All the other methods have the same
 * effect as calling them directly on a {@link CommunicationCenter}.
 * <p>
 * The object works in two timed operation.
 * On the first connection, it will scan the sub-network and connect to the server if it is online.
 * It will then save the IP address of the connection to the config file.
 * <p>
 * If the server is wished to be reached from outside, you must retrieve the World Wide IP address of the server and set it manually using {@link #setExternalIP(String)}
 * This information will also be saved to the config file and won't need to be performed again.
 * <p>
 * To clear the default configs, use {@link #clearIP()}.
 *
 * @author sunny
 */
public class MobileCommunicationCenter {

    /**
     * The default port of the application
     */
    public static final int port = 10246;

    /**
     * The path to the connection configuration file.
     */
    public static final String CONNECTION_CONFIG = "/connection.config";

    /**
     * The client manager that manages the connection to the server
     */
    private JavaObjectCommunicator communicator;
    private ClientManager clientManager;
    private boolean open = false;
    private String externalIP = null;
    private String internalIP = null;
    private DecodedDataReceptionHandler handler;
    private String fileDir;
    private ConnectionListener listener;

    /**
     * Constructs a communication center that will connect to the default port
     * of the application. The communication center will retrieve reception data
     * to the listener in parameters.
     *
     * @param handler The reception of data handler
     */

    public MobileCommunicationCenter(DecodedDataReceptionHandler handler) {
        createManager(handler);
    }


    /**
     * Creates the manager and open connection to the server. This method will
     * block the current thread until server connection.
     *
     * @param handler The handler of data reception
     */
    private void createManager(DecodedDataReceptionHandler handler) {
         // Temporary handler
         clientManager = new ClientManager(port, (buffer, uuid)->{});
        this.handler = handler;
        communicator = new JavaObjectCommunicator(clientManager, handler);
        clientManager.setOnConnectionLostListener(new ConnectionLostListener() {
            @Override
            public void connectionLost(UUID uuid) {
                clientManager.closeConnection();

                if (listener != null) {
                    listener.connectionLost(uuid);
                }

                while (!clientManager.isConnected()) {


                    try {

                        // We know the server is available, so try again with the known IP until it works
                        reducedOpenConnection(true);

                    } catch (IOException e) {
                    }
                }
            }
        });

    }

    /**
     * Connects to the known IP addresses if they are non-null
     *
     * @param trigger WIll notify the {@link #listener} if the connection success if this value is true.
     * @throws IOException If the connection fails to both addresses
     */
    private void reducedOpenConnection(boolean trigger) throws IOException {

        try {
            // Attempts connection on external IP first if it was set.
            if (internalIP != null) {

                clientManager.openConnection(internalIP, 10_000);

            }

        } catch (IOException e) {
            // This will basically means the server is either shut down or the current connection is made on localhost
        }


        // Attempts connecting on internal ip if not working
        if (!clientManager.isConnected() && externalIP != null) {
            clientManager.openConnection(externalIP, 10_000);

        }


        // Notify the listener
        if (clientManager.isConnected() && listener != null && trigger) {
            listener.handleConnection(null);
        }
    }

    /**
     * Sets the connection listener that will be notified on connection success or on disconnection.
     * This allows the application to react to disconnection.
     *
     * @param listener The new listener of connection. Can be null.
     */
    public void setConnectionListener(ConnectionListener listener) {
        this.listener = listener;
    }

    /**
     * Sets the file directory of the class.
     * This class doesn't access the application's context, so it need a path to the file it can read.
     * This method must be called before opening connection, or an exception will occur on {@link #openConnection()} call.
     *
     * @param fileDir The file directory where data can be written for the current application.
     */
    public void setFileDirectory(String fileDir) {
        this.fileDir = fileDir;
    }

    /**
     * Clears the IP addresses. The server won't be reachable from external IP anymore.
     * The scan of ip will need to be performed again after this method is called.
     */
    public void clearIP() {
        writeConfig(null, null);
        this.externalIP = null;
        this.internalIP = null;
    }

    /**
     * @return The external IP that has been set. This IP is the router address of the server.
     */
    public String getExternalIP() {
        if (externalIP == null) {
            readConfig();
        }
        return externalIP;
    }

    /**
     * Sets the external IP address. This IP has to be retrieved from the server via a request.
     * It will allow the app to connect to the server from anywhere, either locally or externally.
     *
     * @param ip The external IP
     */
    public void setExternalIP(String ip) {
        this.externalIP = ip;
        writeConfig(ip, internalIP);
    }

    /**
     * @return The current IP address of the server. May wither be the local IP address of the external IP.
     */
    public String getCurrentIp() {
        return clientManager.getIP();
    }

    /**
     * @return True if connected to the server.
     */
    public boolean isConnected() {
        return clientManager.isConnected();
    }

    /**
     * Reads the IP address config file to know where to connect and sets the attributes value to them.
     */
    private void readConfig() {

        ObjectInputStream in = null;
        try {

            if (fileDir == null) {
                throw new NullPointerException("File directory must be set before executing that openration.");
            }

            File f = new File(fileDir + CONNECTION_CONFIG);

            if (!f.exists()) {
                f.createNewFile();
            }

            in = new ObjectInputStream(new FileInputStream(f));
            externalIP = (String) in.readObject();
            internalIP = (String) in.readObject();


            in.close();

            System.out.println("Read successfull : --> external = " + externalIP + "  --> internal = " + internalIP);

        } catch (EOFException e1) {

            // Nothing written if the file
        } catch (IOException | ClassNotFoundException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * Writes down the internal and external IP addresses to the file.
     *
     * @param externalIP The external IP to write
     * @param internalIP The local IP to write.
     */
    private void writeConfig(String externalIP, String internalIP) {


        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(fileDir + CONNECTION_CONFIG));
            out.writeObject(externalIP);
            out.writeObject(internalIP);


            out.flush();
            out.close();
            System.out.println("writing new ip to file : " + clientManager.getIP());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Sets the listener for data reception. This object will be notified on data reception.
     *
     * @param handler Handler for data reception
     */
    public void setDataReceptionHandler(DecodedDataReceptionHandler handler) {


        if (clientManager.isConnected()) {
            communicator.setDecodedDataReceptionHandler(handler);
        }


    }


    /**
     * Open the connection to the server. Note that this function will block the current thread.
     * <p>
     * <p>
     * WIll first attempt to connect to the internal or external IP found in config files.
     * If the connection fails, a scan over the sub-network is performed.
     * </p>
     * <p>
     * <p>
     * If the sub-network scan works, the configs are saved.
     * </p>
     *
     * @throws NullPointerException If {@link #setFileDirectory(String)} has not been called before to set the path on which the app can write.
     * @throws IOException          If all connection methods fails
     */
    public void openConnection() throws IOException {

        // Allows access to internet on android devices
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        // Allow enormous time. If the connection fails, either connection
        // ip changed, or server is offline.
        Log.d("Server", "Connecting to : " + internalIP + " then " + externalIP);

        try {
            // First try with local and outside
            reducedOpenConnection(false);
        } catch (Exception e) {
            //Failed
        }

        // If none works, we look for connection locally
        if (!clientManager.isConnected()) {
            internalIP = findLocalConnection();

            // Writes down the new connection config, external ip is set from server ip, but local ip changed
            writeConfig(externalIP, internalIP);

        }

        if (listener != null) {
            listener.handleConnection(null);
        }

        this.communicator.setDecodedDataReceptionHandler(handler);

    }

    /**
     * Finds a local connection on the sub-network.
     *
     * @return The ip address to which the device connected if it works, or null on fail.
     */
    private String findLocalConnection() {

        String base = getLocalHostAddress();
        base = base.substring(0, base.lastIndexOf('.') + 1);
        String current = null;
        for (int i = 0; i <= 255; i++) {
            current = base + i;
            try {
                Log.i("Test", current);
                clientManager.openConnection(current, 500);
                return current;
            } catch (IOException e) {

            }
        }

        return null;
    }

    /**
     * @return The local-host address of the device. Normally looks like 192.168.1.XXX.
     */
    private String getLocalHostAddress() {

        try {
            //Enumerate all the network interfaces
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();

                // Make a loop on the number of IP addresses related to each Network Interface
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {

                    InetAddress inetAddress = enumIpAddr.nextElement();

                    //Check if the IP address is not a loopback address, in that case it is
                    //the IP address of your mobile device
                    if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress())
                        return inetAddress.getHostAddress();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Sends the given transferable data to the server. This transferable data
     * might contain a valid flag that the server can read.
     *
     * @param data The data that will be transfered and read on the server.
     */
    public void send(TransferableData data) {
        try {
            communicator.send(data);
        } catch (Exception e) {
            // The connection will be established again soon or cancelled, so app will crash
        }
    }

    /**
     * Sends a transferable data that will have the given flag and object to the
     * server. The flag must be a valid flag that can be read on the server, and
     * the object can be null.
     *
     * @param flag The flag of the sending telling the server what to do.
     * @param data The optionnal data, can be null. To avoid useless parameter,
     *             use {@link #send(int)}
     */
    public void send(int flag, Serializable data) {
        send(new TransferableData(flag, data));
    }

    /**
     * Sends a transferable data that will have the given flag and a null data
     * part. The flag must be a valid flag that the server can read.
     *
     * @param flag The flag telling the server what to do.
     */
    public void send(int flag) {
        send(flag, null);
    }


    /**
     * Closes the connection to the server.
     */
    public void closeConnection() {
        clientManager.closeConnection();
    }

    /**
     * Not supported on android devices.
     */
    public void cancelConnection() {
        clientManager.cancelConnection();
    }

}
