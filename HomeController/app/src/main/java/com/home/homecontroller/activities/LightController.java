package com.home.homecontroller.activities;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.danielnilsson9.colorpickerview.dialog.ColorPickerDialogFragment;
import com.github.danielnilsson9.colorpickerview.view.ColorPickerView;
import com.home.homecontroller.ConnectableActivity;
import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;
import com.home.homecontroller.customsComponents.LightListAdapter;
import com.home.homecontroller.customsComponents.LightSwitchListener;
import com.home.homecontroller.customsComponents.ViewLoadedListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.data.RoomData;

import static android.view.View.GONE;

/**
 * Created by sunny on 2017-03-05.
 */

public class LightController extends ConnectableActivity implements ColorPickerDialogFragment.ColorPickerDialogListener, LightSwitchListener, DecodedDataReceptionHandler {

    private RoomData selectedRoom;

    private ImageButton colorBox;
    private SeekBar luminosity;
    private TextView lumVal;
    private ListView lightList;
    private ProgressBar loading;
    private ImageView reset;

    private LinearLayout colorLayout;
    private LinearLayout luminosityLayout;

    private TextView noLight;
    private TextView noLightText;

    private LightListAdapter adapter;

    private static final int DIALOG_ID = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // sets the current activity to server input listener
        MainAppData.com.setDataReceptionHandler(this);
        MainAppData.selectedLight = null;

        setContentView(R.layout.light_control);

        init();
    }

    private void init() {

        // Gets the selected room
        this.selectedRoom = MainAppData.selectedRoomData;
        this.colorLayout = (LinearLayout) findViewById(R.id.color_layout);
        this.luminosityLayout = (LinearLayout) findViewById(R.id.luminosity_layout);
        this.noLight = (TextView) findViewById(R.id.no_light);
        this.noLightText = (TextView) findViewById(R.id.no_light_text);

        initListview();
        initColorBox();
        initLuminosityBar();
        initResetColor();

        // Sets the label'S name
        ((TextView) findViewById(R.id.room_name_light)).setText(selectedRoom.name);

        requestRoomLights();
        this.loading = (ProgressBar) findViewById(R.id.light_loading);
        setColorBoxEnable(false);
        setLuminosityEnable(false);
    }

    private void initResetColor() {
        reset = (ImageView) findViewById(R.id.reset_light_color);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainAppData.com.send(TransferFlags.RESET_LIGHT_COLOR, MainAppData.selectedLight.getId());
            }
        });
    }

    private void requestRoomLights() {
        MainAppData.com.send(TransferFlags.REQUEST_ROOM_LIGHTS, selectedRoom.currentID);
    }

    private void setColorBoxEnable(boolean val) {
        colorBox.setEnabled(val);
        reset.setEnabled(val);
        colorLayout.setAlpha(val ? 1 : 0.3f);
    }

    private void setLuminosityEnable(boolean val) {
        luminosity.setEnabled(val);
        luminosityLayout.setAlpha(val ? 1 : 0.3f);
    }

    private void initLuminosityBar() {

        this.luminosity = (SeekBar) findViewById(R.id.luminosity);
        this.lumVal = (TextView) findViewById(R.id.luminosity_value);

        this.luminosity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                lumVal.setText(seekBar.getProgress() + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Sets the luminosity
                changeLuminosity();

            }
        });

    }


    private void initColorBox() {

        this.colorBox = (ImageButton) findViewById(R.id.light_color_box);

        this.colorBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // The color picker menu item has been clicked. Show
                // a dialog using the custom ColorPickerDialogFragment class.

                int r = 255;
                int g = 255;
                int b = 255;

                if (MainAppData.selectedLight != null) {
                    final LightData selectedLight = MainAppData.selectedLight;
                    r = (selectedLight.getColor() >> 16) & 0xff;
                    g = (selectedLight.getColor() >> 8) & 0xff;
                    b = (selectedLight.getColor()) & 0xff;
                }

                ColorPickerDialogFragment f = ColorPickerDialogFragment
                        .newInstance(DIALOG_ID, null, null, Color.argb(0xFF, r, g, b), false);

                f.show(getFragmentManager(), "d");


                // Waits for the view to load and add the listener
                f.setOnViewLoaded(new ViewLoadedListener() {
                    @Override
                    public void onViewLoaded() {
                        f.getView().findViewById(R.id.colorpickerview__color_picker_view).setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {

                                ColorPickerView panel = (ColorPickerView) view;

                                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                                    // The selected color changed, changes the light color
                                    onColorSelected(panel.getColor(), true);
                                }
                                return false;
                            }
                        });

                    }
                });
            }
        });

    }

    private void initListview() {

        lightList = (ListView) findViewById(R.id.light_listview);
        lightList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        adapter = new LightListAdapter(this.getApplicationContext(), new ArrayList<>(), this);

        lightList.setAdapter(adapter);

        lightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                MainAppData.selectedLight = adapter.getItem(i);
                adapter.setSelected(i);
                adapter.notifyDataSetChanged();
                selectedLightChanged();
            }
        });
    }

    @Override
    public void selectedLightChanged(LightData newLight) {
        MainAppData.selectedLight = newLight;
        selectedLightChanged();
    }

    private void selectedLightChanged() {

        final LightData selectedLight = MainAppData.selectedLight;

        setColorBoxEnable(selectedLight.isColorisable() && selectedLight.isOpen());
        setLuminosityEnable(selectedLight.isDimmable() && selectedLight.isOpen());
        if (colorLayout.getVisibility() == View.VISIBLE) {

            // Refresh the box for the new color
            onColorSelected(selectedLight.getColor(), false);
        }

        if (luminosityLayout.getVisibility() == View.VISIBLE) {
            luminosity.setProgress((int) (selectedLight.getLuminosity() * 100));
        }
    }

    @Override
    public void onSwitchStateChanged(boolean isOpen) {

        if (MainAppData.selectedLight != null) {
            if (isOpen) {
                MainAppData.com.send(TransferFlags.OPEN_LIGHT, MainAppData.selectedLight.getId());

            } else {
                MainAppData.com.send(TransferFlags.CLOSE_LIGHT, MainAppData.selectedLight.getId());

            }
            MainAppData.selectedLight.updateOpen(isOpen);
        }

    }

    private void changeLuminosity() {
        MainAppData.com.send(TransferFlags.SET_LIGHT_LUMINOSITY, new Object[]{MainAppData.selectedLight.getId(), ((double) luminosity.getProgress() / 100)});
        MainAppData.selectedLight.updateLuminosity((luminosity.getProgress() / 100d));
    }


    @Override
    public void receive(Object o, UUID uuid, byte b) {

        super.receive(o, uuid, b);

        TransferableData dat = (TransferableData) o;
        final int flag = dat.getFlag();
        final Object obj = dat.getData();

        System.out.println("Received : " + obj);
        if (flag == TransferFlags.REQUEST_ROOM_LIGHTS) {
            runOnUiThread(() -> setLights((List<LightData>) obj));
        } else if (flag == TransferFlags.UPDATE_LIGHT_STATUS) {
            runOnUiThread(() -> {
                LightData data = (LightData) obj;
                updateLight(data);
                updateOptionsIfIsSelected(data);
            });
        } else if (flag == TransferFlags.LIGHT_STATE_CHANGED) {
            String id = (String) obj;
            if (anyLightForId(id)) {
                requestSingleLightStatus(id);
            }
        } else if (flag == TransferFlags.GET_LIGHT_STATUS) {
            runOnUiThread(() -> {
                LightData data = (LightData) obj;
                updateLight(data);
                updateOptionsIfIsSelected(data);
            });
        } else if (flag == TransferFlags.MULTIPLE_LIGHTS_STATE_CHANGED) {
            UUID roomId = (UUID) obj;

            if (roomId.equals(MainAppData.selectedRoomData.currentID)) {
                requestRoomLights();
            }
        }
    }

    private void updateOptionsIfIsSelected(LightData data) {
        if (MainAppData.selectedLight != null) {
            if (MainAppData.selectedLight.getId().equals(data.getId())) {
                MainAppData.selectedLight = data;
                selectedLightChanged();
            }
        }
    }

    private boolean anyLightForId(String id) {
        for (LightData dat : adapter.getItems()) {
            if (dat.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    private void requestSingleLightStatus(String o) {
        MainAppData.com.send(TransferFlags.GET_LIGHT_STATUS, o);
    }

    private void updateLight(LightData light) {
        final List<LightData> items = adapter.getItems();

        int index = -1;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(light.getId())) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            adapter.setItemAt(index, light);
            adapter.notifyDataSetChanged();
        }
    }


    public void onColorSelected(int color, boolean updatePhysicLight) {

        // Notifies the server
        if (updatePhysicLight) {
            MainAppData.com.send(TransferFlags.SET_LIGHT_COLOR, new Object[]{MainAppData.selectedLight.getId(), color});
            MainAppData.selectedLight.updateColor(color);
        }

        // Tells the server to change the color<// Sets the button image to the new color
        final int[] colors = new int[colorBox.getWidth() * colorBox.getHeight()];
        Arrays.fill(colors, color);
        Bitmap.Config config = Bitmap.Config.RGB_565;
        Bitmap b = Bitmap.createBitmap(colors, colorBox.getWidth(), colorBox.getHeight(), config);
        this.colorBox.setImageBitmap(b);
    }

    private void setLights(List<LightData> lights) {

        adapter.setItems(lights);
        adapter.notifyDataSetChanged();

        loading.setVisibility(GONE);

        if (lights.isEmpty()) {
            lightList.setVisibility(GONE);
            noLight.setVisibility(View.VISIBLE);
            noLightText.setVisibility(View.VISIBLE);
            colorLayout.setVisibility(GONE);
            luminosityLayout.setVisibility(GONE);
        } else {
            lightList.setVisibility(View.VISIBLE);
            noLight.setVisibility(GONE);
            noLightText.setVisibility(GONE);
            colorLayout.setVisibility(View.VISIBLE);
            luminosityLayout.setVisibility(View.VISIBLE);
        }

        if (MainAppData.selectedLight == null && !lights.isEmpty()) {
            MainAppData.selectedLight = lights.get(0);
        }

        if (MainAppData.selectedLight != null) {

            int index = lights.indexOf(MainAppData.selectedLight);
            MainAppData.selectedLight = lights.get(index); // When list is updated
            lightList.setSelection(index);
            selectedLightChanged();
        }
    }

    public void onBackPressed(View v) {
        super.onBackPressed();
    }

    @Override
    public void onColorSelected(int dialogId, int color) {
        // Handled on release
    }

    @Override
    public void onDialogDismissed(int dialogId) {
        // Handled on release
    }
}
