package com.home.homecontroller.activities;

/**
 * Created by sunny on 2016-12-31.
 */

public interface RoomChangedListener {

    void notifyRoomChanged();
}
