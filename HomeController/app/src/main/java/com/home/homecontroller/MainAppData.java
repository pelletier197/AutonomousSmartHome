package com.home.homecontroller;

import android.telephony.TelephonyManager;

import com.home.homecontroller.serverAndroidAdapter.MobileCommunicationCenter;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.house.data.LightData;
import houseDataCenter.house.data.ResidentData;
import houseDataCenter.house.data.RoomData;

/**
 * Created by sunny on 2016-12-27.
 */

public class MainAppData implements DecodedDataReceptionHandler {

    /**
     * Communicator with the server. Every activity will set itself as the
     * listener of data reception when being loaded.
     */
    public static final MobileCommunicationCenter com = new MobileCommunicationCenter(new MainAppData());
    /**
     * Application data about the house.
     */
    public static final List<ResidentData> residents = new ArrayList<ResidentData>();
    public static final List<RoomData> rooms = new ArrayList<RoomData>();
    private static final String USER_LOG_PATH = "/user.config";
    public static RoomData selectedRoomData = null;

    public static String currentLogin = null;
    public static String currentPIN = null;
    public static LightData selectedLight = null;
    public static Alarm selectedAlarm = null;

    public static void writeUserLogin(String email, String pin, String basePath) {
        ObjectOutputStream out = null;
        try {
            File f = new File(basePath + USER_LOG_PATH);

            if (!f.exists()) {
                f.createNewFile();
            }
            out = new ObjectOutputStream(new FileOutputStream(f));
            out.writeObject(email);
            out.writeObject(pin);

            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String[] readUserLogin(String basePath) {

        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new FileInputStream(basePath + USER_LOG_PATH));
            String mail = (String) in.readObject();
            String pin = (String) in.readObject();
            in.close();

            return new String[]{mail, pin};
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (EOFException e) {

        } catch (FileNotFoundException e) {
            // Happens on first reading
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] getUUIDArray(TelephonyManager tManager) {

        String s = tManager.getDeviceId();

        Long firstlong = Long.valueOf(s.substring(0, 8));
        Long seclong = Long.valueOf(s.substring(8, 15));

        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE * 2);
        buffer.putLong(firstlong);
        buffer.putLong(seclong);

        return buffer.array();

    }


    @Override
    public void receive(Object o, UUID uuid, byte b) {

    }
}
