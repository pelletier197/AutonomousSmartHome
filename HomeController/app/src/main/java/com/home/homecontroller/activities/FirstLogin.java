package com.home.homecontroller.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

/**
 * Created by sunny on 2016-12-23.
 */

public class FirstLogin extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        MainAppData.com.setFileDirectory(this.getFilesDir().getPath().toString());

        checkIsFirstConnection();

        setContentView(R.layout.first_connection);
        initButtonContinue();
    }


    private void checkIsFirstConnection() {

        String ip = MainAppData.com.getExternalIP();

        if (ip != null) {
            // This is not the fist connection on this device, so the communication center knows where to connect

            // Switch activity, go to connecting

            startConnectionActivity();


        }
    }

    private void initButtonContinue() {
        Button button = (Button) findViewById(R.id.start_firstconnec);

        button.setOnClickListener(view -> startConnectionActivity());
    }

    private void startConnectionActivity() {
        Intent i = new Intent(getApplicationContext(), Connecting.class);
        startActivity(i);
    }

}
