package com.home.homecontroller.controlTabs;

import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.home.homecontroller.DrawerRoomLayoutActivity;
import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;
import com.home.homecontroller.activities.AlarmController;
import com.home.homecontroller.activities.LightController;
import com.home.homecontroller.activities.MusicController;
import com.home.homecontroller.activities.RoomChangedListener;
import com.home.homecontroller.activities.UploadingSongs;

import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import houseDataCenter.house.data.RoomData;

import static android.app.Activity.RESULT_OK;

/**
 * Created by sunny on 2016-12-31.
 */

public class ControlTab extends Fragment implements RoomChangedListener, DecodedDataReceptionHandler {

    private View view;
    private SeekBar volume;
    private TextView temp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.control_tab, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        MainAppData.com.setDataReceptionHandler(this);


        this.view = getView();

        initOnMusicClick();
        initOnUploadClick();
        initOnAlarmClick();
        initOnLightClick();
        initVolume();
        initTempButtons();
    }

    private void initTempButtons() {
        temp = (TextView) view.findViewById(R.id.room_temp);

        view.findViewById(R.id.temp_up).setOnClickListener(v -> {
            MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_TEMP_UP, MainAppData.selectedRoomData.currentID));
        });

        view.findViewById(R.id.temp_down).setOnClickListener(v -> {
            MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_TEMP_DOWN, MainAppData.selectedRoomData.currentID));
        });

        if (MainAppData.selectedRoomData != null) {
            displayTemp();
        }
    }

    private void initVolume() {

        volume = (SeekBar) view.findViewById(R.id.volume);
        TextView volLab = (TextView) view.findViewById(R.id.volume_label);

        volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volLab.setText(progress + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MainAppData.com.send(TransferFlags.SET_ROOM_VOLUME, new Object[]{MainAppData.selectedRoomData.currentID, (seekBar.getProgress() / 100d)});
            }
        });
        if (MainAppData.selectedRoomData != null) {
            volume.setProgress((int) (MainAppData.selectedRoomData.volume * 100));
        } else {
            volume.setProgress(100);
        }
    }

    private void initOnLightClick() {

        LinearLayout background = (LinearLayout) view.findViewById(R.id.light_view);

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(view.getContext(), LightController.class);
                startActivity(i);
            }
        });
    }

    private void initOnUploadClick() {

        LinearLayout background = (LinearLayout) getView().findViewById(R.id.upload_view);

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent();
                i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                i.setAction(Intent.ACTION_GET_CONTENT);
                i.setType("audio/mpeg");

                startActivityForResult(i, 10);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Request code for musics
        if (resultCode == RESULT_OK && requestCode == 10 && data != null) {

            Log.d("test", data.toString());
            ClipData clip = data.getClipData();
            if (clip != null) {
                Log.d("test", clip.toString());

                final int count = clip.getItemCount();
                final String[] paths = new String[count];

                for (int i = 0; i < count; i++) {
                    paths[i] = clip.getItemAt(i).getUri().toString();
                }


                Intent i = new Intent(getContext(), UploadingSongs.class);
                i.putExtra("musicPaths", paths);

                startActivity(i);
            } else {

                Intent i = new Intent(getContext(), UploadingSongs.class);
                final String[] paths = new String[1];
                paths[0] = data.getData().toString();

                i.putExtra("musicPaths", paths);

                startActivity(i);
            }


        }
    }


    private void initOnMusicClick() {

        LinearLayout background = (LinearLayout) view.findViewById(R.id.music_view);

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(view.getContext(), MusicController.class);
                startActivity(i);
            }
        });
    }

    private void initOnAlarmClick() {

        LinearLayout background = (LinearLayout) view.findViewById(R.id.alarm_view);

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(view.getContext(), AlarmController.class);
                startActivity(i);
            }
        });
    }


    @Override
    public void notifyRoomChanged() {
        volume.setProgress((int) (MainAppData.selectedRoomData.volume * 100));
        displayTemp();
    }


    @Override
    public void receive(Object o, UUID uuid, byte b) {
        ((DrawerRoomLayoutActivity) getActivity()).receive(o, uuid, b);

        TransferableData data = (TransferableData) o;
        final int flag = data.getFlag();

        if (flag == TransferFlags.SET_ROOM_VOLUME) {
            Object[] dat = (Object[]) data.getData();
            UUID roomID = (UUID) dat[0];
            double volume = (double) dat[1];

            // The selected room is the target of the changes
            if (MainAppData.selectedRoomData != null && MainAppData.selectedRoomData.currentID.equals(roomID)) {
                this.volume.setProgress((int) (volume * 100));
            }
        } else if (flag == TransferFlags.SET_TEMP) {

            Object[] dat = (Object[]) data.getData();
            UUID roomID = (UUID) dat[0];
            double temp = (double) dat[1];

            if (MainAppData.selectedRoomData != null && roomID.equals(MainAppData.selectedRoomData.currentID))
            {
                MainAppData.selectedRoomData.temp = temp;
                getActivity().runOnUiThread(() -> displayTemp());
            }
        }
    }

    private void displayTemp() {
        RoomData r = MainAppData.selectedRoomData;

        if (r != null) {
            temp.setText(r.getDisplayTemperature());
        }
    }
}
