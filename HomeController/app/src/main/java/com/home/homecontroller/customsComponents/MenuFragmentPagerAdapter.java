package com.home.homecontroller.customsComponents;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by sunny on 2016-12-31.
 */

public class MenuFragmentPagerAdapter extends FragmentPagerAdapter{

    private Fragment[] fragments;

    public MenuFragmentPagerAdapter(FragmentManager fm, Fragment... fragments) {
        super(fm);

        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
