package com.home.homecontroller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.home.homecontroller.activities.Login;

import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import houseDataCenter.house.data.RoomData;
import serverDataManaging.ConnectionListener;

/**
 * Created by sunny on 2017-01-11.
 */

public class ConnectableActivity extends AppCompatActivity implements ConnectionListener, DecodedDataReceptionHandler {

    private LinearLayout banner;
    private LinearLayout background;
    private View contentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.setContentView(R.layout.connectable_view);

        background = (LinearLayout) findViewById(R.id.connection_lost_background);
        banner = (LinearLayout) findViewById(R.id.connection_lost_layout);
        MainAppData.com.setConnectionListener(this);

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater factory = LayoutInflater.from(this);
        View fullView = factory.inflate(layoutResID, null);

        this.contentView = fullView;

        if (background.getChildCount() == 1) {
            background.removeViewAt(0);
        }
        background.addView(fullView, 0);
    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {
        TransferableData data = (TransferableData) o;
        final int flag = data.getFlag();

        if (flag == TransferFlags.REQUEST_IDENTIFICATION_DATA) {

            // Tries to send data
            authenticate(MainAppData.currentLogin, MainAppData.currentPIN);

            Log.d("Login", "Login sent : " + MainAppData.currentLogin + ", " + MainAppData.currentPIN);

        } else if (flag == TransferFlags.ACCESS_REFUSED) {

            Log.d("Login", "Access refused");

            // Go back to login phase, because authentication failed
            startActivity(new Intent(getApplicationContext(), Login.class));
        } else if (flag == TransferFlags.ACCESS_GRANTED) {
            Log.d("Login", "Access granted");

        } else if (flag == TransferFlags.SET_ROOM_VOLUME) {
            Object[] dt = (Object[]) data.getData();
            UUID rid = (UUID) dt[0];
            double vol = (double) dt[1];
            RoomData r = getRoom(rid);

            if (r != null) {
                r.volume = vol;
            }
        } else if (flag == TransferFlags.SET_TEMP) {
            Object[] dt = (Object[]) data.getData();
            UUID rid = (UUID) dt[0];
            double temp = (double) dt[1];
            RoomData r = getRoom(rid);

            if (r != null) {
                r.temp = temp;
            }
        }
    }

    private RoomData getRoom(UUID id) {
        for (RoomData data : MainAppData.rooms) {
            if (data.currentID.equals(id)) {
                return data;
            }
        }
        return null;
    }

    private void authenticate(String login, String pass) {
        MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_IDENTIFICATION_DATA, new Object[]{MainAppData.getUUIDArray((TelephonyManager) getSystemService(TELEPHONY_SERVICE)), TransferFlags.DEVICE_INTELLIGENT, login, pass}));
    }


    @Override
    public void handleConnection(UUID uuid) {
        Log.d("Connection status ", "Connected");


        // Closes the connection lost banner
        runOnUiThread(() -> banner.setVisibility(View.GONE));
    }

    @Override
    public void connectionLost(UUID uuid) {
        Log.d("Connection status ", "Closed");

        // Closes the connection lost banner
        runOnUiThread(() -> banner.setVisibility(View.VISIBLE));
    }
}
