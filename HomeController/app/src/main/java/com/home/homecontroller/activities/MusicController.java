package com.home.homecontroller.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.home.homecontroller.ConnectableActivity;
import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;
import houseDataCenter.house.data.MusicStatus;
import houseDataCenter.mp3Player.PlaylistPlayer;
import utils.Compresser;

/**
 * Created by sunny on 2016-12-30.
 */

public class MusicController extends ConnectableActivity implements DecodedDataReceptionHandler {

    private ImageView albumView;
    private ImageButton pause_play;
    private ImageButton next;
    private ImageButton previous;

    private TextView songName;
    private TextView artist_album;
    private TextView roomName;

    private ToggleButton normal, random, repeat;


    private boolean playing;

    private Drawable noImageCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.music_controller);

        MainAppData.com.setDataReceptionHandler(this);
        requestMusicStatus();
        requestReadingMode();

        findViews();
        initBackButton();
        initControlButton();
    }

    @Override
    protected void onResume() {
        super.onResume();


        MainAppData.com.setDataReceptionHandler(this);


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void findViews() {
        this.roomName = (TextView) findViewById(R.id.room_name_music);
        this.albumView = (ImageView) findViewById(R.id.album_view);
        this.songName = (TextView) findViewById(R.id.song_name);
        this.artist_album = (TextView) findViewById(R.id.artist_album);
        this.next = (ImageButton) findViewById(R.id.next_song);
        this.pause_play = (ImageButton) findViewById(R.id.pause_song);
        this.previous = (ImageButton) findViewById(R.id.previous_song);
        this.normal = (ToggleButton) findViewById(R.id.normal_butt);
        this.random = (ToggleButton) findViewById(R.id.random_butt);
        this.repeat = (ToggleButton) findViewById(R.id.repeat_butt);


        this.noImageCover = ContextCompat.getDrawable(getApplicationContext(), R.drawable.no_album_art);
    }

    private void initBackButton() {
        ImageButton back = (ImageButton) findViewById(R.id.back_button_music);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();


            }
        });
    }

    private void initControlButton() {
        this.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainAppData.com.send(new TransferableData(TransferFlags.NEXT_SONG, MainAppData.selectedRoomData.currentID));
            }
        });
        this.previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainAppData.com.send(new TransferableData(TransferFlags.PREVIOUS_SONG, MainAppData.selectedRoomData.currentID));
            }
        });
        this.pause_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainAppData.com.send(new TransferableData(TransferFlags.SWITCH_PLAYING, MainAppData.selectedRoomData.currentID));
            }
        });

    }


    private void requestMusicStatus() {
        try {
            MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_MUSIC_STATUS, new Object[]{MainAppData.selectedRoomData.currentID, albumView.getWidth(), albumView.getHeight()}));
        } catch (NullPointerException e) {
            // Happens on loading, the imageview is null
            MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_MUSIC_STATUS, new Object[]{MainAppData.selectedRoomData.currentID, 1000, 1000}));

        }
    }

    private void switchPlayPause() {
        if (!playing) {
            pause_play.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pause));
        } else {
            pause_play.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_play));
        }
        playing = !playing;
    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {
        TransferableData data = (TransferableData) o;

        final Object obj = data.getData();
        final int flag = data.getFlag();

        Log.d("Received MusicControl", o.toString());

        if (flag == TransferFlags.REQUEST_MUSIC_STATUS) {
            runOnUiThread(() -> setMusicStatus((MusicStatus) obj));

        } else if (flag == TransferFlags.NOTIFY_MEDIA_PLAYING_CHANGED) {
            runOnUiThread(() -> switchPlayPause());

        } else if (flag == TransferFlags.NOTIFY_MEDIA_STATUS_CHANGED) {
            requestMusicStatus();
        } else if (flag == TransferFlags.NOTIFY_MEDIA_PLAYING_CHANGED) {
            runOnUiThread(() -> switchPlayPause());
        } else if (flag == TransferFlags.NOTIFY_READING_MODE_CHANGED) {
            requestReadingMode();
        } else if (flag == TransferFlags.SET_READING_MODE || flag == TransferFlags.REQUEST_MUSIC_READING_MODE) {
            runOnUiThread(() -> selectToggle((PlaylistPlayer.READING_MODE) obj));
        } else {
            super.receive(o, uuid, b);
        }
    }

    private void selectToggle(PlaylistPlayer.READING_MODE mode) {

        System.out.print(mode);
        switch (mode) {
            case NORMAL:
                normal.setChecked(true);
                random.setChecked(false);
                repeat.setChecked(false);
                break;
            case RANDOM:
                normal.setChecked(false);
                random.setChecked(true);
                repeat.setChecked(false);
                break;
            case REPEAT:
                normal.setChecked(false);
                random.setChecked(false);
                repeat.setChecked(true);
                break;
        }
    }

    private void requestReadingMode() {

        MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_MUSIC_READING_MODE, MainAppData.selectedRoomData.currentID));

    }

    public void setMusicStatus(MusicStatus musicStatus) {

        roomName.setText(MainAppData.selectedRoomData.name);

        // displays the image album
        byte[] cover = musicStatus.cover;

        if (cover != null) {
            // Decompresses the data
            byte[] uncompressed = Compresser.uncompress(cover);

            Bitmap bm = BitmapFactory.decodeByteArray(uncompressed, 0, uncompressed.length);
            albumView.setImageBitmap(bm);

        } else {
            albumView.setImageDrawable(noImageCover);
        }

        // Selects the matching toggle button
        selectToggle(musicStatus.mode);

        // Sets the song title
        songName.setText(musicStatus.name);

        // Sets the artist-album
        CharSequence artist = musicStatus.album.isEmpty() ? getText(R.string.unknown_artist) : musicStatus.artist;
        CharSequence album = musicStatus.artist.isEmpty() ? getText(R.string.unkown_album) : musicStatus.album;
        artist_album.setText(artist + " - " + album);

        // Sets play or pause
        int image_res = musicStatus.playing ? R.drawable.ic_pause : R.drawable.ic_play;
        pause_play.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), image_res));

        this.playing = musicStatus.playing;

    }

    public void onToggle(View view) {
        if (view == normal) {
            normal.setChecked(true);
            random.setChecked(false);
            repeat.setChecked(false);
            MainAppData.com.send(new TransferableData(TransferFlags.SET_READING_MODE, new Object[]{PlaylistPlayer.READING_MODE.NORMAL, MainAppData.selectedRoomData.currentID}));

        } else if (view == random) {
            normal.setChecked(false);
            random.setChecked(true);
            repeat.setChecked(false);
            MainAppData.com.send(new TransferableData(TransferFlags.SET_READING_MODE, new Object[]{PlaylistPlayer.READING_MODE.RANDOM, MainAppData.selectedRoomData.currentID}));

        } else if (view == repeat) {
            repeat.setChecked(true);
            random.setChecked(false);
            normal.setChecked(false);
            MainAppData.com.send(new TransferableData(TransferFlags.SET_READING_MODE, new Object[]{PlaylistPlayer.READING_MODE.REPEAT, MainAppData.selectedRoomData.currentID}));

        }
    }


}

