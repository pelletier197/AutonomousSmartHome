package com.home.homecontroller.customsComponents;

/**
 * Created by sunny on 2017-03-06.
 */

public interface ViewLoadedListener {

    void onViewLoaded();
}
