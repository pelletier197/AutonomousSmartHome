package com.home.homecontroller.controlTabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.home.homecontroller.DrawerRoomLayoutActivity;
import com.home.homecontroller.R;
import com.home.homecontroller.activities.RoomChangedListener;

import java.util.UUID;

import communication.DecodedDataReceptionHandler;

/**
 * Created by sunny on 2016-12-31.
 */

public class DevicesTab extends Fragment implements RoomChangedListener, DecodedDataReceptionHandler {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.devices_tab, container, false);
    }

    @Override
    public void notifyRoomChanged() {

    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {
        ((DrawerRoomLayoutActivity) getActivity()).receive(o, uuid, b);
    }
}


