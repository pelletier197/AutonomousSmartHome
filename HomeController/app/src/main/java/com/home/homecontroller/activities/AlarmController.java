package com.home.homecontroller.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.home.homecontroller.ConnectableActivity;
import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;
import com.home.homecontroller.customsComponents.AlarmListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.alarm.Alarm;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;

/**
 * Created by sunny on 2016-12-31.
 */

public class AlarmController extends ConnectableActivity implements DecodedDataReceptionHandler {

    private ListView alarmView;
    private AlarmListAdapter adapter;

    private ProgressBar loading;
    private TextView noAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.alarm_controller);

        MainAppData.com.setDataReceptionHandler(this);

        init();
        MainAppData.com.send(TransferFlags.REQUEST_ROOM_ALARMS, MainAppData.selectedRoomData.currentID);
    }

    private void init() {
        initHeader();
        initListView();
        loading = (ProgressBar) findViewById(R.id.alarm_loading);
        noAlarm = (TextView) findViewById(R.id.no_alarm_text);
    }

    private void initListView() {
        alarmView = (ListView) findViewById(R.id.alarm_listview);
        alarmView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        adapter = new AlarmListAdapter(this, new ArrayList<>(), a -> {

        });

        alarmView.setAdapter(adapter);
    }

    private void initHeader() {
        ((TextView) findViewById(R.id.room_name)).setText(MainAppData.selectedRoomData.name);
        findViewById(R.id.back_button).setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {

        super.receive(o, uuid, b);

        TransferableData dat = (TransferableData) o;

        if (dat.getFlag() == TransferFlags.REQUEST_ROOM_ALARMS) {
            runOnUiThread(() -> setAlarms((List<Alarm>) dat.getData()));
        }
    }

    private void setAlarms(List<Alarm> alarms) {

        loading.setVisibility(View.GONE);

        if (!alarms.isEmpty()) {

            alarmView.setVisibility(View.VISIBLE);
            noAlarm.setVisibility(View.GONE);

            adapter.setItems(alarms);
            adapter.notifyDataSetChanged();
        } else {

            alarmView.setVisibility(View.GONE);
            noAlarm.setVisibility(View.VISIBLE);

        }
    }
}
