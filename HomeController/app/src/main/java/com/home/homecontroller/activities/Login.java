package com.home.homecontroller.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.nio.ByteBuffer;
import java.util.UUID;

import communication.DecodedDataReceptionHandler;
import houseDataCenter.commucationData.TransferFlags;
import houseDataCenter.commucationData.TransferableData;

/**
 * Created by sunny on 2016-12-23.
 */

public class Login extends Activity implements DecodedDataReceptionHandler {

    private EditText mail;
    private EditText password;
    private Button b;
    private TextView info;
    private CheckBox rembMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        MainAppData.com.setDataReceptionHandler(this);

        if (checkFirstLogin()) {

            setContentView(R.layout.login);

            initView();
        }
    }

    private void initView() {

        mail = (EditText) findViewById(R.id.mail_field);
        password = (EditText) findViewById(R.id.password_field);
        info = (TextView) findViewById(R.id.login_info);
        rembMe = (CheckBox) findViewById(R.id.remember_me);

        b = (Button) findViewById(R.id.connect);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mail.setEnabled(false);
                password.setEnabled(false);
                b.setEnabled(false);
                sendAuthentication(mail.getText().toString(), password.getText().toString());
            }
        });
    }

    private boolean checkFirstLogin() {

        String[] logPas = MainAppData.readUserLogin(this.getFilesDir().getPath().toString());

        if (logPas != null) {
            MainAppData.com.setDataReceptionHandler(this);
            String login = logPas[0];
            String pass = logPas[1];

            MainAppData.currentLogin = login;
            MainAppData.currentPIN = pass;

            sendAuthentication(login, pass);
            return false;
        }
        return true;
    }

    private void sendAuthentication(String login, String pass) {
        MainAppData.com.send(new TransferableData(TransferFlags.REQUEST_IDENTIFICATION_DATA, new Object[]{getUUIDArray(), TransferFlags.DEVICE_INTELLIGENT, login, pass}));
    }

    @Override
    public void receive(Object o, UUID uuid, byte b) {

        Log.d("RECEIIIIIIIIIIVED  : " + o.toString(), "");

        TransferableData data = (TransferableData) o;

        // We have been accepted
        if (data.getFlag() == TransferFlags.ACCESS_GRANTED) {

            if (rembMe != null && rembMe.isChecked()) {
                MainAppData.writeUserLogin(mail.getText().toString(), password.getText().toString(), this.getFilesDir().getPath().toString());
            }

            // If the view as been loaded
            if (mail != null) {
                MainAppData.currentLogin = mail.getText().toString();
                MainAppData.currentPIN = password.getText().toString();
            }

            requestServerIP();
        } else if (data.getFlag() == TransferFlags.ACCESS_REFUSED) {
            runOnUiThread(() -> {
                loadUI();

                notifyFailed();
            });
        } else if (data.getFlag() == TransferFlags.REQUEST_SERVER_EXTERNAL_IP) {

            MainAppData.com.setExternalIP((String) data.getData());

            System.out.print(data.getData() + " new IP address");

            startMainActivity();

        }


    }

    private void loadUI() {
        if (findViewById(R.id.mail_field) == null) {
            setContentView(R.layout.login);

            initView();
        }
    }

    private void requestServerIP() {
        MainAppData.com.send(TransferFlags.REQUEST_SERVER_EXTERNAL_IP);
    }


    private void startMainActivity() {

        Intent i = new Intent(getApplicationContext(), RoomView.class);
        startActivity(i);
    }

    private void notifyFailed() {

        mail.setEnabled(true);
        password.setEnabled(true);
        b.setEnabled(true);

        info.setText(getText(R.string.login_failed));
    }

    private byte[] getUUIDArray() {

        TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String s = tManager.getDeviceId();

        Long firstlong = Long.valueOf(s.substring(0, 8));
        Long seclong = Long.valueOf(s.substring(8, 15));

        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE * 2);
        buffer.putLong(firstlong);
        buffer.putLong(seclong);

        return buffer.array();

    }

}
