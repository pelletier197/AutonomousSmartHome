package com.home.homecontroller.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.io.IOException;

/**
 * Created by sunny on 2016-12-23.
 */

public class Connecting extends Activity {

    private TextView text;
    private ProgressBar bar;
    private Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.connecting);

        findViews();
        resetView();

        new Thread(() -> {

            MainAppData.com.setFileDirectory(this.getFilesDir().getPath().toString());


            startConnect();
        }).start();
    }

    private void findViews() {
        bar = (ProgressBar) findViewById(R.id.connection_progress);
        text = (TextView) findViewById(R.id.connection_status);
        b = (Button) findViewById(R.id.cancel);


    }

    private void startLoginActivity() {
        Intent i = new Intent(getApplicationContext(), Login.class);
        startActivity(i);
    }

    private void startConnect() {

        try {

            MainAppData.com.openConnection();

            if (MainAppData.com.isConnected()) {
                startLoginActivity();
            } else {
                notifyFailed();
            }
        } catch (IOException e) {
            e.printStackTrace();
            notifyFailed();
        }


    }

    private void resetView() {


        bar.setVisibility(View.VISIBLE);


        text.setText(getText(R.string.connecting));

        b.setText(getText(R.string.cancel));
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainAppData.com.cancelConnection();
                notifyFailed();
            }
        });


    }

    private void notifyFailed() {

        runOnUiThread(() -> {
            ProgressBar bar = (ProgressBar) findViewById(R.id.connection_progress);
            bar.setVisibility(ProgressBar.INVISIBLE);

            TextView text = (TextView) findViewById(R.id.connection_status);
            text.setText(getText(R.string.connection_failed));

            Button b = (Button) findViewById(R.id.cancel);
            b.setText(getText(R.string.try_again));
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetView();
                    new Thread(() -> startConnect()).start();
                }
            });
        });
    }

    @Override
    public void onBackPressed() {
        // Cannot go back to previous screen
    }
}
