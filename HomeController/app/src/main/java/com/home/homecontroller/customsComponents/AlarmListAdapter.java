package com.home.homecontroller.customsComponents;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.home.homecontroller.MainAppData;
import com.home.homecontroller.R;

import java.util.Collections;
import java.util.List;

import houseDataCenter.alarm.Alarm;
import houseDataCenter.alarm.AlarmDay;
import houseDataCenter.commucationData.TransferFlags;
import utils.Formatters;

/**
 * Created by sunny on 2017-03-05.
 */

public class AlarmListAdapter extends ArrayAdapter<Alarm> {

    private List<Alarm> items;
    private AlarmSwitchListener listener;
    private int selected;
    private int listviewDrawable;

    public AlarmListAdapter(Context context, List<Alarm> items, AlarmSwitchListener listener) {
        super(context, R.layout.light_layout);

        if (listener == null || items == null) {
            throw new NullPointerException("Null switch listener or items");
        }

        this.listener = listener;
        this.items = items;

        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        listviewDrawable = outValue.resourceId;
    }

    @Override
    public Alarm getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {

        return items.size();
    }

    public void setSelected(int i) {
        this.selected = i;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View view = convertView;
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // New view
            view = inflater.inflate(R.layout.alarm_display, null);

        }

        if (position == selected) {
            view.setBackgroundResource(R.color.small_accent_light);
        } else {
            view.setBackgroundResource(listviewDrawable);
        }

        TextView time = (TextView) view.findViewById(R.id.time);
        TextView days = (TextView) view.findViewById(R.id.days);
        Switch swi = (Switch) view.findViewById(R.id.alarm_switch);


        final Alarm item = items.get(position);
        time.setText(Formatters.formatHourMinute(item.getHour(), item.getMin()));
        days.setText(daysFormatted(item));
        swi.setOnCheckedChangeListener(null);
        swi.setChecked(item.isActive());

        // Sets the state change listener
        swi.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                MainAppData.com.send(TransferFlags.ACTIVATE_ALARM, item.getId());
            } else {
                MainAppData.com.send(TransferFlags.DEACTIVATE_ALARM, item.getId());
            }
            item.setActive(b);
            MainAppData.selectedAlarm = item;
            listener.onAlarmSelected(item);
            setSelected(position);
            notifyDataSetChanged();
        });

        // Makes the items deletable
        view.findViewById(R.id.delete_btn).setOnClickListener(v -> {
            MainAppData.selectedAlarm = item;
            setSelected(position);
            notifyDataSetChanged();

            new AlertDialog.Builder(getContext())
                    .setTitle(getContext().getString(R.string.delete_alarm))
                    .setMessage(getContext().getString(R.string.text_delete_alarm))
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {

                        MainAppData.com.send(TransferFlags.REMOVE_ALARM, item.getId());
                        items.remove(item);

                        if (MainAppData.selectedAlarm == item) {
                            // Changes the selected item
                            if (!items.isEmpty()) {
                                MainAppData.selectedAlarm = items.get(0);
                                setSelected(0);
                            }
                        }
                        notifyDataSetChanged();
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        });

        view.setOnClickListener(v -> {
            MainAppData.selectedAlarm = item;
            setSelected(position);
            notifyDataSetChanged();
        });
        return view;
    }

    private String daysFormatted(Alarm a) {

        if (a.isAllDay()) {
            return getContext().getString(R.string.all_days);
        }

        List<AlarmDay> days = a.getAlarmDays();
        StringBuilder builder = new StringBuilder();

        Collections.sort(days, AlarmDay.getDayComparator());

        if (days.isEmpty()) {
            return getContext().getString(R.string.once);
        }


        for (int i = 0; i < days.size(); ++i) {
            switch (days.get(i)) {
                case SUNDAY:
                    builder.append(getContext().getString(R.string.sunday));
                    break;
                case MONDAY:
                    builder.append(getContext().getString(R.string.monday));
                    break;
                case TUESDAY:
                    builder.append(getContext().getString(R.string.tuesday));
                    break;
                case WEDNESDAY:
                    builder.append(getContext().getString(R.string.wednesday));
                    break;
                case THURSDAY:
                    builder.append(getContext().getString(R.string.thursday));
                    break;
                case FRIDAY:
                    builder.append(getContext().getString(R.string.friday));
                    break;
                case SATURDAY:
                    builder.append(getContext().getString(R.string.saturday));
                    break;
            }

            if (i != days.size() - 1) {
                builder.append(", ");
            }

        }

        return builder.toString();
    }

    public void setItems(List<Alarm> alarms) {
        this.items = alarms;
    }

    public List<Alarm> getItems() {
        return items;
    }

    public void setItemAt(int index, Alarm light) {

        this.items.set(index, light);
    }
}
