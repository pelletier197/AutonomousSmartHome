package com.home.homecontroller;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.home.homecontroller.customsComponents.ImageTextItem;
import com.home.homecontroller.customsComponents.ImageTextListviewAdapter;

/**
 * Created by sunny on 2017-01-10.
 */

public class DrawerRoomLayoutActivity extends ConnectableActivity {

    private boolean doubleBackToExitPressedOnce = false;
    private ImageTextListviewAdapter adapter;
    private DrawerLayout layout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Sets the content view to the layout
        super.setContentView(R.layout.drawer_view);
        initDrawer();

    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        LayoutInflater factory = LayoutInflater.from(this);
        View contentView = factory.inflate(layoutResID, null);

        if (layout.getChildCount() == 2) {
            layout.removeViewAt(0);
        }
        layout.addView(contentView, 0);
    }

    private void initDrawer() {
        // Init drawer layout
        layout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageTextItem[] items = new ImageTextItem[]{
                new ImageTextItem(R.drawable.ic_house, getText(R.string.house)),
                new ImageTextItem(R.drawable.ic_params, getText(R.string.params))
        };

        final ListView drawerView = (ListView) findViewById(R.id.drawer_listview);

        adapter = new ImageTextListviewAdapter(this, items);
        drawerView.setAdapter(adapter);

        drawerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                layout.closeDrawer(Gravity.LEFT);
                Log.d("TAG", new Integer(i).toString());
                drawerView.clearChoices();
            }

        });


    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            // It is impossible to press back on this activity, you must leave the app on 2nd press
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getText(R.string.press_back_again_exit), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    protected DrawerLayout getDrawerLayout() {
        return layout;
    }

}
